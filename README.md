Anomaly Detection WorkBench
===========================

[![license](https://img.shields.io/github/license/mashape/apistatus.svg)](https://en.wikipedia.org/wiki/MIT_License)
[![build status](https://gitlab.com/cese/adw/badges/master/build.svg)](https://gitlab.com/cese/adw/commits/master)
[![coverage report](https://gitlab.com/cese/adw/badges/master/coverage.svg)](https://gitlab.com/cese/adw/commits/master)

[![Scaladoc](http://javadoc-badge.appspot.com/com.github.cese.adw/adw_2.12.svg?label=scaladoc)](https://cese.gitlab.io/adw/api/)

![alt text](docs/images/icon_color_small.png)

Acknowledgements
================


License
=======
MIT License