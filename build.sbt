
// Eclipse plugin
EclipseKeys.useProjectId := true
EclipseKeys.createSrc := EclipseCreateSrc.Default + EclipseCreateSrc.ManagedClasses
EclipseKeys.executionEnvironment := Some(EclipseExecutionEnvironment.JavaSE18)
EclipseKeys.withSource := true
EclipseKeys.withJavadoc := true
EclipseKeys.withBundledScalaContainers := false
EclipseKeys.eclipseOutput := Some(".target")

scalacOptions += "-target:jvm-1.8"
javacOptions ++= Seq("-source", "1.8", "-target", "1.8", "-Xlint")

name := "adw"

version := "0.1.0"

scalaVersion in ThisBuild := "2.12.4"

// For ScalaTest: add in ~/.sbt/0.13/global.sbt
// export http_proxy="http://proxy.inescn.pt:3128"
resolvers += "Artima Maven Repository" at "http://repo.artima.com/releases"

//resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases"
resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"
//resolvers += "Sonatype Releases" at "http://oss.sonatype.org/content/repositories/releases"
//resolvers += "mvnrepository" at "http://mvnrepository.com/artifact"
//resolvers += "Maven Central" at "http://repo1.maven.org/maven2"

//classpathTypes += "maven-plugin"

// https://github.com/scala/scala/blob/2.12.x/test/scaladoc/resources/links.scala
// http://stackoverflow.com/questions/15394322/how-to-disambiguate-links-to-methods-in-scaladoc
// https://gist.github.com/VladUreche/8396624
// http://subnormalnumbers.blogspot.pt/2011/08/scaladoc-wiki-syntax.html
// https://wiki.scala-lang.org/display/SW/Configuring+SBT+to+Generate+a+Scaladoc+Root+Page
target in Compile in doc := baseDirectory.value / "api"
scalacOptions in (Compile, doc) ++= Seq("-doc-root-content", baseDirectory.value+"/root-doc.txt")
// Make sure we can reference external libraries
autoAPIMappings := true

//libraryDependencies += "org.scala-lang" % "scala-library" % "2.12.0"

// Unit testing
libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.4" % "test"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.4" % "test"
libraryDependencies += "junit" % "junit" % "4.4"

// Logging
// http://alvinalexander.com/scala/how-to-use-java-style-logging-slf4j-scala
// https://github.com/typesafehub/scala-logging
libraryDependencies += "org.slf4j" % "slf4j-simple" % "1.7.23"
//libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.1"
libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.5.0"

// Date and Time
// http://www.time4j.net/tutorial/appendix.html
libraryDependencies += "org.threeten" % "threeten-extra" % "1.0"
libraryDependencies += "org.json4s" %% "json4s-native" % "3.5.1"
libraryDependencies += "org.json4s" %% "json4s-jackson" % "3.5.1"
libraryDependencies += "org.json4s" %% "json4s-ext" % "3.5.1"

// Type-level
libraryDependencies += "com.chuusai" %% "shapeless" % "2.3.2"

// Linear Algebra
// http://statr.me/2015/09/an-overview-of-linear-algebra-libraries-in-scala-java/
// http://ejml.org/wiki/index.php?title=Main_Page
// http://commons.apache.org/proper/commons-math/index.html
libraryDependencies += "net.sourceforge.f2j" % "arpack_combined_all" % "0.1"
libraryDependencies += "com.github.fommil.netlib" % "all" % "1.1.2" pomOnly()
libraryDependencies += "com.googlecode.matrix-toolkits-java" % "mtj" % "1.0.4"
    
// SCala I/O
libraryDependencies += "com.github.pathikrit" %% "better-files" % "3.2.0"

// DSP functions
libraryDependencies += "uk.me.berndporr" % "iirj" % "1.0"
// https://github.com/JorenSix/TarsosDSP
// http://www.source-code.biz/dsp/java/
// https://sourceforge.net/projects/dsplaboratory/
// http://vadim2000.chat.ru/soft.htm#Signalgo
// http://pauljbotelho.com/software.php?software=jein
// http://www.dickbaldwin.com/tocdsp.htm

// CSV
// Core library, included automatically if any other module is imported.
lazy val kantan_version = "0.1.19"
libraryDependencies += "com.nrinaudo" %% "kantan.csv" % kantan_version
// Java 8 date and time instances.
libraryDependencies += "com.nrinaudo" %% "kantan.csv-java8" % kantan_version
// Automatic type class instances derivation.
libraryDependencies += "com.nrinaudo" %% "kantan.csv-generic" % kantan_version
// jackson-csv engine.
libraryDependencies += "com.nrinaudo" %% "kantan.csv-jackson" % kantan_version

// JASON
val circeVersion = "0.8.0"
libraryDependencies += "io.circe" %% "circe-core" % circeVersion
libraryDependencies += "io.circe" %% "circe-generic" % circeVersion
libraryDependencies += "io.circe" %% "circe-parser" % circeVersion

lazy val saddle_version = "1.3.5-SNAPSHOT"
libraryDependencies += "org.scala-saddle" %% "saddle-core" % saddle_version
//libraryDependencies += "org.scala-saddle" %% "saddle-hdf5" % saddle_version
    
// ML "1.2.2"
lazy val smileVersion = "1.3.1"
libraryDependencies += "com.github.haifengl" % "smile-core" % smileVersion
libraryDependencies += "com.github.haifengl" %% "smile-scala" % smileVersion
//libraryDependencies += "com.github.lwhite1" % "tablesaw" % "0.7.6.4"
//libraryDependencies += "com.github.lwhite1" % "tablesaw" % "0.7.6.9"
libraryDependencies += "com.github.lwhite1" % "tablesaw" % "0.7.7.1"

// Apache Common
libraryDependencies += "org.apache.commons" % "commons-math3" % "3.6.1" withSources() withJavadoc()

libraryDependencies += "org.knowm.xchart" % "xchart" % "3.2.2"



// http://bytedeco.org/
// TODO
libraryDependencies += "org.bytedeco" % "javacpp" % "1.2.4"
//libraryDependencies += "org.bytedeco" % "javacpp" % "1.3.1"

libraryDependencies += "org.nd4j"     % "canova-api" % "0.0.0.17"
libraryDependencies += "org.nd4j"     % "canova-nd4j-image" % "0.0.0.17"


//lazy val dl4jVersion = "0.7.2"
lazy val dl4jVersion = "0.8.0"

//lazy val nd4jVersion = "0.7.2"
lazy val nd4jVersion = "0.8.0"


// fails libraryDependencies += "org.nd4j" % "nd4j-native-platform" % "0.6.0" classifier "" classifier "linux-x86_64"
//libraryDependencies += "org.nd4j"   % "nd4j-native-platform" % nd4jVersion
//libraryDependencies += "org.nd4j" % "nd4j-cuda-7.5-platform" % nd4jVersion
libraryDependencies += "org.nd4j" % "nd4j-cuda-8.0-platform" % nd4jVersion

// TODO
// https://github.com/deeplearning4j/nd4s
// we want https://github.com/deeplearning4j/nd4s/tree/nd4s-0.6.0
// https://github.com/deeplearning4j/nd4s/issues/82
// git clone https://github.com/deeplearning4j/nd4s.git
// cd nd4s/
// git tag -l
// git checkout tags/nd4s-0.6.0
// or git checkout tags/<tag_name> -b <branch_name>
// sbt
// set scalaVersion := "2.12.0"
// test:console
// (1 to 9).asNDArray(3,3)
//libraryDependencies += "org.nd4j" % "nd4s_2.12.0-M3" % "0.4-rc3.8" 
// https://github.com/deeplearning4j/nd4s/issues/96
//libraryDependencies += "org.nd4j" %% "nd4s" % "0.7.0" 
//libraryDependencies += "org.nd4j" %% "nd4s" % "0.6.0"
// https://github.com/deeplearning4j/nd4s/issues/96#issuecomment-260345356
// https://mvnrepository.com/artifact/org.nd4j/nd4s_2.11
//libraryDependencies += "org.nd4j" %% "nd4s" % nd4jVersion


libraryDependencies += "org.datavec" % "datavec-api" % "0.7.2"
libraryDependencies += "org.datavec" % "datavec-data-image" % "0.7.2"
libraryDependencies += "org.datavec" % "datavec-data-codec" % "0.7.2"
libraryDependencies += "org.datavec" % "datavec-data-audio" % "0.7.2"
//libraryDependencies += "org.datavec" % "datavec-nd4j-common" % "0.7.2" already loaded

libraryDependencies += "org.deeplearning4j" % "deeplearning4j-core" % "0.7.2"

lazy val breeze_version = "0.13.1"
libraryDependencies += "org.scalanlp" %% "breeze" % breeze_version
libraryDependencies += "org.scalanlp" %% "breeze-natives" % breeze_version // BLAS, etc.
libraryDependencies += "org.scalanlp" %% "breeze-viz" % breeze_version  // It depends on LGPL code

lazy val hipparchus_version = "1.1"
//libraryDependencies += "org.hipparchus" % "hipparchus-aggregator" % hipparchus_version
libraryDependencies += "org.hipparchus" % "hipparchus-stat" % hipparchus_version
libraryDependencies += "org.hipparchus" % "hipparchus-fft" % hipparchus_version
libraryDependencies += "org.hipparchus" % "hipparchus-geometry" % hipparchus_version
libraryDependencies += "org.hipparchus" % "hipparchus-optim" % hipparchus_version
libraryDependencies += "org.hipparchus" % "hipparchus-fitting" % hipparchus_version
libraryDependencies += "org.hipparchus" % "hipparchus-clustering" % hipparchus_version
libraryDependencies += "org.hipparchus" % "hipparchus-ode" % hipparchus_version
libraryDependencies += "org.hipparchus" % "hipparchus-samples" % hipparchus_version

//lazy val jtransforms_version = "3.1"
//libraryDependencies += "com.github.wendykierp" % "JTransforms" % jtransforms_version

lazy val elki_version = "0.7.1"
libraryDependencies += "de.lmu.ifi.dbs.elki" % "elki" % elki_version
libraryDependencies += "de.lmu.ifi.dbs.elki" % "elki-batik-visualization" % elki_version
libraryDependencies += "de.lmu.ifi.dbs.elki" % "elki-project" % elki_version
libraryDependencies += "de.lmu.ifi.dbs.elki" % "elki-libsvm" % elki_version
//libraryDependencies += "de.lmu.ifi.dbs.elki" % "elki-bundle" % elki_version

lazy val twelvemonkeys_version = "3.3.2"
// Breeze Viz uses JFreeChart which requires these extensions to java's ImageIO
// Due to issue in the dependency graph we had to explicitly add imageio-core and common-lang
// See https://github.com/haraldk/TwelveMonkeys/issues/167
libraryDependencies += "com.twelvemonkeys.imageio" % "imageio-core" % twelvemonkeys_version
libraryDependencies += "com.twelvemonkeys.common" % "common-lang" % twelvemonkeys_version
libraryDependencies += "com.twelvemonkeys.imageio" % "imageio-jpeg" % twelvemonkeys_version
libraryDependencies += "com.twelvemonkeys.imageio" % "imageio-tiff" % twelvemonkeys_version
// Not required explicitly?
libraryDependencies += "org.jfree" % "jfreechart" % "1.0.19"
libraryDependencies += "org.jfree" % "jfreesvg" % "3.2"

libraryDependencies += "io.monix" %% "monix" % "2.3.0"

// https://github.com/vegas-viz/Vegas
// https://mvnrepository.com/artifact/org.vegas-viz/vegas_2.11
//libraryDependencies += "org.vegas-viz" %% "vegas" % "0.3.9"
// 2.12 not available


// TODO
// https://scala-blitz.github.io/
// https://bitbucket.org/oscarlib/oscar/wiki/Home
// https://github.com/bruneli/scalaopt
// https://github.com/scalanlp/breeze
// https://github.com/vagmcs/Optimus
// https://www.hipparchus.org/
// https://www.orekit.org/
// http://www.scalafx.org/
// https://github.com/vigoo/scalafxml
// https://github.com/scalafx/scalafx-ensemble
// Symja Library - Java Symbolic Math System
// https://bitbucket.org/axelclk/symja_android_library/overview
// https://github.com/yuemingl
// https://github.com/yuemingl/SymJava
// https://github.com/merantix/picasso (DNNs)
// http://www.ybrikman.com/writing/2014/05/05/you-are-what-you-document/
// https://github.com/szeiger/ornate
// https://github.com/tpolecat/tut
// https://github.com/laughedelic/literator
// https://github.com/wookietreiber/sbt-scaliterate
// https://stackoverflow.com/questions/16934488/how-to-link-classes-from-jdk-into-scaladoc-generated-doc

// TODO: add scalastyle checks also
scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature", "-Xlint", "-explaintypes")
//scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature", "-Xlint")
//scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature", "-Xlint:deprecation")
//scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature", "-Xlint", "-Xfatal-warnings")
//scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature", "-Xfatal-warnings")
//scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature", "-Xlint", "-uniqid", "-explaintypes")

// http://www.scala-sbt.org/0.13/docs/Howto-Scaladoc.html
//scalacOptions in (Compile,doc) := Seq("-groups", "-implicits")
autoAPIMappings := true
// https://stackoverflow.com/questions/8884866/how-to-exclude-java-source-files-in-doc-task
// sources in (Compile, doc) ~= (_ filter (_.getName endsWith ".scala"))
// https://issues.scala-lang.org/browse/SI-5533
// scaladoc -skip-packages <pack1>:<pack2>:...:<packN>
//scalacOptions in (Compile, doc) := List("-skip-packages",  "com.github.cjlin1")

//
//SBT offers 3 ways to pass JVM parameters:
//
//    Set environment variable SBT_OPTS
//    Set environment variable JAVA_OPTS
//    Pass parameters by command line option `-J-X`
//
// export JAVA_OPTS="-Xmx512m" sbt run
// JAVA_OPTS= -Dhttp.proxyHost=http://proxy2.inescn.pt -Dhttp.proxyPort=3128
// Use -J-X options to sbt for individual options, e.g. -J-Xmx2048 -J-XX:MaxPermSize=512
// This must be true for the line below to work
//fork in run := true
//
// java -XX:+PrintFlagsFinal
// java -XX:+PrintCommandLineFlags
// jps -lvm
// jinfo -flags <vmid>
// jinfo -sysprops <vmid>
//
//javaOptions in run += "-ea"
// Compilation error: stack overflow. Increase stack
// Don't forget to add the option to add the fork for the compile stage
javaOptions in compile += "-J-Xss256M"

//  Using Java Sound API in a Scala SBT-managed project
// When trying to use the Java Sound API to load valid WAV files under SBT, this failed with
// javax.sound.sampled.UnsupportedAudioFileException: file is not a supported file type
// SBT changes the default system class loader, which in trn causes problems when loading the
// Java Sound API classes. Their are 2 ways around this:
// 1. Change the SBT build to for a process so that the default class loader is used
//    See: https://stackoverflow.com/questions/18676712/java-sound-devices-found-when-run-in-intellij-but-not-in-sbt
// 2. Change the class loaders dynamically when using the Java Sound API
//    See: https://stackoverflow.com/questions/24145198/why-does-audiosystem-getmixerinfo-return-different-results-under-sbt-vs-scala
//    https://stackoverflow.com/questions/31727385/sbt-scala-audiosystem
// We have opted for solution (1)

fork in compile := true
fork in run := true
connectInput in run := true
// This is not working is scalatest is messing with the classloader
fork in test := true
//connectInput in test := true

// http://central.sonatype.org/pages/ossrh-guide.html
// http://central.sonatype.org/pages/sbt.html
// https://github.com/xerial/sbt-sonatype
// http://www.scala-sbt.org/release/docs/Using-Sonatype.html
// http://outworkers.com/blog/post/scala-tips-publishing-to-sonatype-and-maven-central
// https://github.com/shekhargulati/52-technologies-in-2016/blob/master/02-sbt/README.md
