
resolvers += "Artima Maven Repository" at "http://repo.artima.com/releases"

// For DL4J
addSbtPlugin("org.bytedeco" % "sbt-javacpp" % "1.8")

// For ScalaTest
// TODO: how do we add to "~/.sbt/0.13/global.sbt" the following resolver?
// TODO: resolvers += "Artima Maven Repository" at "http://repo.artima.com/releases"
// http://stackoverflow.com/questions/42226004/unknown-artifact-sbtplugin-super-safe-compiler-with-scala-2-12/42444449#42444449
// Only works with 2.12.2
//addSbtPlugin("com.artima.supersafe" % "sbtplugin" % "1.1.2")

// For CI coverage testing
// coverageEnabled := true
// sbt clean coverage test
// sbt coverageReport
addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.5.0")

// Avoid using the global sbt config file
addSbtPlugin("com.typesafe.sbteclipse" % "sbteclipse-plugin" % "5.1.0")

// For use with scaladoc referencing external libraries APIs
// See http://www.scala-sbt.org/release/docs/Howto-Scaladoc.html
// Set autoAPIMappings := true and let plugin fill in apiMappings
// Important: works only for managed libraries - in Maven repo
addSbtPlugin("com.thoughtworks.sbt-api-mappings" % "sbt-api-mappings" % "1.0.0")

// http://www.scalastyle.org/
// http://www.scalastyle.org/sbt.html