package inegi

import breeze.plot.{Figure, GradientPaintScale, PaintScale}
import org.hipparchus.complex.Complex
import org.hipparchus.transform.DftNormalization
import pt.inescn.dsp.{Filter, Fourier}
import pt.inescn.dsp.Fourier.plotFourier
import pt.inescn.etl.Transform
import pt.inescn.params.ParamSearch._
import pt.inescn.samplers.{Base, Function}

import scala.language.higherKinds

/**
  * runMain inegi.Experiments param1 param2
  */
object Experiments {

/*
    // sample at twice the Nyquist frequency and sample for 2.2 seconds
    val samplingFreq = Fourier.nyquistNamplingFrequency(f1)
    val fs = 50 * samplingFreq  // we increase the
    val duration = 3
    val (_, xx) = Fourier.samplingSpecification(fs, duration)

    val sig1 = Distribution.Normal(1, 1, new JDKRandomGenerator(98765))

    val (len,xa,fftSig) = Fourier.FFT(fs, xx.length, sig1, pad = false)
    val ya = Fourier.abs(fftSig)
    val yb = Fourier.phase(fftSig)

    val (sya, _) = Fourier.topAbsoluteComponents(40, ya, yb)
    //def frequencyAt(fs: Double, len: Int)(i : Int) = i * ((fs*1.0)/len)
    val threshold = a1 * (len/2) * 0.6  // 60% of the smallest amplitude
    val signifcantAmp = sya.filter( p => p._1 > threshold)

 */

  def maxAbsStftsAbs(stft: Array[Array[Double]]): Array[Double] = {
    val maxs = stft.map{ abs =>
      val tmp = abs.sortBy( -_ )
      tmp(0)
    }
    maxs.sortBy( -_ )
  }


  def maxAbsStfts(stft: Array[Array[Complex]]): Array[(Double, Int)] = {
    val maxs = stft.flatMap{ fft =>
      val tmp = Fourier.topAbsoluteComponents(1, fft)._1
      //println(fft.mkString(","))
      //println(tmp.mkString(","))
      tmp
    }
    maxs.sortBy( p => -p._1)
  }


  def showNormalize(title:String, norm: (Array[Double]) => Array[Double], stfts:Array[Array[Complex]]): Figure = {
    val stfts1 = stfts.map( e => norm( Fourier.abs(e)) )
    val maxAbssGood1: Array[Double] = maxAbsStftsAbs(stfts1)
    val maxAbsGood1 = maxAbssGood1(0)
    val colors1 = GradientPaintScale(lower=0, upper=maxAbsGood1, gradient = PaintScale.BlueToRed)
    Fourier.plotSTFTAbs(stfts1, colors1, title)
  }

  // Wavelet transform
  // Multiresolution analysis

  /**
    * Notes: Signal F0
    *
    * 1. Resolution is limited so a frequency component may be "spread" over
    * one or more components of the FFT. As an example, the 10Hz sine wave
    * should only have one component the 10Hz. But at a sampling rate of
    * 400Hz and an FFT size of 64 components, we have a resolution of
    * 6.35Hz. This means that the maximum absolute values will be found in
    * the 1st (6.35Hz) and second component (12.9Hz). This is visible
    * in the STFT.
    *
    * 2. When the STFT windows do not match up exactly with the the signal
    * length, then the last window must be extended (padded). In this case,
    * the FFT components are not correct. We should not use this window
    * to characterize the signal.
    *
    * 3. When using the STFT we introduce discontinuities in the signal that
    * introduce spurious frequency components (unless we split at exactly at
    * the start and finish of a period, which for multiple waves at different
    * non-multiple frequencies is not possible). These frequency components
    * are NOT the same among the various STFT windows. To see this in action
    * remove the Hann filter fro the STFT.
    *
    */
  def exp0(): Unit ={
    val f1 = 10 /* Hz */ ; val a1 = 1  ;  val phase1 = Math.toRadians(0)
    val f2 = 20 /* Hz */ ; val a2 = 1  ;  val phase2 = Math.toRadians(0)
    val samplingFreq = 10 * Fourier.nyquistNamplingFrequency(f1, f2)
    val duration = 2
    val (dd,xx) = Fourier.samplingSpecification(samplingFreq,duration)

    val good = Function.Sin(f1, a1, phase1, dd) // good
    val bad = Function.Sin(f2, a2, phase2, dd)  // bad

    /*
    import breeze.plot._
    val fig = Figure()
    val plt = fig.subplot(0)
    plt += plot(xx, good(xx.length))
    plt += plot(xx, bad(xx.length))
    plt.title = s"DSL Ops @ $samplingFreq"
    plt.refresh*/

    val (lenGood,xaGood,fftSigGood) = Fourier.FFT(samplingFreq, xx.length, good, pad = false, DftNormalization.STANDARD)
    plotFourier(samplingFreq, duration, xx, good(xx.length).toArray, xaGood, fftSigGood, title="Pure Single Frequency")
    println(s"xaGood length = $lenGood == ${xaGood.length}")

    // Add DC component
    //def fft(fs: Double, signal_length: Int, sig: Array[Double], pad: Boolean) =
    // Fourier.FFT(fs, signal_length, sig, pad=pad, DftNormalization.STANDARD)
    //Fourier.plotFourierTransform(fft, samplingFreq, duration, good(xx.length).toArray.map(_+0.5), "Example Y")

    val (_,xaBad,fftSigBad) = Fourier.FFT(samplingFreq, xx.length, bad, pad = false, DftNormalization.STANDARD)
    //plotFourier(samplingFreq, duration, xx, good(xx.length).toArray, xaBad, fftSigBad, title="Bad")

    // Signal at 20 Hz means that we have to sample at least 1/20 seconds to get a full period (0.05 seconds)
    // So how many samples do we need to get using the selected sampling rate?
    val f1Period = 1.0 / f1
    val (_,xx1) = Fourier.samplingSpecification(samplingFreq,f1Period)

    println(s"samplingFreq ($duration s) = $samplingFreq Hz")
    println(s"Original signal ($duration s) = ${xx.length} samples")
    println(s"Good signal (1T = $f1Period) = ${xx1.length} samples")

    //stft(good, samplingFreq, windowSize, overlap, extendBoundary, pad = true)
    val winSize = 3 * xx1.length
    val windowCount = xx.length.toDouble / winSize
    println(s"Good winSize = $winSize samples")
    println(s"Good windowCount = $windowCount samples")
    // overlap=5,
    //val (stfts, xa, cola) = Fourier.stft(good(xx.length).map(_+0.5), samplingFreq, winSize, pad=false, filter = Filter.initHann)
    val (stfts, xa, cola) = Fourier.stft(good(xx.length), samplingFreq, winSize, pad=false, filter = Filter.initHann)
    //val (stfts, xa, cola) = Fourier.stft(good(xx.length), samplingFreq, winSize, pad=false)
    val FFTwindowCount = xx.length.toDouble / stfts(0).length
    println(s"xa.length = ${xa.length}")
    println(s"stfts(0).length = ${stfts(0).length}")
    println(s"Good FFTwindowSize = ${stfts(0).length} samples")
    println(s"Good FFTwindowCount = $FFTwindowCount samples")

    val maxAbssGood: Array[(Double, Int)] = maxAbsStfts(stfts)
    val maxAbsGood = maxAbssGood(0) // sortedAbssGood(0)
    println(s"maxAbssGood = ${maxAbssGood.mkString(",")}")
    println(s"maxAbsGood = $maxAbsGood")

    val (_, freqResolution) = Fourier.deltaFreq(samplingFreq, stfts(0).length)
    val topComponent = Fourier.frequencyOfIndex(samplingFreq, stfts(0).length, maxAbsGood._2)
    val expectedTopComponent = Fourier.indexOfFrequency(samplingFreq, stfts(0).length, f1)
    println(s"topComponent = $topComponent Hz")
    println(s"expectedTopComponent = $expectedTopComponent")
    println(s"freqResolution = $freqResolution Hz")

    // Plot the STFT (indirectly)
    import breeze.plot._

    val colors = GradientPaintScale(lower=0, upper=maxAbsGood._1, gradient = PaintScale.BlueToRed)
    Fourier.plotSTFT(stfts, colors, title = "Pure Single Frequency")
  }

  /**
    * Notes: Signal with F0 + constant offset
    *
    * 1. When the signal is not detrended (in this case we add a constant
    * component), we see that the STFT windows do not show the same absolute
    * values. We need to detrend by removing the DC component. Note that we
    * still need a filter due to the window splitting - filtering won't
    * solve both issues.
    *
    */
  def exp1(): Unit ={
    val f1 = 10 /* Hz */ ; val a1 = 1  ;  val phase1 = Math.toRadians(0)
    val f2 = 20 /* Hz */ ; val a2 = 1  ;  val phase2 = Math.toRadians(0)
    val samplingFreq = 10 * Fourier.nyquistNamplingFrequency(f1, f2)
    val duration = 2
    val (dd,xx) = Fourier.samplingSpecification(samplingFreq,duration)

    val offset = Function.Const(0.2,dd)
    val good = Function.Sin(f1, a1, phase1, dd) + offset // good
    val bad = Function.Sin(f2, a2, phase2, dd)  + offset // bad

    /*
    import breeze.plot._
    val fig = Figure()
    val plt = fig.subplot(0)
    plt += plot(xx, good(xx.length))
    plt += plot(xx, bad(xx.length))
    plt.title = s"DSL Ops @ $samplingFreq"
    plt.refresh*/

    val (lenGood,xaGood,fftSigGood) = Fourier.FFT(samplingFreq, xx.length, good, pad = false, DftNormalization.STANDARD)
    plotFourier(samplingFreq, duration, xx, good(xx.length).toArray, xaGood, fftSigGood, title="Pure Single Frequency + offset")
    println(s"xaGood length = $lenGood == ${xaGood.length}")

    // Add DC component
    //def fft(fs: Double, signal_length: Int, sig: Array[Double], pad: Boolean) =
    // Fourier.FFT(fs, signal_length, sig, pad=pad, DftNormalization.STANDARD)
    //Fourier.plotFourierTransform(fft, samplingFreq, duration, good(xx.length).toArray.map(_+0.5), "Example Y")

    val (_,xaBad,fftSigBad) = Fourier.FFT(samplingFreq, xx.length, bad, pad = false, DftNormalization.STANDARD)
    //plotFourier(samplingFreq, duration, xx, good(xx.length).toArray, xaBad, fftSigBad, title="Bad")

    // Signal at 20 Hz means that we have to sample at least 1/20 seconds to get a full period (0.05 seconds)
    // So how many samples do we need to get using the selected sampling rate?
    val f1Period = 1.0 / f1
    val (_,xx1) = Fourier.samplingSpecification(samplingFreq,f1Period)

    println(s"samplingFreq ($duration s) = $samplingFreq Hz")
    println(s"Original signal ($duration s) = ${xx.length} samples")
    println(s"Good signal (1T = $f1Period) = ${xx1.length} samples")

    //stft(good, samplingFreq, windowSize, overlap, extendBoundary, pad = true)
    val winSize = 3 * xx1.length
    val windowCount = xx.length.toDouble / winSize
    println(s"Good winSize = $winSize samples")
    println(s"Good windowCount = $windowCount samples")
    // overlap=5,
    //val (stfts, xa, cola) = Fourier.stft(good(xx.length).map(_+0.5), samplingFreq, winSize, pad=false, filter = Filter.initHann)
    val (stftsNoDetrend, xaNoDetrend, colaNoDetrend) = Fourier.stft(good(xx.length), samplingFreq, winSize, pad=false, filter = Filter.initHann)
    val detrended_xx = Filter.removeDC(good(xx.length).toArray)
    val (stfts, xa, cola) = Fourier.stft(detrended_xx, samplingFreq, winSize, pad=false, filter = Filter.initHann)
    val FFTwindowCount = xx.length.toDouble / stfts(0).length
    println(s"xa.length = ${xa.length}")
    println(s"stfts(0).length = ${stfts(0).length}")
    println(s"Good FFTwindowSize = ${stfts(0).length} samples")
    println(s"Good FFTwindowCount = $FFTwindowCount samples")

    val maxAbssGood: Array[(Double, Int)] = maxAbsStfts(stfts)
    val maxAbsGood = maxAbssGood(0) // sortedAbssGood(0)
    println(s"maxAbssGood = ${maxAbssGood.mkString(",")}")
    println(s"maxAbsGood = $maxAbsGood")

    val (_, freqResolution) = Fourier.deltaFreq(samplingFreq, stfts(0).length)
    val topComponent = Fourier.frequencyOfIndex(samplingFreq, stfts(0).length, maxAbsGood._2)
    val expectedTopComponent = Fourier.indexOfFrequency(samplingFreq, stfts(0).length, f1)
    println(s"topComponent = $topComponent Hz")
    println(s"expectedTopComponent = $expectedTopComponent")
    println(s"freqResolution = $freqResolution Hz")

    // Plot the STFT (indirectly)
    import breeze.plot._

    val colors = GradientPaintScale(lower=0, upper=maxAbsGood._1, gradient = PaintScale.BlueToRed)
    Fourier.plotSTFT(stfts, colors, title = "Pure Single Frequency + offset + Detrend")
    Fourier.plotSTFT(stftsNoDetrend, colors, title = "Pure Single Frequency + offset + No detrend")

    //val half = stfts(0).length/2
    //println(half)
    //println(Fourier.abs(stfts(0).take(half)).mkString(",\n"))
    //println(Fourier.abs(stfts(1)).mkString(",\n"))
    //println("--")
  }

  /**
    * Notes: Signal F0 + constant offset + UNIT FFT scaling
    *
    * 1. Use if the FFT scaling does not help. It just changes the absolute
    * scales of the FFT. We still need to use detrending of the offset.
    *
    * 2. Applying normalization/scaling on each window does not help. Open
    * issue is if we should do this normalization/scale at the signal and
    * not the window level
    */
  def exp2(): Unit ={
    val f1 = 10 /* Hz */ ; val a1 = 1  ;  val phase1 = Math.toRadians(0)
    val f2 = 20 /* Hz */ ; val a2 = 1  ;  val phase2 = Math.toRadians(0)
    val samplingFreq = 10 * Fourier.nyquistNamplingFrequency(f1, f2)
    val duration = 2
    val (dd,xx) = Fourier.samplingSpecification(samplingFreq,duration)

    val offset = Function.Const(0.2,dd)
    val good = Function.Sin(f1, a1, phase1, dd) + offset // good
    val bad = Function.Sin(f2, a2, phase2, dd)  + offset // bad

    /*
    import breeze.plot._
    val fig = Figure()
    val plt = fig.subplot(0)
    plt += plot(xx, good(xx.length))
    plt += plot(xx, bad(xx.length))
    plt.title = s"DSL Ops @ $samplingFreq"
    plt.refresh*/

    val (lenGood,xaGood,fftSigGood) = Fourier.FFT(samplingFreq, xx.length, good, pad = false, DftNormalization.UNITARY)
    plotFourier(samplingFreq, duration, xx, good(xx.length).toArray, xaGood, fftSigGood, title="Unitary FFT scaling for F0 + Offset")
    println(s"xaGood length = $lenGood == ${xaGood.length}")

    // Add DC component
    //def fft(fs: Double, signal_length: Int, sig: Array[Double], pad: Boolean) =
    // Fourier.FFT(fs, signal_length, sig, pad=pad, DftNormalization.STANDARD)
    //Fourier.plotFourierTransform(fft, samplingFreq, duration, good(xx.length).toArray.map(_+0.5), "Example Y")

    val (_,xaBad,fftSigBad) = Fourier.FFT(samplingFreq, xx.length, bad, pad = false, DftNormalization.UNITARY)
    //plotFourier(samplingFreq, duration, xx, good(xx.length).toArray, xaBad, fftSigBad, title="Bad")

    // Signal at 20 Hz means that we have to sample at least 1/20 seconds to get a full period (0.05 seconds)
    // So how many samples do we need to get using the selected sampling rate?
    val f1Period = 1.0 / f1
    val (_,xx1) = Fourier.samplingSpecification(samplingFreq,f1Period)

    println(s"samplingFreq ($duration s) = $samplingFreq Hz")
    println(s"Original signal ($duration s) = ${xx.length} samples")
    println(s"Good signal (1T = $f1Period) = ${xx1.length} samples")

    //stft(good, samplingFreq, windowSize, overlap, extendBoundary, pad = true)
    val winSize = 3 * xx1.length
    val windowCount = xx.length.toDouble / winSize
    println(s"Good winSize = $winSize samples")
    println(s"Good windowCount = $windowCount samples")
    // overlap=5,
    //val (stfts, xa, cola) = Fourier.stft(good(xx.length).map(_+0.5), samplingFreq, winSize, pad=false, filter = Filter.initHann)
    //val (stfts, xa, cola) = Fourier.stft(good(xx.length), samplingFreq, winSize, pad=false, filter = Filter.initHann)
    //val detrended_xx = Filter.removeDC(good(xx.length).toArray)
    /*val (stfts, xa, cola) = Fourier.stft(detrended_xx, samplingFreq, winSize,
      pad=false, filter=Filter.initHann, fftNormalization=DftNormalization.STANDARD)*/
    val (stfts, xa, cola) = Fourier.stft(good(xx.length), samplingFreq, winSize,
      pad=false, filter=Filter.initHann, fftNormalization=DftNormalization.UNITARY)
    val FFTwindowCount = xx.length.toDouble / stfts(0).length
    println(s"xa.length = ${xa.length}")
    println(s"stfts(0).length = ${stfts(0).length}")
    println(s"Good FFTwindowSize = ${stfts(0).length} samples")
    println(s"Good FFTwindowCount = $FFTwindowCount samples")

    val maxAbssGood: Array[(Double, Int)] = maxAbsStfts(stfts)
    val maxAbsGood = maxAbssGood(0) // sortedAbssGood(0)
    println(s"maxAbssGood = ${maxAbssGood.mkString(",")}")
    println(s"maxAbsGood = $maxAbsGood")

    val (_, freqResolution) = Fourier.deltaFreq(samplingFreq, stfts(0).length)
    val topComponent = Fourier.frequencyOfIndex(samplingFreq, stfts(0).length, maxAbsGood._2)
    val expectedTopComponent = Fourier.indexOfFrequency(samplingFreq, stfts(0).length, f1)
    println(s"topComponent = $topComponent Hz")
    println(s"expectedTopComponent = $expectedTopComponent")
    println(s"freqResolution = $freqResolution Hz")

    // Plot the STFT (indirectly)
    import breeze.plot._

    val colors = GradientPaintScale(lower=0, upper=maxAbsGood._1, gradient = PaintScale.BlueToRed)
    Fourier.plotSTFT(stfts, colors, title = "Unitary FFT scaling for F0 + Offset + No Detrend")

    /*
    val half = stfts(0).length/2
    println(half)
    //println(Fourier.abs(stfts(0).take(half)).mkString(",\n"))
    println(Fourier.abs(stfts(1)).mkString(",\n"))
    println("--")
    */

    // Apply normalization to the values to see if it improves frequency resolution

    showNormalize("Unitary FFT scaling for F0 + Offset + No Detrend + Normalize", Transform.normalize, stfts)
    showNormalize("Unitary FFT scaling for F0 + Offset + No Detrend + Scale(-1,1)", v => Transform.scale(-1.0,1,v), stfts)
    showNormalize("Unitary FFT scaling for F0 + Offset + No Detrend + softmax", v => Transform.softmax(v), stfts)
    showNormalize("Unitary FFT scaling for F0 + Offset + No Detrend + softmax(2.1)", v => Transform.softMax(v, lambda = 2.1), stfts)
    showNormalize("Unitary FFT scaling for F0 + Offset + No Detrend + logit(1,1,0)", v => Transform.logit(v), stfts)
  }

  case class S[T](a:T)

  sealed trait ParamX[T]
  final case class C[T](p: T) extends ParamX[T]
  final case class P[T1,T2](p1: ParamX[T1], p2: ParamX[T2]) extends ParamX[(T1,T2)]

  trait SearchX[U, T <: ParamX[U]] {
    val search: T
  }

  case object A extends SearchX[(Double,Double), P[Double,Double]] {
    override val search = P(C(1.0), C(2.0))
  }

  def grid[T](numSamples: Int)(p: ParamX[T]) = S(1.0)

  def checkTest[T, U <: ParamX[T]](n:Int, t: SearchX[T,U]): S[Double] = {
    val s1 = t.search
    val s2 = grid(n)(s1)
    s2
  }

  val t1: S[Double] = checkTest(10, A)

  sealed trait TransformSearch[U,T] {
    type F = U => Array[Double] => Array[Double]
    val searchSpace: T
    def transform(vec:Array[Double])(params:U) : Array[Double]
  }

  case object NormSearch extends TransformSearch[Unit, Constant[Unit]] {
    override val searchSpace: Constant[Unit] = Constant(())
    override def transform(vec: Array[Double])(params: Unit): Array[Double] = {
      Transform.normalize(vec)
    }
  }

  case object ScaleSearch extends TransformSearch[((Double, Double)), PairThese[Double, Double]] {
    val lower = List(Constant(-1.0), Constant(0.0), Constant(-1.0))
    val upper = List(Constant(0.0),  Constant(1.0), Constant(1.0))
    val searchSpace: PairThese[Double, Double] = PairThese(lower.zip(upper))
    override def transform(vec: Array[Double])(params: (Double, Double)): Array[Double] = {
      val lower = params._1
      val upper = params._2
      Transform.scale(lower, upper, vec)    }
  }

  case object SoftmaxSearch extends TransformSearch[Unit, Constant[Unit]] {
    override val searchSpace: Constant[Unit] = Constant(())
    override def transform(vec: Array[Double])(params: Unit): Array[Double] = {
      Transform.softmax(vec)
    }
  }

  case object SoftMaxSearch extends TransformSearch[Double, PowerRange] {
    override val searchSpace: PowerRange = PowerRange(2.0,-3,0)
    override def transform(vec: Array[Double])(params: Double): Array[Double] = {
      Transform.softMax(vec, lambda = params)
    }
  }

  case object LogitSearch extends TransformSearch[((Double, Double, Double)), Constant[(Double, Double, Double)]] {
    // l:Double=1.0, k:Double=1.0, zero:Double=0.0
    val p = Constant((1.0, 1.0, 0.0))
    val searchSpace: Constant[(Double, Double, Double)] = p
    override def transform(vec: Array[Double])(params: (Double, Double, Double)): Array[Double] = {
      val l = params._1
      val k = params._2
      val zero = params._3
      Transform.logit(vec, l, k, zero)
    }
  }

  def checkTest[T, U <: Param[T]](numSamples:Int = 10, transform: TransformSearch[T,U], vec: Array[Double]): Unit = {
    println(s"\n$transform ----------")
    val s1: U = transform.searchSpace
    val ps1: Base.Sampler[Base.Finite, T] = Strategy.grid(numSamples)(s1)
    val pa1 = ps1.toList
    println("Search space -----------")
    println(pa1.mkString(";\n"))
    val r1 = pa1.map( transform.transform(vec) )
    println("Transforms -------------")
    println(r1.map( _.mkString(",") + "\n"))
  }

  sealed trait TransformType
  case object Normalize extends TransformType
  case object Scale extends TransformType
  case object Softmax extends TransformType
  case object SoftMax extends TransformType
  case object Logit extends TransformType
  case object DetrendRegression extends TransformType
  case object DetrendDiff extends TransformType

  case class TransformsSearch[U,T](transforms : List[TransformSearch[U,T]]) {

    def transform(vec: Array[Double]): Array[Double] = {
      ???
    }
  }

  def allNorms() = {

  }

  /**
    * Feature design 0
    * Parameter search for the pe-processing step
    *
    *
    */
  def exp3(): Unit = {
    val f1 = 10 /* Hz */ ; val a1 = 1; val phase1 = Math.toRadians(0)
    val f2 = 20 /* Hz */ ; val a2 = 1; val phase2 = Math.toRadians(0)
    val samplingFreq = 10 * Fourier.nyquistNamplingFrequency(f1, f2)
    val duration = 2
    val (dd, xx) = Fourier.samplingSpecification(samplingFreq, duration)

    val offset = Function.Const(0.2, dd)
    val good = Function.Sin(f1, a1, phase1, dd) + offset // good
    val bad = Function.Sin(f2, a2, phase2, dd) + offset // bad

    val data = Array(1.0,2,3,4,5,6,7,8,9,10)

    // Generate the full set of all possible parameter combinations
    // Note: if request more samples than are possible, the error will be
    // detected and returned as an empty type. Make sure this does not
    // happen.
    val numSamples = 10
    checkTest(numSamples, NormSearch, data)
    checkTest(numSamples, ScaleSearch, data)
    checkTest(numSamples, SoftmaxSearch, data)
    checkTest(numSamples, SoftMaxSearch, data)
    checkTest(numSamples, LogitSearch, data)


  }

    /*
      def exp1(): Unit ={
        val f1 = 1 /* Hz */ ; val a1 = 1  ;  val phase1 = Math.toRadians(-45)
        val f2 = 2 /* Hz */ ; val a2 = 1  ;  val phase2 = Math.toRadians(0)
        val f3 = 4 /* Hz */ ; val a3 = 0.6 ; val phase3 = Math.toRadians(20)
        //val samplingFreq = Fourier.nyquistNamplingFrequency(f1, f2, f3)
        // sample at 500Hz and sample for 2 seconds
        val (dd,xx) = Fourier.samplingSpecification(500,2)

        val sig1 = Function.Sin(f1, a1, phase1, dd)
        val sig2 = Function.Sin(f2, a2, phase2, dd)
        val sig3 = Function.Cos(f3, a3, phase3, dd)
        val sig4 = Distribution.Normal(0, 0.3)
        val sig5 = sig1 - sig2 + sig3 + sig4

        import breeze.plot._
        val fig = Figure()
        val plt = fig.subplot(0)
        plt += plot(xx, sig1(xx.length))
        plt += plot(xx, sig2(xx.length))
        plt += plot(xx, sig3(xx.length))
        plt += plot(xx, sig4(xx.length))
        plt += plot(xx, sig5(xx.length))
        plt.title = "DSL Ops"
        plt.refresh
      }
    */

  val experiments: Map[String, () => Unit] = Map(
    "0" -> exp0 _,
    "1" -> exp1 _,
    "2" -> exp2 _,
    "3" -> exp3 _
  )

  def main(args: Array[String]): Unit ={
    for (id <- args){
      if (experiments.contains(id)) {
        println(s"Executing experiment $id.")
        experiments(id)()
      }
      else
        println(s"Experiment arg $id not found.")
    }
  }
}
