/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.detector.perfect

import java.security.MessageDigest

import scala.collection.mutable.ArrayBuffer
import better.files.File
import pt.inescn.utils.NABUtils.{NABResultAll, _}
import pt.inescn.utils.NABUtils.NABDataRow.emptyNABResultAll

import scala.annotation.tailrec
//import scala.collection.JavaConverters._

/**
  * This trait represents the states of the search for a labelled file that can be
  * used to obtain the labels indicating failure or no failure. This data is used
  * to generate a failure score. The intent is to generate a NAB result file with
  * a perfect score. We can use this to test the NAB metric or any other metric we
  * develop. We can use this to test any clean room implementation of the NAB
  * metric.
  *
  * Note that the [[pt.inescn.detector.Detector]] only reads a limited sequence of
  * data points (at least one). Only this data is used to identify the correct file
  * by calculating a hash value.
  */
sealed trait HState {
  /**
    * Data is a time-series consisting of a nano-second precise tme-stamp.
    * The data for each time-stamp is a single real value.
    */
  type Data = (java.time.Instant, Double)
  /**
    * Score is a value between 0 and 1. A 1 is interpreted as a 100% confidence
    * that a given time-stamp value represents a failure (or is within a failure
    * window). Inversely, 0 indicates a 100% confidence that a given time-stamp value
    * does not represent a failure.
    */
  type Score = Double

  /**
    * Returns a file if a hash value has been matched as an [[scala.Option]].
    * If not, a [[scala.None]] is returned.
    *
    * @return
    */
  def gfile: Option[File] = None

  /**
    * Returns the number of data points that have been read up to this point in time.
    *
    * @return
    */
  def gread: Long = Long.MinValue

  /**
    * Indicates what was the previous state. This is useful to track errors.
    *
    * @return
    */
  def gprevious: Option[HState] = None
}

/**
  * The detector is still scanning the data and calculating the hash values of the accumulated
  * data. It still has not found a match and will therefore keep searching.
  *
  * @param read - number of data values read so far
  */
final case class NoHash(read: Int) extends HState {
  override def gread : Long = read
}

/**
  * The detector has found a matching hash value. It can now use the hash value to identify the
  * file with the correct labels.
  *
  * @param file - file that matches digest/key
  * @param read - total number of records consumed
  * @param toProcess - number of records of this batch still to consume
  */
final case class HashFound(file: File, read: Int, toProcess: List[HState#Data]) extends HState {
  override def gfile: Option[File] = Some(file)

  override def gread : Long = read
}

/**
  * As soon as we are in the state [[HashFound]] we get the labelled data and use that data.
  * This state indicates that we have successfully loaded the labelled data and are now using that
  * to make the predictions.
  *
  * @param read - total number of records consumed
  * @param data - data that has been parsed
  */
final case class UseData(read: Long, data: NABResultAll) extends HState {
  override def gread : Long = read
}

/**
  * As soon as we are in the state [[HashFound]] we get the labelled data and use that data.
  * If the hash value does not match an existing file we enter this error state. We store
  * the previous state to facilitate debugging.
  *
  * @param read total number of records read
  * @param previous number of records from the latest batch that have yet to be processed
  */
final case class HashError(read: Long, previous: HState) extends HState {
  override def gread : Long = read

  override def gprevious: Option[HState] = Some(previous)
}

import pt.inescn.detector.Detector

/**
  * This file contains an implementation of a detector that uses orcale data to predict the
  * correct labels. We expect a perfect score from this detector.
  *
  * Created by hmf on 06-04-2017.
  * @param digest - digest used as the key to load the data file
  * @param map - contains a map from a digest to a file name. We use the digest/key to gett he correct file.
  * @param state - we start at the [[NoHash]] state
  */
class PerfectDetector(val digest: MessageDigest, val map: Map[ArrayBuffer[Byte], String],
                      val state: HState = NoHash(0)) extends Detector {

  /**
    * We add one data element (time-stamp and value) at a time to a buffer.
    * At each new addition we calculate the digest. This is converted to a key
    * and this key is used to search for a file that has that key. We keep scanning
    * until we either find a file or no more data is available.
    *
    * @param d - batch of data or slice os stream that was received
    * @return
    */
  def scanForData(d: List[Data]): HState = {
    val len = d.length

    @tailrec
    def go(n: Int): HState = {
      if (n == len) NoHash(len)
      else {
        hashDataPair(digest, d(n)._1, d(n)._2)
        val key = hashToKey(digest)
        val f = map.get(key)
        f match {
          case Some(fn) =>
            val f = File(fn)
            val r = d.drop(n + 1)
            HashFound(f, n + 1, r)
          case None => go(n + 1)
        }
      }
    }

    go(0)
  }

  /**
    * This is the main search routine that changes the state. It starts with the [[NoHash]] state.
    * At this point we keep calculating the digest and check if a file exists with this key.
    * If it does we move to state [[HashFound]] and try to load the file. Any other state
    * will result in a [[HashError]].
    *
    * If the loading of the file was successful we move onto the [[UseData]] state and stay there
    * until we finish making the predictions. Noteb that if we find a [[HashFound]] state between
    * calls then an error must have occurred because we attempt to load th file immediately - so
    * we suld transition to [[UseData]] or [[HashError]].
    *
    *
    * @param d- batch of data or slice os stream that was received
    * @return
    * @see [[scanForData]]
    */
  def identifyData(d: List[Data]): (List[Score], HState) = state match {
    case NoHash(alreadyRead) =>
      scanForData(d) match {
        case NoHash(read) =>
          val d0 = getDefaultScore(d.take(read))
          val s = NoHash(alreadyRead + read)
          (d0, s)
        case e@UseData(read, data) =>
          val d0 = getDefaultScore(d)
          val s = HashError(alreadyRead + read, e)
          (d0, s)
        case HashFound(file, noHashRead, toProcess) =>
          val partialRead = alreadyRead + noHashRead
          val data = loadFile(file, partialRead) // noHashRead) ????
          val d1 = getDefaultScore(d.take(noHashRead))
          val totalRead = partialRead + toProcess.length
          val (rdata, d2) = getPerfectScore(data, toProcess)
          val d0 = d1 ++ d2
          val s = UseData(totalRead, rdata)
          (d0, s)
        case ehe@HashError(alreadyRead1, _) =>
          val scores = getDefaultScore(d)
          val s = HashError(alreadyRead1 + d.length, ehe)
          (scores, s)
      }
    case hf@HashFound(file, noHashRead, toProcess) =>
      val d0 = getDefaultScore(d)
      //val d1 = getDefaultScore(toProcess)
      val s = HashError(noHashRead + toProcess.length, state)
      (d0, s)
    case UseData(alreadyRead, data) =>
      val scores = getDefaultScore(d)
      val s = UseData(alreadyRead + d.length, data)
      (scores, s)
    case HashError(alreadyRead, _) =>
      val scores = getDefaultScore(d)
      val s = HashError(alreadyRead + d.length, state)
      (scores, s)
  }

  /**
    * Here we load the oracle file. We parse the CSV. If it is successful
    * return the list of data from the file except those initial records
    * that were consumed and used for generating and testing the file key
    * (digest).
    *
    * @param f - file with labelled data to load
    * @param alreadyRead - number of records that have been processed to get the key.
    *                    These must be removed.
    * @return
    */
  def loadFile(f: File, alreadyRead: Int): NABResultAll = {
    // We need to bring in shapeless "compile time reflection"
    // https://nrinaudo.github.io/kantan.csv/tut/shapeless.html
    import kantan.csv.generic._

    loadResults(f) match {
      case Left(_) => emptyNABResultAll
      case Right(r) => NABDataRow.drop(r, alreadyRead)
    }
  }

  /**
    * If we are in the [[UseData]] state we can use this call to return the correct prediction label.
    *
    * @param data - oracle file parsed to the correct data structure
    * @param d - predictions generated by the predictor
    * @return
    */
  def getPerfectScore(data: NABResultAll, d: List[Data]): (NABResultAll, List[Score]) = {
    val scores = data.label.take(d.length).map(_.toDouble)
    val ndata = NABDataRow.drop(data, d.length)
    (ndata, scores)
  }

  /**
    * If we are in the [[NoHash]] state we can use this call to return a default (incorrect) prediction label.
    * At this point we do not have the labels to give the correct predictions.
    *
    * @param d - predictions generated by the predictor
    * @return
    */
  def getDefaultScore(d: List[Data]): List[Score] = d.map(_ => 0.0)

  def fileIsLoaded: Boolean = state match {
    case UseData(_, _) => true
    case _ => false;
  }

  /**
    * Here we determine and return the predictions. Depending on the state we
    * either return default values or the correct label/prediction. We the update
    * the state of this detector and return that for the next calls.
    *
    * @param d - predictions generated by the predictor
    * @return
    */
  override def detect(d: List[Data]): (List[Score], PerfectDetector) = {
    val (scores, nstate) = identifyData(d)
    val updated = new PerfectDetector(digest, map, nstate)
    (scores, updated)
  }

}
