/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.eval

import pt.inescn.eval.Base.{Metric, Metrics, MetricsTypes}
import pt.inescn.features.Stats.DescriptiveStats
import pt.inescn.features.Stats.DescriptiveStats.Values
import pt.inescn.models.Base.AlgorithmType.Regression
import pt.inescn.utils.ADWError

/**
  * Mean Square Error
  */
case class MSE[T](override val value: T) extends Metric[T](value)

/**
  * Mean Percentage Error
  * @see https://en.wikipedia.org/wiki/Mean_percentage_error
  */
case class MPE[T](override val value: T) extends Metric[T](value)

/**
  * Root Mean Square Error
  * @see https://en.wikipedia.org/wiki/Root-mean-square_deviation
  */
case class RMSE[T](override val value: T) extends Metric[T](value)

/**
  * Normalized Root Mean Square Error (normalized by largest largest)
  * @see https://en.wikipedia.org/wiki/Root-mean-square_deviation
  */
case class NRMSE[T](override val value: T) extends Metric[T](value)

/**
  * Coefficient of variation of the RMSE (normalized by largest mean)
  * @see https://en.wikipedia.org/wiki/Root-mean-square_deviation
  */
case class CVRMSE[T](override val value: T) extends Metric[T](value)

/**
  * Mean Absolute Error
  * @see https://en.wikipedia.org/wiki/Mean_absolute_error
  */
case class MAE[T](override val value: T) extends Metric[T](value)

/**
  * Mean Absolute Percentage Error
  * @see https://en.wikipedia.org/wiki/Mean_absolute_percentage_error
  */
case class MAPE[T](override val value: T) extends Metric[T](value)


/**
  * Mean Absolute Scaled Error (Non-seasonal)
  * @see https://en.wikipedia.org/wiki/Mean_absolute_scaled_error
  */
case class MASE[T](override val value: T) extends Metric[T](value)

case class MAX[T](override val value: T) extends Metric[T](value)
case class MIN[T](override val value: T) extends Metric[T](value)
case class MEAN[T](override val value: T) extends Metric[T](value)
case class ABSE[T](override val value: T) extends Metric[T](value)

// TODO: https://en.wikipedia.org/wiki/Median_absolute_deviation
// TODO: measurements for skewed dada
// TODO: https://en.wikipedia.org/wiki/Interquartile_range
// TODO: http://www.spiderfinancial.com/support/documentation/numxl/reference-manual/forecasting-performance/mdape
// TODO: Almost correct precisions (how many within x % of the true value)
// http://scikit-learn.org/stable/modules/model_evaluation.html
// https://turi.com/products/create/docs/graphlab.toolkits.evaluation.html
// https://github.com/turi-code


case class Regressions[T](
                           max: MAX[T],
                           min: MIN[T],
                           mean: MEAN[T],
                           mse : MSE[T],
                           mpe: MPE[T],
                           rmse: RMSE[T],
                           nrmse: NRMSE[T],
                           cvrmse: CVRMSE[T],
                           abse: ABSE[T],
                           mae: MAE[T],
                           mape: MAPE[T],
                           mase: MASE[T]
                         )
// Right(Regressions(MAX(maximum), MIN(minimum), MEAN(mean), mse, mpe, rmse, nrmse, cvrmse, ABSE(absError), mae, mape, mase))

trait RegressionTypes extends MetricsTypes {
  type All = Regressions[Double]
  type AllColumns = Regressions[List[Double]]
  type AllStats = Regressions[Values]
}

/**
  * Set of regression errors.
  *
  * TODO: make computation concurrent?
  *
  * Created by hmf on 11-07-2017.
  * @see https://en.wikipedia.org/wiki/Error_metric
  * @see https://www.kaggle.com/wiki/Metrics
  */
case class RegressionMetrics(labels_t: RegressionMetrics#Labels, results_t: RegressionMetrics#Labels)
  extends Metrics[Regression,Double,RegressionTypes] {
  type Labels = Seq[Double]  // labels of predictions

  lazy val len_labels: Int = labels_t.length
  lazy val len_results: Int = results_t.length
  lazy val lens_ok : Boolean = len_labels == len_results

  lazy val n : Double = len_labels.toDouble
  lazy val maximum : Double = labels_t.max
  lazy val minimum : Double = labels_t.min
  lazy val mean : Double = labels_t.sortBy( - _).sum / (1.0 * n) // inverse ordering

  /**
    * Mean Square Error
    */
  lazy val mse = MSE( labels_t.zip(results_t).map(p => (p._1 - p._2)*(p._1 - p._2)).sortBy( - _ ).sum / n )

  /**
    * Mean Percentage Error
    */
  lazy val mpe = MPE( 100 * labels_t.zip(results_t).map(p => (p._1 - p._2)/p._1).sortBy( - _ ).sum / n )

  /**
    * Root Mean Square Error
    */
  lazy val rmse = RMSE( Math.sqrt( mse.value ) )

  /**
    * Normalized Root Mean Square Error (by largest largest)
    */
  lazy val nrmse = NRMSE( rmse.value / (maximum - minimum) )

  /**
    * Coefficient of variation of the RMSE (normalized by largest mean)
    */
  lazy val cvrmse = CVRMSE( rmse.value / mean )

  /**
    * Absolute error
    */
  lazy val absError : Double = labels_t.zip(results_t).map(p => Math.abs(p._1 - p._2)).sortBy( - _ ).sum

  /**
    * Mean Absolute Error
    */
  lazy val mae = MAE(  absError / n )

  /**
    * Mean Absolute Percentage Error
    */
  lazy val mape = MAPE( 100 * labels_t.zip(results_t).map(p => Math.abs((p._1 - p._2)/p._1)).sortBy( - _ ).sum / n )

  /**
    * Mean Absolute Scaled Error
    */
  lazy val mase: MASE[Double] = {
    val difs = results_t.sliding(2).map(pair => Math.abs(pair(1) - pair.head)).toList.sortBy(- _).sum
    val denom = (n/(n-1.0))*difs
    MASE( absError / denom )
  }

  lazy val calculate: Either[ADWError,All] =
    if (lens_ok)
      Right(Regressions(MAX(maximum), MIN(minimum), MEAN(mean), mse, mpe, rmse, nrmse, cvrmse, ABSE(absError), mae, mape, mase))
    else
      Left(ADWError(s"Incorrect lengths: len_labels($len_results) <> len_labels($len_labels)"))


  private val empty = List[Double]()

  override val zero: AllColumns = Regressions(
                                    MAX(empty), MIN(empty), MEAN(empty), MSE(empty), MPE(empty), RMSE(empty),
                                    NRMSE(empty), CVRMSE(empty), ABSE(empty), MAE(empty), MAPE(empty), MASE(empty) )

  override def append(acc: AllColumns, e: All): AllColumns = {
    Regressions(
      MAX(e.max.value :: acc.max.value),
      MIN(e.min.value :: acc.min.value),
      MEAN(e.mean.value :: acc.mean.value),
      MSE(e.mse.value :: acc.mse.value),
      MPE(e.mpe.value :: acc.mpe.value),
      RMSE(e.rmse.value :: acc.rmse.value),
      NRMSE(e.nrmse.value :: acc.nrmse.value),
      CVRMSE(e.cvrmse.value :: acc.cvrmse.value),
      ABSE(e.abse.value :: acc.abse.value),
      MAE(e.mae.value :: acc.mae.value),
      MAPE(e.mape.value :: acc.mape.value),
      MASE(e.mase.value :: acc.mase.value)
    )
  }

  override def stats(acc: AllColumns) : AllColumnsStats = {
    Regressions(
      MAX(DescriptiveStats.batch(acc.max.value)),
      MIN(DescriptiveStats.batch(acc.min.value)),
      MEAN(DescriptiveStats.batch(acc.mean.value)),
      MSE(DescriptiveStats.batch(acc.mse.value)),
      MPE(DescriptiveStats.batch(acc.mpe.value)),
      RMSE(DescriptiveStats.batch(acc.rmse.value)),
      NRMSE(DescriptiveStats.batch(acc.nrmse.value)),
      CVRMSE(DescriptiveStats.batch(acc.cvrmse.value)),
      ABSE(DescriptiveStats.batch(acc.abse.value)),
      MAE(DescriptiveStats.batch(acc.mae.value)),
      MAPE(DescriptiveStats.batch(acc.mape.value)),
      MASE(DescriptiveStats.batch(acc.mase.value))
    )
  }

}
