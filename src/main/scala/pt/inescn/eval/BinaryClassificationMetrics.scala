/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.eval

import pt.inescn.eval.Base.{Metric, Metrics, MetricsTypes}
import pt.inescn.features.Stats.DescriptiveStats
import pt.inescn.features.Stats.DescriptiveStats.Values
import pt.inescn.models.Base.AlgorithmType.BinaryClassification
import pt.inescn.utils.ADWError

/**
  * @see https://en.wikipedia.org/wiki/Precision_and_recall
  */
case class Precision[T](override val value: T) extends Metric[T](value)
/**
  * @see https://en.wikipedia.org/wiki/Precision_and_recall
  */
case class Recall[T](override val value: T) extends Metric[T](value)
/**
  * @see https://en.wikipedia.org/wiki/Precision_and_recall
  */
case class Accuracy[T](override val value: T) extends Metric[T](value)
/**
  * Matthews correlation coefficient
  * @see https://en.wikipedia.org/wiki/Precision_and_recall
  */
case class MCC[T](override val value: T) extends Metric[T](value)
case class FBeta[T](value: T)
/**
  * F-Measure
  * @see https://en.wikipedia.org/wiki/Precision_and_recall
  */
case class FScore[T](beta: FBeta[Double], override val value: T) extends Metric[T](value)
/**
  * @see https://en.wikipedia.org/wiki/Precision_and_recall
  */
case class GMeasure[T](override val value: T) extends Metric[T](value)

case class BinaryClassifications[T](
                                 recall: Recall[T],
                                 precision: Precision[T],
                                 accuracy: Accuracy[T],
                                 mcc: MCC[T],
                                 fscore_1: FScore[T],
                                 fscore_2: FScore[T],
                                 fscore_05: FScore[T],
                                 gmeasure: GMeasure[T]
                               )


trait BinaryClassificationTypes extends MetricsTypes {
  type All = BinaryClassifications[Double]

  type AllColumns = BinaryClassifications[List[Double]]
  type AllStats = BinaryClassifications[Values]
}

// TODO: measurements for multi-classes
// TODO: measurements for skewed dada

/**
  * This class is used to calculate statistical measures of the performance of a binary classifier.
  * Created by hmf on 06-06-2017.
  *
  * TODO: IMPORTANT initiate with a "interpretation function". This function will convert
  * the real values into true and false boolean values.
  * TODO: make computation of the basic values (such as tp, tn, ..) concurrent
  *
  * @see https://www.kaggle.com/wiki/Metrics
  */
case class BinaryClassificationMetrics(labels_t: BinaryClassificationMetrics#Labels, results_t: BinaryClassificationMetrics#Labels)
  extends Metrics[BinaryClassification,Boolean,BinaryClassificationTypes] {
  type Labels = Seq[Boolean]

  lazy val len_labels: Int = labels_t.length
  lazy val len_results: Int = results_t.length
  lazy val length_ok: Boolean = len_labels == len_results

  lazy val tp: Long = true_positives
  lazy val tn: Long = true_negatives
  lazy val fp: Long = false_positives
  lazy val fn: Long = false_negatives


  def true_positives : Long  = {
    val labels = labels_t
    val results = results_t
    val tp = labels.zip(results).filter(v => v._1 && (v._1==v._2))
    tp.length
  }

  def true_negatives : Long = {
    val labels = labels_t
    val results = results_t
    val tn = labels.zip(results).filter(v => (!v._1) && v._1==v._2)
    tn.length
  }

  def false_positives : Long = {
    val labels = labels_t
    val results = results_t
    val fp = labels.zip(results).filter(v => v._2 && (!v._1))
    fp.length
  }

  def false_negatives : Long = {
    val labels = labels_t
    val results = results_t
    val fn = labels.zip(results).filter(v => (!v._2) && v._1)
    fn.length
  }

  /**
    * Sensitivity or true positive rate (TPR)
    * eqv. with hit rate, recall
    * TPR = TP / Parameters = TP / ( TP + FN )
    */
  lazy val recall: Recall[Double] = {
    Recall((tp * 1.0)/ (tp + fn))
  }

  /**
    * Precision or positive predictive value (PPV)
    * PPV = TP / ( TP + FP )
    */
  lazy val precision : Precision[Double] = {
    Precision((tp*1.0) / ( tp + fp ))
  }

  /**
    * Accuracy (ACC)
    * ACC = ( TP + TN ) / ( TP + FP + FN + TN )
    *
    * TODO: why not substitute (tp + fp + fn + tn) by result.length
    */
  lazy val accuracy : Accuracy[Double] = {
    Accuracy(( tp + tn + 0.0) / ( tp + fp + fn + tn ))
  }

  /**
    * Matthews correlation coefficient (MCC)
    *                  TP × TN − FP × FN
    * -----------------------------------------------------
    * Sqrt( ( TP + FP )( TP + FN ) ( TN + FP ) ( TN + FN ) )
    *
    *
    * Wikipedia states on the page for the metric that "If any of the four
    * sums in the denominator is zero, the denominator can be arbitrarily
    * set to one; this results in a Matthews correlation coefficient of
    * zero, which can be shown to be the correct limiting value."
    *
    * @see https://github.com/mlr-org/mlr/issues/1736
    * @see https://github.com/scikit-learn/scikit-learn/issues/1937
    *
    */
  lazy val mcc : MCC[Double] = {
    val num = (tp * tn) - (fp * fn)
    // NOTE: if we place the 1.0 multiplier at the end, then the sum is done in integer type
    // This exceed the limits and generates a negative value. To solve this either place the
    // multiplier n the firs place or inside one of the additions
    val testDenom = Math.sqrt( 1.0 * ( tp + fp ) * ( tp + fn ) * ( tn + fp ) * ( tn + fn ))
    val denom = if (testDenom == 0.0) 1 else testDenom
    MCC(num / denom)
  }

  /**
    * Harmonic mean
    *
    * The F score can be interpreted as a weighted average of the precision and recall, where a score reaches its best
    * value at 1 and worst score at 0. The relative contribution of precision and recall to the F beta are beta and
    * (1-beta). For F1 the relative scores score are equal. F2 weighs recall higher than precision (by placing more
    * emphasis on false negatives). F0.5 measure, weighs recall lower than precision (by attenuating the influence of
    * false negatives).
    *
    * @see [[G]]
    */
  def Fbeta(betaC : FBeta[Double]) : FScore[Double] = {
    val beta = betaC.value
    val beta2 = beta*beta
    val num = (1.0 + beta2) * tp
    val denom = ((1.0 + beta2)* tp) + (beta2*fn) + fp
    FScore(betaC, num / denom)
  }

  /**
    * Same weight for both recall and precision
    */
  lazy val F1: FScore[Double] = Fbeta(FBeta(1.0))

  /**
    * Weighs recall higher than precision
    */
  lazy val F2: FScore[Double] = Fbeta(FBeta(2.0))

  /**
    * Weighs recall lower than precision
    */
  lazy val F0_5: FScore[Double] = Fbeta(FBeta(0.5))

  /**
    * Geometric mean
    * @see [[Fbeta]]
    */
  lazy val G: GMeasure[Double] = GMeasure( Math.sqrt(precision.value * recall.value) )

  lazy val calculate: Either[ADWError,All] =
    if (length_ok)
      Right(BinaryClassifications(recall, precision, accuracy, mcc, F1, F2, F0_5, G))
    else
      Left(ADWError(s"Incorrect lengths: len_labels($len_results) <> len_labels($len_labels)"))

  private val empty = List[Double]()
  override val zero : AllColumns = BinaryClassifications(
                                    Recall(empty), Precision(empty), Accuracy(empty), MCC(empty),
                                    FScore(FBeta(1.0),empty), FScore(FBeta(2.0),empty), FScore(FBeta(0.5),empty),
                                    GMeasure(empty))

  // TODO: add to companion
  override def append(acc: AllColumns, e: All) : AllColumns = {
    BinaryClassifications(
      Recall(e.recall.value :: acc.recall.value),
      Precision(e.precision.value :: acc.precision.value),
      Accuracy(e.accuracy.value :: acc.accuracy.value),
      MCC(e.mcc.value :: acc.mcc.value),
      FScore(e.fscore_1.beta, e.fscore_1.value :: acc.fscore_1.value),
      FScore(e.fscore_2.beta, e.fscore_2.value :: acc.fscore_2.value),
      FScore(e.fscore_05.beta, e.fscore_05.value :: acc.fscore_05.value),
      GMeasure(e.gmeasure.value :: acc.gmeasure.value)
    )
  }

  // TODO: add to companion
  override def stats(acc: AllColumns) : AllColumnsStats = {
    BinaryClassifications(
      Recall(DescriptiveStats.batch(acc.recall.value)),
      Precision(DescriptiveStats.batch(acc.precision.value)),
      Accuracy(DescriptiveStats.batch(acc.accuracy.value)),
      MCC(DescriptiveStats.batch(acc.mcc.value)),
      FScore(acc.fscore_1.beta,DescriptiveStats.batch(acc.fscore_1.value)),
      FScore(acc.fscore_2.beta,DescriptiveStats.batch(acc.fscore_2.value)),
      FScore(acc.fscore_05.beta,DescriptiveStats.batch(acc.fscore_05.value)),
      GMeasure(DescriptiveStats.batch(acc.gmeasure.value))
    )
  }


}

