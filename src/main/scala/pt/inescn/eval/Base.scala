/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.eval

import pt.inescn.models.Base._
import pt.inescn.utils.ADWError

/**
  * Created by hmf on 24-05-2017.
  *
  * when we collect the metrics. No need for conversion to columns later. In addition to
  * this, the descriptive statistics should be calculate directly on the columns without
  * copying to the stats library we selected. These stats calculation should be delayed
  * and threaded.
  */
object Base {

  /**
    * This class represents a single metric such as accuracy (for boolean
    * classification problems) or mean square error (for regression
    * problems).
    *
    * @see [[pt.inescn.eval.BinaryClassificationMetrics]], [[pt.inescn.eval.RegressionMetrics]]
    */
  abstract class Metric[T](val value: T)


  /**
    * Concrete types that the Metrics generates. This allows us to "tag" the
    * values so that we know what metric is being generated and manipulates.
    */
  trait MetricsTypes {
    type All // Metrics that are calculated
    type AllColumns // How they are stored as columns (column major)
    type AllStats // How statistics are stored (column major)
  }

  /**
    * This is a class that is used to generate a set of related metrics.
    * It allows one to calculate those metrics in one a single call. The
    * user can then use these metrics to evaluate and select results.
    *
    * Note: this class should effectively delay computation and allow
    * concurrent computation because these calculations are usually CPU
    * intensive. Some of the current concrete classes use lazy values
    * tha delay computation.
    *
    * @see [[pt.inescn.eval.BinaryClassificationMetrics]], [[pt.inescn.eval.RegressionMetrics]]
    * @tparam A      - Algorithm type such as classification or regression
    * @tparam Labels - The type used for labeling
    */
  trait Metrics[A <: AlgorithmType, Labels, M<:MetricsTypes] {
    type All = M#All
    type AllColumns = M#AllColumns
    type AllColumnsStats = M#AllStats

    val calculate: Either[ADWError, All]
    val zero: AllColumns

    def append(acc: AllColumns, e: All): AllColumns

    def stats(acc: AllColumns): AllColumnsStats
  }

  /**
    * This class provides several methods to operate on a set of related
    * metrics. More specifically one can: initiate delayed calculation of a
    * set of metric types, collect all or some of the metrics, select the
    * maximum or minimum of a combination of metrics (for example using a
    * a calculation of a penalty or reward) and calculate the a number of
    * descriptive statistics of each metric.
    *
    * Note that streams are used to delay the calculation of the metrics.
    * One can use the streams to calculate the metrics simultaneously
    * (parallel multi-core).
    *
    * Note that we do not use the `type Z <: Metrics[A,L]` because
    * we are then using subclasses were the inner types `All` and
    * `AllColumns` remain abstract (and path dependent).
    * @see https://typelevel.org/blog/2015/07/19/forget-refinement-aux.html
    *
    * Note on the use of `M` type parameter. This is necessary to "lift"
    * the "hidden" type member so that they are visible. Once visible
    * it ensures that all containers have the same (visible) `M`. This
    * allows us to constraint all elements f a list or stream to be the exact
    * same type (in this case `Metrics`.
    *
    * @see [[pt.inescn.eval.BinaryClassificationMetrics]]
    * @tparam A - Algorithm type such as classification or regression
    * @tparam Label - The type used for labeling
    */
  trait MetricEval[A <: AlgorithmType, Label, Value, Predict, Z <: Metrics[A,_,M], M <: MetricsTypes] {

    /**
      * Important conversion function. It converts the labels of both the
      * data set and the predictions to conform to the metric. For example
      * a data set may use the dependent variable in float to represent
      * boolean values. Values greater than 0.5 are true otherwise false.
      * For an example see [[BinaryClassificationEvalDouble]]
      *
      * @param test - test data set with dependent type of type `L`
      * @param predicted - predicted output with of type `P`
      * @return
      */
    def apply(test: DataIn[Label,Value], predicted: Prediction[Predict]): Z

    /**
      * Collects rows of Metrics. One can select the subset of metrics to be collected.
      * Note that here collect mans that the metrics are calculated. This may be
      * CPU intensive. The use of the stream delays these calculations.
      *
      * @param sel     selects a subset of the possible metrics
      * @param metrics the stream of metrics to be calculated
      * @tparam T - is the subset fo metrics columns tha were selected
      * @return a Stream os the subset of metrics that were selected in row major
      */
    def collectOnly[T](sel: Z#All => T)(metrics: => Stream[Either[ADWError, Z]]): Either[ADWError, Stream[T]] = {
      val zero: Either[ADWError, Stream[T]] = Right(Stream.empty)
      metrics.foldLeft(zero) {
        (acc, e: Either[ADWError, Z]) =>
          for {
            l <- acc
            el <- e
            p <- el.calculate
            result: Stream[T] = sel(p) #:: l
          } yield result
      }
    }

    /**
      * Collect the set of metrics by calculating them via the metric
      * evaluator.
      *
      * @param metrics - a sequence of metrics evaluators that will be executed.
      * @return
      */
    def collect(metrics: => Stream[Either[ADWError, Z]]): Either[ADWError, Stream[Z#All]] =
      collectOnly(identity)(metrics)

    /**
      * Collects the metrics into columns. This allows one to calculate for
      * aggregates or other statistics for each of the columns. Note that
      * the metrics are not calculated imediatelly. The stream is sued to delay
      * this so that one compute this in parallel.
      *
      * Note that we use generic type parameter to ensure that we access the
      * specific types of the metrics.
      *
      * @param metrics metrics to be calculated
      * @return
      */
    def collectCols(metrics: => Stream[Either[ADWError, Z]]):
    Either[ADWError, Z#AllColumns] = {
      val zero: Either[ADWError, Z#AllColumns] = metrics.head.flatMap(p => Right(p.zero)) //Right(metrics.head.zero)
      metrics.foldLeft(zero) {
        (acc, e) =>
          for {
            l <- acc
            el <- e
            all <- el.calculate
            nacc = el.append(l, all)
          } yield nacc
      }
    }

    /**
      * Calculate the statistics for each metric separately. One can use this
      * for example to analyze the robustness of a algorithm by checking the
      * standard deviation for example. It can als be used to select algorithms
      * with the best performance.
      *
      * @param metrics - the metrics that have been calculated from an evaluation
      * @return
      */
    def stats(metrics: => Stream[Either[ADWError, Z]]): Either[ADWError, Z#AllColumnsStats] = {
      for {
        metric <- metrics.head
        cols <- collectCols(metrics)
        results = metric.stats(cols)
      } yield results
    }

    /**
      * Select the maximum metric set. Selection is done by calculating a
      * value that can be compared. The selection value can be computed from
      * a single or a combination of any of the metrics in the set.
      *
      * @param sel      function that select and computes the weight of the
      *                 metrics
      * @param es       set of metrics that were computed and are now processed
      * @param ordering selection criteria must be comparable
      * @tparam T selection criteria. Can be a numerical value or any
      *           orderable such as a tuple.
      * @return full set of metrics that were selected
      */
    def max[S, T](sel: S => T, es: Stream[S])(implicit ordering: Ordering[T]): S = {
      //es.map( sel ).max
      val t1: S = es.maxBy(p => sel(p))
      t1
    }

    /**
      * Select the minimum metric set. Selection is done by calculating a
      * value that can be compared. The selection value can be computed from
      * a single or a combination of any of the metrics in the set.
      *
      * @param sel      function that select and computes the weight of the
      *                 metrics
      * @param es       set of metrics that were computed and are now processed
      * @param ordering selection criteria must be comparable
      * @tparam T selection criteria. Can be a numerical value or any
      *           orderable such as a tuple.
      * @return full set of metrics that were selected
      */
    def min[S, T](sel: S => T, es: Stream[S])(implicit ordering: Ordering[T]): S = {
      //es.map( sel ).min
      val t1: S = es.minBy(p => sel(p))
      t1
    }
  }

  /**
    * Concrete implementation for the [[pt.inescn.eval.BinaryClassificationMetrics]].
    * Note that the metrics expect dependent (y) variable to be of a boolean.
    * This means that the input y column and prediction will be converted
    * to the boolean type. The conversion is delegated to [[pt.inescn.models.Base.DataIn]] and
    * [[pt.inescn.models.Base.Prediction]] classes.
    *
    * TODO: can/should we delay and thread the conversion of the label type.
    *
    * @see [[MetricEval]]
    */
  case class BinaryClassificationEvalDouble(trueVal: Double) extends MetricEval[AlgorithmType.BinaryClassification, Double, Double, Double,
    BinaryClassificationMetrics, BinaryClassificationTypes] {

    override def apply(test: DataIn[Double,Double], predicted: Prediction[Double]): BinaryClassificationMetrics = {
      lazy val labels: Array[Boolean] = test.getBooleanColumn(0)
      lazy val predictions: Seq[Boolean] = predicted.toBoolean(trueVal)
      val metrics: BinaryClassificationMetrics = BinaryClassificationMetrics(labels, predictions)
      metrics
    }
  }

}
