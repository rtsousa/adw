package pt.inescn.scratchpad

import pt.inescn.features.Stats.{DescriptiveStats, StatsCalculator}

//import scala.language.higherKinds


object ColumnMajor {

  class Metric[T](val value:T)

  case class M1(override val value: Double) extends Metric[Double](value)
  case class M2(override val value: Double) extends Metric[Double](value)

  trait Metrics[All,AllColumns] {
    type A = All
    type C = AllColumns

    val calculate: All
    val zero : AllColumns
    def append(acc: AllColumns, e: All) : AllColumns
  }

  type BinAll = (M1,M2)
  type BinAllColumns = (List[M1], List[M2])

  case class Binary() extends Metrics[BinAll, BinAllColumns] {

    type All = (M1, M2)
    type AllColumns = (List[M1], List[M2])

    override val calculate: All = (M1(0),M2(1))
    override val zero: AllColumns = (List[M1](), List[M2]())

    override def append(acc: AllColumns, e: All): AllColumns = {
      (e._1 :: acc._1, e._2 :: acc._2)
    }
  }

  trait MetricEval {
    type Z[A,B] = Metrics[A,B]

    def collect[A,B](metrics: => Stream[Z[A,B]]): B = {
      val zero: B = metrics.head.zero
      metrics.foldLeft(zero) {
        (acc, e: Z[A,B]) =>
          val all: A = e.calculate
          val nacc: B = e.append(acc, all)
          nacc
      }
    }
  }


  case class BinaryMetricEval() extends MetricEval {
    //override type Z[A,B] = Metrics[(M1,M2), (List[M1], List[M2])]
  }

  def main(args: Array[String]): Unit = {
    val bin1: Metrics[(M1,M2), (List[M1], List[M2])] = Binary()
    val eval1: BinaryMetricEval = BinaryMetricEval()
    val eval2 = new MetricEval {}
    val cols1: Binary#AllColumns = bin1.append(bin1.zero,(M1(0),M2(0)))
    println(cols1)
    val l1: List[Binary] = List(Binary(), Binary(), Binary(), Binary())
    //val col1: Binary#AllColumns = bin1.collect(l1.toStream)
    val col1: Binary#AllColumns = eval1.collect(l1.toStream)
    println(col1)
    val col2: Binary#AllColumns = eval2.collect(l1.toStream)
    println(col2)
  }
}

object ColumnMajor1 {

  class Metric[T](val value:T)

  case class M1(override val value: Double) extends Metric[Double](value)
  case class M2(override val value: Double) extends Metric[Double](value)

  trait Metrics {
    type All
    type AllColumns

    val calculate: All
    val zero : AllColumns
    def append(acc: AllColumns, e: All) : AllColumns
  }

  case class Binary() extends Metrics {

    type All = (M1, M2)
    type AllColumns = (List[M1], List[M2])

    override val calculate: All = (M1(0),M2(1))
    override val zero: AllColumns = (List[M1](), List[M2]())

    override def append(acc: AllColumns, e: All): AllColumns = {
      (e._1 :: acc._1, e._2 :: acc._2)
    }
  }

  trait MetricEval {
    //type Z <: Metrics

    // https://users.scala-lang.org/t/why-do-i-need-to-indicate-the-type-of-functions-generic-type-parameter-that-should-be-inferred/1704
    // https://typelevel.org/blog/2015/07/13/type-members-parameters.html
    /*
    def collect0[T <: Metrics,A,B](metrics: => Stream[T { type All = A ; type AllColumns = B }]) = {
      val zero: B = metrics.head.zero
      metrics.foldLeft(zero) {
        (acc, e: T) =>
          val all:A = e.calculate
          val nacc:B = e.append(acc, all)
          nacc
      }
    }*/

    def collect[A,B](metrics: => Stream[Metrics { type All = A ; type AllColumns = B }]): B = {
      val zero: B = metrics.head.zero
      metrics.foldLeft(zero) {
        (acc, e ) =>
          val all:A = e.calculate
          val nacc:B = e.append(acc, all)
          nacc
      }
    }
  }


  case class BinaryMetricEval() extends MetricEval {
    //override type Z[A,B] = Metrics[(M1,M2), (List[M1], List[M2])]
  }

  def main(args: Array[String]): Unit = {
    val bin1: Binary = Binary()
    val eval1: BinaryMetricEval = BinaryMetricEval()
    val eval2 = new MetricEval {}
    val cols1: Binary#AllColumns = bin1.append(bin1.zero,(M1(0),M2(0)))
    println(cols1)
    val l1: List[Binary] = List(Binary(), Binary(), Binary(), Binary())
    //val col1: Binary#AllColumns = bin1.collect(l1.toStream)
    val col1: Binary#AllColumns = eval1.collect(l1.toStream)
    println(col1)
    val col2: Binary#AllColumns = eval2.collect(l1.toStream)
    println(col2)
  }
}


object ColumnMajor2 {

  class Metric[T](val value:T)

  case class M1(override val value: Double) extends Metric[Double](value)
  case class M2(override val value: Double) extends Metric[Double](value)

  trait Metrics {
    type All
    type AllColumns

    val calculate: All
    val zero : AllColumns
    def append(acc: AllColumns, e: All) : AllColumns
  }

  case class Binary() extends Metrics {

    type All = (M1, M2)
    type AllColumns = (List[M1], List[M2])

    override val calculate: All = (M1(0),M2(1))
    override val zero: AllColumns = (List[M1](), List[M2]())

    override def append(acc: AllColumns, e: All): AllColumns = {
      (e._1 :: acc._1, e._2 :: acc._2)
    }
  }

  trait MetricEval {
    type Z = Metrics
    // type Z <: Metrics won't work

    def collect[A,B](metrics: => Stream[Z { type All = A ; type AllColumns = B }]): B = {
      val zero: B = metrics.head.zero
      metrics.foldLeft(zero) {
        (acc, e ) =>
          val all:A = e.calculate
          val nacc:B = e.append(acc, all)
          nacc
      }
    }

    // Issue: we need to add implicits that can handle a specific C/AllColumns
    def stats[B,C,R1,R2](metrics: => Stream[Z { type All = B ; type AllColumns = C }])
                        (implicit calc : StatsCalculator[C,R1,R2]): R1 = {
      val cols: C = collect[B,C](metrics)
      val results = DescriptiveStats.batch(cols)
      results
    }
  }


  case class BinaryMetricEval() extends MetricEval {
    //override type Z[A,B] = Metrics[(M1,M2), (List[M1], List[M2])]
    //override type Z = Binary
  }

  def main(args: Array[String]): Unit = {
    val bin1: Binary = Binary()
    val eval1: BinaryMetricEval = BinaryMetricEval()
    val eval2 = new MetricEval {}
    val cols1: Binary#AllColumns = bin1.append(bin1.zero,(M1(0),M2(0)))
    println(cols1)
    val l1: List[Binary] = List(Binary(), Binary(), Binary(), Binary())
    //val col1: Binary#AllColumns = bin1.collect(l1.toStream)
    val col1: Binary#AllColumns = eval1.collect(l1.toStream)
    println(col1)
    val col2: Binary#AllColumns = eval2.collect(l1.toStream)
    println(col2)
  }
}

/*
object ColumnMajor3 {

  class Metric[T](val value:T)

  case class M1(override val value: Double) extends Metric[Double](value)
  case class M2(override val value: Double) extends Metric[Double](value)

  trait Metrics {
    type All
    type AllColumns

    val calculate: All
    val zero : AllColumns
    def append(acc: AllColumns, e: All) : AllColumns
  }

  case class Binary(n:Int) extends Metrics {

    type All = (M1, M2)
    type AllColumns = (List[M1], List[M2])

    override val calculate: All = (M1(0),M2(1))
    override val zero: AllColumns = (List[M1](), List[M2]())

    override def append(acc: AllColumns, e: All): AllColumns = {
      (e._1 :: acc._1, e._2 :: acc._2)
    }
  }

  trait MetricEval {
    type Z = Metrics
    // type Z <: Metrics won't work

    val builder: MetricBuilder

    def collect[A,B](metrics: => Stream[Z { type All = A ; type AllColumns = B }]): B = {
      val zero: B = metrics.head.zero
      metrics.foldLeft(zero) {
        (acc, e ) =>
          val all:A = e.calculate
          val nacc:B = e.append(acc, all)
          nacc
      }
    }

    // Issue: we need to add implicits that can handle a specific C/AllColumns
    def stats[B,C,R1,R2](metrics: => Stream[Z { type All = B ; type AllColumns = C }])
                        (implicit calc : StatsCalculator[C,R1,R2]): R1 = {
      val cols: C = collect[B,C](metrics)
      val results = DescriptiveStats.batch(cols)
      results
    }
  }

  trait MetricBuilder {

    type Z <: Metrics
    def apply(n: Int): Z
  }

  case class BinBuilder() extends MetricBuilder {
    override type Z = Binary

    override def apply(n: Int): Binary = Binary(n)
  }

  case class BinaryMetricEval() extends MetricEval {
    //override type Z[A,B] = Metrics[(M1,M2), (List[M1], List[M2])]
    //override type Z = Binary
    override val builder: MetricBuilder = BinBuilder()
  }

  object ParamSelection {

    def crossValidation(ev: MetricEval): Stream[Metrics] = {
      val m: MetricBuilder = ev.builder
      val t: Stream[Metrics] = (0 to 5).toStream.map(e => m(e) )
      t
    }

    def select(ev: MetricEval) = {
      val mets: Stream[Metrics] = crossValidation(ev)
      ev.stats(mets)
    }
  }


  def main(args: Array[String]): Unit = {
    val bin1: Binary = Binary(0)
    val eval1: BinaryMetricEval = BinaryMetricEval()
    val eval2 = new MetricEval {}
    val cols1: Binary#AllColumns = bin1.append(bin1.zero,(M1(0),M2(0)))
    println(cols1)
    val l1: List[Binary] = List(Binary(0), Binary(1), Binary(2), Binary(3))
    //val col1: Binary#AllColumns = bin1.collect(l1.toStream)
    val col1: Binary#AllColumns = eval1.collect(l1.toStream)
    println(col1)
    val col2: Binary#AllColumns = eval2.collect(l1.toStream)
    println(col2)
  }
}
*/

/*
object ColumnMajor4 {

  class Metric[T](val value:T)

  case class M1(override val value: Double) extends Metric[Double](value)
  case class M2(override val value: Double) extends Metric[Double](value)

  trait Metrics[All, AllColumns, AllStats] {

    val calculate: All
    val zero: AllColumns

    def append(acc: AllColumns, e: All): AllColumns
    def stats(acc: AllColumns): AllStats
  }

  type BinaryAll = (M1, M2)
  type BinaryAllColumns = (List[M1], List[M2])
  type BinaryColumnsStats = (List[Double], List[Double])

  case class Binary(n:Int) extends Metrics[BinaryAll, BinaryAllColumns,BinaryColumnsStats] {

    type All = BinaryAll
    type AllColumns = BinaryAllColumns
    type AllStats = BinaryColumnsStats

    override val calculate: All = (M1(0),M2(1))
    override val zero: AllColumns = (List[M1](), List[M2]())

    override def append(acc: AllColumns, e: All): AllColumns =
      (e._1 :: acc._1, e._2 :: acc._2)

    override def stats(acc: (List[M1], List[M2])): (List[Double], List[Double]) =
      (acc._1.map(_.value),acc._1.map(_.value))
  }

  trait MetricBuilder[Z[X,Y,W] <: Metrics[X,Y,W],A,B,C] {

    def apply(n: Int): Z[A,B,C]
  }

  trait MetricEval[Z[X,Y,W] <: Metrics[X,Y,W],A,B,C] {

    val builder: MetricBuilder[Z,A,B,C]

    def collect(metrics: => Stream[Z[A,B,C]]): B = {
      val zero: B = metrics.head.zero
      metrics.foldLeft(zero) {
        (acc, e ) =>
          val all:A = e.calculate
          val nacc:B = e.append(acc, all)
          nacc
      }
    }

    // Issue: we need to add implicits that can handle a specific C/AllColumns
    def stats(metrics: => Stream[Z[A,B,C]]) = {
      val cols: B = collect(metrics)
      val results = ???
      results
    }
  }

  case class BinBuilder() extends MetricBuilder[Metrics,Binary#All,Binary#AllColumns,Binary#AllStats] {
    override def apply(n: Int): Binary = Binary(n)
  }

  case class BinaryMetricEval() extends MetricEval[Metrics,Binary#All,Binary#AllColumns,Binary#AllStats] {
    override val builder: BinBuilder = BinBuilder()
  }

  object ParamSelection {

    def crossValidation[Z[X,Y,W] <: Metrics[X,Y,W],A,B,C](ev: MetricEval[Z,A,B,C]): Stream[Z[A,B,C]] = {
      val m: MetricBuilder[Z,A,B,C] = ev.builder
      //val v: Z[A,B] = m(0)
      val t: Stream[Z[A,B,C]] = (0 to 5).toStream.map(e => m(e) )
      t
    }

    def select[Z[X,Y,W] <: Metrics[X,Y,W],A,B,C](ev: MetricEval[Z,A,B,C]) = {
      val mets: Stream[Z[A,B,C]] = crossValidation(ev)
      ev.stats(mets)
    }
  }

  def main(args: Array[String]): Unit = {
    val bin1: Binary = Binary(0)
    val eval1: BinaryMetricEval = BinaryMetricEval()
    val cols1: Binary#AllColumns = bin1.append(bin1.zero,(M1(0),M2(0)))
    println(cols1)
    val l1: List[Binary] = List(Binary(0), Binary(1), Binary(2), Binary(3))
    val col1: Binary#AllColumns = eval1.collect(l1.toStream)
    println(col1)
  }
}
*/

/*
object ColumnMajor5 {

  class Metric[T](val value:T)

  case class M1(override val value: Double) extends Metric[Double](value)
  case class M2(override val value: Double) extends Metric[Double](value)

  trait Metrics[All, AllColumns, AllStats] {

    val calculate: All
    val zero: AllColumns

    def append(acc: AllColumns, e: All): AllColumns
    def stats(acc: AllColumns): AllStats
  }

  type BinaryAll = (M1, M2)
  type BinaryAllColumns = (List[M1], List[M2])
  type BinaryColumnsStats = (List[Double], List[Double])

  case class Binary(n:Int) extends Metrics[BinaryAll, BinaryAllColumns,BinaryColumnsStats] {

    type All = BinaryAll
    type AllColumns = BinaryAllColumns
    type AllStats = BinaryColumnsStats

    override val calculate: All = (M1(0),M2(1))
    override val zero: AllColumns = (List[M1](), List[M2]())

    override def append(acc: AllColumns, e: All): AllColumns =
      (e._1 :: acc._1, e._2 :: acc._2)

    override def stats(acc: (List[M1], List[M2])): (List[Double], List[Double]) =
      (acc._1.map(_.value),acc._1.map(_.value))
  }

  trait MetricEval[Z[X,Y,W] <: Metrics[X,Y,W],A,B,C] {

    def apply(n: Int): Z[A,B,C]

    def collect(metrics: => Stream[Z[A,B,C]]): B = {
      val zero: B = metrics.head.zero
      metrics.foldLeft(zero) {
        (acc, e ) =>
          val all:A = e.calculate
          val nacc:B = e.append(acc, all)
          nacc
      }
    }

    // Issue: we need to add implicits that can handle a specific C/AllColumns
    def stats(metrics: => Stream[Z[A,B,C]]) = {
      val cols: B = collect(metrics)
      val results = ???
      results
    }
  }

  case class BinaryMetricEval() extends MetricEval[Metrics,Binary#All,Binary#AllColumns,Binary#AllStats] {
    override def apply(n: Int): Binary = Binary(n)
  }

  object ParamSelection {

    def crossValidation[Z[X,Y,W] <: Metrics[X,Y,W],A,B,C](ev: MetricEval[Z,A,B,C]): Stream[Z[A,B,C]] = {
      //val v: Z[A,B] = m(0)
      val t: Stream[Z[A,B,C]] = (0 to 5).toStream.map(e => ev(e) )
      t
    }

    def select[Z[X,Y,W] <: Metrics[X,Y,W],A,B,C](ev: MetricEval[Z,A,B,C]) = {
      val mets: Stream[Z[A,B,C]] = crossValidation(ev)
      ev.stats(mets)
    }
  }

  def main(args: Array[String]): Unit = {
    val bin1: Binary = Binary(0)
    val eval1: BinaryMetricEval = BinaryMetricEval()
    val cols1: Binary#AllColumns = bin1.append(bin1.zero,(M1(0),M2(0)))
    println(cols1)
    val l1: List[Binary] = List(Binary(0), Binary(1), Binary(2), Binary(3))
    val col1: Binary#AllColumns = eval1.collect(l1.toStream)
    println(col1)
  }
}
*/

/*
object ColumnMajor6 {

  class Metric[T](val value:T)

  case class M1(override val value: Double) extends Metric[Double](value)
  case class M2(override val value: Double) extends Metric[Double](value)

  trait Metrics[All, AllColumns, AllStats] {

    val calculate: All
    val zero: AllColumns

    def append(acc: AllColumns, e: All): AllColumns
    def stats(acc: AllColumns): AllStats
  }

  type BinaryAll = (M1, M2)
  type BinaryAllColumns = (List[M1], List[M2])
  type BinaryColumnsStats = (List[Double], List[Double])

  case class Binary(n:Int) extends Metrics[BinaryAll, BinaryAllColumns,BinaryColumnsStats] {

    type All = BinaryAll
    type AllColumns = BinaryAllColumns
    type AllStats = BinaryColumnsStats

    override val calculate: All = (M1(0),M2(1))
    override val zero: AllColumns = (List[M1](), List[M2]())

    override def append(acc: AllColumns, e: All): AllColumns =
      (e._1 :: acc._1, e._2 :: acc._2)

    override def stats(acc: (List[M1], List[M2])): (List[Double], List[Double]) =
      (acc._1.map(_.value),acc._1.map(_.value))
  }

  trait MetricEval[Z <: Metrics[A,B,C],A,B,C] {

    def apply(n: Int): Z

    def collect(metrics: => Stream[Z]): B = {
      val zero: B = metrics.head.zero
      metrics.foldLeft(zero) {
        (acc, e ) =>
          val all:A = e.calculate
          val nacc:B = e.append(acc, all)
          nacc
      }
    }

    // Issue: we need to add implicits that can handle a specific C/AllColumns
    def stats(metrics: => Stream[Z]) = {
      val cols: B = collect(metrics)
      val results = ???
      results
    }
  }

  case class BinaryMetricEval() extends MetricEval[Binary,Binary#All,Binary#AllColumns,Binary#AllStats] {
    override def apply(n: Int): Binary = Binary(n)
  }

  object ParamSelection {

    def crossValidation[Z <: Metrics[A,B,C],A,B,C](ev: MetricEval[Z,A,B,C]): Stream[Z] = {
      //val v: Z[A,B] = m(0)
      val t: Stream[Z] = (0 to 5).toStream.map(e => ev(e) )
      t
    }

    def select[Z <: Metrics[A,B,C],A,B,C](ev: MetricEval[Z,A,B,C]) = {
      val mets: Stream[Z] = crossValidation(ev)
      ev.stats(mets)
    }
  }

  def main(args: Array[String]): Unit = {
    val bin1: Binary = Binary(0)
    val eval1: BinaryMetricEval = BinaryMetricEval()
    val cols1: Binary#AllColumns = bin1.append(bin1.zero,(M1(0),M2(0)))
    println(cols1)
    val l1: List[Binary] = List(Binary(0), Binary(1), Binary(2), Binary(3))
    val col1: Binary#AllColumns = eval1.collect(l1.toStream)
    println(col1)
  }
}
*/

object ColumnMajor7 {

  class Metric[T](val value:T)

  case class M1(override val value: Double) extends Metric[Double](value)
  case class M2(override val value: Double) extends Metric[Double](value)

  trait MetricsTypes {
    type All
    type AllColumns
    type AllStats
  }

  trait Metrics[M<:MetricsTypes] {
    type All = BinMetricsTypes#All
    type AllColumns = BinMetricsTypes#AllColumns
    type AllStats = BinMetricsTypes#AllStats

    val calculate: M#All
    val zero: M#AllColumns

    def append(acc: M#AllColumns, e: M#All): M#AllColumns
    def stats(acc: M#AllColumns): M#AllStats
  }

  trait BinMetricsTypes extends MetricsTypes {
    type All = (M1, M2)
    type AllColumns = (List[M1], List[M2])
    type AllStats = (List[Double], List[Double])
  }

  case class Binary(n:Int) extends Metrics[BinMetricsTypes] {

    /* Nore required
    type All = BinMetricsTypes#All
    type AllColumns = BinMetricsTypes#AllColumns
    type AllStats = BinMetricsTypes#AllStats
     */

    override val calculate: All = (M1(0),M2(1))
    override val zero: AllColumns = (List[M1](), List[M2]())

    override def append(acc: AllColumns, e: All): AllColumns =
      (e._1 :: acc._1, e._2 :: acc._2)

    override def stats(acc: (List[M1], List[M2])): (List[Double], List[Double]) =
      (acc._1.map(_.value),acc._1.map(_.value))
  }

  trait MetricEval[Z <: Metrics[M], M <: MetricsTypes] {

    def apply(n: Int): Z

    def collect(metrics: => Stream[Z]): M#AllColumns = {
      val zero: M#AllColumns = metrics.head.zero
      metrics.foldLeft(zero) {
        (acc, e ) =>
          val all:M#All = e.calculate
          val nacc:M#AllColumns = e.append(acc, all)
          nacc
      }
    }

    // Issue: we need to add implicits that can handle a specific C/AllColumns
    def stats(metrics: => Stream[Z]): M#AllColumns = {
      val cols: M#AllColumns = collect(metrics)
      cols
    }
  }

  case class BinaryMetricEval() extends MetricEval[Binary,BinMetricsTypes] {
    override def apply(n: Int): Binary = Binary(n)
  }

  object ParamSelection {

    def crossValidation[Z <: Metrics[M],M <: MetricsTypes](ev: MetricEval[Z,M]): Stream[Z] = {
      //val v: Z[A,B] = m(0)
      val t: Stream[Z] = (0 to 5).toStream.map(e => ev(e) )
      t
    }

    def select[Z <: Metrics[M],M <: MetricsTypes](ev: MetricEval[Z,M]) = {
      val mets: Stream[Z] = crossValidation(ev)
      ev.stats(mets)
    }
  }

  def main(args: Array[String]): Unit = {
    val bin1: Binary = Binary(0)
    val eval1: BinaryMetricEval = BinaryMetricEval()
    val cols1: Binary#AllColumns = bin1.append(bin1.zero,(M1(0),M2(0)))
    println(cols1)
    val l1: List[Binary] = List(Binary(0), Binary(1), Binary(2), Binary(3))
    val col1: Binary#AllColumns = eval1.collect(l1.toStream)
    println(col1)
  }
}

object ColumnMajor8 {

  sealed trait Base
  trait B1 extends Base
  trait B2 extends Base

  trait ML {
    type ML1 <: Base
    type ML2
  }

  trait Algo[S <: ML] {
    def exec(arg1: S#ML1, arg2: S#ML2): S#ML2
  }

  trait MT {
    type MT1
    type MT2
  }

  trait Met[A <: Base, B, M<:MT] {
    def exec(arg1: B, arg2 : M#MT1): M#MT2
  }


  trait MLI1 extends ML {
    override type ML1 = B1
    override type ML2 = Double
  }

  case class AlgoI1() extends Algo[MLI1] {
    override def exec(arg1: MLI1#ML1, arg2: MLI1#ML2): MLI1#ML2 = 0.0
  }


  trait MTI1 extends MT {
    override type MT1 = Double
    override type MT2 = Double
  }

  case class Met1() extends Met[B1, Double, MTI1] {
    override def exec(arg1: Double, arg2: MTI1#MT1): MTI1#MT2 = 0.0
  }

  def use[A <: ML, Z <: Met[A#ML1,A#ML2,M], M <: MT]
  ( algorithm: Algo[A])
  ( metric: Met[A#ML1,A#ML2,M]) : Z = {
      ???
  }

  def use0[A <: ML, Z <: Met[A#ML1,A#ML2,M], M <: MT]
  ( algorithm: Algo[A], metric: Met[A#ML1,A#ML2,M]) : Z = {
    ???
  }

  def use1[A <: ML, Z <: Met[A#ML1,A#ML2,M], M <: MT]
  ( metric: Met[A#ML1,A#ML2,M], algorithm: Algo[A]) : Z = {
    ???
  }

}