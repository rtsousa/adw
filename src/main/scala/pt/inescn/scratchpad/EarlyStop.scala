package pt.inescn.scratchpad

object EarlyStop {

  /**
    * Example of how `return` can be used to exit a closure from
    * within a function.
    *
    * @param nums
    * @return
    */
  def sumEvenNumbers(nums: Iterable[Int]): Option[Int] = {
    nums.foldLeft (Some(0): Option[Int]) {
      case (None, _) => println("done 1") ; return None
      case (Some(s), n) if n % 2 == 0 => println("done % 2") ; Some(s + n)
      case (Some(_), _) => println("done 2") ; return None
    }
  }

  def main(args: Array[String]): Unit = {
    println( "Two print, one for the 2, the other stops, we get None: " + sumEvenNumbers(2 to 10))
    println( "Keeps printing % 2 to get Some(30): " + sumEvenNumbers(2 to 10 by 2))
  }

}
