package pt.inescn.scratchpad

import better.files.File
import pt.inescn.etl.Base.Source
import pt.inescn.models.Base.AlgorithmType
import pt.inescn.models.Base.AlgorithmType.{BinaryClassification, MultiClassification, Regression}
import pt.inescn.models.Data.SparseMatrixData
import pt.inescn.utils.ADWError

// See https://github.com/scala/bug/issues/10466
object UnboundedTypeParameterMin {

  sealed trait S
  sealed trait S1 extends S
  sealed trait S2 extends S

  type Funk[T <: S] = (Double) => Either[String, Double]

  implicit def check: Funk[S1] = (v : Double) => Right(v)
  implicit def check0: Funk[S2] = (v : Double) => Right(v)

  class Do[A <: S]() {
    def exec[T <: A](v: Double)(implicit ev: Funk[T]): Either[String, Double] = ev(v)
  }

  val t1: Do[S1] = new Do[S1]()
  val r2: Either[String, Double] = t1.exec(100.0)
  println(r2)
  // ambiguous implicit values:
  /*val r4: Either[String, Double] = t1.exec[S1](100.0)
  println(r4)*/
}


object UnboundedTypeParameter {

  sealed trait S
  sealed trait S1 extends S
  sealed trait S2 extends S
  sealed trait S3 extends S
  sealed trait S4 extends S

  type Funk[T <: S] = (Double) => Either[String, Double]

  implicit def check: Funk[S1] = (v : Double) => Right(v)
  implicit def check0: Funk[S2] = (v : Double) => Right(v)
  implicit def check1: Funk[S3] = (v : Double) => Right(v)
  implicit def check2: Funk[S4] = (v : Double) => Right(v)

  class Do[A <: S]() {
    def check(v: Double)(implicit ev: Funk[A]): Either[String, Double] = ev(v)
    def exec[T <: A](v: Double)(implicit ev: Funk[T]): Either[String, Double] = ev(v)
    //def exec0[T](v: Double)(implicit ev: String[T], ev2 : A =:= T): Either[ADWError, Double] = ev(v)
    //def exec0[T >: A <: A](v: Double)(implicit ev: String[T]): Either[ADWError, Double] = ev(v)
    def exec0[T >: A <: A](v: Double)(implicit ev: Funk[T]): Either[String, Double] = ev(v)
    def subExec[T <: A](s: String)(implicit ev: Funk[T]) : Either[String, Double] = {
      val t1 = s.toDouble
      exec(t1)(ev)
    }
    // ambiguous implicit values:
    /*def proExec[T <: A : PhantomCheckFunk](s: String) : Either[String, Double] = {
      val t1 = s.toDouble
      val chk = implicitly[PhantomCheckFunk[T]]
      exec(t1)(chk)
    }*/
  }

  val t1: Do[S1] = new Do[S1]()
  // ambiguous implicit values:
  /*val r1: Either[String, Double] = t1.check(100.0)
  println(r1)*/
  val r2: Either[String, Double] = t1.exec(100.0)
  println(r2)
  // ambiguous implicit values:
  /*val r4: Either[String, Double] = t1.exec[S1](100.0)
  println(r4)*/
  val r3: Either[String, Double] = t1.subExec("100.0")
  println(r3)
  // ambiguous implicit values:
  /*val t2: Do[S2] = new Do[S2]()
  val r4: Either[String, Double] = t2.exec0(100.0)
  println(r4)*/
}

/**
  * Experimenting with selecting and using a function during compile time.
  * Uses implicits and phantoms.
  *
  * NOTE: THIS DOES NOT work. The Phantom type is simply ignored/dropped.
  * In fact this should not compile due to `ambiguous implicit values` error.
  * The working example may be due to a bug. Dotty can detect this.
  *
  * @see https://users.scala-lang.org/t/enforcing-type-equality-bound-on-implicit-type-parameter/1524
  */
object InjectPhantomFunc {

  type PhantomFunk[T <: AlgorithmType] = (Double) => Either[ADWError, Double]

  implicit def check: PhantomFunk[MultiClassification] = (v : Double) => Right(v)
  implicit def check0: PhantomFunk[BinaryClassification] = (v : Double) => Right(v)
  //implicit def check1: PhantomFunk[Classification] = (v : Double) => Right(v)
  implicit def check2: PhantomFunk[Regression] = (v : Double) => Right(v)

  class Do[A <: AlgorithmType]() {
    def check(v: Double)(implicit ev: PhantomFunk[A]): Either[ADWError, Double] = ev(v)
    def exec[T <: A](v: Double)(implicit ev: PhantomFunk[T]): Either[ADWError, Double] = ev(v)
    //def exec0[T](v: Double)(implicit ev: PhantomFunk[T], ev2 : A =:= T): Either[ADWError, Double] = ev(v)
    //def exec0[T >: A <: A](v: Double)(implicit ev: PhantomFunk[T]): Either[ADWError, Double] = ev(v)
    def exec0[T >: A <: A](v: Double)(implicit ev: PhantomFunk[T]): Either[ADWError, Double] = ev(v)
    def subExec[T <: A](s: String)(implicit ev: PhantomFunk[T]) : Either[ADWError, Double] = {
      val t1 = s.toDouble
      exec(t1)(ev)
    }
    // ambiguous implicit values:
    /*def proExec[T <: A : PhantomCheckFunk](s: String) : Either[String, Double] = {
      val t1 = s.toDouble
      val chk = implicitly[PhantomCheckFunk[T]]
      exec(t1)(chk)
    }*/
  }

  implicit object user extends Source[File, SparseMatrixData] {
    val t1: Do[BinaryClassification] = new Do[BinaryClassification]()

    def useIt(v:Double): Unit = println(t1.exec(v))

    /** Processes input */
    override def map(i: File): Either[ADWError, SparseMatrixData] =
      Left(ADWError(""+t1.exec(100)))
  }

  val t1: Do[Regression] = new Do[Regression]()
  // ambiguous implicit values:
  /*val r1: Either[ADWError, Double] = t1.check(100.0)
  println(r1)*/
  val r2: Either[ADWError, Double] = t1.exec(100.0)
  println(r2)
  /* ambiguous implicit values:
  val r4: Either[ADWError, Double] = t1.exec[Regression](100.0)
  pr*/
  val r3: Either[ADWError, Double] = t1.subExec("100.0")
  println(r3)
  // ambiguous implicit values:
  /*val t2: Do[BinaryClassification] = new Do[BinaryClassification]()
  val r4: Either[ADWError, Double] = t2.exec0(100.0)
  println(r4)*/

}
