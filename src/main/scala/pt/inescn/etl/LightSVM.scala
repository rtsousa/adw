package pt.inescn.etl

import better.files.File
import pt.inescn.etl.Base.Source
import pt.inescn.etl.Load.Fold
import pt.inescn.models.Base.DataIn
import pt.inescn.models.Data.SparseMatrixData
import pt.inescn.utils.ADWError

import scala.collection.immutable.TreeMap


/**
  * Contans a class that loads a file with sparse matrix data in the light SVM format, also used by the
  * libSVM library. The data collection and storage is left to a specific implementation. Data storage may use
  * for example dense or sparse matrices. It does **NOT** support the full set of valid LightSVM and libSVM tokens and
  * formats. More specifically it:
  * 1. Does not read the values of a user defined kernel (libSM)
  * 2. Does not read the "qid" ranking restrictions (lightSVM)
  *
  * NOTE: the libSVM scaler cannot handle comments (see [[com.github.cjlin1.SVM.Scale]])
  *
  * Format (were ":" is a token separator):
  * label1 index1_1:value1_1 index1_2:value1_2 ..... # comment
  * label2 index2_1:value1_1 index2_2:value1_2 ..... # comment
  * ...
  * labeln index1_n:value1_1 indexn_2:value1_2 ..... # comment
  *
  * All index_i_j must ne an integral value (index of the data set column). Labels can be: any integral value in case
  * of classinfication of a real number in case of regression.
  *
  * This object also contains a set of implicit loaders that cater for specific algorithm types (classification,
  * regression) and specific data storage types (sparse and dense matrices for example).
  *
  * @see [[http://svmlight.joachims.org/]]
  */
object LightSVM {
  /**
    * A set of functions used to check the value of the label (dependent variable). If the
    * algorithm is for regression no check is done. If it is any form of classification,
    * then it checks that the label is an integral value.
    *
    * @see https://users.scala-lang.org/t/enforcing-type-equality-bound-on-implicit-type-parameter/1524
    * @see https://users.scala-lang.org/t/how-can-we-override-shadow-predef-conforms/1142
    */
  class ValidateDepVar(private val f : (Double) => Either[ADWError, Double])
    extends AnyVal {
    def apply(v: Double) : Either[ADWError, Double] = f(v)
  }

  /**
    * A set of functions used to check the value of the label (dependent variable). If the
    * algorithm is for regression no check is done. If it is any form of classification,
    * then it checks that the label is an integral value.
    *
    * @see https://users.scala-lang.org/t/enforcing-type-equality-bound-on-implicit-type-parameter/1524
    * @see https://users.scala-lang.org/t/how-can-we-override-shadow-predef-conforms/1142
    */
  object ValidateDepVar {

    private def isIntegral(v: Double): Either[ADWError, Double] =
      if (v % 1 == 0)
        Right(v)
      else
        Left(ADWError(s"isValidLabel : Classification - $v is not an integer"))

    /** If we are doing classification the dependent value must be integral */
    val multiClassification: ValidateDepVar = new ValidateDepVar(isIntegral)
    /** If we are doing classification the dependent value must be integral */
    val binaryClassification: ValidateDepVar = new ValidateDepVar(isIntegral)
    /** If we are doing classification the dependent value must be integral */
    val oneClass: ValidateDepVar = new ValidateDepVar(isIntegral)
    /** If we are doing regression, any value is acceptable */
    val regression: ValidateDepVar = new ValidateDepVar(Right.apply)
    val noCheck = new ValidateDepVar(Right.apply)
  }

  /**
    * This class loads a file with sparse matrix data in the light SVM format, also used by the
    * libSVM library. The data collection and storage is left to a specific implementation. Data
    * storage may use for example dense or sparse matrices. It does **NOT** support the full set of
    * valid LightSVM and libSVM tokens and formats. More specifically it:
    * 1. Does not read the values of a user defined kernel (libSM)
    * 2. Does not read the "qid" ranking restrictions (lightSVM)
    *
    * NOTE: the libSVM scaler cannot handle comments (see [[com.github.cjlin1.SVM.Scale]])
    *
    * Format (were ":" is a token separator):
    * label1 index1_1:value1_1 index1_2:value1_2 ..... # comment
    * label2 index2_1:value1_1 index2_2:value1_2 ..... # comment
    * ...
    * labeln index1_n:value1_1 indexn_2:value1_2 ..... # comment
    *
    * All index_i_j must ne an integral value (index of the data set column)
    * Labels can be: any integral value in case of classification ro a real number in case of regression.
    *
    * @see [[http://svmlight.joachims.org/]]
    *
    * @tparam D - [[pt.inescn.models.Base.DataIn]]
    */
  class libSVMFileParser[D <: DataIn[_,_]]
  (val fold : Fold[D])
  {
    type Row = D#Row
    type Acc = fold.Acc

    val delimiters = "\\s+|:"

    /**
      * Parsers the token `e` to a [[scala.Double]]
      * @param e - string token
      * @return
      */
    def parseDouble(e: String): Either[ADWError, Double] = try {
      Right(e.toDouble)
    } catch {
      case ex: java.lang.Exception => Left(ADWError(ex.toString))
    }

    /**
      * Parsers the token `e` to an [[scala.Int]]
      * @param e - string token
      * @return
      */
    def parseInt(e: String): Either[ADWError, Int] = try {
      Right(e.toInt)
    } catch {
      case ex: java.lang.Exception => Left(ADWError(ex.toString))
    }

    /**
      * Utility function that takes in a list of tokens and checks if its is emtpy.
      * If it is empty, then an [[pt.inescn.utils.ADWError]] is returned otherwise the
      * function `f` is applied to those tokens. This function should either return a
      * [[pt.inescn.utils.ADWError]] or it can return any generic type `T`.
      *
      * @param d - List f tokens
      * @param f - Function that processes the list of tokens
      * @tparam T - Result of applying the function to the list of tokens
      * @return - Either a [[pt.inescn.utils.ADWError]] or the generic type `T`
      */
    def emptyOrElse[T](d: List[String])(f : List[String] => Either[ADWError, T]): Either[ADWError, T] =
      if (d.isEmpty) Left(ADWError("No tokens available")) else f(d)

    /**
      * Function is called on the first element of the data set row. This is the
      * dependent variable. It can be +1, -1 or 0 for classification, or a real
      * for regression.
      *
      * @return a label parsed to a real [[scala.Double]]
      */
    def transformLabel(s: String, ev: ValidateDepVar): Either[ADWError, Double] = {
      val tmp = parseDouble(s)
      val ok = tmp.flatMap( ev.apply )
      ok
    }

    /**
      * Function is called on the list of tokens representing the features in sparse format
      * (a column index and its value). The first element is parsed into an integer value.
      * The second is parsed into a real value. If any of these strings cannot be parsed into
      * their numeric values then an error is returned.
      *
      * @param d - list of string tokens
      * @return
      */
    def transformFeatures(d: List[String]): Either[ADWError, (Int, Double)] = {
      emptyOrElse(d) {
        p =>
          if (p.length != 2)
            Left(ADWError(s"Incorrect feature ${p.mkString("[",",","]")}"))
          else {
            for {
              idx <- parseInt(p.head)
              feature <- parseDouble(p(1))
            } yield (idx, feature)
          }
      }
    }

    /**
      * This function takes a list of tokens and splits them into two parts. The first is the
      * label and the seconds is the sparse list of features. The first is converted to a real
      * value. The second set of tokens are paired into a tuple were the first element represents
      * the column index and the second the value at that column. Each of these pairs are then
      * converted into their numeric representation via the [[transformFeatures]] function. If
      * any of the conversions generate an error, return that error otherwise return the parsed
      * data.
      *
      * @param d - list of string tokens
      * @return
      */
    def formatData(d : List[String], ev: ValidateDepVar): Either[ADWError, (Double, Iterator[(Int, Double)])] = {
      if (d.isEmpty)
        Left(ADWError(s"Empty line : $d"))
      else {
        val labelResult: Either[ADWError, Double] = transformLabel(d.head, ev)
        val pairsResult: Iterator[Either[ADWError, (Int, Double)]] = d.tail.sliding(2,2).map( transformFeatures )
        // Split into errors and non-errors
        val tmp = ADWError.disjoint(pairsResult)
        // Get a single combined error (if any exists) or get parsed data
        val tmp1: Either[ADWError, Iterator[(Int, Double)]] = ADWError.flatten(tmp)
        // Either return an error the parsed data
        val result: Either[ADWError, (Double, Iterator[(Int, Double)])] = for {
          label <- labelResult
          pairs <- tmp1
        } yield (label, pairs)
        result
      }
    }

    /**
      * This function takes in a line number and the raw line string. It first checks if
      * any comment exists at the end of the line and if so discards it. The rest of the line
      * is then parsed splitting the line into tokens. The tokens are split on white spaces
      * and the `:` index and value separator. These tokens are then parsed using the [[formatData]]
      * function that identifies and converts the label and features that are sparsely encoded.
      *
      * @param lineNumber - number of the line read from the source
      * @param iline - the raw line that may also contain a comment at the end
      * @return a sparsely encoded data row
      */
    def parseLine(lineNumber: Int, iline: String, ev: ValidateDepVar): Either[ADWError, (Double, Iterator[(Int, Double)])] = {
      val comment = iline.indexOf('#')
      val line = if (comment < 0) iline else iline.substring(0, comment)
      val tokens = line.trim.split(delimiters).map(_.trim).toList
      val parsed: Either[ADWError, (Double, Iterator[(Int, Double)])] = formatData(tokens, ev)
      parsed.fold(
        { err => Left(ADWError(lineNumber + " @ " + err.msg)) },
        { data => Right(data) }
      )
    }

    /**
      * Calls the client to accumulate a successfully parsed line otherwise accumulates the
      * the [[pt.inescn.utils.ADWError]].
      *
      * @param acc - client's accumulator
      * @param line - raw line from the file that will be parsed, converted and then stored
      * @return - Either an [[pt.inescn.utils.ADWError]] or the client's container
      */
    def addLine(acc: (Int, Either[ADWError, Acc]), line: String, ev: ValidateDepVar): (Int, Either[ADWError, Acc]) = {
      val id = acc._1 + 1
      acc._2.fold(
        // Left
        { e => (id,Left(e)) },
        // Right
        { dmap =>
          val record: Either[ADWError, (Double, Iterator[(Int, Double)])] = parseLine(acc._1, line, ev)
          val tmp: Either[ADWError, fold.Acc] = record.flatMap(data => Right(fold.add(id, data, dmap)) )
          val result = tmp.fold( p => (id, Left(p)), p => (id, Right(p)) )
          result
        })
    }

    /**
      * This is essentially a fold function that allows an arbitrary client to collect
      * the parsed data into a final data container. The client must provide an initializer
      * of an accumulator, a function to update the accumulator and a function to finalize
      * and return the daa set in a specific container.
      *
      * This function opens a file and iterates over all of the lines. Each line is parsed,
      * converted and then passed onto the client for processing (accumulation and storage).
      * Note that in the event of parsing or conversion error of a line the [[addLine]] does
      * not call the client accumulate that line. Note also that no early stopping occurs, so
      * all errors are also accumulated and reported at the end.
      *
      * @param f - File that contains the data set
      * @return - Either an [[pt.inescn.utils.ADWError]] or the container with the data set
      */
    def parse(f: File, ev: ValidateDepVar): Either[ADWError, D] = {
      val i = f.lineIterator
      val empty = fold.init()
      val zero : (Int, Either[ADWError, Acc]) = (0, Right[ADWError, Acc](empty))
      val tmp = i.foldLeft(zero) { (acc, pp) => addLine(acc, pp, ev) }
      tmp._2.flatMap( pp => fold.finish(pp) )
    }
  }


  /**
    * A Fold instance that collects [[pt.inescn.models.Data.SparseMatrixData]] rows
    * into a list and then stores that as a [[pt.inescn.models.Data.SparseMatrixData]]
    * typed for [[pt.inescn.models.Base.AlgorithmType]] use.
    *
    * Check if a function that should return true is some validation has failed.
    */
  class SparseFold(check : SparseMatrixData => Boolean) extends Fold[SparseMatrixData] {
    override type Acc = List[SparseMatrixData#Row]

    override def init(): Acc = List[SparseMatrixData#Row]()
    override def add(id: Int, data: (Double, Iterator[(Int, Double)]), mp: Acc): Acc = {
      //val tmp = data._2.map( p => (p._1,p._2)).toMap
      // make sure the keys are ordered. Some formats (LightSVM) assumes this
      val tmp = TreeMap( data._2.map( p => (p._1,p._2)).toSeq :_* )
      val tuple = (data._1, tmp)
      tuple :: mp
    }
    override def finish(zero: Acc):  Either[ADWError, SparseMatrixData] = {
      val tmp = new SparseMatrixData(zero.toArray)
      if (check(tmp))
        Left(ADWError(s"Incorrect number of classes found: ${tmp.numLabels}"))
      else
        Right(tmp)
    }
  }

  /**
    * Tests to check if input data is ok. Currently only checks the labels.
    *
    * TODO: we have an issue here. In most cases we can check if the full data
    * set is ok. For example we assume that binary classification requires only
    * two labels.
    *
    * TODO: regression - test for columns with near zero variability?
    * TODO: add general tests in transform and decouple from load ?
    * TODO: allow the combination of single and aggregate checks to be executed
    * at the same time?
    */
  object SparseFold {
    def neverFail(p : SparseMatrixData) : Boolean = false
    def binaryClassFail(p : SparseMatrixData) : Boolean = p.numLabels <= 1 || p.numLabels > 2
    def multiClassFail(p : SparseMatrixData) : Boolean = {
      val (lines, _) = p.size
      val percent = p.numLabels / lines.toDouble
      if (percent >= 0.5) true else false
    }
    def oneClassTrain(p : SparseMatrixData) : Boolean = p.numLabels != 1
    def regressionFail(p : SparseMatrixData) : Boolean = ! multiClassFail(p)
  }

  trait SVMSource[I,O] extends Source[I,O]

  /**
    * This class implements a lightSVM/libSVM data set loader. It loads the
    * [[pt.inescn.models.Base.AlgorithmType.BinaryClassification]] data into a
    * [[pt.inescn.models.Data.SparseMatrixData]] container. Specifically, this class
    * provides the functions to initialize the accumulator, update the accumulator and
    * finally convert the accumulator into the data set container. This is an implicit
    * class that allows users to simply specify the [[pt.inescn.models.Base.AlgorithmType]]
    * such as [[pt.inescn.models.Base.AlgorithmType.BinaryClassification]] or
    * [[pt.inescn.models.Base.AlgorithmType.MultiClassification]] and the container type
    * such as [[pt.inescn.models.Data.SparseMatrixData]] or [[pt.inescn.models.Data.DenseMatrixData]].
    *
    * Note: checks that the number of labels is valid
    */
  case class libSVMSparseFile(ev: ValidateDepVar, check : SparseMatrixData => Boolean) extends SVMSource[File, SparseMatrixData] {
    val folder: SparseFold = new SparseFold(check)
    val svmLoader: libSVMFileParser[SparseMatrixData] = new libSVMFileParser[SparseMatrixData](folder)

    /** Processes input */
    override def map(i: File): Either[ADWError, SparseMatrixData] = svmLoader.parse(i, ev)
  }

}
