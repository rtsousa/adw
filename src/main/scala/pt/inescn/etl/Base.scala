package pt.inescn.etl

import pt.inescn.utils.ADWError

object Base {

  /**
    * This expresses the state of a pre-processing step. This allows us to ensure that we
    * ensure that the data has correctly processed before the training. It can also allow us
    * to ensure that an estimator (algorithm) can only execute the fitting function on
    * scaled data.
    */
  sealed trait ScalerState
  object ScalerState {
    /** No scaling performed yet */
    sealed trait Unscaled extends ScalerState
    /** Scaling done and the parameters have been stored (can be used for training) */
    sealed trait RangeRecorded extends ScalerState
    /** Scaling done and used (can be used again for another training session) */
    sealed trait RangeUsed extends ScalerState
  }

  /**
    * Generic operation that processes input data `I` and produces output `O`
    * @tparam I - Input
    * @tparam O - Output
    */
  trait Transfer[I, O] {

    /** Processes input */
    //def map(i: I, ev: ValidateDepVar): Either[ADWError, O]
    def map(i: I): Either[ADWError, O]

    def flatMap[OO](t: Transfer[O, OO]): (I) => Either[ADWError, OO] = (i: I) => {
      val tmp = map(i)
      tmp.flatMap(o => t.map(o))
    }
  }

  // TODO: how can we make Algorithm an Estimator?
  /**
    * Generic type that tags an operation that will generate a metric.
    *
    * @tparam I - Input
    * @tparam O - Output
    */
  trait Estimator[I, O] extends Transfer[I,O]

  /**
    * Generic operation that is used to load data from inout `I` and places it in the format `O`.
    * May also be used to: load files of different formats, convert data between formats and also
    * generate sub-sets of data (for cross validation for example).
    *
    * @tparam I - Input
    * @tparam O - Output
    */
  trait Source[I, O] extends Transfer[I, O]


}
