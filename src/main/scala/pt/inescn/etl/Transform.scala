/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.etl

import org.hipparchus.transform.{DftNormalization, FastFourierTransformer, TransformType}
import pt.inescn.dsp.Fourier.truncatePowerOf2
import pt.inescn.etl.Save.saveLine

import scala.collection.mutable.{ArrayBuffer, ListBuffer}
import pt.inescn.etl.Load.TimeSeriesFrame3
import pt.inescn.etl.Load._
import better.files.Dsl._
import kantan.csv.generic._
import org.hipparchus.stat.regression.SimpleRegression
import org.hipparchus.util.FastMath
import pt.inescn.etl.Load.TimeSeriesFrame3
import pt.inescn.models.Base.Data


/**
  * Function to perform transformation on the loaded data.
  *
  * @see [[pt.inescn.models.Data.SparseMatrixData]] [[pt.inescn.models.Data.DenseMatrixData]]
  */
object Transform {

  def fftAbs(fileName: String, fft: FastFourierTransformer, data : Array[Double]) : Array[Double] = {
    val len = data.length
    println(len + " -> " + truncatePowerOf2(len))
    val dataToUse = data.take(truncatePowerOf2(len))
    val result = fft.transform(dataToUse, TransformType.FORWARD).map(_.abs())
    saveLine(fileName, result mkString ", ")
    result
  }

  def fftWindow(fileName: String, fft: FastFourierTransformer, windowSize: Int, stepSize: Int, data : Array[Double]) : Unit = {
    val windows = data.sliding(windowSize, stepSize).toList
    val result = new ListBuffer[Array[Double]]()
    for(window <- windows) {
      result += fftAbs(fileName,fft,window)
    }
  }

  def createFFT(dataName: String, windowSize: Int, stepSize: Int): Unit ={
    val data = cwd / dataName
    val rr = TimeSeriesFrame3().load(data)
    val d = rr.getOrElse(TimeSeriesFrame3())
    val fft = new FastFourierTransformer(DftNormalization.STANDARD)

    fftWindow("/data/inegi/fft_x.csv",fft,windowSize,stepSize,d.value1.toArray)
    fftWindow("/data/inegi/fft_y.csv",fft,windowSize,stepSize,d.value2.toArray)
    fftWindow("/data/inegi/fft_z.csv",fft,windowSize,stepSize,d.value3.toArray)
  }

  // TODO: test fails
  def denseToSparse(mat0: Array[Array[Double]]): Array[(Double, Map[Int, Double])] ={
    var mat = new ArrayBuffer[(Double, Map[Int, Double])]
    for(r_idx <- mat0.indices){
      var ms = Map.empty[Int,Double]
      for(c_idx <- mat0(r_idx).indices){
        ms += (c_idx -> mat0(r_idx)(c_idx))
      }
      mat = mat ++ ArrayBuffer((r_idx.toDouble, ms))
    }
    mat.toArray
  }

  // Generic stuff
  // http://sebastianraschka.com/Articles/2014_about_feature_scaling.html
  // TODO: separate queries from transforms
  // TODO: standardization
  // TODO: min-max scale
  def maxOfCol(acc: Double, value: Double): Double = Math.max(acc, value)
  def minOfCol(acc: Double, value: Double): Double = Math.min(acc, value)
  def minMaxOfCol( acc : (Double,Double), value: Double): (Double, Double) = (minOfCol(acc._1, value), maxOfCol(acc._2,value))

  // org.hipparchus.stat.descriptive.DescriptiveStatistics
  // org.hipparchus.stat.descriptive.moment.Mean
  // org.hipparchus.stat.descriptive.moment.Variance
  // private static final UnivariateStatistic POPULATION_VARIANCE = new Variance(false);
  // org.apache.commons.math.stat.descriptive.moment.StandardDeviation
  // org.hipparchus.stat.descriptive.moment.GeometricMean
  // org.hipparchus.stat.descriptive.moment.Kurtosis
  // org.hipparchus.stat.descriptive.moment.Skewness
  // org.hipparchus.stat.descriptive.rank.Max
  // org.hipparchus.stat.descriptive.rank.Min
  // org.hipparchus.stat.descriptive.rank.Percentile
  // org.hipparchus.stat.descriptive.summary.Sum
  // org.hipparchus.stat.descriptive.summary.SumOfSquares
  def sum(acc: Double, value: Double): Double = acc + value
  def sumOfDiff(toSubtract: Double)(acc: Double, value: Double): Double = acc + (value - toSubtract)
  def sumOfDiffX[O](toSubtract: O)(acc: O, value: O)(implicit ev1: Numeric[O]): O = {
    import ev1._
    acc + (value - toSubtract)
  }
  def sumX[O](acc: O, value: O)(implicit ev1: Numeric[O]): O = {
    import ev1._
    acc + value
  }

  def scale(lower:Double, upper:Double)(min: Double, max: Double): Double => Double = {
    val max_minus_min = max - min
    val range = upper - lower

    (x:Double) => {
      if (x == min)
        lower
      else if (x == max)
        upper
      else
        lower + (((x - min)*range)/max_minus_min)
    } : Double
  }

  def mean(sum: Double, N: Double, correction: Double): Double = {
    val xbar = sum / N
    xbar + (correction / N)
  }

  def normalize(mean: Double, standard_deviation: Double): Double => Double = {

    (x:Double) => {
      (x - mean)/standard_deviation
    } : Double
  }

  /*
    Computes the variance of the available values. By default, the unbiased "sample variance" definitional formula is
    used:
      variance = sum((x_i - mean)^2) / (n - 1)
    where mean is the Mean and n is the number of sample observations.
    The definitional formula does not have good numerical properties, so this implementation does not compute the
    statistic using the definitional formula.

    The getResult method computes the variance using updating formulas based on West's algorithm, as described in
    Chan, T. F. and J. G. Lewis 1979, Communications of the ACM, vol. 22 no. 9, pp. 526-531.
    The evaluate methods leverage the fact that they have the full array of values in memory to execute a two-pass
    algorithm. Specifically, these methods use the "corrected two-pass algorithm" from Chan, Golub, Levesque,
    Algorithms for Computing the Sample Variance, American Statistician, August 1983.

    Note that adding values using increment or incrementAll and then executing getResult will sometimes give a
    different, less accurate, result than executing evaluate with the full array of values. The former approach should
    only be used when the full array of values is not available.
    The "population variance" ( sum((x_i - mean)^2) / n ) can also be computed using this statistic. The
    isBiasCorrected property determines whether the "population" or "sample" value is returned by the evaluate and
    getResult methods. To compute population variances, set this property to false.
   */
  def varDiffs(mean:Double)(acc: (Double,Double), value: Double): (Double, Double) = {
    val dev = value - mean
    val accum = acc._1 + (dev * dev)
    val accum2 = acc._2 + dev
    (accum, accum2)
  }

  // Source dependent stuff

  /*
    "Comparison of Several Algorithms for Computing Sample Means and Variances," Robert F. Ling,
    Journal of the American Statistical Association, Vol. 69, No. 348 (Dec., 1974), pp. 859-866.
    see  org.apache.commons.math.stat.descriptive.moment.Mean

    TODO: we should support multi-phase applications to the column
    Have something like an andThen: sum andThen calcMean andThen sumOfDiff and Then correctedMean
    TODO: generalize to any Data[Label,Value]
   */
  def mean[D <: Data[Double,Double]](col: Int, data: D): Double = {
    val colSum  = data.apply(col, 0.0, sum _ )
    val n = data.size._1
    val mean = colSum / n
    // mean deviation
    val correction = data.apply(col, 0.0, sumOfDiff(mean) _ )
    mean + (correction/n)
  }
  def meanX[O,D <: Data[Double,O]](col: Int, data: D)(implicit ev1: Fractional[O]): O = {
    import ev1._

    val zero = ev1.zero
    val func: (O, O) => O = (a:O,b:O) => sumX(a,b)(ev1)
    val colSum  = data.apply(col, zero, func )
    val n = ev1.fromInt(data.size._1)
    val mean = colSum / n
    // mean deviation
    val correction = data.apply(col, zero, sumOfDiffX(mean) _ )
    mean + (correction/n)
  }

  def mean[D <: Array[Double]](data: D): Double = {
    val colSum  = data.sum
    val n = data.length
    val mean = colSum / n
    // mean deviation
    //val correction = data.map( _ - mean).sum
    val correction = data.foldLeft(0.0)(sumOfDiff(mean) )
    mean + (correction/n)
  }

  /**
    * TODO: generalize to any Data[Label,Value].
    *
    * @see [[varDiffs]]
    * @param col
    * @param data
    * @param isBiasCorrected
    * @return
    */
  def variance[O <: Data[Double,Double]](col: Int, data: O, isBiasCorrected : Boolean): Double = {
    val len = data.size._1
    if (len == 1)
      0.0
    else {
      val m = mean(col, data)
      val acc = data.apply(col, (0.0,0.0), varDiffs(m) _ )
      val (accum, accum2) = acc
      if (isBiasCorrected) {
        (accum - (accum2 * accum2 / len)) / (len - 1.0)
      } else {
        (accum - (accum2 * accum2 / len)) / len
      }
    }
  }

  def variance[O <: Array[Double]](data: O, isBiasCorrected : Boolean): Double = {
    val len = data.length
    if (len == 1)
      0.0
    else {
      val m = mean(data)
      val acc = data.foldLeft((0.0,0.0))(varDiffs(m) )
      val (accum, accum2) = acc
      if (isBiasCorrected) {
        (accum - (accum2 * accum2 / len)) / (len - 1.0)
      } else {
        (accum - (accum2 * accum2 / len)) / len
      }
    }
  }

  // TODO: generalize to any Data[Label,Value]
  def populationVariance[O <: Data[Double,Double]](col: Int, data: O): Double = variance(col, data, isBiasCorrected = false)

  // TODO: generalize to any Data[Label,Value]
  def standardDeviation[O <: Data[Double,Double]](col: Int, data: O): Double = FastMath.sqrt(variance(col, data, isBiasCorrected = true))

  def populationVariance[O <: Array[Double]](data: O): Double = variance(data, isBiasCorrected = false)
  def standardDeviation[O <: Array[Double]](data: O): Double = FastMath.sqrt(variance(data, isBiasCorrected = true))

  sealed trait TransformParams
  case class NOP() extends TransformParams
  case class Scale(lower: Double, upper: Double, scaleY: Boolean = false, min_max: IndexedSeq[(Double,Double)] = Vector()) extends TransformParams
  case class Normalize(scaleY: Boolean = false, means: IndexedSeq[Double] =  Vector(), sds: IndexedSeq[Double] = Vector()) extends TransformParams
  case class TransformPipe(pipe:Seq[TransformParams]) extends TransformParams

  def init(transform: TransformParams, mat: Data[Double,Double]) : TransformParams = {
    transform match {
      case s@Scale(lower, upper, scaleY, _) => scale(lower, upper, mat, scaleY)
      case n@Normalize(scaleY, _, _) => normalize(mat, scaleY)
      case TransformPipe(l) => TransformPipe( l.map( e => init(e,mat) ) )
      case NOP() => NOP()
    }
  }

  def exec(transform: TransformParams, mat: Data[Double,Double]) : Unit = {
    transform match {
      case s@Scale(_, _, _, _) => scale(s, mat)
      case n@Normalize(_, _, _) => normalize(n, mat)
      case TransformPipe(l) => l.foreach( e => exec(e,mat) )
      case NOP() => NOP()
    }
  }


  // TODO: make parallel versions
  // TODO: generalize to any Data[Label,Value].
  // Get the min and maximum for each column
  def scale(params:Scale, mat: Data[Double,Double]) : Unit = {
    val lower = params.lower
    val upper = params.upper
    val min_max = params.min_max
    val scaleY = params.scaleY
    val (_,ncols) = mat.size
    // scale y column also?
    val start = if (scaleY) 0 else 1
    // columns we will scale
    val cols = start until ncols

    // Set the limits of the scaling functions
    val range_scale = Transform.scale(lower,upper) _
    // Generate a scaling function for each column
    val scales: IndexedSeq[Double => Double] = min_max.map { range =>
      val (min,max) = range
      range_scale(min,max)
    }
    // Now apply the transformation for each columns
    cols.foreach{ col => mat.apply(col, if (scaleY) scales(col) else scales(col-1) ) }
  }

  // TODO: make parallel versions
  // TODO: generalize to any Data[Label,Value].
  def scale(lower:Double, upper:Double, mat: Data[Double,Double], scaleY : Boolean = false): Scale = {
    val (_,ncols) = mat.size
    // scale y column also?
    val start = if (scaleY) 0 else 1
    // columns we will scale
    val cols = start until ncols
    val zero = (Double.MaxValue, Double.MinValue)
    // Get the min and maximum for each column
    val min_max = cols.map( mat.apply(_,zero,Transform.minMaxOfCol _))

    // Do actual scaling
    val params = Scale(lower, upper, scaleY, min_max)
    scale(params, mat)
    // Record scaling parameters for later use
    params
  }

  def normalize[O <: Data[Double,Double]](norm : Normalize, mat: O): Unit  = {
    val (nrows,ncols) = mat.size
    // scale y column also?
    val start = if (norm.scaleY) 0 else 1
    // columns we will scale
    val cols = start until ncols

    // Get the means for each column
    val means = norm.means
    val sds = norm.sds
    // Generate a normalizing function for each column
    val norms: IndexedSeq[Double => Double] = means.zip(sds).map { case (m,sd) => normalize(m,sd) }
    // Now apply the transformation for each columns
    cols.foreach{ col => mat.apply(col, if (norm.scaleY) norms(col) else norms(col-1) ) }
  }

  // TODO: make parallel versions
  // TODO: generalize to any Data[Label,Value].
  def normalize[O <: Data[Double,Double]](mat: O, scaleY : Boolean): Normalize = {
    val (nrows,ncols) = mat.size
    // scale y column also?
    val start = if (scaleY) 0 else 1
    // columns we will scale
    val cols = start until ncols

    // Get the means for each column
    val means: IndexedSeq[Double] = cols.map(col => mean(col, mat) )
    val sds: IndexedSeq[Double] = cols.map(col => standardDeviation(col, mat) )
    val norm = Normalize(scaleY, means, sds)

    // Do actual normalization
    normalize(norm, mat)
    // Record normalizing parameters for later use
    norm
  }

  /**
    * Returns a copy of the `vec` values normalized to within
    * one standard deviation.
    *
    * @param vec vector to normalize
    * @return values normalized to within one standard deviation.
    */
  def normalize(vec: Array[Double]): Array[Double] = {
    // Get the means for each column
    val meanv = mean(vec)
    val sdv = standardDeviation(vec)
    val norm = normalize(meanv,sdv)

    // Do actual normalization
    vec.map( norm )
  }

  /**
    * Returns a copy of the `vec` values scaled to a range
    * within `lower` and `upper`.
    *
    * @param lower minimum value of range
    * @param upper maximum value of range
    * @param vec vector to scale
    * @return copy of the `vec` values scaled to a range
    * within `lower` and `upper`.
    */
  def scale(lower:Double, upper:Double, vec: Array[Double]): Array[Double] = {
    val zero = (Double.MaxValue, Double.MinValue)
    // Get the min and maximum for each column
    val (minv, maxv) = vec.foldLeft(zero)(Transform.minMaxOfCol)
    // Prepare scaling function
    val range_scale = Transform.scale(lower,upper)(minv,maxv)

    // Do actual scaling
    vec.map( range_scale )
  }

  /**
    * Returns a copy of the `vec` values scaled to a unit vector.
    *
    * @param vec vector to scale
    * @return a copy of the `vec` values scaled to a unit vector.
    */
  def unit(vec: Array[Double]): Array[Double] = {
    val sumSquares = vec.map(e => e*e).sum
    val denominator = Math.sqrt(sumSquares)

    // Do actual scaling
    vec.map( _ / denominator )
  }

  /**
    * @see https://en.wikipedia.org/wiki/Softmax_function
    * @param vec vector to scale
    * @return a copy of the `vec` values scaled by the softmax
    */
  def softmax(vec: Array[Double]): Array[Double] = {
    val exps = vec.map(Math.exp)
    val sumExps = exps.sum

    // Do actual scaling
    exps.map( _ / sumExps )
  }

  /**
    * Book "Data Mining with R, learning with case studies" by Luis Torgo, CRC Press 2010.
    *   vt <- (x - avg)/(lambda * (std/(2 * pi)))
    *   1/(1 + exp(-vt))
    * @see https://www.rdocumentation.org/packages/DMwR/versions/0.4.1/topics/SoftMax
    *
    * @param vec vector to scale
    * @param lambda
    * @return a copy of the `vec` values scaled by a parametrized softmax
    */
  def softMax(vec: Array[Double], lambda:Double=2.0): Array[Double] = {
    val avg = mean(vec)
    val std = standardDeviation(vec)
    def vt(x:Double) = (x - avg)/(lambda * (std/(2.0 * Math.PI)))
    //vec.map(x => 1.0 / (1.0 + Math.exp( -((x - avg) / (lambda*std/2*Math.PI) ))) )
    vec.map(x => 1.0/(1.0 + Math.exp(-vt(x))) )
  }

  /**
    * @see https://en.wikipedia.org/wiki/Logistic_function
    *
    * @param l upper asymptote (symmetrical at x=0)
    * @param k slope of the curve at zero
    * @param zero y crossing at x=0
    * @param vec vector to scale
    * @return a copy of the `vec` values scaled by the logistic function.
    */
  def logit(vec: Array[Double], l:Double=1.0, k:Double=1.0, zero:Double=0.0): Array[Double] = {
    vec.map(x => l / (1 + Math.exp(-k*(x - zero))) )
  }

  // TODO: https://stackoverflow.com/questions/15215457/standardize-data-columns-in-r
  // R: data.Normalization function in clusterSim package

  // TODO: https://dzone.com/articles/handling-character-data-for-machine-learning?edition=338998&utm_source=Zone%20Newsletter&utm_medium=email&utm_campaign=ai%202017-11-29
  // http://www.willmcginnis.com/2015/11/29/beyond-one-hot-an-exploration-of-categorical-variables/
  // http://www.statsmodels.org/devel/contrasts.html


  /* Detrend */
  /* Linear */

  /**
    * Linear detrending using linear regression. Determine
    * the line that fits closest to the data and remove
    * that linear signal. Note that it is important to ensure
    * that the trend is in fact linear.
    *
    * Note that any other curve fitting and prediction models
    * may also be used. Issue is when the trends change
    * (change point detection?).
    *
    * @see https://machinelearningmastery.com/time-series-trends-in-python/
    *      https://en.wikipedia.org/wiki/Linear_trend_estimation
    *
    *
    * @param x domain of the signal
    * @param y co-domain of the signal
    * @return return the detrended co-domain
    */
  def regressionDetrend(x: Seq[Double], y: Seq[Double]): Seq[Double] = {
    val xy = x.zip(y)

    // linear regression
    val regression = new SimpleRegression()
    xy.foreach(p => regression.addData(p._1, p._2))

    val slope = regression.getSlope
    val intercept = regression.getIntercept

    // y' = y - (x*m + c)
    xy.map( p => p._2 - (p._1*slope + intercept))
  }

  /**
    * Linear detrending using simple difference. Simply
    * calculate the difference between every two consecutive
    * samples. Note that it is important to ensure that the
    * trend is in fact linear.
    *
    * This methods exacerbates the noise in the signal.
    * May require additional filtering.
    *
    * Note that any other curve fitting and prediction models
    * may also be used. Issue is when the trends change
    * (change point detection?).
    *
    * @see https://machinelearningmastery.com/time-series-trends-in-python/
    *      https://en.wikipedia.org/wiki/Linear_trend_estimation
    *
    *
    * @param x domain of the signal
    * @param y co-domain of the signal
    * @return return the detrended co-domain
    */
  def differenceDetrend(x: Seq[Double], y: Seq[Double]): Seq[Double] = {
    y.sliding(2).map( e => e(1) - e.head).toSeq
  }

}
