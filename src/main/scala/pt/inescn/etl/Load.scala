/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.etl

import java.time.Duration

import better.files.File
import pt.inescn.models.Base.DataIn
import pt.inescn.models.Data.SparseMatrixData
import pt.inescn.utils.ADWError

import scala.collection.mutable.ArrayBuffer
import scala.util.Try

import better.files._
import better.files.Dsl._

/**
  * Contains the code that allows us to load and process CSV data. We assume the first
  * column has a time-stamp /(up to nanosecond precision) and the rest of the columns
  * are numerical.
  *
  * Contains code to load the data sets in LightSVM/LibSVM into sparse matrix format.
  *
  * Created by hmf on 07-06-2017.
  */
object Load {

  trait Fold[D <: DataIn[_,_]] {
    type Acc

    def init() : Acc
    def add(id: Int, data: (Double, Iterator[(Int, Double)]), mp: Acc): Acc
    def finish(zero: Acc) :  Either[ADWError, D]
  }


  // TODO: adapt existing CSV loader to use circe and/opr Saddle
  // TODO: circe and Saddle must be able to handle nano-seconds
  // TODO: change TimeSeries to have DenseMatrix loader (need to convert the time-stamp to a single numeric offset)

  /**
    * Aggregates all of the loaders so that they can be identified via implicit resolution
    */
  object Source {

    import LightSVM._

    // http://blog.bruchez.name/2015/11/generalized-type-constraints-in-scala.html
    // https://stackoverflow.com/questions/27800502/error-higher-kinded-types-scala-type-arguments-do-not-conform-type-ts-bounds
    // https://stackoverflow.com/questions/12863331/matching-type-constraints-in-scala-fails
    // https://stackoverflow.com/questions/32636339/scala-error-type-arguments-do-not-conform-to-class-type-parameter-bounds
    def libSVMSparse(ev: ValidateDepVar, check : SparseMatrixData => Boolean): SVMSource[File, SparseMatrixData] = libSVMSparseFile(ev,check)

  }



  //import org.threeten.extra.Interval

  import java.time.Instant
  import kantan.csv._
  //import kantan.csv.ops._
  import kantan.csv.java8._
  import java.time.format.DateTimeFormatter
  import java.time.ZoneOffset

  /*
   *  Implicit date parser for Kantan CSV library: we must provide the correct format to parse
   *  the date-time stamps found in the data and result files. Note that in these files the
   *  time-stamps have only precision up to the second. The result (JSON) labels however use
   *  microseconds. We have opted to allow for microsecond precision -  however we do not require
   *  the input to have full precision (see [[java.time.format.DateTimeFormatterBuilder.appendFraction]])
   */

  /*
   * Make sure we can parser the dates in the data files with second precision
   */
  val instantPattern = "yyyy-MM-dd HH:mm:ss" // Data files

  // Original decoder that used only second precision.
  //val format = DateTimeFormatter.ofPattern( instantPattern ).withZone( ZoneOffset.UTC )
  //implicit val decoder: CellDecoder[ Instant ] = instantDecoder( format )

  /**
    * We use Kantan Scala library to parse CSV. It allows us to use decoders and encoders to
    * provide type-safety coding. We have opted to use the Jackson back-end which is also used for
    * the JSON parsing. This format is used to define a Kantan decoder that is the activated implicitly.
    * Note that we use  `appendFraction` to enable optional microsecond precision.
    *
    * @see https://nrinaudo.github.io/kantan.csv/
    * @see https://github.com/nrinaudo/kantan.csv
    * @see https://github.com/FasterXML/jackson
    * @see https://github.com/FasterXML/jackson-dataformat-csv
    * @see https://github.com/FasterXML/jackson-dataformats-text
    * @see see [[https://docs.oracle.com/javase/8/docs/api/java/time/format/DateTimeFormatterBuilder.html#appendFraction-java.time.temporal.TemporalField-int-int-boolean- java.time.format.DateTimeFormatterBuilder.appendFraction]]
    * @see http://stackoverflow.com/questions/30103167/jsr-310-parsing-seconds-fraction-with-variable-length
    * @see https://github.com/uniVocity/csv-parsers-comparison
    * @see http://stackoverflow.com/questions/17126365/strongly-typed-access-to-csv-in-scala
    * @see https://github.com/marklister/product-collections
    */
  val format: DateTimeFormatter = new java.time.format.DateTimeFormatterBuilder()
    .appendPattern(instantPattern)
    .appendFraction(java.time.temporal.ChronoField.MICRO_OF_SECOND, 0, 6, true)
    .toFormatter()
    .withZone(ZoneOffset.UTC)
  implicit val decoder: CellDecoder[Instant] = instantDecoder(format)

  import kantan.csv.ops._
  import kantan.csv.generic._


  sealed trait TimeSeriesRow[+T] {
    val dt: java.time.Instant

    def value: T
  }


  case class TimeSeriesRow1(override val dt: Instant, value1: Double) extends TimeSeriesRow[(Instant, Double)] {
    type Row = (Instant, Double)

    override def value: Row = (dt, value1)
  }

  case class TimeSeriesRow2(override val dt: Instant, value1: Double, value2: Double) extends TimeSeriesRow[(Instant, Double, Double)] {
    type Row = (Instant, Double, Double)

    override def value: Row = (dt, value1, value2)
  }

  case class TimeSeriesRow3(override val dt: Instant, value1: Double, value2: Double, value3: Double) extends TimeSeriesRow[(Instant, Double, Double, Double)] {
    type Row = (Instant, Double, Double, Double)

    override def value: Row = (dt, value1, value2, value3)
  }

  case class TimeSeriesRow4(override val dt: Instant, value1: Double, value2: Double, value3: Double, value4: Double) extends TimeSeriesRow[(Instant, Double, Double, Double, Double)] {
    type Row = (Instant, Double, Double, Double, Double)

    override def value: Row = (dt, value1, value2, value3, value4)
  }


  /**
    * Represents a data frame that processes a given row of data from the CSV file. We use Kantan
    * implicits to find and load the approriate data type. When required, just define a new row,
    * frame and relevant operations (see for example [[pt.inescn.features.Stats]]).
    *
    * @tparam T - Row described as a tuple of types
    * @tparam S - F-Bounded type to ensure we return the specific data-frame
    * @see https://tpolecat.github.io/2015/04/29/f-bounds.html
    * @see https://stackoverflow.com/questions/5387602/subclasses-and-return-types
    * @see https://stackoverflow.com/questions/37418436/generic-way-of-reading-csv-of-class-hierarchy-in-scala
    */
  trait TimeSeriesFrame[T, S <: TimeSeriesFrame[T, S]] {
    this: S =>
    def dt: Seq[java.time.Instant]

    def add(e: TimeSeriesRow[T]): S

    def reverse: S


    /**
      * This is a helper function that converts the `kantan.csv.ReadResult`(Success or Failure) to
      * a `Either[ List[ Throwable ], B ]`. This allows us to collect the Kantan read failures that
      * store an Exception and report those at the end. Specialized function will also be provided to
      * deal with the case of `Success`.
      */
    def conv[B](x: Throwable): Either[List[Throwable], B] = Left(List(x))


    /**
      * This s a generic (polymorphic) function that allows us read a [[better.files.File]], parse its contents
      * using the Kasntan CSV parser. The parser uses the `RowDecoder[ A ]` that is implicitly passed onto it.
      * The parser returns a list of roes that were either successfully (contain data) or unsuccessfully read
      * (contains a Throwable).  If the data row was unsuccessfully read then we collect these as a list of
      * `Left[ List[ Throwable ]]`. If the data is successfully read then it is collected as a `Right[ B ]`
      * where be can be for example a `TimeFrame`. These conversions to columns are done by the `toColumns`
      * function parameter.
      *
      * Note that is the CSV file cannot be read and parsed then the Exception is converted (via
      * `conv` utility function) to a `Left[ List[ Throwable ]]` type.
      *
      */
    def load(file: File)(implicit dt: RowDecoder[TimeSeriesRow[T]]): Either[List[Throwable], S] = {
      import kantan.csv._
      import kantan.csv.ops._

      val reader = Try {
        val rawData = file.toJava
        rawData.asCsvReader[TimeSeriesRow[T]](rfc.withHeader)
      }

      reader.fold(
        { rr => conv(rr) }, { rr =>
          //val z: (f.S, List[Throwable]) = (f, List[Throwable]())
          val z = (this, List[Throwable]())
          val tmp = rr.foldLeft(z) {
            case ((acc, el), kantan.codecs.Result.Success(e)) => (acc.add(e), el)
            case ((acc, el), kantan.codecs.Result.Failure(e)) => (acc, e :: el)
          }
          if (tmp._2.nonEmpty) Left(tmp._2.reverse) else Right(tmp._1.reverse)
        })
    }

    /**
      * We use streams so that the check stops at the first failure
      *
      * @return True if the timestamps are ordered otherwise false
      */
    lazy val isOrdered: Boolean = {
      val ss = dt.toStream
      ss.zip(ss.tail).forall(x => x._1.isBefore(x._2))
    }

    /**
      * Checks that the time-stamps all have the same sampling rate.
      *
      * @return
      */
    lazy val hasSameSamplingRate: Boolean = {
      val ss = dt.toStream
      val t2 = ss(1)
      val t1 = ss.head
      val delta = Duration.between(t1, t2).toNanos
      if (delta < 0)
        false
      else
        ss.zip(ss.tail).forall { x =>
          val tt = Duration.between(x._1, x._2).toNanos
          tt == delta
        }
    }

    /**
      * Returns the list of differences between the consecutive time-stamps
      *
      * @return Returns a stream of durations. These can be converted into nanoseconds
      *         for example.
      * @see [[https://docs.oracle.com/javase/8/docs/api/java/time/Duration.html java.time.Duration]]
      */
    lazy val delta: Stream[Duration] = {
      val ss = dt.toStream
      ss.zip(ss.tail).map { x => Duration.between(x._1, x._2) }
    }

  }

  case class TimeSeriesFrame1(override val dt: List[Instant] = List[Instant](), value1: List[Double] = List[Double]()) extends TimeSeriesFrame[TimeSeriesRow1#Row, TimeSeriesFrame1] {
    //type S = G1F

    override def add(e: TimeSeriesRow[TimeSeriesRow1#Row]): TimeSeriesFrame1 = {
      val v = e.value
      TimeSeriesFrame1(v._1 :: dt, v._2 :: value1)
    }

    override def reverse: TimeSeriesFrame1 = TimeSeriesFrame1(dt.reverse, value1.reverse)
  }

  case class TimeSeriesFrame2(override val dt: List[Instant] = List[Instant](), value1: List[Double] = List[Double](), value2: List[Double] = List[Double]()) extends TimeSeriesFrame[TimeSeriesRow2#Row, TimeSeriesFrame2] {
    override def add(e: TimeSeriesRow[TimeSeriesRow2#Row]): TimeSeriesFrame2 = {
      val v = e.value
      TimeSeriesFrame2(v._1 :: dt, v._2 :: value1, v._3 :: value2)
    }

    override def reverse: TimeSeriesFrame2 = TimeSeriesFrame2(dt.reverse, value1.reverse, value2.reverse)
  }

  case class TimeSeriesFrame3(override val dt: List[Instant] = List[Instant](), value1: List[Double] = List(), value2: List[Double] = List(), value3: List[Double] = List()) extends TimeSeriesFrame[TimeSeriesRow3#Row, TimeSeriesFrame3] {
    override def add(e: TimeSeriesRow[TimeSeriesRow3#Row]): TimeSeriesFrame3 = {
      val v = e.value
      TimeSeriesFrame3(v._1 :: dt, v._2 :: value1, v._3 :: value2, v._4 :: value3)
    }

    override def reverse: TimeSeriesFrame3 = TimeSeriesFrame3(dt.reverse, value1.reverse, value2.reverse, value3.reverse)
  }

  case class TimeSeriesFrame4(override val dt: List[Instant] = List[Instant](), value1: List[Double] = List(), value2: List[Double] = List(), value3: List[Double] = List(), value4: List[Double] = List()) extends TimeSeriesFrame[TimeSeriesRow4#Row, TimeSeriesFrame4] {
    override def add(e: TimeSeriesRow[TimeSeriesRow4#Row]): TimeSeriesFrame4 = {
      val v = e.value
      TimeSeriesFrame4(v._1 :: dt, v._2 :: value1, v._3 :: value2, v._4 :: value3, v._5 :: value4)
    }

    override def reverse: TimeSeriesFrame4 = TimeSeriesFrame4(dt.reverse, value1.reverse, value2.reverse, value3.reverse, value4.reverse)
  }

  import scala.language.reflectiveCalls

  def using[A <: { def close(): Unit }, B](resource: A)(f: A => B): B =
    try {
      f(resource)
    } finally {
      resource.close()
    }

  def readDense(filePath: String): Array[Array[Double]] ={
    val rows = ArrayBuffer[Array[Double]]()
    var size = 0
    using(scala.io.Source.fromFile(cwd+filePath)) { source =>
      for (line <- source.getLines) {
        var this_row = line.split(",").map(_.trim).map(x => x.toDouble)
        if(size == 0){
          size = this_row.length
        } else {
          if(this_row.length < size){
            this_row = this_row ++ Array.fill(size-this_row.length)(0.0)
          }
        }
        rows += this_row
      }
    }
    rows.toArray
  }

}
