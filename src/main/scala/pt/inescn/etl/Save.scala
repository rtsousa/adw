package pt.inescn.etl

import better.files._
import better.files.Dsl._

object Save {
  def saveLine(pathName: String, text: String): Unit ={
    import java.io._

    val fileName = cwd + pathName
    val w = new BufferedWriter(new FileWriter(fileName, true)) //true: append
    w.write(text+"\n")
    w.close()
  }

  def saveSparse(mat: Array[(Double, Map[Int, Double])], pathFile: String): Unit ={
    for(row <- mat){
      var write = "" + row._1
      for(value <- row._2){
        write += " " + value._1 + ":" + value._2
      }
      saveLine(pathFile,write)
    }
  }
}
