/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.dsp

/**
  * Envelope detection.
  *
  * @see https://www.dsprelated.com/showarticle/938.php
  */
object Envelope {
  // TODO: add peak detectors to be used for envelope detection (using interpolation)

  /**
    * This a simple envelope follower (detector) simulating a RC circuit.
    * TODO: add object with example envelope detectors
    *
    * Created by hmf on 12-07-2017.
    * @param sampleRate - sampling rate of signal in Hz
    * @param attackTime - Defines how fast the envelope raises, defined in seconds.
    * @param releaseTime - Defines how fast the envelope goes down, defined in seconds.
    * @see http://musicdsp.org/showone.php?id=136
    *      https://github.com/JorenSix/TarsosDSP/blob/master/src/core/be/tarsos/dsp/EnvelopeFollower.java
    *      https://en.wikipedia.org/wiki/Envelope_detector
    *      https://en.wikipedia.org/wiki/Synthesizer#ADSR_envelope
    */
  case class Follower(sampleRate: Double, attackTime: Double = 0.0004, releaseTime: Double = 0.0004) {

    private val gainAttack = Math.exp(-1.0 / (sampleRate * attackTime))
    private val gainRelease = Math.exp(-1.0 / (sampleRate * releaseTime))
    private var envelopeOut = 0.0

    def this(sampleRate: Double, decayTime: Double) {
      this(sampleRate, decayTime, decayTime)
    }

    def update(in: Double, out : Double) : Double = {
      val envelopeIn = in // Math.abs(in)
      if (out < envelopeIn)
        envelopeIn + gainAttack * (out - envelopeIn)
      else
        envelopeIn + gainRelease * (out - envelopeIn)
    }

    def filter(buffer: Array[Double]): Array[Double] = {
      buffer.map { p => envelopeOut = update(p, envelopeOut) ; envelopeOut }
    }
  }

}

