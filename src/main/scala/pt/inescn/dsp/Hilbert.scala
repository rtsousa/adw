/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.dsp

import org.hipparchus.complex.Complex
import org.hipparchus.transform.DftNormalization

/**
  * Computes the N-point Bounded Hilbert Transform of real valued vector x: The algorithm consists of the following
  * stages: -
  * 1. X(w) = FFT(x) is computed
  * 2. H(w), DFT of a Hilbert transform filter h[n], is created: H[0]=H[N/2]=1 H[w]=2 for w=1,2,...,N/2-1
  *    H[w]=0 for w=N/2+1,...,N-1
  * 3. x[n] and h[n] are convolved (i.e. X(w) and H(w) multiplied)
  * 4. y[n], the Bounded Hilbert Transform of x[n] is computed by y[n]=IFFT(X(w)H(w)) for n=0,...,N-1
  *
  * Added a first version of the Hilbert transform. Problem: the analytical signal is cut because we use an FFT which
  * causes the truncation of the sample length. This means that the inverse FFT only reconstructs part of the signal.
  *
  * @see https://github.com/marytts/marytts
  * @see https://github.com/marytts/marytts/blob/master/marytts-signalproc/src/main/java/marytts/util/math/Hilbert.java
  * @see https://octave.sourceforge.io/signal/function/hilbert.html
  * @author Oytun Tuumlrk
  * Created by hmf on 12-07-2017.
  *
  * References on the (Bounded) Hilbert transform (EMD)
  * @see https://en.wikipedia.org/wiki/Hilbert_transform
  *      https://en.wikipedia.org/wiki/Hilbert%E2%80%93Huang_transform
  *      https://en.wikipedia.org/wiki/Analytic_signal
  *      https://en.wikipedia.org/wiki/Hilbert%E2%80%93Huang_transform
  *      https://en.wikipedia.org/wiki/Hilbert_spectral_analysis
  *      https://github.com/marytts/marytts
  *      https://github.com/marytts/marytts/blob/master/marytts-signalproc/src/main/java/marytts/util/math/Hilbert.java
  *      https://github.com/DiegoCatalano/Catalano-Framework/blob/master/Catalano.Math/src/Catalano/Math/Transforms/HilbertTransform.java
  *      http://dhale.github.io/jtk/api/edu/mines/jtk/dsp/HilbertTransformFilter.html
  *      https://github.com/dhale/jtk
  *      https://github.com/MinesJTK/jtk
  *      https://sourceforge.net/p/pamguard/svn/2791/tree/PamguardJava/branches/smru/src/signal/Hilbert.java#l36
  *      https://octave.sourceforge.io/signal/function/hilbert.html
  *      https://dsp.stackexchange.com/questions/10560/precisions-on-hilbert-huang-transform
  *
  * TODO: Calculate the Hilbert transform using the Empirical Mode Decomposition (EMD). Usually referred as the hilbert-huang transform.
  * @see https://dsp.stackexchange.com/questions/10837/hilbert-huang-transform-implementation
  *      http://perso.ens-lyon.fr/patrick.flandrin/emd.html
  *      https://github.com/scikit-signal/pytftb
  *      https://github.com/jaidevd/pyhht
  *      http://www.ncl.ucar.edu/Applications/eemd.shtml
  *      http://perso.ens-lyon.fr/patrick.flandrin/talkDL_EMD12.pdf
  *      https://srcole.github.io/2016/01/18/emd/
  *      https://stackoverflow.com/questions/10230106/is-there-some-empirical-mode-decomposition-library-in-java
  *
  * TODO: instead of using the Fourier transform to generate the Hilbert transform, use a Hilbert IIR/FIR filter
  *
  */
object Hilbert {
  /**
    * Performs a Hilbert transform using the Enumeration Fourier transform (DFT). The DFT is
    * very inefficient. use only for testing small samples.
    *
    * @param fs - Sampling rate in Hertz
    * @param x - the sampled signal in te time domain
    * @return - Hilbert transform as an array of [[org.hipparchus.complex.Complex]]
    */
  def transformDFT(fs: Double, x: Array[Double]): Array[Complex] = transformDFT(fs, x.length, x)

  /**
    * Performs a Hilbert transform using the Fast Enumeration Fourier transform (FFT). Because the FFT
    * requires a sample length that is a power of 2, the original samples may be either truncated or
    * padded with 0's. If the signal is extended however, only the analytic signal of the original
    * sampled length is returned.
    *
    * @param fs - Sampling rate in Hertz
    * @param x - the sampled signal in te time domain
    * @param pad - if this value is true then the signal is extended with 0's to a length that is a power of 2. If
    *            the value is false, then the signal is truncated to a length that is a power of 2. Only if true
    *            do we get the analytic signal of the full sample.
    * @return - Hilbert transform as an array of [[org.hipparchus.complex.Complex]]
    */
  def transformFFT(fs: Double, x: Array[Double],
                   pad : Boolean=true,
                   fftNormalization: DftNormalization=DftNormalization.STANDARD): Array[Complex] =
    transformFFT(fs, x.length, x, pad, fftNormalization)

  /**
    * Performs a Hilbert transform using the Fast Enumeration Fourier transform (FFT). Because the FFT
    * requires a sample length that is a power of 2, the original samples may be either truncated or
    * padded with 0's. If the signal is extended however, only the analytic signal of the original
    * sampled length is returned.
    *
    * @param fs - Sampling rate in Hertz
    * @param x - the sampled signal in te time domain
    * @param pad - if this value is true then the signal is extended with 0's to a length that is a power of 2. If
    *            the value is false, then the signal is truncated to a length that is a power of 2. Only if true
    *            do we get the analytic signal of the full sample.
    * @return - Hilbert transform as an array of [[org.hipparchus.complex.Complex]]
    */
  def transform(fs: Double, x: Array[Double], pad : Boolean = true): Array[Complex] = transformFFT(fs, x, pad)

  /**
    * Performs the Hilbert transform using the Fourier Transform.
    * IMPORTANT: the Fourier transform values `X` are changed in-place.
    *
    * @param X - The fourier transform of the sampled signal
    * @param N - Number of samples used in the Fourier transform
    */
  def filter(X : Array[Complex], N: Int) : Unit = {

    val H = new Array[Double](N)

    val NOver2 = Math.floor((N/2.0) + 0.5).toInt
    var w = 0

    H(0) = 1.0
    H(NOver2) = 1.0

    w = 1
    while ( w <= NOver2 - 1 ) {
      H(w) = 2.0
      w += 1
    }

    w = NOver2 + 1
    while ( w <= N - 1) {
      H(w) = 0.0
      w += 1
    }

    w = 0
    while ( w < N) {
      val r = X(w).getReal * H(w)
      val j = X(w).getImaginary * H(w)
      X(w) = new Complex(r,j)
      //X.real(w) *= H(w)
      //X.imag(w) *= H(w)
      w += 1
    }
  }

  /**
    * Performs a Hilbert transform using the Enumeration Fourier transform.
    * IMPORTANT: this method is very inefficient (O(N*N))
    *
    * @param fs - Sampling rate in Herts
    * @param signal_length - number of samples of signal to use. Must be less than or equal to the length of `x`
    * @param x - the sampled signal in te time domain
    * @return - Hilbert transform as an array of [[org.hipparchus.complex.Complex]]
    */
  def transformDFT(fs: Double, signal_length: Int, x: Array[Double]): Array[Complex] = {
    val rr = Fourier.DFT(fs, signal_length, x, DftNormalization.STANDARD)
    val X = rr._3 // FT as Complex
    val N = rr._1 // DFT does not change length of samples
    filter(X, N)
    Fourier.iDFT(X)
  }

  /**
    * Performs a Hilbert transform using the Fast Enumeration Fourier transform (FFT). Because the FFT
    * requires a sample length that is a power of 2, the original samples may be either truncated or
    * padded with 0's. If the signal is extended however, only the analytic signal of the original
    * sampled length is returned.
    *
    * @param fs - Sampling rate in Hertz
    * @param signal_length - number of samples of signal to use. Must be less than or equal to the length of `x`
    * @param x - the sampled signal in te time domain
    * @param pad - if this value is true then the signal is extended with 0's to a length that is a power of 2. If
    *            the value is false, then the signal is truncated to a length that is a power of 2. Only if true
    *            do we get the analytic signal of the full sample.
    * @return - Hilbert transform as an array of [[org.hipparchus.complex.Complex]]
    */
  def transformFFT(fs: Double, signal_length: Int, x: Array[Double], pad : Boolean, fftNormalization: DftNormalization): Array[Complex] = {
    val rr = Fourier.FFT(fs, signal_length, x, pad)
    val X = rr._3 // FF as Complex
    val N = rr._1 // FFT changes length of samples to power of 2
    filter(X, N)
    val tmp = Fourier.iFFT(X, fftNormalization)
    if (pad) tmp.slice(0, signal_length) else tmp
  }

  def envelope(x: Array[Double]): Array[Double] = {
    val h = transform(0, x)
    Fourier.abs(h)
  }
}
