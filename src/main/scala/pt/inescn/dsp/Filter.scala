/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.dsp

import org.hipparchus.stat.regression.SimpleRegression


/**
  * Contains a set of very simple FIR filters to be used in for example envelope detection.
  * Also contains full wave and half wave rectifiers. More sophisticated filters can be found
  * in the IIRj package (see package [[uk.me.berndporr.iirj]]). In addition to this we also
  * have a envelope follower (see [[pt.inescn.dsp.Envelope.Follower]]).
  *
  * Created by hmf on 12-07-2017.
  *
  * @see [[uk.me.berndporr.iirj.Butterworth]]
  * @see [[pt.inescn.dsp.Envelope]]
  * @see [[https://github.com/berndporr/iirj]]
  * @see [[http://www.berndporr.me.uk/]]
  *
  * Some general references
  * IIR filters and automated design
  * http://www.source-code.biz/dsp/java/
  * https://github.com/berndporr/iirj
  *   - Data acquisition : http://comedi.org/
  *   - Author : http://www.berndporr.me.uk/
  * https://github.com/piotr-szachewicz/iir-filter-designer
  * https://dsp.stackexchange.com/questions/6453/frequency-response-error-for-iir-butterworth-in-gnu-octave
  *
  * Interactive designers
  * http://www-users.cs.york.ac.uk/~fisher/mkfilter/ (implemented in http://www.source-code.biz/dsp/java/)
  * http://www.micromodeler.com/dsp/
  * http://t-filter.engineerjs.com/
  * http://www.falstad.com/dfilter/
  * https://dsp.stackexchange.com/questions/85/are-there-any-standard-implementation-forms-for-tunable-butterworth-filters
  *
  * Introductory stuff
  * https://en.wikipedia.org/wiki/Digital_filter
  * https://en.wikipedia.org/wiki/Bilinear_transform
  * https://en.wikipedia.org/wiki/Digital_biquad_filter
  * https://github.com/vinniefalco/DSPFiltersDemo
  * https://github.com/vinniefalco/DSPFilters
  */
object Filter {
  // TODO: add filters to remove trends

  /* Frequency Domain */

  /**
    * NoOP filter. Does not change the signal.
    *
    * @param data - sampled data in time domain
    * @return - returns the same signal
    */
  def allPass(data : Array[Double]): Array[Double] = data

  /**
    * Calculates the mean value of the signal.
    *
    * @param data - sampled data in time domain
    * @return - returns the mean value of the signal
    */
  def mean(data : Array[Double]): Double = data.sum / (data.length * 1.0)

  /**
    * Filter removes the DC component. It calculates the mean and then subtracts this
    * value from all of the samples.
    *
    * @param data - sampled data in the tim domain
    * @return - returns the sampled data with mean zer0
    */
  def removeDC(data : Array[Double]): Array[Double] = {
    val data_mean = mean(data)
    data.map( _ - data_mean)
  }


  /**
    * This is a simple sliding window filter that returns the sum. Note that
    * the number of samples of the returned signal is equal to the length of
    * the original signal minus the windows width.
    *
    * @see https://ccrma.stanford.edu/~jos/fp/Simplest_Lowpass_Filter.html
    * @param data - sampled signal in time domain
    * @param windowSize - size of sliding window (in number of samples)
    * @param stepSize - step size of sliding window (in number of samples)
    * @return filtered signal
    */
  def lowPassWindowSum(windowSize: Int, stepSize : Int)(data : Array[Double]): Array[Double] = {
    data.sliding(windowSize, stepSize).map( _.sum ).toArray
  }

  /**
    * This is a simple sliding window filter that returns the mean. Note that the number of samples of
    * the returned signal is equal to the length of the original signal minus the windows width.
    *
    * @see https://ccrma.stanford.edu/~jos/fp/Simplest_Lowpass_Filter.html
    * @param data - sampled signal in time domain
    * @param windowSize - size of sliding window (in number of samples)
    * @param stepSize - step size of sliding window (in number of samples)
    * @return filtered signal
    */
  def lowPassWindowAvg(windowSize: Int, stepSize : Int)(data : Array[Double]): Array[Double] = {
    val win : Double = windowSize
    data.sliding(windowSize, stepSize).map( _.sum / win ).toArray
  }

  /**
    * This is a simple sliding window filter that returns the mean. Unlike the sliding window filters, the
    * sliding windows do not overlap. All data samples within the window are replaced. Note that the number
    * of samples of the returned signal is equal to the length of the original signal minus the windows width.
    *
    * @param data - sampled signal in time domain
    * @param windowSize - size of block of signals samples to be replaced by new value
    * @return filtered signal
    */
  def lowPassBlockFilterAverage(windowSize: Int)(data : Array[Double]): Array[Double] = {
    val win : Double = windowSize
    def avg(a: Array[Double]): Seq[Double] = {
      val tmp = a.sum / win
      List.fill(a.length){tmp}
    }
    val tt: Iterator[Double] = data.sliding(windowSize, step = windowSize).flatMap( avg )
    tt.toArray
  }

  /**
    * This is a simple sliding window filter that returns the root mean square. Unlike the sliding window
    * filters, the sliding windows do not overlap. All data samples within the window are replaced. Note
    * that the number of samples of the returned signal is equal to the length of the original signal minus
    * the windows width.
    *
    * @param data - sampled signal in time domain
    * @param windowSize - size of block of signals samples to be replaced by new value
    * @return filtered signal
    */
  def lowPassBlockFilterRMS(windowSize: Int)(data : Array[Double]): Array[Double] = {
    def rms(a: Array[Double]): Seq[Double] = {
      val tmp = Math.sqrt( a.zip(a).map( p => p._1 * p._2).sum )
      List.fill(a.length){tmp}
    }
    val tt: Iterator[Double] = data.sliding(windowSize, step = windowSize).flatMap( rms )
    tt.toArray
  }

  /**
    * This is a simple sliding window filter that returns the maximum value. Unlike the sliding window
    * filters, the sliding windows do not overlap. All data samples within the window are replaced. Note
    * that the number of samples of the returned signal is equal to the length of the original signal minus
    * the windows width.
    *
    * @param data - sampled signal in time domain
    * @param windowSize - size of block of signals samples to be replaced by new value
    * @return filtered signal
    */
  def lowPassBlockFilterMax(windowSize: Int)(data : Array[Double]): Array[Double] = {
    def max(a: Array[Double]): Seq[Double] = {
      val tmp = a.max
      List.fill(a.length){tmp}
    }
    val tt: Iterator[Double] = data.sliding(windowSize, step = windowSize).flatMap( max )
    tt.toArray
  }


  /**
    * Performs half-Wave rectification (all values greater than 0.0)
    *
    * @see https://www.dsprelated.com/showarticle/938.php
    * @param data - sampled signal in the time domain
    * @return - sampled signal in the time domain
    */
  def halfWaveRectify(data : Array[Double]) : Array[Double] = data.map( e => if (e < 0.0) 0.0 else e) // data.filter( _ >= 0.0 )

  /**
    * Performs half-Wave rectification (convert all samples to their absolute values)
    *
    * @see https://www.dsprelated.com/showarticle/938.php
    * @param data - sampled signal in the time domain
    * @return - sampled signal in the time domain
    */
  def fullWaveRectify(data : Array[Double]) : Array[Double] = data.map( Math.abs )

  // TODO: One euro filter
  // https://github.com/SableRaf/signalfilter
  // http://cristal.univ-lille.fr/~casiez/1euro/

  // TODO (detrending): DC blocking filter
  // https://ccrma.stanford.edu/~jos/fp/DC_Blocker.html
  // https://en.wikipedia.org/wiki/Local_regression
  // Real time cascaded moving average filter for detrending of electroencephalogram signals
  // Remove DC
  // regression <-----
  //   http://excerptsworld.blogspot.pt/2011/06/how-to-remove-line-trend-in-java.html
  // try simple diff or diff to the n <----
  //    https://machinelearningmastery.com/time-series-trends-in-python/
  // Evaluation of two detrending techniques for application in Heart Rate Variability
  // https://www.researchgate.net/publication/311251067_Real_time_cascaded_moving_average_filter_for_detrending_of_electroencephalogram_signals
  // On the trend, detrending, and variability of nonlinear and nonstationary time series
  // https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3310232/
  // An Efficient Time-Varying Filter for Detrending and Bandwidth Limiting the Heart Rate Variability Tachogram
  // without Resampling: MATLAB Open-Source Code and Internet Web-Based Implementation
  // Comparison of detrending methods for fluctuation analysis
  // Examples
  // https://help.ptc.com/mathcad/en/index.html#page/PTC_Mathcad_Help/example_linear_detrending.html

  /* https://en.wikipedia.org/wiki/Window_function */

  private val PI2: Double = 2.0*Math.PI
  private val PI4: Double = 4.0*Math.PI
  private val PI6: Double = 4.0*Math.PI

  def initNoFilter(len:Int): IndexedSeq[Double] = {
    0 to len map ( _ => 1.0 )
  }

  /**
    * @see https://en.wikipedia.org/wiki/Window_function#Triangular_window
    * @param len length of the window
    * @return weights of the filter that will be multiplied to the signal
    *         window
    */
  def initBartlett(len:Int): IndexedSeq[Double] = {
    import Math._
    def f(i:Double) = 1.0 - abs(i - len) / len
    0 to len map ( i => f(i) )
  }

  /**
    * @see https://en.wikipedia.org/wiki/Hann_function
    * @param len length of the window
    * @return weights of the filter that will be multiplied to the signal
    *         window
    */
  def initHann(len:Int): IndexedSeq[Double] = {
    import Math._

    def f(i:Double) = 0.5*(1-cos(PI2*i/(len-1.0))) * 2.0
    0 to len map (i => f(i) )
  }

  /**
    * @see http://en.wikipedia.org/wiki/Window_function#Blackman_windows
    * @param len length of the window
    * @return weights of the filter that will be multiplied to the signal
    *         window
    */
  def initBlackman(alpha: Double)(len:Int): IndexedSeq[Double] = {
    import Math._

    val a0 = (1.0 - alpha) / 2.0
    val a1 = 0.5
    val a2 = alpha / 2.0

    def f(i:Double) = a0 - a1 * cos(PI2 * i/(len - 1)) + a2 * cos(PI4 * i / (len - 1))
    0 to len map (i => f(i))
  }

  /**
    * @see https://en.wikipedia.org/wiki/Window_function#Blackman%E2%80%93Harris_window
    * @param len length of the window
    * @return weights of the filter that will be multiplied to the signal
    *         window
    */
  def initBlackmanHarris(len:Int): IndexedSeq[Double] = {
    import Math._

    val a0 = 0.35875
    val a1 = 0.48829
    val a2 = 0.14128
    val a3 = 0.01168

    def f(i:Double) = a0 - a1*cos(PI2*i/(len-1)) + a2*cos(PI4*i/(len-1)) - a3*cos(PI6*i/(len-1))

    0 to len map (i => f(i))
  }

  /**
    * @see https://en.wikipedia.org/wiki/Window_function#Kaiser_window
    * @param alpha window parameter
    * @param len length of the window
    * @return weights of the filter that will be multiplied to the signal
    *         window
    */
  def initKaiser(alpha:Double)(len:Int): IndexedSeq[Double] = {
    import Math._

    val I0 = new org.apache.commons.math3.special.BesselJ(0)
    val dn = I0.value(PI * alpha)

    def f(i:Double) = {
      val t = 2.0*i/(len-1)-1.0
      I0.value(PI * alpha * sqrt(1 - (t * t))) / dn
    }

    0 to len map (i => f(i))
  }

  /**
    * @see https://en.wikipedia.org/wiki/Window_function#Flat_top_window
    * @param len length of the window
    * @return weights of the filter that will be multiplied to the signal
    *         window
    */
  def initFlatTop(len:Int): IndexedSeq[Double] = {
    import Math._

    val a0 = 1.0
    val a1 = 1.93
    val a2 = 1.29
    val a3 = 0.388
    val a4 = 0.028

    def f(i:Double) = {
      val scale = 2 * PI * i / (len - 1)
      a0 - a1*cos(scale) +  a2*cos(2*scale) - a3*cos(3*scale) +  a4*cos(4*scale)
    }

    0 to len map (i => f(i))
  }

  /**
    * @see https://en.wikipedia.org/wiki/Window_function#Nuttall_window,_continuous_first_derivative
    * @param len length of the window
    * @return weights of the filter that will be multiplied to the signal
    *         window
    */
  def initNuttall(len:Int): IndexedSeq[Double] = {
    import Math._

    val a0 = 0.355768
    val a1 = 0.487396
    val a2 = 0.144232
    val a3 = 0.012604

    def f(i:Double) = {
      val scale = PI * i / (len - 1)
      a0 - a1 * cos(2.0 * scale) + a2 * cos(4.0 * scale) - a3 * cos(6.0 * scale)
    }

    0 to len map (i => f(i))
  }

  /**
    * @see https://en.wikipedia.org/wiki/Window_function#Gaussian_window
    * @param sigma window parameter
    * @param len length of the window
    * @return weights of the filter that will be multiplied to the signal
    *         window
    */
  def initGaussian(sigma:Double)(len:Int): IndexedSeq[Double] = {
    import Math._

    val d = sigma*(len-1) / 2.0
    def f(i:Double) = {
      val n = i - ((len-1)/2.0)
      val a = n / d
      exp(-0.5 * (a*a))
    }

    0 to len map (i => f(i))
  }

  /**
    * @see https://en.wikipedia.org/wiki/Window_function#Parzen_window
    * @param len length of the window
    * @return weights of the filter that will be multiplied to the signal
    *         window
    */
  def initParzen(len:Int): IndexedSeq[Double] = {
    import Math._

    val N2 = len / 2
    def f1(i:Double) = 1.0 - 6*(i/N2)*(i/N2)*(1.0 - abs(i)/N2)
    def f2(i:Double) = 2 * pow(1.0 - abs(i)/N2,3)

    0 to len map (i => if (i <= len/4) f1(i) else if ((i >len/4) && (i<=len/2)) f2(i) else 0.0 )
  }

}
