/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.dsp

import java.nio.ByteOrder
import javax.sound.sampled.{AudioFileFormat, AudioInputStream, AudioSystem}

import better.files.File

/**
  * This object contains various uituility functions that allow us to load and use an audio file.
  * Only the container and encoders supported by javax.sound are supported.
  *
  * Created by hmf on 04-07-2017.
  *
  * @see https://github.com/JorenSix/TarsosDSP
  *      (https://github.com/librosa/librosa)
  * @see http://openimaj.org/
  * @see http://www.softsynth.com/
  *
  */
object Audio {

  /**
    * Converts two bytes into an Integer (The system's endianess is Little)
    * Little Endian - smallest address of array are the least significant bytes of the integer
    * Big Endian - smallest address of array are the most significant bytes of the integer
    *
    * https://stackoverflow.com/questions/4768933/read-two-bytes-into-an-integer
    * https://stackoverflow.com/questions/9810010/scala-library-to-convert-numbers-int-long-double-to-from-arraybyte
    *
    * NOTE: this function seems to have problems with negative values, TODO: add tests
    *
    * @param b0 first of two bytes that represent the Short
    * @param b1 second of two bytes that represent the Short
    * @param isBigEndian indicate if we are to assume big or little endian order
    * @return
    */
  def toInt(b0: Byte, b1: Byte, isBigEndian: Boolean): Int = {
    toShort(Array[Byte](b0, b1), isBigEndian).toInt
  }

  /**
    * Converts the first two elements of the `data` to an [[scala.Short]]
    * @see [[https://docs.oracle.com/javase/8/docs/api/java/lang/Short.html java.lang.Short.BYTES]]
    * @param data of two bytes that represent the [[scala.Short]]
    * @param isBigEndian indicate if we are to assume big or little endian order
    * @return
    */
  def toShort(data: Array[Byte], isBigEndian: Boolean) : Short = {
    import java.nio.ByteBuffer
    import java.nio.ByteOrder

    val byteBuffer = ByteBuffer.allocateDirect( java.lang.Short.BYTES )
    // by choosing big endian, high order bytes must be put to the buffer before low order bytes
    if (isBigEndian) byteBuffer.order(ByteOrder.BIG_ENDIAN) else byteBuffer.order(ByteOrder.LITTLE_ENDIAN)
    data.foreach( byteBuffer.put )
    byteBuffer.flip
    byteBuffer.getShort
  }

  /**
    * Converts the first four elements of the `data` to an [[scala.Int]]
    * @see [[https://docs.oracle.com/javase/8/docs/api/java/lang/Integer.html java.lang.Integer.BYTES]]
    *
    * @param data of four bytes that represent the [[scala.Int]]
    * @param isBigEndian indicate if we are to assume big or little endian order
    * @return
    */
  def toInt(data: Array[Byte], isBigEndian: Boolean) : Int = {
    import java.nio.ByteBuffer
    import java.nio.ByteOrder

    val byteBuffer = ByteBuffer.allocateDirect( java.lang.Integer.BYTES )
    // by choosing big endian, high order bytes must be put to the buffer before low order bytes
    if (isBigEndian) byteBuffer.order(ByteOrder.BIG_ENDIAN) else byteBuffer.order(ByteOrder.LITTLE_ENDIAN)
    data.foreach( byteBuffer.put)
    byteBuffer.flip
    byteBuffer.getInt
  }

  /**
    * Converts the first eight elements of the `data` to a [[scala.Long]]
    * @see [[https://docs.oracle.com/javase/8/docs/api/java/lang/Long.html java.lang.Long.BYTES]]
    *
    * @param data of eight bytes that represent the [[scala.Long]]
    * @param isBigEndian indicate if we are to assume big or little endian order
    * @return
    */
  def toLong(data: Array[Byte], isBigEndian: Boolean) : Int = {
    import java.nio.ByteBuffer
    import java.nio.ByteOrder

    val byteBuffer = ByteBuffer.allocateDirect( java.lang.Long.BYTES )
    // by choosing big endian, high order bytes must be put to the buffer before low order bytes
    if (isBigEndian) byteBuffer.order(ByteOrder.BIG_ENDIAN) else byteBuffer.order(ByteOrder.LITTLE_ENDIAN)
    data.foreach( byteBuffer.put )
    byteBuffer.flip
    byteBuffer.getInt
  }


  /**
    * Converts an array of bytes to a double value. The size of the
    * array is used to determine what integral type is being ([[scala.Short]],
    * [[scala.Int]] or [[scala.Long]]). These ae then converted to a [[scala.Double]]
    *
    * @param data that represents an integral value
    * @param isBigEndian indicate if we are to assume big or little endian order
    * @return
    */
  def toDouble(data: Array[Byte], isBigEndian: Boolean) : Double = {
    data.length match {
      case java.lang.Byte.BYTES => data(0).toDouble
      case java.lang.Short.BYTES => toShort(data, isBigEndian).toDouble
      case java.lang.Integer.BYTES => toInt(data, isBigEndian).toDouble
      case java.lang.Long.BYTES => toLong(data, isBigEndian).toDouble
    }

  }

  /**
    * Returns an integer representation assuming the `arr` contains the bytes in Big Endian format.
    * Big Endian - smallest address of array are the most significant bytes of the integer
    *
    * @param arr - array with the bytes to decode
    * @param off - offset from which to decode the byt array
    * @return
    */
  // Converting directly to integer causes problems with the sign
  //def getIntBigEndian(arr: Array[Byte], off: Int = 0): Int = arr(off) << 8 & 0xFF00 | arr(off + 1) & 0xFF
  def getIntBigEndian(arr: Array[Byte], off: Int = 0): Int = ((arr(off) << 8) | (arr(off + 1) & 0xFF)).toShort.toInt
  /**
    * Returns an integer representation assuming the `arr` contains the bytes in Litte Endian format.
    * Little Endian - smallest address of array are the least significant bytes of the integer
    *
    * @param arr - array with the bytes to decode
    * @param off - offset from which to decode the byt array
    * @return
    */
  // Converting directly to integer causes problems with the sign
  //def getIntLittleEndian(arr: Array[Byte], off: Int = 0): Int = arr(off+1) << 8 & 0xFF00 | arr(off) & 0xFF
  //def getIntLittleEndian(arr: Array[Byte], off: Int = 0): Short = (arr(off+1) << 8 | (arr(off) & 0xFF)).toShort
  def getIntLittleEndian(arr: Array[Byte], off: Int = 0): Int = (arr(off+1) << 8 | (arr(off) & 0xFF)).toShort.toInt


  /**
    * This function takes a audio frame and splits it into `numChannels` samples. Each sample has a size
    * of `bytesPerChannelSample` bytes. The sample bytes are composed to form a numerical value. Each
    * sample is then converted to a [[scala.Double]] to facilitate processing. Note that the `frame` data
    * has to be a multiple of `bytesPerChannelSample` samples and must have `numChannels` of those samples.
    *
    * @param bytesPerChannelSample - number of bytes per channel
    * @param numChannels - number of channels in each frame
    * @param isBigEndian - whether the audio data is using big or little Endian.
    * @param frame - a single frame of audio data
    * @return
    */
  def getFrameDataPerChannel(bytesPerChannelSample : Int, numChannels : Int, isBigEndian : Boolean)(frame : Array[Byte]): Array[Double] = {
    val left_right: Iterator[Double] = frame.grouped(bytesPerChannelSample).map(pp => toDouble(pp, isBigEndian) )
    left_right.toArray
  }

  /**
    * Opens an audio file and returns an [[https://docs.oracle.com/javase/8/docs/api/javax/sound/sampled/AudioInputStream.html javax.sound.sampled.AudioInputStream]]
    * that gives one access to the data. If the file does not exist or the format is not readable, a [[scala.None]] is returned.
    *
    * @param sample - file with audio samples
    * @return
    */
  def open(sample : File): Option[(AudioFileFormat, AudioInputStream)] = {
    if (sample.exists) {
      val f = sample.toJava
      val format: AudioFileFormat = AudioSystem.getAudioFileFormat(f)
      val data: AudioInputStream = AudioSystem.getAudioInputStream(f)
      Some((format, data))
    }
    else
      None
  }

  def printSystemInformation() = {
    println(s"Byte.SIZE = ${java.lang.Byte.SIZE}")
    println(s"Integer.SIZE = ${Integer.SIZE}")
    println(s"Long.SIZE = ${java.lang.Long.SIZE}")
    println(s"System Endianess = ${ByteOrder.nativeOrder()}")  // ByteOrder.LITTLE_ENDIAN
  }

  def printFileFormat(audioData : Option[(AudioFileFormat, AudioInputStream)]) = {
    val types = AudioSystem.getAudioFileTypes
    val lst = types.mkString(",")
    println( s"Supported audio file formats (containers) (${types.length}) = $lst" )

    audioData match {
      case None =>
        println("No file found")
      case Some((format, data)) =>
        println(s"File format: ${format.toString}")
        println(s"Byte Length (includes header) = ${format.getByteLength}")
        println(s"Frame Length* = ${data.getFrameLength}")
        println(s"Nº channels* = ${data.getFormat.getChannels}")
        println(s"Encoding= ${data.getFormat.getEncoding}")
        println(s"Frame rate (Hz)* = ${data.getFormat.getFrameRate}")
        println(s"Frame size (bytes) = ${data.getFormat.getFrameSize}")
        println(s"Sample rate (Hz) = ${data.getFormat.getSampleRate}")
        println(s"Sample size (bits)= ${data.getFormat.getSampleSizeInBits}")
        println(s"Is big endian = ${data.getFormat.isBigEndian}")
        println(s"Total bytes per channel ok ? ${format.getByteLength % data.getFormat.getChannels == 0}")
    }
  }

  /**
    * This function loads an audio file and stores all the channel data. If a file exists and can
    * be parsed we return the data and the number of frames read. If not, a [[scala.None]] is
    * returned. Note that a matrix is returned for the data. The first index is for the channel read,
    * and the second is for the channels sample.
    *
    * @param sample - audio file name
    * @return - [[scala.Option]] with a tuple containing the channel data and the number of frames read.
    */
  def load(sample : File): Option[(Array[Array[Double]], Int)] = {
    val audioData: Option[(AudioFileFormat, AudioInputStream)] = Audio.open(sample)
    audioData match {
      case None => None
      case Some((_, data)) =>

        val numChannels = data.getFormat.getChannels
        val bytesPerFrame = data.getFormat.getFrameSize
        val bytesPerChannelSample = data.getFormat.getFrameSize / numChannels

        // IMPORTANT: data.getFrameLength is correct, format.getByteLength seems to have a few extra bytes
        // This may include the additional header data
        val channels = Array.ofDim[Double](numChannels, data.getFrameLength.toInt)
        // Index of channels
        val i = 0 until numChannels

        // Just read one frame at a time
        // https://docs.oracle.com/javase/tutorial/sound/converters.html
        val numBytes = 1 * bytesPerFrame
        val audioBytes = new Array[Byte](numBytes)
        var numBytesRead = 0
        var idx = 0
        // Try to read numBytes bytes from the file.
        while ( numBytesRead != -1 ) {
          numBytesRead = data.read(audioBytes)
          if (numBytesRead > 0) {
            // Convert frame bytes to channel samples
            val frameData: Array[Double] = Audio.getFrameDataPerChannel(bytesPerChannelSample, numChannels, isBigEndian = data.getFormat.isBigEndian)(audioBytes)
            // Store the samples iof each channel
            i.foreach( j => channels(j)(idx) = frameData(j) )
            idx += 1
          }
        } // while
        Some((channels, idx))
    }
  }

}
