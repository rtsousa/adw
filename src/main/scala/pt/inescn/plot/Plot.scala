/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.plot

import com.github.cjlin1.SVM
import pt.inescn.models.Base.AlgorithmState.ModelRecorded
import pt.inescn.models.Base.AlgorithmType.BinaryClassification


/**
  * Created by hmf on 13-06-2017.
  *
  * run-main pt.inescn.plot.Plot
  */
object Plot {


  /**
    *
    * @param x - domain
    * @param y - values
    * @param pname - plot's name
    * @param title - plot's title
    * @see http://henning.kropponline.de/2016/03/06/plotting-graphs-data-science-with-scala/
    * @see https://darrenjw.wordpress.com/2013/12/30/brief-introduction-to-scala-and-breeze-for-statistical-computing/
    */
  def showPlot(x: Seq[Double], y: Seq[Double], pname : String = "No name", title : String = "No Title"): Unit = {
    import breeze.plot._
    val fig = Figure()
    val plt = fig.subplot(0)
    plt += plot(x, y, name = pname)
    plt.title = title
    plt.refresh
  }

  /**
    * Using Breeze-viz (based on jFreeChart) to plot a Histogram.
    *
    * @param data - time series data (no time stamps)
    * @param numBins - number of bins to use ikn the histogram
    * @see www.jfree.org/jfreechart/
    * @see http://henning.kropponline.de/2016/03/06/plotting-graphs-data-science-with-scala/
    * @see https://stats.stackexchange.com/questions/798/calculating-optimal-number-of-bins-in-a-histogram
    * @see https://github.com/scalanlp/breeze/wiki/Quickstart
    * @see https://www.mathworks.com/matlabcentral/answers/36428-sine-wave-plot
    *
    * TODO: histogram - get optimal number of bins
    * https://stats.stackexchange.com/questions/798/calculating-optimal-number-of-bins-in-a-histogram
    * https://stackoverflow.com/questions/10786465/how-to-generate-bins-for-histogram-using-apache-math-3-0-in-java
    *
    */
  def showHistogram(data: Seq[Double], numBins : Int, title : String = "No Title"): Unit = {
    import breeze.plot._
    val fig = Figure()
    val plt = fig.subplot(0)
    plt += hist(data,numBins)
    plt.title = title
    plt.refresh
  }

  def experimentSVM(): Unit = {

    import better.files.Dsl._
    val trainData = cwd / "data/libsvm/svmguide1"
    val testData = cwd / "data/libsvm/svmguide1.t"
    //val modelData = cwd / "data/libsvm/svmguide1.model"
    //val tmp = File(System.getProperty("java.io.tmpdir"))
    //val tmp = File(System.getProperty("java.io.tmpdir")) / "a1"

    val gen1 = SVM.TrainParameters()
    val pred1 = SVM.PredictParameters()
    val tr1 = SVM.Train()
    val tr2 = tr1.train(gen1, trainData)
    //tr2.flatMap( tr => tr.predict( tr.modelDataFile, testData ) ) // wont' compile, Ok only set the model if not already set
    val tr4 = tr2.flatMap( tr => tr.predict(pred1, testData) ) // OK, use trained model
    // tr1.predict(testData) // wont' compile, Ok no model available yet
    val tr3: SVM.Train[ModelRecorded, BinaryClassification] = tr2.right.get
    tr1.predict(pred1, tr3.getModel, testData) // compiles, Ok - we can always use a pre-existing model

    val met1 = tr4.right.get.getBoolMetrics
    val acc1 = met1.accuracy
    println(met1.tp)
    println(met1.tn)
    println(met1.fp)
    println(met1.fn)
    println(acc1) // * 100 shouldBe 66.925 +- eps
    println(met1.calculate)


/*
    val s0: SVM.Scale[State.Clean] = SVM.Scale()
    println(s0.trainArgs) // List(-s, , train)
    println(s0.testArgs)  // List(-r, , train)
    val r0: Either[String, SVM.Scale[State.RangeRecorded]] = s0.scale(trainData)
    r0 match {
      case Right(r0s) =>
        // val s2: SVM.Scale[State.RangeRecorded] = s1.scale(trainData) won't compile
        println(r0s.trainArgs) // List(-s, /tmp/SVMScaleRange_fae2cf36-0717-4cc9-bbed-97ca9bb7cdde, /home/hmf/git/adw/data/libsvm/a1a)
        println(r0s.testArgs) // List(-r, /tmp/SVMScaleRange_fae2cf36-0717-4cc9-bbed-97ca9bb7cdde, /home/hmf/git/adw/data/libsvm/a1a)
      case Left(msg) =>
        println("Warning or error")
        println(msg) // ShouldBe this
    }

    val s1 = s0.xLowerTo(0.0).yLowerTo(-1.0).yUpperTo(1.0).outDirTo(tmp)
    println(s1.trainArgs.mkString(",")) // ShouldBe -l,0.0,-y,-1.0,1.0,-s,,train
    println(s1.testArgs.mkString(",")) // ShouldBe
    val r1: Either[String, SVM.Scale[State.RangeRecorded]] = s1.scale(trainData)
    r1 match {
      case Right(r1s) =>
        // val s2: SVM.Scale[State.RangeRecorded] = s1.scale(trainData) won't compile
        println(r1s.trainArgs) // List(-l, 0.0, -y, -1.0, 1.0, -s, /tmp/SVMScaleRange_a133ffdc-343d-40b4-9b0c-077993b00b70, /home/hmf/git/adw/data/libsvm/a1a)
        println(r1s.testArgs) // List(-l, 0.0, -y, -1.0, 1.0, -r, /tmp/SVMScaleRange_a133ffdc-343d-40b4-9b0c-077993b00b70, /home/hmf/git/adw/data/libsvm/a1a)
      case Left(msg) =>
        println("Warning or error")
        println(msg)
    }

    val r2: Either[String, SVM.Scale[State.RangeRecorded]] = r1.flatMap( _.reScale(testData) )
    println(r2)
*/

    // This automatically initializes and plugins
    // Such plugin are found in JFreeChart, however we need not so this explicitly.
    import javax.imageio.ImageIO
    ImageIO.scanForPlugins()
  }

  // LibSVM
  // https://github.com/cjlin1/libsvm
  // https://github.com/tfahub/fluent-libsvm
  // http://nd4j.org/getstarted - DataVec
  // libraryDependencies += "com.datumbox" % "libsvm" % "3.21"
  // http://svmlight.joachims.org/


  // KDE
  // https://en.wikipedia.org/wiki/Kernel_density_estimation
  // https://en.wikipedia.org/wiki/Multivariate_kernel_density_estimation
  // https://github.com/joluet/okde-java
  // http://weka.sourceforge.net/doc.stable/weka/estimators/KernelEstimator.html
  //   . http://www.programcreek.com/java-api-examples/index.php?example_code_path=weka-weka.estimators-UnivariateKernelEstimator.java
  // https://bitbucket.org/lbl-cascade/fastkde (python)
  // https://github.com/timnugent/kernel-density
  // https://github.com/decamp/kde
  // https://www.iro.umontreal.ca/~simul/ssj-2/doc/html/umontreal/iro/lecuyer/gof/KernelDensity.html
  // http://daveakshat.blogspot.pt/2013/04/convolution-probabilty-density.html
  // https://stackoverflow.com/questions/13722324/java-geospatial-2d-kernel-densisty-estimation
  // https://github.com/compomics/cellmissy/blob/master/src/main/java/be/ugent/maf/cellmissy/analysis/impl/NormalKernelDensityEstimator.java

  // https://github.com/haifengl/smile/blob/355198c504f1c45652542da6580a3041799cb0f8/math/src/test/java/smile/stat/distribution/KernelDensityTest.java

  // https://github.com/josephmisiti/awesome-machine-learning
  // http://weka.sourceforge.net/doc.stable/weka/estimators/KernelEstimator.html
  //    https://svn.cms.waikato.ac.nz/svn/weka/
  // https://github.com/padreati/rapaio
  // https://github.com/elki-project/elki/search?utf8=%E2%9C%93&q=density&type=
  //   https://github.com/elki-project/elki/blob/e8f3c6fdf54e1e0aa8444b94ad5374ad518dfc0c/elki-core-math/src/main/java/de/lmu/ifi/dbs/elki/math/statistics/KernelDensityEstimator.java
  //   https://github.com/elki-project/elki
  // https://github.com/EdwardRaff/JSAT
  //  https://github.com/EdwardRaff/JSAT/search?utf8=%E2%9C%93&q=density&type=
  // https://github.com/datumbox/datumbox-framework
  // http://java-ml.sourceforge.net/
  // https://github.com/umontreal-simul/ssj/search?utf8=%E2%9C%93&q=density&type=
  // http://jwork.org/main/
  // http://www.jsc.nildram.co.uk/


  // http://www.cs.waikato.ac.nz/ml/weka/index.html
  // http://moa.cms.waikato.ac.nz/
  // http://mallet.cs.umass.edu/
  // https://elki-project.github.io/



/*
  class myEmpiricalDistribution(binCount: Int, randomData: RandomDataGenerator) extends EmpiricalDistribution(binCount: Int, randomData: RandomDataGenerator) {
    def this() = this(EmpiricalDistribution.DEFAULT_BIN_COUNT, new RandomDataGenerator)
    def this(binCount: Int) = this(binCount, new RandomDataGenerator)
    def this(binCount: Int, generator: RandomGenerator) = this(binCount, RandomDataGenerator.of(generator))
    def this(generator: RandomGenerator) = this(EmpiricalDistribution.DEFAULT_BIN_COUNT, generator)

    lazy val sampleStats = getSampleStats
    lazy val min = sampleStats.getMin
    lazy val max = sampleStats.getMax
    lazy val delta = (max - min) / binCount

    def findBin(value: Double) = {
      FastMath.min(FastMath.max(FastMath.ceil((value - min) / delta).toInt - 1, 0), binCount - 1)
    }

    private def pB(i: Int) = {
      val upperBounds = getUpperBounds
      if (i == 0) upperBounds(0) else upperBounds(i) - upperBounds(i - 1)
    }

    private def kB(i: Int) = {
      val binBounds = getUpperBounds
      val binStats = getBinStats
      val kernel = getKernel(binStats.get(i))
      if (i == 0) kernel.probability(min, binBounds(0))
      else kernel.probability(binBounds(i - 1), binBounds(i))
    }

    lazy val allKernels : mutable.Seq[Real] = {
      val bStats = getBinStats
      bStats.asScala.map( getKernel(_) )
    }

    def binDensity(x: Double): Double = {
      if (x < min || x > max) return 0d
      val binIndex = findBin(x)
      val binStats = getBinStats
      val kernel = getKernel(binStats.get(binIndex))
      kernel.density(x) * pB(binIndex) / kB(binIndex)
    }

    def fullDensity(x : Double) = {
      //val binBounds = getUpperBounds
      val bin = findBin(x)
      //val lower = if (bin == 0) getSupportLowerBound else binBounds(bin - 1)
      //val upper = binBounds(bin)
      //val kernelLower = if (bin == 0) 0.0 else allKernels(bin-1).density(x)
      //val kernelUpper = allKernels(bin).density(x)
      val kernelLower = allKernels(bin).density(x)
      //val kernelUpper = if (bin == binCount-1) 0.0 else allKernels(bin+1).density(x)
      /*
      val kernelLower = allKernels(bin)
      val kernelUpper = if (bin == binCount-1) new NormalDistribution(0.0,1.0) else allKernels(bin+1)
      val mean = if (bin == binCount-1) kernelLower.getNumericalMean else (kernelLower.getNumericalMean + kernelUpper.getNumericalMean + 1.0)/2.0
      val sd = if (bin == binCount-1) Math.sqrt(kernelLower.getNumericalVariance) else Math.sqrt((kernelLower.getNumericalVariance + kernelUpper.getNumericalVariance)/2.0)
      val d = new NormalDistribution(mean, sd)
      d.density(x)
      */
      //kernelLower + kernelUpper
      //kernelUpper
      //kernelLower
      binDensity(x)
    }
  }


  // http://alvinalexander.com/java/jwarehouse/commons-math3-3.6.1/src/test/java/org/apache/commons/math3/random/EmpiricalDistributionTest.java.shtml
  def test1(ed: EmpiricalDistribution, testPoints: Seq[Double]) = {
/*    val testPoints = getCumulativeTestPoints
    val densityValues = new Array[Double](testPoints.length)
    val empiricalDistribution = makeDistribution.asInstanceOf[Nothing]*/
    val binBounds = ed.getUpperBounds
    /*
    var i = 0
    while ( {
      i < testPoints.length
    }) {
      val bin = findBin(testPoints(i))
      val lower = if (bin == 0) ed.getSupportLowerBound
      else binBounds(bin - 1)
      val upper = binBounds(bin)
      val kernel = findKernel(lower, upper)
      val withinBinKernelMass = kernel.cumulativeProbability(lower, upper)
      val density = kernel.density(testPoints(i))
      densityValues(i) = density * (if (bin == 0) firstBinMass else binMass) / withinBinKernelMass
      {
        i += 1; i - 1
      }
    }*/
  }

*/
  def main(args: Array[String]): Unit = {

    experimentSVM()

    /*
    import org.hipparchus.stat.fitting.EmpiricalDistribution

    val binCount = 20
    val numSamples = 1000 // 100000
    //val data = Array(1.2, 0.2, 0.333, 1.4, 1.5, 1.2, 1.3, 10.4, 1, 2.0)
    val sampler1 = NormalSampler(0.0, 1.0)
    val data = sampler1.toStream.take(numSamples).toArray


    val distribution = new EmpiricalDistribution(binCount)
    //val distribution = new myEmpiricalDistribution(binCount)
    distribution.load(data)

    /*
    val rr = distribution.getBinStats.asScala

    val histogrami = new Array[Long](binCount)
    var k = 0
    for (stats <- distribution.getBinStats.asScala) {
      histogrami({
        k += 1; k - 1
      }) = stats.getN
    }*/

    // histogram(data, BIN_COUNT, "Normal Distribution")


    import breeze.plot._
    val fig = Figure()
    val plt = fig.subplot(0)
    plt += hist(data,binCount)
    plt.title = "Original"
    //plt.refresh

    //println(plt.xlim)
    //println(plt.ylim)

    //val stats = distribution.getSampleStats
    //println(stats)

    val binStats = distribution.getBinStats.asScala
    val bin0 = binStats(0)
    //println(bin0)

    val ndata = (1 to numSamples).map( x=> distribution.getNextValue() ).toArray
    val p2 = fig.subplot(2,1,1)
    p2 += hist(ndata,binCount)
    p2.title = "Kernel Density Sampling"
    //p2.refresh

    val p3 = fig.subplot(3,1,2)
    val delta = (plt.xlim._2 - plt.xlim._1)/(numSamples*1.0)
    //println(delta)
    val xs : Array[Double] = (plt.xlim._1 until plt.xlim._2 by delta).toArray
    //println(xs)
    val ys = xs.map(distribution.density(_))
    //val ys = xs.map(distribution.cumulativeProbability(_))
    //println(ys.length)

    p3 += plot(xs, ys)
    p3.title = "Kernel Density Estimation"

    // http://alvinalexander.com/java/jwarehouse/commons-math3-3.6.1/src/test/java/org/apache/commons/math3/random/EmpiricalDistributionTest.java.shtml

/*
    val bounds = distribution.getUpperBounds
    val ndelta = (bounds(1) - bounds(0)) / 2.0
    val len = bounds.length
    //val lower = bounds.take(len-1)
    //val upper = bounds.drop(1)
    //val mid = upper.zip(lower).map((a) => a._1 - a._2)
    val mid = bounds.map((a) => a - ndelta)
    //println(ndelta)
    //println(mid.mkString(","))

    val p4 = fig.subplot(4,1,3)
    //val nys = lower.map(distribution.density(_))
    val nys = mid.map(distribution.density(_))
    //val nys = xs.map(distribution.fullDensity(_))
    //println(nys.length)

    p4 += plot(mid, nys.toArray)
    //p4 += plot(xs, nys.toArray)
    p4.title = "Kernel Density Estimation (2)"
    //plt.refresh
*/
    import de.lmu.ifi.dbs.elki.math.statistics._
    import de.lmu.ifi.dbs.elki.math.statistics.kernelfunctions.GaussianKernelDensityFunction
    import de.lmu.ifi.dbs.elki.math.statistics.kernelfunctions.BiweightKernelDensityFunction
    import de.lmu.ifi.dbs.elki.math.statistics.kernelfunctions.CosineKernelDensityFunction
    import de.lmu.ifi.dbs.elki.math.statistics.kernelfunctions.EpanechnikovKernelDensityFunction
    import de.lmu.ifi.dbs.elki.math.statistics.kernelfunctions.TriangularKernelDensityFunction
    import de.lmu.ifi.dbs.elki.math.statistics.kernelfunctions.TricubeKernelDensityFunction
    import de.lmu.ifi.dbs.elki.math.statistics.kernelfunctions.TriweightKernelDensityFunction
    import de.lmu.ifi.dbs.elki.math.statistics.kernelfunctions.UniformKernelDensityFunction


    println("Start ------ ????????????????")
    //val elkiKDE = new KernelDensityEstimator(data, GaussianKernelDensityFunction.KERNEL, 1e-6)
    //val elkiKDE = new KernelDensityEstimator(data, BiweightKernelDensityFunction.KERNEL, 1e-6)
    //val elkiKDE = new KernelDensityEstimator(data, CosineKernelDensityFunction.KERNEL, 1e-6)
    val elkiKDE = new KernelDensityEstimator(data, EpanechnikovKernelDensityFunction.KERNEL, 1e-6)
    /*val min = -2.9 // data.min // -2.9062721532345117
    val max = 2.9 // data.max // 3.7543580833606973
    val windows = 6 // 1 + Math.log(data.length).toInt // 7
    val elkiKDE = new KernelDensityEstimator(data.sorted, min, max, GaussianKernelDensityFunction.KERNEL, windows, 1e-6)*/
    //val xxs = (1.0 to data.length by 1.0).toArray
    println("getDensity ------ ????????????????")
    val nys: Array[Double] = elkiKDE.getDensity
    println((xs.length, nys.length))
    println("plot ------ ????????????????")
    val p4 = fig.subplot(4,1,3)
    p4 += plot(xs, nys)
    //p4 += plot(xs, elkiKDE.getVariance)
    p4.title = "Kernel Density Estimation (2)"

    println("Stop ------- ????????????????")
    /*
    import smile.stat.distribution.KernelDensity

    val p5 = fig.subplot(5,1,4)
    val instance = new KernelDensity(data)
    val sys = xs.map(instance.p(_))
    println(s"xs = ${xs.length}")
    println(s"sys = ${sys.length}")
    println(sys.take(20).mkString(","))
    System.out.flush

    p5 += plot(xs, sys.toArray)
    p5.title = "Kernel Density Estimation (3)"
*/

    plt.refresh
    //fig.saveas("/home/hmf/Desktop/kde_1.png")
    println("Done ----------")
    */
  }

  // https://java-matrix.org/
  /* http://www.scalanlp.org/api/breeze/#package
  import breeze.stats.distributions._
  import breeze.stats._

  val gau: Gaussian =Gaussian(0.0,1.0)
  val y: IndexedSeq[Double] =gau.sample(20)
  val mm: Double = mean(List(1.0))*/


}
