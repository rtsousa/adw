/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.features

import pt.inescn.etl.Load.{TimeSeriesFrame1, TimeSeriesFrame2, TimeSeriesFrame3, TimeSeriesFrame4}

/**
  * Here we use https://www.hipparchus.org funcions to caslculate the descrioptive statistucs of
  * a data frame.
  *
  * Created by hmf on 13-06-2017.
  *
  * @see [[pt.inescn.etl.Load.TimeSeriesRow]], [[pt.inescn.etl.Load.TimeSeriesFrame]]
  */
object Stats {

  //import scala.collection.JavaConverters._
  import org.hipparchus.stat.descriptive.StreamingStatistics
  import org.hipparchus.stat.descriptive.DescriptiveStatistics

  trait BatchStatsCalculator[T,R1] {
    def batch(data: T) : R1
  }

  trait StreamStatsCalculator[T,R1] {
    def stream(data: T) : R1
  }

  trait StatsCalculator[T,R1,R2] extends BatchStatsCalculator[T,R1] with StreamStatsCalculator[T,R2]

  /* Value classes */

  case class GeoMean(value: Double) extends AnyVal
  case class Kurtosis(value: Double) extends AnyVal
  case class Max(value : Double) extends AnyVal
  case class Mean(value: Double) extends AnyVal
  case class Min(value: Double) extends AnyVal
  case class N(value: Double) extends AnyVal
  case class Quartile1(value : Double) extends AnyVal
  case class Quartile2(value: Double) extends AnyVal // median
  case class Quartile3(value: Double) extends AnyVal
  case class PopulationVariance(value: Double) extends AnyVal
  case class QuadraticMean(value: Double) extends AnyVal
  case class Skewness(value: Double) extends AnyVal
  case class StandardDeviation(value: Double) extends AnyVal
  case class SumTotal(value: Double) extends AnyVal
  case class SumSquares(value: Double) extends AnyVal
  case class Variance(value : Double) extends AnyVal


  object DescriptiveStats {
    /**
      * Here we store and show the descriptive statistics that are calculated for each time-series
      *
      * @param geo_mean            - Geometric mean
      * @param kurtosis            - Kurtosis (only for batch processing)
      * @param max                 - maximum value
      * @param mean                - average
      * @param min                 - minimum value
      * @param n                   - number of elements in stream (so far)
      * @param quartile_1          - 25% quartile
      * @param quartile_2          - median (50% quartile)
      * @param quartile_3          - 75% quartile
      * @param population_variance - variance with (n-1) scale
      * @param quadratic_mean      - quadratic mean
      * @param skewness            - skewness (only for batch processing)
      * @param standard_deviation  - standard deviation (square root of variance)
      * @param sum                 - sum of all values (so far)
      * @param sum_squares         - sum of all values squared (so far)
      * @param variance            - variance with n scale
      * @see [[DescriptiveStats]], [[StatsCalculator]]
      */
    case class Values(
                       geo_mean: GeoMean,
                       kurtosis: Kurtosis,
                       max: Max,
                       mean: Mean,
                       min: Min,
                       n: N,
                       quartile_1: Quartile1,
                       quartile_2: Quartile2, // median
                       quartile_3: Quartile3,
                       population_variance: PopulationVariance,
                       quadratic_mean: QuadraticMean,
                       skewness: Skewness,
                       standard_deviation: StandardDeviation,
                       sum: SumTotal,
                       sum_squares: SumSquares,
                       variance: Variance
                     )

    type StreamStats = (Values, StreamingStatistics)

    /**
      * Calculates all of the available descriptive statistics:
      * - Geometric Mean
      * - Kurtosis
      * - Max
      * - Mean
      * - Min
      * - Total N
      * - 25th Percentile
      * - 50th Percentile (median)
      * - 75th Percentile
      * - Population Variance
      * - Quadratic Mean
      * - Skewness
      * - Standard Deviation
      * - SumTotal
      * - SumTotal Of Squares
      * - Variance
      *
      * @param data - sequence of number whose descriptive statistics we want to calculate
      * @return - descriptive statistics
      * @see [[pt.inescn.features.Stats.DescriptiveStats$.streaming]]
      */
    def batch(data: Seq[Double]): Values = {
      val stats = new DescriptiveStatistics
      data.foreach(e => stats.addValue(e))

      val geo_mean = GeoMean(stats.getGeometricMean)
      val kurtosis = Kurtosis(stats.getKurtosis)
      val max = Max(stats.getMax)
      val mean = Mean(stats.getMean)
      val min = Min(stats.getMin)
      val n = N(stats.getN)
      val quartile_1 = Quartile1(stats.getPercentile(25))
      val quartile_2 = Quartile2(stats.getPercentile(50)) // median
      val quartile_3 = Quartile3(stats.getPercentile(75))
      val population_variance = PopulationVariance(stats.getPopulationVariance)
      val quadratic_mean = QuadraticMean(stats.getQuadraticMean)
      val skewness = Skewness(stats.getSkewness)
      val standard_deviation = StandardDeviation(stats.getStandardDeviation)
      val sum = SumTotal(stats.getSum)
      val sum_squares = SumSquares(stats.getSumOfSquares)
      val variance = Variance(stats.getVariance)

      Values(
        geo_mean, kurtosis, max, mean, min, n,
        quartile_1, quartile_2, quartile_3,
        population_variance, quadratic_mean, skewness, standard_deviation,
        sum, sum_squares, variance
      )
    }
    /**
      * Calculates all of the possible descriptive statistics that can be done via
      * stream processing.
      * Calculates all of the available descriptive statistics:
      * - Geometric Mean
      * - Max
      * - Mean
      * - Min
      * - Total N
      * - 25th Percentile
      * - 50th Percentile (median)
      * - 75th Percentile
      * - Population Variance
      * - Quadratic Mean
      * - Standard Deviation
      * - SumTotal
      * - SumTotal Of Squares
      * - Variance
      *
      * In this case we cannot generate Kurtosis nor skewness.
      * The percentiles are an approximation. **Important**: the [[org.hipparchus.stat.descriptive.StreamingStatistics]]
      * that is used must be initialized with `computePercentiles` parameter set to true.
      *
      * IMPORTANT: TODO: clone?
      *
      * @param stats - previous statistics that can be updated wit ths data
      * @param data - sequence of number whose descriptive statistics we want to calculate
      * @return - descriptive statistics
      * @see [[[pt.inescn.features.Stats.DescriptiveStats$.batch]]]
      */
    def streaming(data: Seq[Double], stats: StreamingStatistics = new StreamingStatistics(true)): StreamStats = {
      data.foreach(e => stats.addValue(e))

      val geo_mean = GeoMean(stats.getGeometricMean)
      val kurtosis = Kurtosis(Double.NaN) // stats.getKurtosis not supported
      val max = Max(stats.getMax)
      val mean = Mean(stats.getMean)
      val min = Min(stats.getMin)
      val n = N(stats.getN)
      val quartile_1 = Quartile1(stats.getPercentile(25))
      val quartile_2 = Quartile2(stats.getPercentile(50)) // median
      val quartile_3 = Quartile3(stats.getPercentile(75))
      val population_variance = PopulationVariance(stats.getPopulationVariance)
      val quadratic_mean = QuadraticMean(stats.getQuadraticMean)
      val skewness = Skewness(Double.NaN) // stats.getSkewness not supported
      val standard_deviation = StandardDeviation(stats.getStandardDeviation)
      val sum = SumTotal(stats.getSum)
      val sum_squares = SumSquares(stats.getSumOfSquares)
      val variance = Variance(stats.getVariance)

      val s = Values(
        geo_mean, kurtosis, max, mean, min, n,
        quartile_1, quartile_2, quartile_3,
        population_variance, quadratic_mean, skewness, standard_deviation,
        sum, sum_squares, variance
      )

      (s, stats)
    }

    def batch[T,R1,R2](data: T)(implicit calc : StatsCalculator[T,R1,R2]): R1 = calc.batch(data)
    def streaming[T,R1,R2](data: T)(implicit calc : StatsCalculator[T,R1,R2]): R2 = calc.stream(data)

    /*
       Below we have the implicits that allow us to transparently calculate the descriptive statistics
       for various CSB data frames.
     */

    /**
      * Calculates all of the available descriptive statistics for a [[pt.inescn.etl.Load.TimeSeriesFrame1]]
      *
      * @return
      * @see [[DescriptiveStats]], [[StatsCalculator]]
      */
    implicit def frame1Stats = new StatsCalculator[TimeSeriesFrame1, Values, DescriptiveStats.StreamStats] {
      override def batch(data: TimeSeriesFrame1): Values = DescriptiveStats.batch(data.value1)
      override def stream(data: TimeSeriesFrame1): DescriptiveStats.StreamStats =
        DescriptiveStats.streaming(data.value1, new StreamingStatistics(true))
    }

    /**
      * Calculates all of the available descriptive statistics for a [[pt.inescn.etl.Load.TimeSeriesFrame2]]
      *
      * @return
      * @see [[DescriptiveStats]], [[StatsCalculator]]
      */
    implicit def frame2Stats = new StatsCalculator[TimeSeriesFrame2, (Values, Values), (DescriptiveStats.StreamStats, DescriptiveStats.StreamStats)] {
      override def batch(data: TimeSeriesFrame2): (Values, Values) =
        ( DescriptiveStats.batch(data.value1),
          DescriptiveStats.batch(data.value2) )
      override def stream(data: TimeSeriesFrame2): (DescriptiveStats.StreamStats, DescriptiveStats.StreamStats) =
        ( DescriptiveStats.streaming(data.value1,new StreamingStatistics(true)),
          DescriptiveStats.streaming(data.value2,new StreamingStatistics(true)))
    }

    /**
      * Calculates all of the available descriptive statistics for a [[pt.inescn.etl.Load.TimeSeriesFrame3]]
      *
      * @return
      * @see [[DescriptiveStats]], [[StatsCalculator]]
      */
    implicit def frame3Stats = new StatsCalculator[TimeSeriesFrame3, (Values, Values, Values), (DescriptiveStats.StreamStats, DescriptiveStats.StreamStats, DescriptiveStats.StreamStats)] {
      override def batch(data: TimeSeriesFrame3): (Values, Values, Values) =
        ( DescriptiveStats.batch(data.value1),
          DescriptiveStats.batch(data.value2),
          DescriptiveStats.batch(data.value3) )
      override def stream(data: TimeSeriesFrame3): (DescriptiveStats.StreamStats, DescriptiveStats.StreamStats, DescriptiveStats.StreamStats) =
        ( DescriptiveStats.streaming(data.value1,new StreamingStatistics(true)),
          DescriptiveStats.streaming(data.value2, new StreamingStatistics(true)),
          DescriptiveStats.streaming(data.value3, new StreamingStatistics(true)))
    }

    /**
      * Calculates all of the available descriptive statistics for a [[pt.inescn.etl.Load.TimeSeriesFrame4]]
      *
      * @return
      * @see [[DescriptiveStats]], [[StatsCalculator]]
      */
    implicit def frame4Stats = new StatsCalculator[TimeSeriesFrame4, (Values, Values, Values, Values), (DescriptiveStats.StreamStats, DescriptiveStats.StreamStats, DescriptiveStats.StreamStats, DescriptiveStats.StreamStats)] {
      override def batch(data: TimeSeriesFrame4): (Values, Values, Values, Values) =
        ( DescriptiveStats.batch(data.value1),
          DescriptiveStats.batch(data.value2),
          DescriptiveStats.batch(data.value3),
          DescriptiveStats.batch(data.value4))
      override def stream(data: TimeSeriesFrame4): (DescriptiveStats.StreamStats, DescriptiveStats.StreamStats, DescriptiveStats.StreamStats, DescriptiveStats.StreamStats) =
        (DescriptiveStats.streaming(data.value1, new StreamingStatistics(true)),
          DescriptiveStats.streaming(data.value2, new StreamingStatistics(true)),
          DescriptiveStats.streaming(data.value3, new StreamingStatistics(true)),
          DescriptiveStats.streaming(data.value4, new StreamingStatistics(true)))
    }

    /*
        Utility functions
     */


    /**
      * Utility function to extract the descriptive Statistics via stream processing
      *
      * @see [[DescriptiveStats.StreamStats]], [[Values]]
      */
    def unpack(s: DescriptiveStats.StreamStats): Values = s._1

    /**
      * Utility function to extract the descriptive Statistics via stream processing
      *
      * @see [[DescriptiveStats.StreamStats]], [[Values]]
      */
    def unpack(s: (DescriptiveStats.StreamStats, DescriptiveStats.StreamStats)): (Values, Values) = (s._1._1, s._2._1)

    /**
      * Utility function to extract the descriptive Statistics via stream processing
      *
      * @see [[DescriptiveStats.StreamStats]], [[Values]]
      */
    def unpack(s: (DescriptiveStats.StreamStats, DescriptiveStats.StreamStats, DescriptiveStats.StreamStats)): (Values, Values, Values) = (s._1._1, s._2._1, s._3._1)

    /**
      * Utility function to extract the descriptive Statistics via stream processing
      *
      * @see [[DescriptiveStats.StreamStats]], [[Values]]
      */
    def unpack(s: (DescriptiveStats.StreamStats, DescriptiveStats.StreamStats, DescriptiveStats.StreamStats, DescriptiveStats.StreamStats)): (Values, Values, Values, Values) = (s._1._1, s._2._1, s._3._1, s._4._1)

  }


}
