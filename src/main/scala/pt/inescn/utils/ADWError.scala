package pt.inescn.utils


// TODO: list of ADWError to an ADWError of the messages (flatMap) + map?
// TODO: wrap the ADWError in a left (see above)
// TODO: split a list of Either[ADWError,A] in Left[ADWError] and Right[A]
case class ADWError(msg: String)


object ADWError {

  type RightProjection[A] = Iterator[Either[ADWError, A]]
  type LeftProjection[A] = Iterator[Either[ADWError, A]]
  type Projection[A] = (LeftProjection[A], RightProjection[A])

  /**
    * NOTE: partition is not lazy. The stream will be split by `filter`
    * and `filterNot` separately and therefore memoized.
    *
    * @param l
    * @tparam A
    * @return
    */
  def disjoint[A](l : => Stream[Either[ADWError, A]]): Projection[A] = {
    val tmp: (Stream[Either[ADWError, A]], Stream[Either[ADWError, A]]) = l.partition( _.isLeft )
    (tmp._1.toIterator, tmp._2.toIterator)
  }

  def disjointX[A](l : Stream[Either[ADWError, A]]): Projection[A] = {
    val tmp: (Seq[Either[ADWError, A]], Seq[Either[ADWError, A]]) = l.partition( _.isLeft )
    (tmp._1.toIterator, tmp._2.toIterator)
  }

  def disjoint[A](l : Iterator[Either[ADWError, A]]): Projection[A] =  l.partition( _.isLeft )

  def flattenLeft[A](tmp:Iterator[Either[ADWError, A]]): Iterator[ADWError] =
    tmp.flatMap{ case Left(pp) => List(pp) ; case _ => List() }
  def flattenRight[A](tmp:Iterator[Either[ADWError, A]]): Iterator[A] =
    tmp.flatMap{ case Right(pp) => List(pp) ; case _ => List() }

  def flatten[A](data: Projection[A]): Either[ADWError, Iterator[A]] = {
    if (data._1.nonEmpty){
      val bad: Iterator[ADWError] = flattenLeft(data._1)
      Left(ADWError(bad.map(_.msg).mkString(";")))
    } else {
      Right(ADWError.flattenRight(data._2))
    }
  }
}