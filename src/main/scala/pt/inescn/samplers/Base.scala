/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.samplers

import org.hipparchus.random.RandomGenerator
import pt.inescn.samplers.Distribution.getGenerator

import scala.annotation.tailrec

/**
  * Created by hmf on 22-05-2017.
  */
object Base {

  sealed trait LengthSample
  trait Finite extends LengthSample
  trait Infinite extends LengthSample

  /**
    * Base class of a sampler. A sampler returns the next value from a collection of values.
    * It is a type class whose generic parameter `A` can hold any type of value. It may be used to sample
    * numeric values from a range or from a distribution, for example. It may also be used to sample
    * ordinal or nominal values that represent options, for example.
    *
    * NOTE: The conversion to a [[scala.collection.immutable.List]] assume that the list is finite. The conversion to a
    * [[scala.collection.immutable.Stream]] does not assume that the collection is finite. However, care must be take
    * not to assign the result to a `val`. Because Scala [[scala.collection.immutable.Stream]]s memoize the results,
    * very large collections assigned to a `val`. Use a `def` instead.
    *
    * @tparam T - indicate if it is finite or infinite.
    * @tparam A - elements that will be sampled. May be numeric, nominal or ordinal.
    */
  abstract class Sampler[T <: LengthSample,A] {

    def reset: Sampler[T,A]

    def finished: Boolean

    def current: A

    /**
      * Sample the next value. To get the value use [[current]]
      *
      * @return get next sample. Keep state in new [[pt.inescn.samplers.Base.Sampler]]
      */
    def next: Sampler[T,A]

    /**
      * Samples all collection elements and place them in a list.
      * The [[Sampler]] is always rest before generating the collection.
      * The collection must have a finite number of elements.
      *
      * NOTE: the elements are generated in the inverse order of the [[next]] calls.
      *
      * @return Lits[A]
      */
    def toList: List[A] = {
      @tailrec
      def go(s: Sampler[T,A], acc: List[A]): List[A] = {
        val n = s.next
        val nacc = n.current :: acc
        if (n.finished)
          nacc
        else
          go(n, nacc)
      }

      go(this.reset, List[A]())
    }

    /**
      * Samples all collection elements and place them in a (lazy) Stream.
      * The [[Sampler]] is always rest before generating the collection.
      * The collection need not be finite but the stream values should not
      * be assigned dto a `val` otherwise you may get an Out-of-Memory
      * exception.
      * NOTE: the elements are generated in the same order as the [[next]] calls.
      *
      * @return
      */
    def toStream: Stream[A] = {
      def stream(s: Sampler[T,A]): Stream[A] = {
        val n = s.next
        if (n.finished)
          n.current #:: Stream.empty
        else
          n.current #:: stream(n)
      }

      stream(this.reset)
    }

    /**
      * Generate `n` samples
      * @param n - number of samples to generate
      * @return
      */
    def apply(n:Int) : Seq[A] = toStream.take(n)

    /**
      * Cartesian product of samplers.
      * Cartesian product of two [[Sampler]]s. This combinator can be used an several number of times
      * to create an arbitrary Tuple of samples. This combinator is associative but *not* commutative.
      * The result is a [[Sampler]]. So we can sample it via [[Sampler.next]] or convert it to a [[scala.collection.immutable.List]]
      * or even a [[scala.collection.immutable.Stream]].
      *
      * TODO: allow only finite stuff?
 *
      * @see [[Cartesian]]
      */
    def ##[B](that: Sampler[T,B]): Cartesian[T, A, B] = Cartesian[T,A,B](this, that)
    /**
      * Sampling samplers simultaneously. Stop on the shortest.
      * Combines simultaneous sampling of two samplers and stops as soon as one is finished
 *
      * @see [[And]]
      */
    def &&[B](that: Sampler[T,B]): And[T,A,B] = And[T,A,B](this, that)
    /**
      * Cartesian product of samplers. Reset on the shortest. Combines simultaneous sampling of two samplers.
      * If the shorter one ends, then is is reset and recycled. Stops as soon as both samplers are finished
 *
      * @see [[Or]]
      */
    def ||[B](that: Sampler[T,B]): Or[T,A,B] = Or(this, that)

    def +(that: Sampler[T,A])(implicit num: Numeric[A]) : Sum[T,A] = Sum(this,that)
    def -(that: Sampler[T,A])(implicit num: Numeric[A]) : Subtract[T,A] = Subtract(this,that)
    def *(that: Sampler[T,A])(implicit num: Numeric[A]) : Multiply[T,A] = Multiply(this,that)
    def /(that: Sampler[T,A])(implicit num: Fractional[A]) : Divide[T,A] = Divide(this,that)
  }

  /*
  case class Empty() extends Sampler[Finite,Unit] {
    override def reset: Sampler[Finite, Unit] = this
    override def finished: Boolean = true
    override def current: Unit = ()

    /**
      * Sample the next value. To get the value use [[current]]
      *
      * @return get next sample. Keep state in new [[pt.inescn.samplers.Base.Sampler]]
      */
    override def next: Sampler[Finite, Unit] = this
  }*/

  /*
  case class Empty[T <: LengthSample,A](default : A) extends Sampler[T,A] {
    override def reset: Sampler[T, A] = this
    override def finished: Boolean = true
    override def current: A = default
    override def next: Sampler[T, A] = this
  }
  */

  case class Empty2[L <: LengthSample,T1,T2](v1:Sampler[L,T1],v2:Sampler[L,T2]) extends Sampler[L,(T1,T2)] {
    override def reset: Sampler[L, (T1,T2)] = this
    override def finished: Boolean = true
    override def current: (T1,T2) = (v1.current,v2.current)
    override def next: Sampler[L, (T1,T2)] = this
  }



  /**
    * Calculates the cartesian of two samplers. Note that the samplers may be combined samplers,
    * which means that the result is already a tuple of a cartesian or sum combination. The cartesian
    * is generic in that we need only track and sample one of the parts of the combined samplers
    * `s1` or `s2`.
    *
    * NOTE: we assume that the [[Sampler]] `s1` is the combined sampler and `s2` Sampler is the one we
    * will be sampling at the higher rate. Whenever the higher rate sampler reaches the end, the
    * sampler `s1` will then be sampled. in this way we get a cartesian product of the two samplers.
    *
    * @param s1 - Sampler (possibly combined)
    * @param s2 - Sampler
    * @tparam A - May be a tuple if the sampler `s1` is a combined [[Sampler]].
    * @tparam B - May be a tuple if the sampler `s1` is a combined [[Sampler]].
    * @return - Tuple (A,B)
    */
  case class Cartesian[T <: LengthSample, A, B](s1: Sampler[T,A], s2: Sampler[T,B] ) extends Sampler[T,(A, B)] {
    override def reset: Sampler[T,(A, B)] = {
      val s = (s1.reset.next, s2.reset)
      Cartesian(s._1, s._2)
    }
    override def finished: Boolean = s1.finished && s2.finished
    override def current: (A, B) = (s1.current, s2.current)
    override def next: Sampler[T,(A, B)] = {
      val s =
        if (s1.finished && s2.finished) {
        (s1, s2)
        }
        else if (s2.finished) {
          val na = s1.next
          val nb = s2.reset.next
          (na, nb)
        } else {
          val nb = s2.next
          (s1, nb)
        }
      Cartesian(s._1, s._2)
    }
  }

  /**
    * Combines the elements of two samples by paring their values in the order they are sampled simultaneously.
    * The result is as long as the shorted sampled elements. For example combining `[a,b]` and
    * `[1,2,3]` we get `[(a,1), (b,2)]`.
    * This combinator can be used with finite and infinite length samplers.
    *
    * @param s1 - Sampler (possibly combined)
    * @param s2 - Sampler
    * @tparam A - May be a tuple if the sampler `s1` is a combined [[Sampler]].
    * @tparam B - May be a tuple if the sampler `s1` is a combined [[Sampler]].
    * @return - Tuple (A,B)
    */
  case class And[T <: LengthSample, A, B](s1: Sampler[T,A], s2: Sampler[T,B] ) extends Sampler[T,(A, B)] {
    override def reset: Sampler[T,(A, B)] = {
      val s = (s1.reset, s2.reset)
      And(s._1, s._2)
    }
    override def finished: Boolean = s1.finished || s2.finished
    override def current: (A, B) = (s1.current, s2.current)
    override def next: Sampler[T,(A, B)] = {
      val s =
        if (s1.finished || s2.finished)
          (s1, s2)
        else
          (s1.next, s2.next)
      And(s._1, s._2)
    }
  }

  /**
    * Combines the elements of two samples by paring their values in the order they are sampled simultaneously.
    * If one of the samplers terminates, then it is reset and sampling continues until both samplers terminate.
    * This means that if the samplers with the least elements is a multiple of the longer sampler length, then
    * the result will have the length of the longer sampler. If it is not a multiple, then the resetting will
    * keep cycling the shorter sampler until both reach the end. This means that the total length will be the
    * product of the length of each sampler. For example combining `[0,1,2]` and
    * `[1,2,3]` we get `[(0,0), (1,1), (2,2), (0,3), (1,0), (2,1), (0,2), (1,3), (2,0), (0,1), (1,2), (2,3))]`.
    * This combinator can be used with finite and infinite length samplers.
    *
    * @param s1 - first sampler to combine
    * @param s2 - second sampler to combine
    * @tparam T - indicate if it is finite or infinite.
    * @tparam A - elements that will be sampled. May be numeric, nominal or ordinal.
    * @tparam B - Samplers may have different types
    */
  case class Or[T <: LengthSample, A, B](s1: Sampler[T,A], s2: Sampler[T,B] ) extends Sampler[T,(A, B)] {
    override def reset: Sampler[T,(A, B)] = {
      val s = (s1.reset, s2.reset)
      Or(s._1, s._2)
    }
    override def finished: Boolean = s1.finished && s2.finished
    override def current: (A, B) = (s1.current, s2.current)
    override def next: Sampler[T,(A, B)] = {
      val s =
        (s1.finished, s2.finished) match {
          case (true, true) =>
            (s1, s2)
          case (false, false) =>
            (s1.next, s2.next)
          case (true, false) =>
            val na = s1.reset
            (na.next, s2.next)
          case (false, true) =>
            val nb = s2.reset
            (s1.next, nb.next)
        }
      Or(s._1, s._2)
    }
  }

  /**
    * This class allows us to define the numeric operation to perform on two samplers.
    * When se samples are extracted this operation, the calculation is performed.
    * The implementation need only provide the function and constructor of the specific operation.
    * Using an F-Bounded type so user can see the operation clearly - hence the need to
    * provide the constructor function [[make]]
    *
    * @param func - numerical operation to perform
    * @param s1 - first argument for the numerical operation
    * @param s2 - second argument for the numerical operation
    * @tparam T - indicate if it is finite or infinite.
    * @tparam A - elements that will be sampled. May be numeric, nominal or ordinal.
    * @tparam S - self type for F-Bounded use
    */
  abstract class Op[T <: LengthSample, A, S <: Op[T,A,S]](func: (A,A) => A, s1: Sampler[T,A], s2: Sampler[T,A])
    extends Sampler[T,A]  { self:S =>

    def make(self:S, a: Sampler[T,A], b: Sampler[T,A]) : S

    override def reset: S = {
      val z1 = s1.reset
      val z2 = s2.reset
      make(this,z1,z2)
    }
    override def finished: Boolean = s1.finished || s2.finished
    override def current: A = func(s1.current, s2.current)
    override def next: Op[T,A,S] = {
      val ns1 = s1.next
      val ns2 = s2.next
      make(this,ns1,ns2)
    }
  }

  def sumFunc[A](a: A, b: A)(implicit num: Numeric[A]) : A = {
    import num._
    a + b
  }
  def subtractFunc[A](a: A, b: A)(implicit num: Numeric[A]) : A = {
    import num._
    a - b
  }
  def multiplyFunc[A](a: A, b: A)(implicit num: Numeric[A]) : A = {
    import num._
    a * b
  }
  def divideFunc[A](a: A, b: A)(implicit num: Fractional[A]) : A = {
    import num._
    a / b
  }

  /*
  https://users.scala-lang.org/t/can-we-pass-and-use-a-function-that-requires-an-implicit-parameter/1016/3
  https://github.com/scala/bug/issues/9717
  class FuncY[A:Numeric](func: (A,A) => A, s1: A, s2: A) {
    def current : A = func(s1,s2)
  }
  case class Sum[A](s1: A, s2: A)(implicit num: Numeric[A]) extends FuncY[A](sumFunc[A](_,_)(num),s1,s2)  {
    override def current: A = sumFunc[A](s1, s2)(num)
  }
  }*/

  /**
    * Add values of two samplers.
    * Note: we need to pass the `num` implicit explicitly possibly due to a bug.
    * @see https://users.scala-lang.org/t/can-we-pass-and-use-a-function-that-requires-an-implicit-parameter/1016/3
    * @see https://github.com/scala/bug/issues/9717
    *
    *
    * @param s1- first argument for the numerical operation
    * @param s2 - second argument for the numerical operation
    * @param num - implicit
    * @tparam T - indicate if it is finite or infinite.
    * @tparam A - elements that will be sampled. May be numeric, nominal or ordinal.
    */
  case class Sum[T <: LengthSample, A](s1: Sampler[T,A], s2: Sampler[T,A])
                                      (implicit num: Numeric[A])
    extends Op[T,A,Sum[T,A]](sumFunc[A](_,_)(num),s1,s2)  {
    override def make(self: Sum[T, A], a: Sampler[T, A], b: Sampler[T, A]): Sum[T, A] = Sum(a,b)
  }

  case class Subtract[T <: LengthSample, A](s1: Sampler[T,A], s2: Sampler[T,A])
                                           (implicit num: Numeric[A])
    extends Op[T,A,Subtract[T,A]](subtractFunc[A](_,_)(num),s1,s2)  {
    override def make(self: Subtract[T, A], a: Sampler[T, A], b: Sampler[T, A]): Subtract[T, A] = Subtract(a,b)
  }

  case class Multiply[T <: LengthSample, A](s1: Sampler[T,A], s2: Sampler[T,A])
                                           (implicit num: Numeric[A])
    extends Op[T,A,Multiply[T,A]](multiplyFunc[A](_,_)(num),s1,s2)  {
    override def make(self: Multiply[T, A], a: Sampler[T, A], b: Sampler[T, A]): Multiply[T, A] = Multiply(a,b)
  }

  case class Divide[T <: LengthSample, A](s1: Sampler[T,A], s2: Sampler[T,A])
                                           (implicit num: Fractional[A])
    extends Op[T,A,Divide[T,A]](divideFunc[A](_,_)(num),s1,s2)  {
    override def make(self: Divide[T, A], a: Sampler[T, A], b: Sampler[T, A]): Divide[T, A] = Divide(a,b)
  }


  /**
    * This class serves as a marker all samplers tha have a finite length. This tag is used by the
    * [[pt.inescn.samplers.Base.AppendB]] class that allows us to append samplers of the same type.
    * This can be used to combine samplers constructed via operations such as cartesian or an products.
    *
    * @tparam A - elements that will be sampled. May be numeric, nominal or ordinal.
    */
  abstract class Bounded[A] extends Sampler[Finite, A] {
    // USe this to ensure correct typing for AppendB
    // Alternatively use F-bounded type (see https://tpolecat.github.io/2015/04/29/f-bounds.html)
    override def reset: Bounded[A]
    override def next: Bounded[A]
  }

  /**
    * Ths sampler performs the append of each sampler in the order they appear. It does this by activating
    * and using each sampler until that sampler has no more data.
    *
    * @param l          - list of [[Bounded]] samplers that will be sampled in succession. Note that this implies
    *                   that all the samplers are [[pt.inescn.samplers.Base.Finite]]. This condition should be guaranteed
    *                   for all [[Bounded]] samplers
    * @param currentIdx - Index of the currently active [[pt.inescn.samplers.Base.Sampler]] that is being used to
    *                   obtain the samples
    * @tparam A - elements that will be sampled. May be numeric, nominal or ordinal.
    */
  //case class AppendB[A](l: IndexedSeq[Bounded[A]], currentIdx: Int = 0) extends Bounded[A] {
  case class AppendB[A](l: IndexedSeq[Sampler[Finite,A]], currentIdx: Int = 0) extends Bounded[A] {

    override def reset: AppendB[A] = {
      val nl = l.map(_.reset)
      AppendB(nl)
    }

    override def finished: Boolean = l.forall( _.finished )

    override def current: A = l(currentIdx).current

    override def next: AppendB[A] = {
      // execute on active sampler
      //val n0: Bounded[A] = l(currentIdx).next
      val n0: Sampler[Finite, A] = l(currentIdx).next
      // If finished, activate the next sampler if it exists
      val nCurrentIdx = if (l(currentIdx).finished) Math.min(currentIdx + 1, l.length - 1)  else currentIdx
      // Make sure that the next active sampler starts immediately on the first element
      val nl0 = if (nCurrentIdx != currentIdx) {
        //val n1: Bounded[A] = l(nCurrentIdx).next
        val n1: Sampler[Finite,A] = l(nCurrentIdx).next
        Vector(n0, n1)
      } else Vector(n0)
      // Update the current state
      val nl1 = l.patch(currentIdx, nl0, nl0.length)
      AppendB(nl1, nCurrentIdx)
    }
  }

  /**
    * This class serves as a marker all samplers that have an infinite number of elements. This tag is
    * used by the [[pt.inescn.samplers.Base.AppendU]] class that allows us to append samplers of the same
    * type. This can be used to combine samplers constructed via operations such as cartesian or an products.
    *
    * @tparam A - elements that will be sampled. May be numeric, nominal or ordinal.
    */
  abstract class Unbounded[A] extends Sampler[Infinite, A] {
    // Use this to ensure correct typing for AppendB
    // Alternatively use F-bounded type (see https://tpolecat.github.io/2015/04/29/f-bounds.html)
    override def reset: Unbounded[A]
    override def next: Unbounded[A]
  }

  /**
    * Ths sampler performs the append of each sampler in the order they appear. It does this by activating
    * and using each sampler until that sampler has no more data.
    *
    * @param l          - list of [[Unbounded]] samplers that will be sampled in succession. Note that this implies
    *                   that all the samplers are [[pt.inescn.samplers.Base.Finite]]. This condition should be guaranteed
    *                   for all [[Unbounded]] samplers
    * @param currentIdx - Index of the currently active [[pt.inescn.samplers.Base.Sampler]] that is being used to
    *                   obtain the samples
    * @tparam A - elements that will be sampled. May be numeric, nominal or ordinal.
    */
  case class AppendU[A](l: IndexedSeq[Sampler[Infinite,A]], rnd : RandomGenerator = getGenerator, currentIdx: Int = 0)
    extends Unbounded[A] {

    override def reset: AppendU[A] = {
      val nl = l.map(_.reset)
      AppendU(nl,rnd)
    }

    override def finished: Boolean = l.forall( _.finished )

    override def current: A = l(currentIdx).current

    override def next: AppendU[A] = {
      // execute on active sampler
      //val n0: Unbounded[A] = l(currentIdx).next
      val n0: Sampler[Infinite, A] = l(currentIdx).next
      val nl0 = l.updated(currentIdx, n0)
      // Randomly select and activate the next sampler
      val nCurrentIdx = rnd.nextInt(l.length)
      // Make sure that the next active sampler starts immediately on the first element
      val nl1 = if (nCurrentIdx != currentIdx) {
        //val n1: Unbounded[A] = l(nCurrentIdx).next
        val n1: Sampler[Infinite, A] = l(nCurrentIdx).next
        nl0.updated(nCurrentIdx, n1)
      } else nl0
      // Update the current state
      AppendU(nl1, rnd, nCurrentIdx)
    }
  }


  /**
    * Combinators that will let us combine various [[Sampler]]s that can be used to generate the search space.
    */
  object SamplerOps {

    // http://stackoverflow.com/questions/2510108/why-avoid-method-overloading
    def flatten3[T1, T2, T3](a: ((T1, T2), T3)): (T1, T2, T3) = (a._1._1, a._1._2, a._2)

    def flatten4[T1, T2, T3, T4](a: (((T1, T2), T3), T4)): (T1, T2, T3, T4) = (a._1._1._1, a._1._1._2, a._1._2, a._2)

    def flatten2_2[T1, T2, T3, T4](a: ((T1, T2), (T3, T4))): (T1, T2, T3, T4) = (a._1._1, a._1._2, a._2._1, a._2._2)
  }


}
