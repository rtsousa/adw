/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.params


import better.files.File
import org.hipparchus.random.RandomGenerator
import pt.inescn.etl.Base.Source
import pt.inescn.etl.Transform
import pt.inescn.etl.Transform.TransformParams
import pt.inescn.eval.Base.{MetricEval, Metrics, MetricsTypes}
import pt.inescn.eval._
import pt.inescn.features.Stats.DescriptiveStats
import pt.inescn.models.Algorithms.Algo
import pt.inescn.models.Base.{Algorithm, DataIn, ML}
import pt.inescn.samplers.Base.{Finite, Sampler, _}
import pt.inescn.samplers.{Base, Distribution}
import pt.inescn.samplers.Distribution._
import pt.inescn.samplers.Enumeration.{EmptyRange, ListRange, RangeOf, Single}
import pt.inescn.utils.{ADWError, Utils}

import scala.collection.immutable


/**
  * This package has a set [[pt.inescn.params.ParamSearch.Param]] classes that
  * can be used to describe the parameters of an
  * [[pt.inescn.scratchpad.TypeSafeParameterSearch.Algorithm]]. It also
  * includes a couple of parsers that parse these descriptions of the
  * parameters and generate a data [[pt.inescn.samplers.Base.Sampler]]. These samplers can then
  * be used to generate a set of parameters that can be used by the
  * function in a type safe manner. This can be used by automated machine
  * learning (for example in parameter optimization).
  *
  *
  * NOTE: The DSL has two member classes of the GADT that are specialized
  * specifically to [[scala.Int]] and [[scala.Double]]. If we have many such
  * cases, then a good way to deal with this is via *advanced GADT-typeclass
  * pattern matching*. The following is an xample from Stephen Compall:
  * "add a typeclass that describes the possible Range types"
  * {{
  *
  * sealed trait TC1[T]
  *
  * object TC1 {
  *   implicit object TInt extends TC1[Int]
  *   implicit object TDouble extends TC1[Double]
  *   }
  *
  * }}
  *
  * "Then add this to your single Range constructor"
  * {{
  *   final case class Range[T](start: T, end: T)(implicit val tc1: TC1[T]) extends Param[T]
  * }}
  *
  * "You can use tc1 to figure out (at runtime) where you are in the space
  * of possible Ts. This is GADT pattern matching."
  *
  * {{
  * case r@Range(_, _) => r.tc1 match {
  *   case TC1.TInt => intRange(r, numSamples)
  *   case TC1.TDouble => doubleRange(r, numSamples)
  * }
  * }}
  *
  * @see https://users.scala-lang.org/t/dry-matching-generic-types-of-a-specific-type/1644/3
  *
  * Created by hmf on 21-05-2017.
  */
object ParamSearch {

  /**
    * Represents the parameters that are consumed by an
    * [[pt.inescn.models.Algorithms]]. It includes information on the type of
    * the parameter and the values these parameters can hold. All extending
    * classes form a DSL that allows one to describe the full set of parameters
    * used by an algorithm. This DSL is then processed to generate a
    * [[pt.inescn.samplers.Base.Sampler]]. This Sampler will automatically generate the
    * corresponding values which can be fed to the [[pt.inescn.models.Algorithms]].
    */
  sealed trait Param[T]

  /** Represents a parameter consisting of a single value. */
  final case class Constant[T](c: T) extends Param[T]
  /** Represents an enumeration of valid values of a given parameter */
  final case class Items[T](e: List[T]) extends Param[T]
  /**
    * Represents a set of values of two parameters. Depending on the strategy
    * used process this statement, one could generate all possible elements
    * (cartesian product) or a subset of these values(a random sample).
    */
  final case class Composite[T1,T2](paramA: Param[T1], paramB: Param[T2]) extends Param[(T1,T2)]

  /**
    * As with [[Composite]] this statement represents the combination of two
    * parameters. But unlike [[Composite]] the composition will iterate
    * through each pair of parameters, combines them using the selected strategy and
    * then append the result into a single [[pt.inescn.samplers.Base.Sampler]].
    */
  final case class PairThese[T1,T2](l: Seq[(Param[T1],Param[T2])]) extends Param[(T1,T2)]

  /** This represents a range of [[scala.Int]]. The range is defined by its
    * initial and final values. The processing strategy can then be used to
    * either generate a fixed set of ordered values or a subset of random
    * values.
    */
  final case class IntRange(start: Int, end: Int) extends Param[Int]
  /** This represents a range of [[scala.Double]]. The range is defined by its
    * initial and final values. The processing strategy can then be used to
    * either generate a fixed set of ordered values or a subset of random
    * values.
    */
  final case class DoubleRange(start: Double, end: Double) extends Param[Double]

  /**
    * This represents a sampler range that consist of all values from the
    * initial value power(base, start) to power(base, end). The processing
    * strategy can then be used to either generate a fixed set of ordered
    * values or a subset of random values.
    * @param base base of power
    * @param start initial exponent
    * @param end final exponent
    */
  final case class PowerRange(base: Double, start: Double, end: Double) extends Param[Double]

  /**
    * Checks is the sampler has been identified as invalid and marked empty.
    *
    * @param a - [[pt.inescn.samplers.Base.Sampler]] that may be marked empty
    * @tparam L - type indicating if sampler is bounded or not
    * @tparam T - elements of the sampler
    * @return - true if marked empty otherwise false
    */
  def isEmpty[L <: LengthSample, T](a: Sampler[L,T]) : Boolean = a match {
    case EmptyRange(_) => true
    case Empty2(_,_) => true
    case _ => false
  }

  def empty[L <: LengthSample, T1,T2](c : (Sampler[L,T1], Sampler[L,T2]) => Sampler[L,(T1,T2)])
                                     (a: Sampler[L,T1], b: Sampler[L,T2]): Sampler[L, (T1,T2)] =
    if (isEmpty(a) || isEmpty(b))
      Empty2(a,b)
    else
      c(a, b)

  /**
    * Grid search strategy: generates all possible combinations of values
    */
  object Grid {

    private def intRange(r : IntRange, numSamples : Int): RangeOf[Int] = {
      val delta = (r.end - r.start) / numSamples.toDouble
      RangeOf(r.start, r.end, Math.floor(delta).toInt)
    }
    private def doubleRange(r : DoubleRange, numSamples : Int): RangeOf[Double] = {
      val delta = (r.end - r.start) / numSamples.toDouble
      RangeOf(r.start, r.end, delta)
    }
    private def powerRange(r : PowerRange, numSamples : Int): RangeOf[Double] = {
      val delta = (r.end - r.start) / numSamples.toDouble
      RangeOf( (t:Double) => Math.pow(r.base,t), r.start, r.end, delta)
    }
    private def const[T](r : Constant[T]): Single[T] = Single[T](r.c)
    private def items[T](l : Items[T]): ListRange[T] = ListRange(l.e)
    private def cartesian[L <: LengthSample, T1, T2](a: Sampler[L,T1], b: Sampler[L,T2]): Cartesian[L, T1, T2] =
      Cartesian[L, T1, T2](a, b)
    private def emptyCartesian[L <: LengthSample, T1, T2](a: Sampler[L,T1], b: Sampler[L,T2]): Sampler[L, (T1, T2)] =
      empty(cartesian[L,T1,T2])(a,b)

    def search[L <: LengthSample, T](numSamples: Int)(p: Param[T]): Sampler[Finite, T] = p match {
      case r@Constant(_) => const(r)
      case r@IntRange(_, _) => intRange(r,numSamples)
      case r@DoubleRange(_, _) => doubleRange(r,numSamples)
      case r@PowerRange(_,_, _) => powerRange(r,numSamples)
      case l@Items(_) => items(l)
      case c@Composite(paramA, paramB) =>
        val a = search(numSamples)(paramA)
        val b = search(numSamples)(paramB)
        val c = emptyCartesian(a,b)
        c
      case c@PairThese(prs) =>
        val r1 = prs.map { e =>
          val a = search(numSamples)(e._1)
          val b = search(numSamples)(e._2)
          emptyCartesian(a, b)
        }
        val r2 = r1.toVector
        val r3 = AppendB(r2)
        r3
    }

  }

  /**
    * Random search strategy: generates a set of values randomly using a uniform distribution
    */
  object Random {

    private def uniformIntRange(r : IntRange, rnd: RandomGenerator): IntUniform = IntUniform(r.start, r.end, rnd)
    private def uniformDoubleRange(r : DoubleRange, rnd: RandomGenerator): Uniform = Uniform(r.start, r.end, rnd)
    private def uniformPowerRange(r: PowerRange, rnd: RandomGenerator): ApplyFunc[Double, Double, DiscreteUniform[Double]] = {
      val items = (r.start to r.end by 1.0).toList
      val exponents = DiscreteUniform(items, rnd)
      ApplyFunc( exponents, e =>  Math.pow(r.base, e))
    }
    private def uniformItems[T](l : Items[T], rnd: RandomGenerator): DiscreteUniform[T] = DiscreteUniform(l.e, rnd)
    private def uniformConst[T](r : Constant[T]): Const[T] = Const[T](r.c)
    private def and[L <: LengthSample, T1, T2](a: Sampler[L,T1], b: Sampler[L,T2]): And[L, T1, T2] = And[L, T1, T2](a, b)
    private def emptyAnd[L <: LengthSample, T1, T2](a: Sampler[L,T1], b: Sampler[L,T2]): Sampler[L, (T1, T2)] =
      empty(and[L,T1,T2])(a,b)

    def search[T](p: Param[T], rnd: RandomGenerator): Sampler[Infinite,T] = p match {
      case r@Constant(_) => uniformConst(r)
      case r@IntRange(_, _) => uniformIntRange(r,rnd)
      case r@DoubleRange(_, _) => uniformDoubleRange(r,rnd)
      case r@PowerRange(_, _, _) => uniformPowerRange(r,rnd)
      case l@Items(_) => uniformItems(l,rnd)
      case c@Composite(paramA, paramB) =>
        val a = search(paramA,rnd)
        val b = search(paramB,rnd)
        val c = emptyAnd(a, b)
        c
      case c@PairThese(prs) =>
        val r1 = prs.map { e =>
          val a = search(e._1,rnd)
          val b = search(e._2,rnd)
          emptyAnd(a, b)
        }
        val r2 = r1.toVector
        val r3 = AppendU(r2,rnd)
        r3
    }
  }

  object Strategy {
    def grid[L <: LengthSample, T](numSamples: Int)(p: Param[T]): Sampler[Finite, T] = Grid.search(numSamples)(p)
    def random[T](p: Param[T],rnd:RandomGenerator=Distribution.getGenerator): Sampler[Infinite,T] = Random.search(p,rnd)
  }


  object ParamSelect {

    /**
      * Uses an `algorithm` to fit a model to the training data. It then
      * applies the resulting model to to the test data and predicts the
      * outcome. It then calculates the metrics to evaluate how the good
      * the fit is.
      *
      * The signature of the method is complicated. It ensures that the
      * metric used is compatible with the algorithm's type. For example
      * binary classification metrics can only be applied to ML algorithms
      * for binary classification. It also ensures that the metric can only
      * be used on the dependent variables and predictions of the ML
      * algorithm that have the same compatible types. For example, a data
      * set used by SVM the binary classification SVM have both the dependent
      * and prediction set to Double. This means that both these values
      * have to be read as boolean according to some convention. This
      * conversion is done by the `apply`method a some specific [[pt.inescn.eval.Base.MetricEval]].
      * See for example [[pt.inescn.eval.Base.BinaryClassificationEvalDouble]].
      *
      * @see [[pt.inescn.models.Base.ML]],[[pt.inescn.eval.Base.Metrics]],
      *     [[pt.inescn.eval.Base.MetricsTypes]],[[pt.inescn.models.Base.Algorithm]]
      *
      * Note that we are using a currying version of the function due to
      * Scala's bnon-backtracking, left to right inference used in in single
      * parameter list. This means that the [[pt.inescn.models.Base.ML]] type
      * values will only be inferred and used in the other parameters if those
      * parameters on in
      * another (curried) parameter list.
      * @see https://github.com/ljwagerfield/scala-type-inference
      *
      * @param algorithm Algorithm used for fitting and prediction
      * @param train Training data set
      * @param test Testing data set
      * @param args ML algorithm parameters used by the fitting
      * @param metric Metric used to evaluate the goodness of fit
      * @tparam A The Algorithms characteristics (classification, argument types,
      *           data input, etc.)
      * @return Stream that allows us to measure the goodness of fit. Lazy evaluation used.
      */
    def trainAndEval[A <: ML, Z <: Metrics[A#Algorithm,_,M], M <: MetricsTypes]
    ( algorithm: Algorithm[A]) (args: A#Arguments, train: DataIn[A#Label,A#Value], test: DataIn[A#Label,A#Value],
      metric: MetricEval[A#Algorithm,A#Label,A#Value,A#Prediction,Z,M]) : Either[ADWError, Z] = {
      for {
        model <- algorithm.fit(train, args)
        prediction <- algorithm.predict(test, model)
        //_ = println("trainAndEval") ;
        eval <- Right(metric(test, prediction))
      } yield eval
    }


    /**
      * It evaluates the performance of an ML `algorithm` configured to use a given
      * parameter `args` by calculating a set of `metrics` that measure goodness of
      * fit. Note that the folds ar passed in as a lazy value (thunk) to ensure that
      * the data is not split and kept in memory at the same time. As we go through
      * the folds, data is split and used. We return a lazy stream, so model fitting
      * and evaluation are only performed when required.
      *
      * The signature of the method is complicated. It ensures that the
      * metric used is compatible with the algorithm's type. For example
      * binary classification metrics can only be applied to ML algorithms
      * for binary classification. It also ensures that the metric can only
      * be used on the dependent variables and predictions of the ML
      * algorithm that have the same compatible types. For example, a data
      * set used by SVM the binary classification SVM have both the dependent
      * and prediction set to Double. This means that both these values
      * have to be read as boolean according to some convention. This
      * conversion is done by the `apply`method a some specific
      * [[pt.inescn.eval.Base.MetricEval]].
      * See for example [[pt.inescn.eval.Base.BinaryClassificationEvalDouble]].
      *
      * @see [[pt.inescn.models.Base.ML]],[[pt.inescn.eval.Base.Metrics]],
      *     [[pt.inescn.eval.Base.MetricsTypes]],[[pt.inescn.models.Base.Algorithm]]
      *
      * Note that we are using a currying version of the function due to
      * Scala's bnon-backtracking, left to right inference used in in single
      * parameter list. This means that the [[pt.inescn.models.Base.ML]] type values will only be
      * inferred and used in the other parameters if those parameters on in
      * another (curried) parameter list.
      * @see https://github.com/ljwagerfield/scala-type-inference
      *
      * @param algorithm Algorithm used for fitting and prediction
      * @param args ML algorithm parameters used by the fitting
      * @param metric Metric used to evaluate the goodness of fit
      * @param folds Lazy stream that will split data into testing and training sets
      * @tparam A The Algorithms characteristics (classification, argument types,
      *           data input, etc.)
      * @tparam Z Is the metric used. Type constraints make sure that it is compatible with
      *           the Algorithm
      * @tparam M This is used to lift the type values from within the [[pt.inescn.eval.Base.Metrics]]. This
      *           constraints ensures that all metrics generated and placed in the stream
      *           are of the exact type (not sub-type)
      * @return Stream that allows us to measure the goodness of fit. Lazy evaluation used.
      */
    def prepareCrossValidation[A <: ML,Z <: Metrics[A#Algorithm,_,M], M <: MetricsTypes](algorithm: Algorithm[A])(
                                                               args: A#Arguments,
                                                               metric: MetricEval[A#Algorithm,A#Label,A#Value,A#Prediction,Z,M])
                                                              ( folds: => Stream[(DataIn[A#Label,A#Value], DataIn[A#Label,A#Value])]):
    Stream[Either[ADWError, Z]]
    = {
      val tmp1: Stream[Either[ADWError, Z]] = folds.map {
        case((train,test)) =>
          trainAndEval(algorithm)( args, train, test, metric)
      }
      tmp1
    }

    /**
      * This function performs a parameter search. For each set of
      * parameters obtained from the `args` sampler the data is split into
      * training and test sets. The `algorithm` is then evaluated by fitting,
      * predicting and evaluating performance based on a set of metrics.
      * Note that [[pt.inescn.eval.Base.MetricEval]] will convert the data dependent variable
      * and predictions in order to be compatible wit the algorithm's output.
      * The Type constraints also ensure that the algorithm, metrics and data
      * are "compatible".
      *
      * Samples can be finite or infinite. We must therefore request a given
      * number of samples `argsSampleSize` to use when generating algorithm
      * parameters. We must also indicate the number of folds `nrFolds` to
      * use for cross validation .
      *
      * TODO: how to execute this in parallel (concurrent but not distributed)
      * computing)
      * https://stackoverflow.com/questions/40701215/scala-lazy-parallel-collection-is-possible
      * https://alexn.org/blog/2017/01/30/asynchronous-programming-scala.html
      *
      * @param algorithm Algorithm used for fitting and prediction
      * @param args ML algorithm parameters used by the fitting
      * @param metricEval Used to prepare the lazy evaluation of a specific set
      *                   of metrics
      * @param data data set that will be split into training and test data sets
      * @param nrFolds number of folds to be use in cross validation
      * @param argsSampleSize - number of samples of the algorithm parameters to be
      *                       generated
      * @tparam A The Algorithms characteristics (classification, argument types,
      *           data input, etc.)
      * @tparam Z Is the metric used. Type constraints make sure that it is compatible with
      *           the Algorithm
      * @tparam M This is used to lift the type values from within the [[pt.inescn.eval.Base.Metrics]]. This
      *           constraints ensures that all metrics generated and placed in the stream
      *           are of the exact type (not sub-type)
      * @return Stream that allows us to measure the goodness of fit. Lazy evaluation used.
      *         All results are paired with the parameters. So we can use this to select
      *         the most appropriate parameters.
      */
    def testParams[A <: ML, Z <: Metrics[A#Algorithm,_,M], M <: MetricsTypes](algorithm: Algorithm[A])(
                                                     args: Sampler[_, A#Arguments],
                                                     metricEval: MetricEval[A#Algorithm,A#Label,A#Value,A#Prediction,Z,M],
                                                     data : DataIn[A#Label,A#Value],
                                                     nrFolds : Option[Int] = Some(10),
                                                     argsSampleSize: Option[Int] = Some(1000) ):
    Stream[Either[ADWError, (M#AllStats, A#Arguments)]] = {


      val argsTest: Stream[A#Arguments] = argsSampleSize match {
        case None => args.reset.toStream
        case Some(size) => args.reset.toStream.take(size)
      }
      val nFolds = nrFolds match {
        case None => 10
        case Some(f) => f
      }
      def tmp: Stream[Either[ADWError, (M#AllStats, A#Arguments)]] = argsTest.map { arg =>
        def folds: Either[ADWError, Stream[(DataIn[A#Label,A#Value], DataIn[A#Label,A#Value])]] = data.splitStratifiedFolds(nFolds)
        def evals: Either[ADWError, (M#AllStats, A#Arguments)] = folds.flatMap {
          (f:Stream[(DataIn[A#Label,A#Value], DataIn[A#Label,A#Value])]) =>
            val t: Stream[Either[ADWError, Z]] = prepareCrossValidation(algorithm)( arg, metricEval)(f)
            val stats: Either[ADWError, M#AllStats] = metricEval.stats(t)
            val option: Either[ADWError, (M#AllStats, A#Arguments)] = stats.flatMap(s => Right((s, arg)) )
            option
        }
        evals
      }
      tmp
    }


    /*
      Generation of training and test data-sets
     */

    // TODO: parameterize the pre-processing
    /**
      * This function takes in stream of training and test data sets,
      * applies a transformation to the training set (form example
      * scaling) and then applies this same transform to the test
      * data set (using the training transform scales). Note that
      * one should use lazy stream of the (train, test) data fails
      * in order to ensure that we do not run out of memory.
      *
      * In the destination folder `experimentFolder` file names
      * train_[ID] and test_[ID] are created where ID is a sequential
      * numeric value that makes the file name unique.
      *
      * @param folds lazy stream with tuples containing the training
      *              and test data sets.
      * @param experimentFolder folder were the data files containing
      *                         the preprocessed data sets will be
      *                         deposited.
      * @tparam O generic type representing the data set.
      * @return a list tuples containing the training and test file names
      *         that were generated
      */
    def generateFolds[O <: DataIn[Double,Double]](folds: Stream[(O, O)], experimentFolder: File, preProcess: TransformParams):
    Either[ADWError, List[(TransformParams,File, File)]] = {
      if (!experimentFolder.isDirectory)
        Left(ADWError("Experiment folder is not a directory"))

      val paramsEmpty = List[TransformParams]()
      val filesEmpty = List[File]()
      val zero = (0,paramsEmpty,filesEmpty,filesEmpty)
      val files = folds.foldLeft(zero){ (acc,files) =>
        val (id,params,trains,tests) = acc
        val (train,test) = files
        val trainName = experimentFolder / ("train" + id)
        val testName = experimentFolder / ("test" + id)

        // scale train data and get the scaling parameters
        // default: scaleY = false
        val param = Transform.init(preProcess, train)
        // scale the test data using the scaling parameters from the test data
        Transform.exec(param, test)

        train.saveTo(trainName)
        test.saveTo(testName)

        (id+1, param::params, trainName::trains, testName::tests)
      }
      val r1 = files._2 zip files._3 zip files._4
      val r2 = r1.map(p => (p._1._1, p._1._2, p._2))
      Right(r2)
    }

    /**
      * This function takes in a training data set, a loader that can
      * read tis fle and convert it into a [[pt.inescn.models.Base.DataIn]].
      * This class is then used to lazy generate a set of cross validation
      * training and test data sets. This list is processed so that each tuple
      * of data sets are generated, preprocessed and then saved tio a files.
      * All files are saved to the `experimentFolder`
      *
      * @param trainData Training data set
      * @param loader Class to load and split data into cross validation
      *               training and test data sets. The loader must be
      *               compatible with the file data.
      * @param experimentFolder folder in which to generate the cross
      *                         validation data sets
      * @param nrFold number of splits to generate
      * @tparam O specific [[pt.inescn.models.Base.DataIn]] conversion
      * @return list of the files generated.
      */
    def stratifiedCrossValidationDataFiles[O <: DataIn[Double,Double]](trainData: File, loader: Source[File,O],
                                                                       experimentFolder: File,
                                                                       preProcess: TransformParams,
                                                                       nrFold : Int = 5, delete : Boolean = true):
    Either[ADWError, List[(TransformParams, File, File)]] = {

      if (delete) experimentFolder.delete(true)
      experimentFolder.createDirectory()

      val train = loader.map(trainData)
      for {
        trainMat <- train
        data <- trainMat.splitStratifiedFolds(nrFold)
        files <- generateFolds(data, experimentFolder, preProcess)
      } yield files
    }

    def oneClassCrossValidationDataFiles[O <: DataIn[Double,Double]](trainData: File, loader: Source[File,O],
                                                                       experimentFolder: File,
                                                                       preProcess: TransformParams,
                                                                       nrFold : Int = 5, delete : Boolean = true):
    Either[ADWError, List[(TransformParams, File, File)]] = {

      if (delete) experimentFolder.delete(true)
      experimentFolder.createDirectory()

      val train = loader.map(trainData)
      for {
        trainMat <- train
        data <- trainMat.splitOneClassFolds(nrFold)
        files <- generateFolds(data, experimentFolder, preProcess)
      } yield files
    }

    def crossValidationDataFiles[O <: DataIn[Double,Double]](trainData: File, loader: Source[File,O],
                                                                       experimentFolder: File,
                                                                       preProcess: TransformParams,
                                                                       nrFold : Int = 5, delete : Boolean = true):
    Either[ADWError, List[(TransformParams, File, File)]] = {

      if (delete) experimentFolder.delete(true)
      experimentFolder.createDirectory()

      val train = loader.map(trainData)
      for {
        trainMat <- train
        data <- trainMat.splitFolds(nrFold)
        files <- generateFolds(data, experimentFolder, preProcess)
      } yield files
    }

    /*
     * Initial code for CV with files using LibSVM.
     * Focus is on concurrent parallel computation
     */

    import java.io.{IOException, PrintWriter}
    import java.nio.channels._

    /**
      * Writes a string with the results to a file. This will be used to
      * collect the results of the spawned tasks (threads). It is important to
      * ensure that the file `f` is not being used by another process. Use
      * Java's `FileChannel` to do this.
      *
      * IMPORTANT: The call to this blocking IO should be done withing a
      * `blocking` call of the function that uses this function.
      *
      * @param f - function which will hold the results
      * @param r - type to be saves as text. Either have thi as a string
      *          or ensure that it has a valid `toString`representation.
      * @return - Unit. Side effect
      */
    def logResult[T](f: PrintWriter)(r:T): Unit = {
      f.synchronized{
        f.println(r)
        f.flush()
      }
    }

    import monix.eval.Task
    import monix.reactive.Observable
    import monix.execution.CancelableFuture
    import scala.concurrent.blocking
    import scala.concurrent.Await
    import scala.concurrent.duration._
    import monix.execution.Scheduler


    /**
      * Here we take a machine learning algorithm and use that to fit
      * a model using the training data. This model is then used to
      * predict the results on a test data set. The test data set and
      * the predictions are then used to evaluate the performance
      * metric.
      *
      * @see [[pt.inescn.models.Algorithms.Algo]]
      *
      * @param algorithm machine learning algorithm
      * @param id identifier of the experiment. Used to name state uniquely
      * @param train training data set
      * @param test test data set
      * @param args arguments passed on to the machine learning algorithm
      * @tparam A parameterization of the algorithm that determines the
      *           algorithm type, input, output and parameter types
      * @tparam Model model type
      * @tparam Metric metric type (classification, regression, etc.)
      * @return Returns the learned model and the evaluation
      */
    def trainAndEval[A <: pt.inescn.models.Base.ML,Model,Metric](algorithm: Algo[A,Model,Metric])
                                                                (id:Int,
                                                                 train: algorithm.Train,
                                                                 test: algorithm.Test,
                                                                 args: algorithm.Args): Either[ADWError, (Metric, Model)]
    = {
      for {
        model  <- algorithm.fit(id, train, args)
        prediction <- algorithm.predict(id, test,model)
        metric <- algorithm.evaluate(id, test, prediction)
      } yield (metric,model)
    }

    /**
      * Creates a Monix task that allows us to execute the `trainAndEval`
      * function parallel (concurrency). The output of `trainAndEval` function
      * ([[pt.inescn.eval.Base.Metric]] and `Model` is saved in text format
      * to a Java PrintWriter. We use the [[pt.inescn.models.Algorithms.Algo]]
      * specific methods to convert the output types to a toString method.
      * Note that the input is curried so that the `Task` creation can be
      * delayed.
      *
      * @see [[pt.inescn.models.Algorithms.Algo]]
      *
      * @param file - `PrintWriter` were results are saved.
      * @param algorithm - the algorithm that will be used to fit the models
      *                  and evaluate the performance of the model
      * @param i - input that is passed to `func` to produce the final `Task`.
      *          Note that the input is a tuple with the second member being the
      *          ID of the task.
      * @tparam A - Types used by the algorithm
      * @tparam Model - Model generated by the algorithm
      * @tparam Metric - Metrics generated by the algorithm's evaluation
      * @return - the algorithm's [[pt.inescn.eval.Base.Metric]] and `Model`
      */
    def makeTask[A <: pt.inescn.models.Base.ML,Model,Metric]
    (file: PrintWriter, algorithm : Algo[A,Model,Metric])
    (i: ((TransformParams, algorithm.Train, algorithm.Test, algorithm.Args),Int))
    : Task[Either[ADWError,(Metric,Model)]] =
    {
      def log[U]: U => Unit = logResult(file)
      Task {
        def func = trainAndEval(algorithm) _
        val (result, elapsed) = Utils.timeOf{ func(i._2, i._1._2, i._1._3, i._1._4) }
        val elapsedTime = elapsed.toNanos // Utils.decomposeToString(elapsed)
        val in = (i._1._2, i._1._3, i._1._4)
        val input = algorithm.inputToString(in)
        import io.circe.syntax._
        val output = result.fold( err => err.msg.asJson, r => algorithm.resultToString(r))
        val out = saveResult(i._2, elapsedTime, input, i._1._1, output)
        //println(out)
        blocking(log(out))
        result
      }
    }

    /**
      * Takes in a line splits it by the field separator. The first field
      * is converted into a [[scala.Long]] and is the experiment ID. The second
      * is converted into a [[scala.Long]] and is the time taken to execute the
      * experiment in nanoseconds. The last two fields represent the
      * [[pt.inescn.models.Algorithms.Algo]] input and output respectively.
      * They are intended to be parsed by the
      * [[pt.inescn.models.Algorithms.Algo.parseInput]] and
      * [[pt.inescn.models.Algorithms.Algo.parseResult]]
      * functions.
      *
      * @param line result file line that is to be parsed
      * @return returns a tuple with the experiment ID, the time taken
      *         to execute the experiment in nanoseconds and the
      *         machine learning algorithm's input and output respectively.
      */
    def parseResulti(separator:String)(line:String): (Long, Long, String, String) = {
      val token: Array[String] = line.split(separator)
      val id:Long = token.applyOrElse(0, (_:Int) => "-1").toLong
      val elapsed = token.applyOrElse(1, (_:Int) => "0").toLong
      val input = token.applyOrElse(2, (_:Int) => "")
      val output = token.applyOrElse(3, (_:Int) => "")
      //println(s"id = $id .. input = $input")
      (id, elapsed, input, output)
    }

    import io.circe._
    import io.circe.generic.auto._
    import io.circe.parser._
    //import pt.inescn.utils.CirceCoders._
    // import io.circe.syntax._ asJason

    /**
      * Wrapper class to holds result file data. Note that
      * the [[pt.inescn.models.Algorithms.Algo]] are expressed in
      * Json and allow us to "plug-in" algorithms hat use different
      * data. Parsing is done by the [[pt.inescn.models.Algorithms.Algo]]
      *
      * @see [[SearchResult]]
      *
      * @param id ID of the experiment
      * @param elapsedTime time of execution of the experiment in nanoseconds
      * @param input [[pt.inescn.models.Algorithms.Algo]] input
      * @param preProcessing [[pt.inescn.etl.Transform.TransformParams]]
      * @param result [[pt.inescn.models.Algorithms.Algo]] output
      */
    case class TaskResult(id:Long, elapsedTime: Long, input:Json, preProcessing: TransformParams, result:Json)

    def saveResult(id:Long, elapsedTime: Long, input:Json, preProcessing: TransformParams, result:Json):String = {
      import io.circe.syntax._
      val r = TaskResult(id, elapsedTime, input, preProcessing, result)
      r.asJson.noSpaces
    }

    /**
      * Takes in a line splits ans parsers it according to the [[TaskResult]]
      * case class using Circe's  automated. [[TaskResult]] holds the
      * [[scala.Long]] experiment ID, the  [[scala.Long]] time taken to
      * execute the experiment in nanoseconds. The last two fields represent the
      * [[pt.inescn.models.Algorithms.Algo]] input and output respectively.
      * They are intended to be parsed by the
      * [[pt.inescn.models.Algorithms.Algo.parseInput]] and
      * [[pt.inescn.models.Algorithms.Algo.parseResult]]
      * functions.
      *
      * @see [[SearchResult]]
      *
      * @param line result file line that is to be parsed
      * @return returns a [[TaskResult]] with the experiment ID, the
      *         time taken to execute the experiment in nanoseconds
      *         and the machine learning algorithm's input and output
      *         respectively.
      */
    def parseResult[Model](line:String): Either[ADWError, TaskResult] = {
      val tmp: Either[Error, TaskResult] = decode[TaskResult](line)
      tmp match  {
        case Left(e) => Left(ADWError(e.getMessage + e.fillInStackTrace().toString))
        case Right(r) => Right(r)
      }
    }

    val TotalElapsedTime = "Total elapsed time"

    /**
      * Here we create a file in which to log the results of the experiment.
      * This can allow us t analyze the results, perform statistical analysis
      * or simple select the best performing Model based on the
      * [[pt.inescn.eval.Base.Metrics]]. Note that any previously exiting file
      * is **deleted**. An attempt also tries to lock it. The avoids JVMs
      * attempting to use the same log file concurrently (other JVMs must
      * also attempt lock).
      *
      * @param resultFile File into which data must be logged
      * @return
      */
    def createResultsFile(resultFile: File): Either[ADWError, (FileLock, PrintWriter)] = {
      resultFile.delete(true)
      resultFile.createIfNotExists()
      val channel: FileChannel = resultFile.newFileChannel(File.OpenOptions.append)
      val lock = channel.tryLock()
      try {
        if (lock == null)
          Left(ADWError("OverlappingFileLockException: file already locked by another thread"))
        else {
          val results: PrintWriter = resultFile.newPrintWriter()
          Right((lock,results))
        }
      } catch {
        case e : ClosedChannelException => Left(ADWError(e.getMessage + e.getStackTrace))
        case e : AsynchronousCloseException => Left(ADWError(e.getMessage + e.getStackTrace))
        case e : FileLockInterruptionException => Left(ADWError(e.getMessage + e.getStackTrace))
        case e : OverlappingFileLockException => Left(ADWError(e.getMessage + e.getStackTrace))
        case e : NonWritableChannelException => Left(ADWError(e.getMessage + e.getStackTrace))
        case e : IOException => Left(ADWError(e.getMessage + e.getStackTrace))
      }
    }

    /**
      * Creates a Monix task that allows us to execute the function `func` in
      * parallel (concurrency). The output of `func` is saved in text format
      * to a Java `PrintWriter`. We assume that the output type of `func` has
      * a valid `toString` method. Note that the input is curried so that the
      * `Task`s creation can be delayed.
      * This function is used for basic testing of Monix related code
      *
      * @param separator - string used to split saved fields when recording to file.
      * @param file `PrintWriter` were results are saved.
      * @param func generic function that takes in an input `S` and experiment
      *             ID and outputs a `T`.
      * @param i - input that is passed to `func` to produce the final `Task`.
      *          Note that the input is a tuple with the second member being the
      *          ID of the task.
      * @tparam S type of input
      * @tparam T type of output
      * @return the `func` output
      */
    def makeTaski[S,T](separator: String, file: PrintWriter, func : S => T)(i: (S,Int)) : Task[T] = {
      def log[U]: U => Unit = logResult(file)
      Task {
        val (rr, elapsed) = Utils.timeOf{ func(i._1) }
        val elapsedTime = elapsed.toNanos // Utils.decomposeToString(elapsed)
        val out = i._2.toString + separator + elapsedTime + separator + i._1 + separator + rr.toString
        //println(out)
        blocking(log(out))
        rr
      }
    }

    /**
      * Generic function that uses Monix to execute the function `func`.
      * This function take in a generic type `S` and a numeric identifier
      * of the experiment and returns a Monix task with an output type `T`.
      * This task will be execute in parallel for each input in the `data`
      * stream. The implicit parameter lets a user specify implicitly or
      * explicitly the type os task scheduling to use.
      *
      * @param parallelism number of threads to execute concurrently
      * @param duration length of time to wait for results. If the
      *                 timeout occurs befiore the results are all
      *                 in, then rest of the tasks are NOT
      *                 executed.
      * @param resultsFile file which holds the results of the task executions
      * @param func function used to create the concurrent Monix tasks.
      * @param data stream on input data for the Monix tasks
      * @param s the concurrency model used to schedule the Monix tasks
      * @tparam S task input type
      * @tparam T task output type
      * @return returns the time (in nanoseconds) the experiment
      *                took. If this time is less than the time-out, then
      *                we know that all planned tasks completed. If an error
      *                is returned, then at least one task failed. Note that
      *                as soon as a task fails, all pending tasks are discarded
      *                and concurrent execution stops. Make sure you catch
      *                all exceptions within a task in order to execute as
      *                many tasks as possible.
      */
    def execFunci[S,T](parallelism: Int, duration: Duration, resultsFile: PrintWriter, func : ((S,Int)) => Task[T])
                     (data: Stream[S])
                     (implicit s: Scheduler): Either[ADWError, FiniteDuration] = {
      try {
        val (_, elapsed) = Utils.timeOf {

          // Create the Observable that executes task on each event
          val dataWithIDS = data.zipWithIndex
          val source = Observable.fromIterable(dataWithIDS)
          val processed = source.mapAsync(parallelism = parallelism)(func)
          // Execute all tasks until it is finished or time-out
          val t1: Task[Unit] = processed.completedL
          val r1: CancelableFuture[Unit] = t1.runAsync
          Await.result(r1, duration)
        }
        val elapsedStr = elapsed.toNanos.toString // Utils.decomposeToString(elapsed)
        val msg = s"$TotalElapsedTime = $elapsedStr"
        logResult(resultsFile)(msg)
        Right(elapsed)
      } catch {
        case e: Throwable => Left(ADWError("Concurrent processing failed: " + e.getMessage))
      } finally {
        resultsFile.close()
      }
    }

    /**
      * Function that uses Monix to execute evaluation of a machine learning
      * algorithm. This function take in the algorithms input (train data,
      * test data, arguments) and a numeric identifier of the experiment and
      * returns a Monix task with an output of a [[pt.inescn.eval.Base.Metric]]
      * and a `Model`. This task will be execute in parallel for each input in
      * the `data` stream. The implicit parameter lets a user specify implicitly
      * or explicitly the type os task scheduling to use.
      *
      * @see [[execFunci]], [[pt.inescn.models.Algorithms.Algo]]
      *
      * @param parallelism number of threads to execute concurrently
      * @param duration length of time to wait for results. If the
      *                 timeout occurs before the results are all
      *                 in, then rest of the tasks are NOT
      *                 executed.
      * @param resultsFile file which holds the results of the task
      *                    executions
      * @param algorithm algorithm used in the concurrent Monix task that
      *                  will evaluate its performance.
      * @param data stream on input data for the algorithm tasks
      * @param s the concurrency model used to schedule the Monix tasks
      * @tparam A Algorithm [[pt.inescn.models.Algorithms.Algo]] parameters
      * @tparam Model model generated by the machine learning algorithm
      * @tparam Metric evaluation ofd the algorithm's performance
      * @return returns the time (in nanoseconds) the experiment
      *                took. If this time is less than the time-out, then
      *                we know that all planned tasks completed. If an error
      *                is returned, then at least one task failed. Note that
      *                as soon as a task fails, all pending tasks are discarded
      *                and concurrent execution stops. Make sure you catch
      *                all exceptions within a task in order to execute as
      *                many tasks as possible.
      */
    def execFunc[A <: pt.inescn.models.Base.ML,Model,Metric](parallelism: Int, duration: Duration, resultsFile: PrintWriter,
                                                             algorithm : Algo[A,Model,Metric])
                                                            (data: Stream[(TransformParams,algorithm.Train, algorithm.Test, algorithm.Args)])
                                                            (implicit s: Scheduler): Either[ADWError, FiniteDuration] = {
      def task = makeTask(resultsFile, algorithm) _
      execFunci(parallelism, duration, resultsFile, task)(data)
    }

    /**
      * This function generates a cross product of the list of cross
      * validation training and test data files with the set of parameters
      * to search. For each combination a concurrent task that evaluates
      * the algorithms performance is generated. These tasks are executed
      * concurrently (multi-core) until either all tasks are executed or
      * a time-out occurs, which ever comes first.
      *
      * NOTE: if any task raises an exception all pending tasks are not
      * executed and the exception will be recorded.
      *
      * @see [[stratifiedCrossValidationDataFiles]], [[execFunc]]
      * @param numThreads     number of threads to execute concurrently
      * @param timeOut        length of time to wait for results. If the
      *                       timeout occurs before the results are all
      *                       in, then rest of the tasks are NOT
      *                       executed.
      * @param logFile        file which holds the results of the task
      *                       executions
      * @param algorithm      algorithm used in the concurrent Monix task that
      *                       will evaluate its performance.
      * @param foldFiles      these are the files with the training and test
      *                       data sets. These files can be generated by the
      *                       function [[stratifiedCrossValidationDataFiles]].
      *                       Each of these files will be trained with the
      *                       same et of arguments. In effect one can do cross
      *                       validation.
      * @param args           set of arguments that will be used by the algorithm.
      * @param argsSampleSize number of samples to be drawn from `args`.
      *                       If the stream is generated from random sequences,
      *                       then we can set this value freely. If it is from
      *                       a finite sequence then one should be careful to
      *                       request at least many as their are in that
      *                       stream.
      * @param s the concurrency model used to schedule the Monix tasks
      * @tparam A Algorithm [[pt.inescn.models.Algorithms.Algo]] parameters
      * @tparam Model model generated by the machine learning algorithm
      * @tparam Metric evaluation ofd the algorithm's performance
      * @return duration of executing all the tasks or an error
      */
    def searchParams[A <: pt.inescn.models.Base.ML,Model,Metric](numThreads: Int,
                                                                 timeOut: Duration,
                                                                 logFile: Either[ADWError, (FileLock, PrintWriter)],
                                                                 algorithm: Algo[A,Model,Metric])
                                                                (foldFiles: Either[ADWError, List[(TransformParams, algorithm.Train, algorithm.Test)]],
                                                                 args: Base.Sampler[_, algorithm.Args],
                                                                 argsSampleSize: Option[Int] = Some(1000))(implicit s: Scheduler):
    Either[ADWError, FiniteDuration] =
    {
      logFile.flatMap{ log =>
        foldFiles.flatMap { data: immutable.Seq[(TransformParams, algorithm.Train, algorithm.Test)] =>
          val argsTest: Stream[algorithm.Args] = argsSampleSize match {
            case None => args.reset.toStream
            case Some(size) => args.reset.toStream.take(size)
          }
          val experiment: Stream[(TransformParams, algorithm.Train, algorithm.Test, algorithm.Args)] => Either[ADWError, FiniteDuration] =
            ParamSelect.execFunc(numThreads, timeOut, log._2, algorithm)
          val crossProduct: Stream[(TransformParams,algorithm.Train, algorithm.Test, algorithm.Args)] = for {
            (pre, train, test) <- data.toStream
            arg <- argsTest
          } yield (pre, train, test, arg)
          experiment(crossProduct)
        }
      }
    }

    /**
      * Generic wrapper class that holds data that will be parsed
      * from the result file.
      *
      * @see [[TaskResult]]
      *
      * @param id ID of the experiment
      * @param elapsedTime time of execution of the experiment in nanoseconds
      * @param input [[pt.inescn.models.Algorithms.Algo]] input
      * @param preProcessing [[pt.inescn.etl.Transform.TransformParams]]
      * @param result [[pt.inescn.models.Algorithms.Algo]] output
      * @tparam I type of [[pt.inescn.models.Algorithms.Algo]] input
      * @tparam O type of [[pt.inescn.models.Algorithms.Algo]] output
      */
    case class SearchResult[I,P,O](id:Long, elapsedTime: Long, input:I, preProcessing: P, result:O)

    /**
      * Here we read the experiment results from the `resultFile` using the
      * appropriate `separator` and `algorithm`. Note that if an error has
      * occurred the the [[scala.Either]] can be used to detect this.
      *
      * @param resultFile - `PrintWriter` were results are saved.
      * @param algorithm - the algorithm that will be used to fit the models
      *                  and evaluate the performance of the model
      * @tparam A Algorithm [[pt.inescn.models.Algorithms.Algo]] parameters
      * @tparam Model model generated by the machine learning algorithm
      * @tparam Metric evaluation ofd the algorithm's performance
      * @return iterator to all of the parsed data that resulted from the
      *         tasks executions
      */
    def parseSearchResults[A <: pt.inescn.models.Base.ML,Model,Metric](resultFile: File,
                                                                       algorithm: Algo[A,Model,Metric]):
    Iterator[Either[ADWError, SearchResult[(algorithm.Train, algorithm.Test, algorithm.Args), TransformParams, (Metric, Model)]]]
    = {
      val t1: Iterator[String] = resultFile.lineIterator
        .filter( !_.startsWith(ParamSelect.TotalElapsedTime)) // ignore last line that records total processing time

      val t2: Iterator[Either[ADWError, TaskResult]] = t1.map(s => ParamSelect.parseResult(s))

      val t3 = t2.map(ss =>
        for {
          cv <- ss
          in <- algorithm.parseInput(cv.input)
          out <- algorithm.parseResult(cv.result)
        } yield {
             SearchResult(cv.id, cv.elapsedTime, in, cv.preProcessing, out)
        }
      )
      t3
    }


    /**
      * Takes the parsed data from the result file and orders it by the
      * parameters. This lets us group the experiments by each cross-validation
      * set. Each group should have the same number of entries as the number
      * of cross-validation folds.
      *
      * @param algorithm - the algorithm that will be used to fit the models
      *                  and evaluate the performance of the model
      * @param data - iterator to all of the parsed data that resulted from
      *             the tasks executions
      * @tparam A Algorithm [[pt.inescn.models.Algorithms.Algo]] parameters
      * @tparam Model model generated by the machine learning algorithm
      * @tparam Metric evaluation ofd the algorithm's performance
      * @return list with the result data ordered by the parameters and then
      *         by the experiment ID
      */
    def sortResultsByParameters[A <: pt.inescn.models.Base.ML,Model,Metric]
    (algorithm: Algo[A,Model,Metric])
    (data: Iterator[Either[ADWError, SearchResult[(algorithm.Train, algorithm.Test, algorithm.Args), TransformParams, (Metric, Model)]]]):
    List[Either[ADWError, SearchResult[(algorithm.Train, algorithm.Test, algorithm.Args), TransformParams, (Metric, Model)]]] = {

      type E = Either[ADWError, SearchResult[(algorithm.Train, algorithm.Test, algorithm.Args), TransformParams, (Metric, Model)]]
      def compare(a:E, b :E) : Int = {
        (a,b) match {
          case (Left(_),Left(_)) => 0
          case (Left(_),Right(_)) => -1
          case (Right(_),Left(_)) => +1
          case (Right(ea),Right(eb)) => algorithm.compare(ea.input, eb.input)
        }
      }
      type L = Either[ADWError, SearchResult[(algorithm.Train, algorithm.Test, algorithm.Args), TransformParams, (Metric, Model)]]
      def lt(a:L, b:L) : Boolean = {
        val r = compare(a,b)
        val aa = if (a.isRight) a.right.get.id else 0
        val bb = if (b.isRight) b.right.get.id else 0
        (r < 0.0) || ((r == 0.0) && (aa < bb))
      }
      data.toList.sortWith(lt)
    }

    /**
      * Takes the parsed data from the result file and groups it by the
      * parameters. This lets us group the experiments by each cross-validation
      * set. Each group should have the same number of entries as the number
      * of cross-validation folds.
      *
      * @param algorithm - the algorithm that will be used to fit the models
      *                  and evaluate the performance of the model
      * @param data - iterator to all of the parsed data that resulted from
      *             the tasks executions
      * @tparam A Algorithm [[pt.inescn.models.Algorithms.Algo]] parameters
      * @tparam Model model generated by the machine learning algorithm
      * @tparam Metric evaluation ofd the algorithm's performance
      * @return
      */
    def groupResultsByParameters[A <: pt.inescn.models.Base.ML,Model,Metric]
    (algorithm: Algo[A,Model,Metric])
    (data: Iterator[Either[ADWError, SearchResult[(algorithm.Train, algorithm.Test, algorithm.Args), TransformParams, (Metric, Model)]]]):
    Map[algorithm.Args, List[Either[ADWError, SearchResult[(algorithm.Train, algorithm.Test, algorithm.Args), TransformParams, (Metric, Model)]]]]
    = {
      data.toList.filter(_.isRight).groupBy(_.right.get.input._3)
    }

    /**
      * Takes the results parsed data and applies a function to all of the
      * result's metric aggregated by each algorithm's input parameters. This
      * is used to for example calculate the the descriptive statistics of the
      * metrics.
      *
      * @param algorithm - the algorithm that will be used to fit the models
      *                  and evaluate the performance of the model
      * @param func - function used to transform all results of the experiments
      *             using the same input arguments to an output of type `T`
      * @param data - iterator to all of the parsed data that resulted from
      *             the tasks executions
      * @tparam T output of `func`
      * @tparam A Algorithm [[pt.inescn.models.Algorithms.Algo]] parameters
      * @tparam Model model generated by the machine learning algorithm
      * @tparam Metric evaluation of the algorithm's performance
      * @return returns the type `T` for each all experiments using the same
      *         input parameters
      */
    def applyResultsByParameters[T, A <: pt.inescn.models.Base.ML,Model,Metric]
    (algorithm: Algo[A,Model,Metric])
    (func: List[SearchResult[(algorithm.Train, algorithm.Test, algorithm.Args), TransformParams, (Metric, Model)]] => T)
    (data: Map[algorithm.Args, List[Either[ADWError, SearchResult[(algorithm.Train, algorithm.Test, algorithm.Args), TransformParams, (Metric, Model)]]]]):
    Map[algorithm.Args, T]
    = {
      data.map { e =>
        val (k,v) = e
        //val t = v.map(_._4).filter(_.isRight).map(_.right.get._1)
        val t = v.filter(_.isRight).map(_.right.get)
        (k,func(t))
      }
    }

    /*
       Utility functions for scoring the ML algorithm
    */

    /**
      * Gets the result from a [[SearchResult]]
      *
      * @param algorithm - the algorithm that will be used to fit the models
      *                  and evaluate the performance of the model
      * @param out - output of the experiments. See [[SearchResult]]
      * @tparam A Algorithm [[pt.inescn.models.Algorithms.Algo]] parameters
      * @tparam Model model generated by the machine learning algorithm
      * @tparam Metric evaluation of the algorithm's performance
      * @return returns the result field of [[SearchResult]]
      */
    def getResult[A <: pt.inescn.models.Base.ML,Model,Metric]
    (algorithm: Algo[A,Model,Metric])
    (out: SearchResult[(algorithm.Train, algorithm.Test, algorithm.Args), TransformParams, (Metric, Model)]): (Metric, Model) = {
      out.result
    }

    /**
      * Gets the `Metric` fields from from a [[SearchResult]] result
      *
      * @param algorithm - the algorithm that will be used to fit the models
      *                  and evaluate the performance of the model
      * @tparam A Algorithm [[pt.inescn.models.Algorithms.Algo]] parameters
      * @tparam Model model generated by the machine learning algorithm
      * @tparam Metric evaluation of the algorithm's performance
      * @return returns the `Metric` field from the  result field of [[SearchResult]]
      */
    def getMetric[A <: pt.inescn.models.Base.ML,Model,Metric]
    (algorithm: Algo[A,Model,Metric])
    (result: (Metric, Model)): Metric = {
      result._1
    }

    /**
      * Helper function to score the metrics generated by experiments.
      * The `calc` function is applied to a list of `Metric` to produce
      * a generic `T`. We can use this for example by processing binary
      * classification metrics calculating the statistics and then selecting
      * and combining the statistics to form a score. These statistics can
      * then be used as a selection criteria for the best model. This is usually
      * used in conjunction with [[applyResultsByParameters]] that groups the
      * results
      *
      * @see [[applyResultsByParameters]]
      *
      * @param algorithm - the algorithm that will be used to fit the
      *                  models and evaluate the performance of the model
      * @param calc - function that takes a set of Metrics and generates
      *             a score
      * @param out - output of the experiments. See [[SearchResult]]
      * @tparam T - score type
      * @tparam A Algorithm [[pt.inescn.models.Algorithms.Algo]] parameters
      * @tparam Model model generated by the machine learning algorithm
      * @tparam Metric evaluation of the algorithm's performance
      * @return returns the type `T` when `calc` is applied to all
      *         metrics
      */
    def score[T, A <: pt.inescn.models.Base.ML,Model,Metric]
    (algorithm: Algo[A,Model,Metric])
    (calc: Seq[Metric] => T)
    (out: Seq[SearchResult[(algorithm.Train, algorithm.Test, algorithm.Args), TransformParams, (Metric, Model)]]): T = {
      val r1 = out.map( getResult(algorithm) )
      val r2 = r1.map( getMetric(algorithm) )
      calc(r2)
    }

    /**
      * This is a utility function that is used to calculate the statistics
      * of the parameter search. The algorithm generates metrics for each
      * abd here we generate the statistics iof these metrics. The
      * algorithms mus generate te compatible metrics in order to use this
      * functions.
      *
      * See the `ModelSpec` test ort an example.
      *
      * @param l list of metrics calculated by the ML algorithm that can be
      *          used to score the results
      * @return returns the descriptive statistics of the algorithm's metrics
      */
    def statsBinaryClassMetric(l: Seq[BinaryClassificationMetrics#All]): BinaryClassificationMetrics#AllColumnsStats = {
      val recall = l.map(_.recall.value)
      val precision = l.map(_.precision.value)
      val accuracy = l.map(_.accuracy.value)
      val mcc = l.map(_.mcc.value)
      val FScore1 = l.map(_.fscore_1.value)
      val FScore2 = l.map(_.fscore_2.value)
      val FScore05 = l.map(_.fscore_05.value)
      val gmeasure = l.map(_.gmeasure.value)
      BinaryClassifications(
        Recall(DescriptiveStats.batch(recall)),
        Precision(DescriptiveStats.batch(precision)),
        Accuracy(DescriptiveStats.batch(accuracy)),
        MCC(DescriptiveStats.batch(mcc)),
        FScore(FBeta(1.0),DescriptiveStats.batch(FScore1)),
        FScore(FBeta(2.0),DescriptiveStats.batch(FScore2)),
        FScore(FBeta(0.5),DescriptiveStats.batch(FScore05)),
        GMeasure(DescriptiveStats.batch(gmeasure))
      )
    }


    /**
      * This is a utility function that is used to calculate the statistics
      * of the parameter search. The algorithm generates metrics for each
      * abd here we generate the statistics iof these metrics. The
      * algorithms mus generate te compatible metrics in order to use this
      * functions.
      *
      * See the `ModelSpec` test ort an example.
      *
      * @param l list of metrics calculated by the ML algorithm that can be
      *          used to score the results
      * @return returns the descriptive statistics of the algorithm's metrics
      */
    def statsRegressionClassMetric(l: Seq[RegressionMetrics#All]): RegressionMetrics#AllColumnsStats = {
      val max = l.map(_.max.value)
      val min = l.map(_.min.value)
      val mean = l.map(_.mean.value)
      val mse = l.map(_.mse.value)
      val mpe = l.map(_.mpe.value)
      val rmse = l.map(_.rmse.value)
      val nrmse = l.map(_.nrmse.value)
      val cvrmse = l.map(_.cvrmse.value)
      val abse = l.map(_.abse.value)
      val mae = l.map(_.mae.value)
      val mape = l.map(_.mape.value)
      val mase = l.map(_.mase.value)
      Regressions(
        MAX(DescriptiveStats.batch(max)),
        MIN(DescriptiveStats.batch(min)),
        MEAN(DescriptiveStats.batch(mean)),
        MSE(DescriptiveStats.batch(mse)),
        MPE(DescriptiveStats.batch(mpe)),
        RMSE(DescriptiveStats.batch(rmse)),
        NRMSE(DescriptiveStats.batch(nrmse)),
        CVRMSE(DescriptiveStats.batch(cvrmse)),
        ABSE(DescriptiveStats.batch(abse)),
        MAE(DescriptiveStats.batch(mae)),
        MAPE(DescriptiveStats.batch(mape)),
        MASE(DescriptiveStats.batch(mase))
      )
    }

  } // ParamSelect


  ////////////////////////////////////////////////////////


  /* TODO
  // https://robjhyndman.com/hyndsight/tscv/
  // https://github.com/clulab/reach/blob/master/assembly/src/main/scala/org/clulab/reach/assembly/relations/classifier/ClassifyAssemblyRelations.scala
  // https://gerardnico.com/wiki/data_mining/cross_validation
  // http://www.scala-lang.org/old/node/4584
  // https://stackoverflow.com/questions/23461023/method-parameter-of-dependent-type-of-another-implicit-method-parameter-is-it
  */
}
