/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.models

import better.files.File
//import java.io.{File => JFile}
import com.github.cjlin1.SVM
import org.hipparchus.random.JDKRandomGenerator
import io.circe._

import pt.inescn.eval._
import pt.inescn.models.Base._
import pt.inescn.models.Base.AlgorithmType._
import pt.inescn.models.Data.DenseVectorData
import pt.inescn.params.ParamSearch._
import pt.inescn.utils.ADWError


/**
  * This object contains access to simple algorithms via the standard
  * interface [[pt.inescn.models.Base.Algorithm]]. This interface provides
  * access to a description of the algorithm parameters. These parameters can
  * then be processed to automatically generate a set of (possibly all)
  * parameter values. Each record of the parameter values can then be fed to
  * the algorithm for fitting (learning) and model generation (semi) during
  * automated machine learning.
  *
  * TODO:
  * No support is yet provided for algorithms that require complex
  * parameterization, such as topology of neural networks. In these cases each
  * NN topology can be coded and wrapped in a standard interface
  *
  * Created by hmf on 12-10-2017.
  *
  * @see [[pt.inescn.models.Data.SparseMatrixData]] [[pt.inescn.models.Data.DenseMatrixData]]
  *     [[pt.inescn.models.Base.Algorithm]]
  */
object Algorithms {

  trait TestAlgorithm1SetUp extends ML {
    override type Algorithm = BinaryClassification
    override type Label = Double
    override type Value = Double
    override type Prediction = Double
    override type Arguments = (String, String)
  }

  case class TestAlgorithm1() extends Algorithm[TestAlgorithm1SetUp] {

    type Value = TestAlgorithm1SetUp#Value
    type Label = TestAlgorithm1SetUp#Label
    override type Parameters = PairThese[String, String]

    class Model(val doPerfectClassification: Boolean)

    val rnd = new JDKRandomGenerator(9876)
    def inRange(rangeMin: Double, rangeMax: Double): Double = rangeMin + ((rangeMax - rangeMin) * rnd.nextDouble)
    // Real values mapped to false
    def falseRange: Label = inRange(0.0, 0.49)
    // Real values mapped to true
    def trueRange: Label = inRange(0.5, 0.99)
    def flipBoolean(v:Double): Label = if (v > 0.5) falseRange else trueRange

    // Parameter description
    val q1 = Constant("kernel 1")
    val q2 = Constant("kernel 2")
    val q3 = Constant("kernel 3")
    val q4 = Items(List("k1p1", "k1p2", "p3"))
    val q5 = Items(List("k2p1", "k2p2", "p3"))
    val q6 = Items(List("k3p1", "k3p2", "k3p4"))
    val q7: Seq[(Constant[String], Items[String])] = List((q1,q4), (q2,q5), (q3,q6))
    val q8 = PairThese(q7)

    override val params: Parameters = q8

    override def fit(data: DataIn[Label,Value], params: (String, String)): Either[ADWError,Model] = {
      // Best parameter
      if ((params._1 == q1.c) && (params._2 == q4.e.head)){
        Right(new Model(true))
      } else {
        // others don't work
        Right(new Model(false))
      }
    }
    override def predict(data: DataIn[Label,Value], m: Model): Either[ADWError, Prediction[Label]] = {
      if (m.doPerfectClassification) {
        val labels: Array[Label] = data.getColumn(0)
        val i1 = rnd.nextInt(data.size._1)
        val i2 = rnd.nextInt(data.size._1)
        labels(i1) = flipBoolean(labels(i1))
        labels(i2) = flipBoolean(labels(i2))
        val predictions: DenseVectorData = DenseVectorData(labels)
        Right(predictions)
      } else {
        // others don't work
        //val d: Array[Label] = Array.fill[Label](data.size._1)(0.0)
        val d: Array[Label]  = (0 until data.size._1).map(_ => rnd.nextDouble() ).toArray
        val predictions: DenseVectorData = DenseVectorData(d)
        Right(predictions)
      }
    }
  }

  /*
  SVMLibSpec
  (Astroparticle Physics)
  /svm-scale -l -1 -u 1 -s range1 svmguide1 > svmguide1.scale
$ ./svm-scale -r range1 svmguide1.t > svmguide1.t.scale
$ ./svm-train svmguide1.scale
$ ./svm-predict svmguide1.t.scale svmguide1.scale.model svmguide1.t.predict


cacheSize: Int = defaultI, epsilon: Double = defaultD,
                                                  shrinking: Boolean = defaultB, probabilityEstimates: Boolean = defaultB,
                                                  v: Int = defaultI, q: Boolean = defaultB,
                                                  algorithm: SVMAlgorithm[T] = DefaultAlgorithm(),
                                                  kernel: Kernel=DefaultKernel())
  val defaultD: Double = Double.NaN
  val defaultI: Int = Integer.MIN_VALUE
  val defaultB = false
  val defaultType: SVM.SVMAlgorithm[BinaryClassification] = DefaultAlgorithm()
  val defaultKernel: SVM.Kernel = DefaultKernel()

		+"-s svm_type : set type of SVM (default 0)\n"
		+"	0 -- C-SVC		(multi-class classification)\n"    <------------
		+"	1 -- nu-SVC		(multi-class classification)\n"
		+"	2 -- one-class SVM\n"
		+"	3 -- epsilon-SVR	(regression)\n"
		+"	4 -- nu-SVR		(regression)\n"
		+"-t kernel_type : set type of kernel function (default 2)\n"
		+"	0 -- linear: u'*v\n"
		+"	1 -- polynomial: (gamma*u'*v + coef0)^degree\n"
		+"	2 -- radial basis function: exp(-gamma*|u-v|^2)\n"  <-------------
		+"	3 -- sigmoid: tanh(gamma*u'*v + coef0)\n"
		+"	4 -- precomputed kernel (kernel values in training_set_file)\n"
		+"-d degree : set degree in kernel function (default 3)\n"            <----------------
		+"-g gamma : set gamma in kernel function (default 1/num_features)\n"  <---------------- ?
		+"-r coef0 : set coef0 in kernel function (default 0)\n"
		+"-c cost : set the parameter C of C-SVC, epsilon-SVR, and nu-SVR (default 1)\n"  <--------- ?
		+"-n nu : set the parameter nu of nu-SVC, one-class SVM, and nu-SVR (default 0.5)\n"
		+"-p epsilon : set the epsilon in loss function of epsilon-SVR (default 0.1)\n"
		+"-m cachesize : set cache memory size in MB (default 100)\n"
		+"-e epsilon : set tolerance of termination criterion (default 0.001)\n"
		+"-h shrinking : whether to use the shrinking heuristics, 0 or 1 (default 1)\n"
		+"-b probability_estimates : whether to train a SVC or SVR model for probability estimates, 0 or 1 (default 0)\n"
		+"-wi weight : set the parameter C of class i to weight*C, for C-SVC (default 1)\n"
		+"-v n : n-fold cross validation mode\n"
		+"-q : quiet mode (no outputs)\n"

   */


  // TODO: remove, change and use Algorithm
  // TODO: add comments
  trait Algo[S <: ML, Model, Metric] {
    type Train
    type Test
    type Prediction
    type Args = S#Arguments
    type Parameters <: Param[Args]

    val params: Parameters

    def fit(id: Int, trainData: Train, params: Args): Either[ADWError,Model]
    def predict(id: Int, testData: Test, m: Model): Either[ADWError,Prediction]
    def evaluate(id: Int, testData: Test, prediction: Prediction) : Either[ADWError, Metric]

    // TODO: change to case for easier reading?
    type In = (Train,Test,Args)
    type Out = (Metric,Model)

    def inputToString(in:(Train,Test,Args)) : Json
    def resultToString(out:(Metric,Model)) : Json
    def parseInput(in:Json) : Either[ADWError, (Train,Test,Args)]
    def parseResult(out:Json) : Either[ADWError, (Metric,Model)]
    def compare(a:In, b:In): Int
  }


  case class SVMModel(modelFile: File)

  /**
    * -s svm_type : set type of SVM (default 0)
    *  0 -- C-SVC		(multi-class classification)
    *
    * -t kernel_type : set type of kernel function (default 2)
    *  2 -- radial basis function: exp(-gamma*|u-v|**2)
    *
    * These will be used as searchable parameters
    * -g gamma : set gamma in kernel function (default 1/num_features)
    * -c cost : set the parameter C of C-SVC, epsilon-SVR, and nu-SVR (default 1)
    * TODO		+"-wi weight : set the parameter C of class i to weight*C, for C-SVC (default 1)\n"
    *
    * -m cachesize : set cache memory size in MB (default 100)
    * -e epsilon : set tolerance of termination criterion (default 0.001)
    * -h shrinking : whether to use the shrinking heuristics, 0 or 1 (default 1)
    * -b probability_estimates : whether to train a SVC or SVR model for probability estimates, 0 or 1 (default 0)
    * -v n : n-fold cross validation mode (grid.py uses 5)
    * -q : quiet mode (no outputs)
    */
  trait MultiClassRadialBasisSVMSetUp extends ML {
    override type Algorithm = BinaryClassification
    override type Label = Double
    override type Prediction = Double
    override type Arguments = (Double, Double)
  }

  case class MultiClassRadialBasisSVM(outDir: String) extends Algo[MultiClassRadialBasisSVMSetUp,SVMModel,BinaryClassificationMetrics#All] {

    type Label = MultiClassRadialBasisSVMSetUp#Label
    override type Parameters = Composite[Double, Double]
    override type Train = File
    override type Test = File
    override type Prediction = File

    val c = PowerRange(2,-5,15)
    val g = PowerRange(2,-5,15)
    override val params = Composite(c,g)

    val tr1 = SVM.Train(outDir = outDir)

    override def fit(id: Int, train: Train, params: Args): Either[ADWError, SVMModel] = {
      val modelFile = tr1.modelFile(id.toString, train)
      val testDataFile = ""
      val commandLine = List("-c ", params._1.toString, "-g ", params._2.toString, train.toString, modelFile.toString)
      val result = tr1.run(tr1.callTrain, commandLine, train.toString, modelFile.toString, testDataFile )
      result match {
        case Left(s) => Left(ADWError(s))
        case Right(_) => Right(SVMModel(modelFile))
      }
    }

    override def predict(id: Int, test: Test, m: SVMModel): Either[ADWError, Prediction] = {
      val prediction = tr1.predictFile(id.toString, test)
      //println(s"prediction = $prediction")
      val commandLine = List(test.toString, m.modelFile.toString, prediction.toString)
      //println(commandLine.mkString(","))
      val result = tr1.run(tr1.callPredict, commandLine, test.toString, m.modelFile.toString, test.toString )
      result match {
        case Left(s) => Left(ADWError(s))
        case Right(_) => Right(prediction)
      }
    }

    /**
      * Takes the test data and prediction output of the libSVM for classification
      * on that test data and generates a [[pt.inescn.eval.BinaryClassificationMetrics]]
      * that can be used to evaluate performance.
      *
      * @param id - usually the identifier of an experiment (used for naming state uniquely)
      * @param testData - test data in LightSVM format (sparse)
      * @param prediction - predicted data as generated by LibSVM when predicting on `testData`
      * @return - [[pt.inescn.eval.BinaryClassificationMetrics]]
      */
    override def evaluate(id: Int, testData: File, prediction: File): Either[ADWError, BinaryClassificationMetrics#All] = {
      val predictions: Seq[Boolean] = prediction.lineIterator.toSeq.map(p => if (p.toDouble > 0.0 ) true else false)
      val labels = testData.lineIterator.toSeq.map {
        p => val label = p.split(" ")(0).trim
          if (label.toDouble > 0.0 ) true else false }
      val metric = BinaryClassificationMetrics(labels, predictions)
      metric.calculate
    }

    override type Out = (BinaryClassificationMetrics#All, SVMModel)
    override type In = (File, File, (Double, Double))

    import io.circe.generic.auto._
    //import io.circe.parser._
    // This is required because better-files File is not a case class
    import pt.inescn.utils.CirceCoders._
    // import io.circe.syntax._ asJason

    override def inputToString(in: In): Json = {
      import io.circe.syntax._
      in.asJson
    }

    override def resultToString(out: Out): Json = {
      import io.circe.syntax._
      out.asJson
    }

    override def parseInput(in:Json) : Either[ADWError, (Train,Test,Args)] = {
      val tmp: Either[Error, In] = in.as[In]
      tmp match  {
        case Left(e) => Left(ADWError(e.getMessage + e.fillInStackTrace().toString))
        case Right(r) => Right(r)
      }
    }

    // Out == (Metric,Model)
    override def parseResult(out:Json) : Either[ADWError, Out] = {
      val tmp: Either[Error, Out] = out.as[Out]
      tmp match  {
        case Left(e) => Left(ADWError(e.getMessage + e.fillInStackTrace().toString))
        case Right(r) => Right(r)
      }
    }

    override def compare(a:In, b:In): Int = {
      // Less than
      // import scala.math.Ordering.Implicits._
      //a._3 < b._3
      //println(s"${a._3} < ${b._3} = ${a._3 < b._3}")
      // implicitly[Ordering[Tuple2[Int, Int]]].compare((1,2), (2,3))
      implicitly[Ordering[Args]].compare(a._3, b._3)
    }

  }

  /**
    * -s svm_type : set type of SVM (default 0)
    *  3 -- epsilon-SVR	(regression)
    *
    * -t kernel_type : set type of kernel function (default 2)
    *  2 -- radial basis function: exp(-gamma*|u-v|**2)
    *
    * These will be used as searchable parameters
    * -g gamma : set gamma in kernel function (default 1/num_features)
    * -c cost : set the parameter C of C-SVC, epsilon-SVR, and nu-SVR (default 1)
		+ -p epsilon : set the epsilon in loss function of epsilon-SVR (default 0.1)
		* -e epsilon : set tolerance of termination criterion (default 0.001)
    *
    * -m cachesize : set cache memory size in MB (default 100)
    * -e epsilon : set tolerance of termination criterion (default 0.001)
    * -h shrinking : whether to use the shrinking heuristics, 0 or 1 (default 1)
    * -b probability_estimates : whether to train a SVC or SVR model for probability estimates, 0 or 1 (default 0)
    * -v n : n-fold cross validation mode (grid.py uses 5)
    * -q : quiet mode (no outputs)
    */
  trait RegressionRadialBasisSVMSetUp extends ML {
    override type Algorithm = Regression
    override type Label = Double
    override type Prediction = Double
    override type Arguments = (Double, (Double, (Double, Double)))
  }


  case class RegressionRadialBasisSVM(outDir: String) extends Algo[RegressionRadialBasisSVMSetUp,SVMModel,RegressionMetrics#All] {

    type Label = MultiClassRadialBasisSVMSetUp#Label
    override type Parameters = Composite[Double, (Double, (Double, Double))]
    override type Train = File
    override type Test = File
    override type Prediction = File

    val c = PowerRange(2,-5,15)
    val g = PowerRange(2,-5,15)
    val p = PowerRange(2,-5,15)
    val e = PowerRange(2,-5,15)
    override val params = Composite(c,Composite(g,Composite(p,e)))

    val tr1 = SVM.Train(outDir = outDir)

    override def fit(id: Int, train: Train, params: Args): Either[ADWError, SVMModel] = {
      val modelFile = tr1.modelFile(id.toString, train)
      val testDataFile = ""
      val commandLine = List(
        "-s ", "3", // epsilon-SVR	(regression)
        "-c ", params._1.toString,
        "-g ", params._2._1.toString,
        "-p ", params._2._2._1.toString,
        "-e ", params._2._2._2.toString,
        train.toString, modelFile.toString)
      val result = tr1.run(tr1.callTrain, commandLine, train.toString, modelFile.toString, testDataFile )
      result match {
        case Left(s) => Left(ADWError(s))
        case Right(_) => Right(SVMModel(modelFile))
      }
    }

    override def predict(id: Int, test: Test, m: SVMModel): Either[ADWError, Prediction] = {
      val prediction = tr1.predictFile(id.toString, test)
      //println(s"prediction = $prediction")
      val commandLine = List(test.toString, m.modelFile.toString, prediction.toString)
      //println(commandLine.mkString(","))
      val result = tr1.run(tr1.callPredict, commandLine, test.toString, m.modelFile.toString, test.toString )
      result match {
        case Left(s) => Left(ADWError(s))
        case Right(_) => Right(prediction)
      }
    }

    /**
      * Takes the test data and prediction output of the libSVM for classification
      * on that test data and generates a [[pt.inescn.eval.BinaryClassificationMetrics]]
      * that can be used to evaluate performance.
      *
      * @param id - usually the identifier of an experiment (used for naming state uniquely)
      * @param testData - test data in LightSVM format (sparse)
      * @param prediction - predicted data as generated by LibSVM when predicting on `testData`
      * @return - [[pt.inescn.eval.RegressionMetrics]]
      */
    override def evaluate(id: Int, testData: File, prediction: File): Either[ADWError, RegressionMetrics#All] = {
      val predictions: Seq[Double] = prediction.lineIterator.toSeq.map(p => p.toDouble )
      val labels = testData.lineIterator.toSeq.map {
        p => val label = p.split(" ")(0).trim
          label.toDouble
      }
      val metric = RegressionMetrics(labels, predictions)
      metric.calculate
    }

    override type Out = (RegressionMetrics#All, SVMModel)
    override type In = (File, File, (Double, (Double, (Double, Double))))

    import io.circe.generic.auto._
    //import io.circe.parser._
    // This is required because better-files File is not a case class
    import pt.inescn.utils.CirceCoders._
    // import io.circe.syntax._ asJason

    override def inputToString(in: In): Json = {
      import io.circe.syntax._
      in.asJson
    }

    override def resultToString(out: Out): Json = {
      import io.circe.syntax._
      out.asJson
    }

    override def parseInput(in:Json) : Either[ADWError, (Train,Test,Args)] = {
      val tmp: Either[Error, In] = in.as[In]
      tmp match  {
        case Left(e) => Left(ADWError(e.getMessage + e.fillInStackTrace().toString))
        case Right(r) => Right(r)
      }
    }

    // Out == (Metric,Model)
    override def parseResult(out:Json) : Either[ADWError, Out] = {
      val tmp: Either[Error, Out] = out.as[Out]
      tmp match  {
        case Left(e) => Left(ADWError(e.getMessage + e.fillInStackTrace().toString))
        case Right(r) => Right(r)
      }
    }

    override def compare(a:In, b:In): Int = {
      // Less than
      // import scala.math.Ordering.Implicits._
      //a._3 < b._3
      //println(s"${a._3} < ${b._3} = ${a._3 < b._3}")
      // implicitly[Ordering[Tuple2[Int, Int]]].compare((1,2), (2,3))
      implicitly[Ordering[Args]].compare(a._3, b._3)
    }

  }

/*
		+"-s svm_type : set type of SVM (default 0)\n"
		+"	0 -- C-SVC		(multi-class classification)\n"    <------------
		+"	1 -- nu-SVC		(multi-class classification)\n"
		+"	2 -- one-class SVM\n"
		+"	3 -- epsilon-SVR	(regression)\n"
		+"	4 -- nu-SVR		(regression)\n"
		+"-t kernel_type : set type of kernel function (default 2)\n"
		+"	0 -- linear: u'*v\n"
		+"	1 -- polynomial: (gamma*u'*v + coef0)^degree\n"
		+"	2 -- radial basis function: exp(-gamma*|u-v|^2)\n"  <-------------
		+"	3 -- sigmoid: tanh(gamma*u'*v + coef0)\n"
		+"	4 -- precomputed kernel (kernel values in training_set_file)\n"
		+"-d degree : set degree in kernel function (default 3)\n"            <----------------
		+"-g gamma : set gamma in kernel function (default 1/num_features)\n"  <---------------- ?
		+"-r coef0 : set coef0 in kernel function (default 0)\n"
		+"-c cost : set the parameter C of C-SVC, epsilon-SVR, and nu-SVR (default 1)\n"  <--------- ?
		+"-n nu : set the parameter nu of nu-SVC, one-class SVM, and nu-SVR (default 0.5)\n"
		+"-p epsilon : set the epsilon in loss function of epsilon-SVR (default 0.1)\n"
		+"-m cachesize : set cache memory size in MB (default 100)\n"
		+"-e epsilon : set tolerance of termination criterion (default 0.001)\n"
		+"-h shrinking : whether to use the shrinking heuristics, 0 or 1 (default 1)\n"
		+"-b probability_estimates : whether to train a SVC or SVR model for probability estimates, 0 or 1 (default 0)\n"
		+"-wi weight : set the parameter C of class i to weight*C, for C-SVC (default 1)\n"
		+"-v n : n-fold cross validation mode\n"
		+"-q : quiet mode (no outputs)\n"

 */
  /**
    * -s svm_type : set type of SVM (default 0)
    *  2 -- one-class SVM
    *  5 -- one-class SVM (SVDD)
    *
    * -t kernel_type : set type of kernel function (default 2)
    *  2 -- radial basis function: exp(-gamma*|u-v|**2)
    *
    * These will be used as searchable parameters
    * -g gamma : set gamma in kernel function (default 1/num_features)
    * -n nu : set the parameter nu of nu-SVC, one-class SVM, and nu-SVR (default 0.5)
    * -e epsilon : set tolerance of termination criterion (default 0.001)
    *
    * -m cachesize : set cache memory size in MB (default 100)
    * -e epsilon : set tolerance of termination criterion (default 0.001)
    * -h shrinking : whether to use the shrinking heuristics, 0 or 1 (default 1)
    * -b probability_estimates : whether to train a SVC or SVR model for probability estimates, 0 or 1 (default 0)
    * -v n : n-fold cross validation mode (grid.py uses 5)
    * -q : quiet mode (no outputs)
    */
  trait OneClassRadialBasisSVMSetUp extends ML {
    override type Algorithm = OneClass
    override type Label = Double
    override type Prediction = Double
    override type Arguments = (Double, (Double,Double))
  }


  /**
    * Sets up one-class SVM classifier (Schölkopf algorithm, default RBF kernel).
    *
    * @param outDir output directory of results and temporary files
    */
  case class OneClassRadialBasisSVM(outDir: String) extends Algo[OneClassRadialBasisSVMSetUp,SVMModel,BinaryClassificationMetrics#All] {

      type Label = OneClassRadialBasisSVMSetUp#Label
      override type Parameters = Composite[Double, (Double, Double)]
      override type Train = File
      override type Test = File
      override type Prediction = File

      val g = PowerRange(2,-7,1)
      val n = PowerRange(2,-7,-1)
      val e = PowerRange(2,-7,-1)
      //override val params = Composite(g,Composite(n,e))
      override val params = Composite(g,Composite(n,e))

      val tr1 = SVM.Train(outDir = outDir)

      override def fit(id: Int, train: Train, params: Args): Either[ADWError, SVMModel] = {
        val modelFile = tr1.modelFile(id.toString, train)
        val testDataFile = ""
        val commandLine = List(
          "-s ", "2", // one-class SVM
          "-g ", params._1.toString,
          "-n ", params._2._1.toString,
          "-e ", params._2._2.toString,
          train.toString,
          modelFile.toString)
        val result = tr1.run(tr1.callTrain, commandLine, train.toString, modelFile.toString, testDataFile )
        result match {
          case Left(s) =>
            //println(s"One-class fit:$result")
            Left(ADWError(s))
          case Right(_) =>
            Right(SVMModel(modelFile))
        }
      }

      override def predict(id: Int, test: Test, m: SVMModel): Either[ADWError, Prediction] = {
        val prediction = tr1.predictFile(id.toString, test)
        //println(s"prediction = $prediction")
        val commandLine = List(test.toString, m.modelFile.toString, prediction.toString)
        //println(commandLine.mkString(","))
        val result = tr1.run(tr1.callPredict, commandLine, test.toString, m.modelFile.toString, test.toString )
        result match {
          case Left(s) => Left(ADWError(s))
          case Right(_) => Right(prediction)
        }
      }

      /**
        * Takes the test data and prediction output of the libSVM for classification
        * on that test data and generates a [[pt.inescn.eval.BinaryClassificationMetrics]]
        * that can be used to evaluate performance.
        *
        * @param id - usually the identifier of an experiment (used for naming state uniquely)
        * @param testData - test data in LightSVM format (sparse)
        * @param prediction - predicted data as generated by LibSVM when predicting on `testData`
        * @return - [[pt.inescn.eval.BinaryClassificationMetrics]]
        */
      override def evaluate(id: Int, testData: File, prediction: File): Either[ADWError, BinaryClassificationMetrics#All] = {
        val predictions: Seq[Boolean] = prediction.lineIterator.toSeq.map(p => if (p.toDouble >= 1.0 ) true else false)
        val labels = testData.lineIterator.toSeq.map {
          p => val label = p.split(" ")(0).trim
            if (label.toDouble > 0.0 ) true else false }
        //println(s"One-class predict = ${predictions.mkString(",")}")
        //println(s"One-class labels = ${labels.mkString(",")}")
        val metric = BinaryClassificationMetrics(labels, predictions)
        metric.calculate
      }

      override type Out = (BinaryClassificationMetrics#All, SVMModel)
      override type In = (File, File, (Double, (Double,Double)))

      import io.circe.generic.auto._
      //import io.circe.parser._
      // This is required because better-files File is not a case class
      import pt.inescn.utils.CirceCoders._
      // import io.circe.syntax._ asJason

      override def inputToString(in: In): Json = {
        import io.circe.syntax._
        in.asJson
      }

      override def resultToString(out: Out): Json = {
        import io.circe.syntax._
        out.asJson
      }

      override def parseInput(in:Json) : Either[ADWError, (Train,Test,Args)] = {
        val tmp: Either[Error, In] = in.as[In]
        tmp match  {
          case Left(e) => Left(ADWError(e.getMessage + e.fillInStackTrace().toString))
          case Right(r) => Right(r)
        }
      }

      // Out == (Metric,Model)
      override def parseResult(out:Json) : Either[ADWError, Out] = {
        val tmp: Either[Error, Out] = out.as[Out]
        tmp match  {
          case Left(e) => Left(ADWError(e.getMessage + e.fillInStackTrace().toString))
          case Right(r) => Right(r)
        }
      }

      override def compare(a:In, b:In): Int = {
        // Less than
        // import scala.math.Ordering.Implicits._
        //a._3 < b._3
        //println(s"${a._3} < ${b._3} = ${a._3 < b._3}")
        // implicitly[Ordering[Tuple2[Int, Int]]].compare((1,2), (2,3))
        implicitly[Ordering[Args]].compare(a._3, b._3)
      }

    }

}
