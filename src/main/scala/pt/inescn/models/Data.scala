/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.models

import better.files.File
import pt.inescn.models.Base.AlgorithmType.Classification
import pt.inescn.models.Base.{DataIn, Prediction}
import pt.inescn.utils.ADWError

import scala.util.Random

object Data {

  case class DenseVectorData(data: DenseVectorData#Labels) extends Prediction[Double] {
    type Label = Double
    override type Labels = Array[Label]

    override def toArray: Labels = data

    override def toBoolean(trueVal: Label): Seq[Boolean] = data.map(p => if (p >= trueVal) true else false)
  }

  /**
    * This class is a sparse data container that can then be used for transformation and evaluation (score).
    * It provides methods to generate a stratified cross-validation training and test data sets. The
    * cross-validation function ensures that the data is properly shuffled to ensure no biasing in
    * learning and evaluating performance.
    *
    * The data is shuffled twice when it is split. This ensures that each call to the splits will
    * generate different folds.
    *
    * NOTE: on indexing conventions. The dependent variable is the first element of the row. This is
    * the first element of a tuple. If an equivalent dense matrix is returned then the dependent variable
    * is in inedx `0`.
    *
    * NOTE: this is an unoptimized implementation used for sanity checking. Areas in which this can be
    * improved include: avoid concatenating arrays, use lists were possible (more memory by faster
    * concatenation, no indexing required) and pre-allocating the split data arrays (use buffer arrays
    * if possible). We may also consider caching seeing as the splits have always the same sizes.
    *
    * @param rnd - random generator that will be used to shuffling the data
    */
  class SparseMatrixData(data: SparseMatrixData#Rows,
                         rnd: Random = Random) extends DataIn[Double,Double] {
    type Value = Double
    type SparseFeature = Map[Int, Value]
    type SparseRow = (Double, SparseFeature)

    type Label = Double
    override type Row = SparseRow
    override type Rows = Array[Row]


    /**
      * Helper class that is used to append two arrays into a single array. It does this for the
      * training set and test set.
      *
      * @param fold      - the number of the (cross validation) fold whose data we are generating
      * @param testData  - test data set with features only
      * @param trainData - train data set with features only
      */
    case class CVData(fold: Int = 0,
                      testData: Rows = Array[Row](),
                      trainData: Rows = Array[Row]()) {
      def +(that: CVData) = CVData(fold, testData ++ that.testData, trainData ++ that.trainData)
    }

    /**
      * Calculates the maximum and minimum column indexes. Here we specifically refer to the
      * sparse map of features. The minimum should not be less than 1 (0 is reserved for the
      * dependent variable).
      */
    private lazy val maxMinCols: (Int, Int) = data.foldLeft((Int.MinValue, Int.MaxValue)) {
      (acc, p) =>
        val maxI = p._2.keys.max
        val minI = p._2.keys.min
        val mx = Math.max(acc._1, maxI)
        val mi = Math.min(acc._2, minI)
        (mx, mi)
    }
    /** Number of rows (examples) in the data set */
    private lazy val maxLines: Int = data.length
    /** Number of columns (includes the dependent variable). */
    private lazy val maxCols: Int = maxMinCols._1 - maxMinCols._2 + 1 // indexed from 0 (file format indexed from 1)

    /**
      * This is a simple index used to iterate trough the data.
      */
    private val index: Index = data.indices.toArray // (0 until dependent.length).toArray

    // TODO: move to the base trait ?
    /**
      * Groups all the data by the dependent variable.
      */
    def classes: Map[Label, Rows] = {
      // group by the label
      val groups: Map[Label, Index] = index.groupBy(idx => apply(idx)._1)
      // println(s"index = ${index.mkString(",")}")
      // println(s"groups = ${groups.map( p => p._1.toString + " -> " + p._2.mkString(",") )}")
      val result: Map[Label, Rows] = groups.map {
        p =>
          val label: Label = p._1
          val idxs: Index = p._2
          // map the label data index to the rows themselves
          val features = idxs.map(data(_))
          val rndFeatures = rnd.shuffle[Row, IndexedSeq](features).toArray
          (label, rndFeatures)
      }
      result
    }

    /**
      * Returns the value of the data cell at the `row` and `col`umn.
      *
      * @param row - row of the data set
      * @param col - column of the data set
      * @return
      */
    def at(row: Int, col: Int): Double = if (col == 0) data(row)._1 else data(row)._2.getOrElse(col, 0.0)

    /**
      * Get the data row label and features at a given index.
      *
      * @param i - index to the data row
      * @return - tuple with the dependent variable and the features
      */
    override def apply(i: Int): Row = data(i)

    /**
      * Applies the function `f` to a column `col` using the fold left
      * function. The function takes in a the index of the row, an
      * accumulator value that is initially se to `zero` and the value
      * of the (row, 'col').
      *
      * @param col - column on which to apply the function
      * @param zero - initial accumulator value
      * @param f - function to apply to the column's row values
      * @tparam T - type of accumulator (allows us to compose functions)
      * @return T that has been accumulated by `f`
      */
    override def apply[T](col: Int, zero: T, f: (Int, T, Value) => T): T = {
      val tmp = index.foldLeft(zero) { (acc,row) =>
        val ele = if (col == 0) data(row)._1 else data(row)._2.getOrElse(col, 0.0)
        val result: T = f(row, acc, ele)
        result
      }
      tmp
    }

    /**
      * Applies the function `f` to a column `col` using the fold left
      * function. The function takes in the value of the (row, 'col')
      * and maps that to another value. Such a mapping can be used
      * for example to scale data per column.
      *
      * NOTE: does in-place changes to data
      *
      * @param col - column on which to apply the function
      * @param f - function to apply to the column's row values
      * @return T that has been accumulated by `f`
      */
    override def apply(col: Int, f: Value => Value): Unit  = {
      index.map { row =>
        val ele = if (col == 0) data(row)._1 else data(row)._2.getOrElse(col, 0.0)
        val result = f(ele)
        val nRow = if (col == 0) (result, data(row)._2) else (data(row)._1, data(row)._2.updated(col, result))
        data(row) = nRow
        row + 1
      }
    }

    /**
      * Returns a copy of the column `i` data
      *
      * @param i - column index of the data set
      * @return a vector of data
      */
    override def getColumn(i: Int): Array[Double] = {
      if (i == 0)
        data.map(p => p._1)
      else
        data.map(p => p._2.getOrElse(i, 0.0))
    }

    override def getBooleanColumn(i: Int): Array[Boolean] = {
      if (i == 0)
        data.map(p => if (p._1 > 0) true else false)
      else
        data.map { p =>
          val t = p._2.getOrElse(i, 0.0)
          if (t > 0) true else false
        }
    }

    /**
      * Return a subset of the data selected by `func`
      *
      * @param func function used to filter the data
      * @return
      */
    override def select(func: Row => Boolean): SparseMatrixData = {
      val rr = data.filter( func )
      val sparse = new SparseMatrixData(rr,rnd)
      sparse
    }

    /**
      * Return two subsets of the data selected by `func`.
      * Data selected by `func` are placed on the tuple's
      * first element. Data not selected by `func` are placed
      * on the tuple's second element.
      *
      * @param func function used to filter the data
      * @return
      */
    override def split(func: Row => Boolean): (SparseMatrixData, SparseMatrixData) = {
      val (rr1, rr2) = data.partition( func )
      val sparse1 = new SparseMatrixData(rr1,rnd)
      val sparse2 = new SparseMatrixData(rr2,rnd)
      (sparse1, sparse2)
    }

    /**
      * Replace *in place* an element by `substitute` if `sel` os true
      *
      * @param sel predicate that selects elements to be replaced
      * @param substitute replaced the elements (in place)
      */
    override def replace(sel: Row => Boolean, substitute: Row => Row) : Unit = {
      index.foreach( i => if (sel(data(i))) data(i) = substitute(data(i))  )
    }

    // TODO: return sparse matrix !?
    /**
      * Returns a copy of data sets as a dense sub-matrix. The order of
      * the `rows` and `col`umns are sets as per the parameters.
      *
      * @param rows - indexes of the rows to select
      * @param cols - indexes of the columns to select
      * @return
      */
    override def getMatrix(rows: Seq[Int], cols: Seq[Int]): Array[Array[Double]] =
      rows.map(x => cols.map(y => at(x, y)).toArray).toArray

    /**
      * Returns the `rows`
      *
      * @param rows - set of row indexes of the data set
      * @return
      */
    def getRows(rows: Seq[Int]): Array[Row] = rows.map(data).toArray


    /** Number of labels or classes in the data */
    override def numLabels[V <: Classification]: Int = classes.size // TODO: move to base trait ?

    /** Number or records (rows or examples) for a given class or label */
    override def classSize[V <: Classification](labelId: Label): Int = classes.get(labelId) match {
      case Some(d) => d.length
      case None => 0
    }

    /**
      * Size of the data set.
      *
      * @return - returns a tuple were the first element are the number of rows and the
      *         second the number of columns (including the label)
      */
    override def size: (Int, Int) = (maxLines, maxCols + 1)

    /**
      * The class labels that were identified. This a set of unique values.
      */
    def classesKeys: Iterable[Label] = classes.keys
    /**
      * Number of data rows (records) for each of the classes/labels.
      */
    def classesSizes: Map[Label, Int] = classes.map(p => (p._1, p._2.length))
    /**
      * This a map to the indexes of each class. It allows us to iterate through
      */
    def classesIndexes: Map[Label, Index] = classes.map(p => (p._1, p._2.indices.toArray))


    /**
      * Shuffles the dependent and independent data vectors (columns). It uses a
      * common index in order to ensure that the ordering between labels and features
      * is consistent.
      *
      * @param indices - the index for a given label's
      * @return - a tuple with the shuffled dependent and independent variables.
      */
    def shuffle(indices: Seq[Int], rows: Rows): Rows = {
      // note that shuffle expects a TraversableOnce, which an Array is not. So use toArray
      val shuffled: Index = rnd.shuffle[Int, Seq](indices).toArray
      val shuffledIndep = shuffled.map(rows)
      shuffledIndep
    }

    /**
      * This method creates the data sets for the fold number `fold` of a given class (label).
      * It assumes the dara records are splits into a fixed number of folds, each with the same
      * size (fold size). If the class size is not a multiple, then the last fold will hold
      * additional data records. For a given fold we determines the index were this fold starts and
      * ends. We then use these indexes to select the test set. All other folds are then assembled
      * to form the training data set.
      *
      * @param fold            - number of the fold
      * @param classi          - class (or label) that is being processed
      * @param classesFoldSize - number of records (rows or examples) of this label used for the test fold
      * @param classesSizes    - number of all records (rows or examples) of this label
      * @param data            the full data set map (used to obtain the label's data records)
      * @return - return the `CVData` that holds the fold number, training data set and the test set.
      * @see [[DenseMatrixData.CVData]] [[DenseMatrixData.splitStratifiedFolds]]
      */
    def collectTrainAndTest(fold: Int, classi: Label,
                            classesFoldSize: Map[Label, Int],
                            classesSizes: Map[Label, Int],
                            data: Map[Label, Rows]): CVData = {
      val foldSize = classesFoldSize(classi)
      val classSize = classesSizes(classi)
      val start = fold * foldSize
      val end = start + foldSize
      //println(s"start=$start , end=$end")
      val classData = data(classi)
      val test: Rows = classData.slice(start, end) // Rows
      //val testLabels = Array.fill[DataIn#L](foldSize)(classi)
      // Not all fold sizes may have the same size (use class size for end)
      val train: Rows = classData.slice(0, start) ++ classData.slice(end, classSize)
      //val trainLabels = Array.fill[DataIn#L](foldSize*(classSize -1))(classi)
      CVData(fold, test, train)
    }

    /**
      * This function iterates through the various folds and splits the data of each `classi`.
      * It also shuffles the data to ensure that no biases are introduced into the data set.
      * Assumes splits are stratified.
      *
      * @param folds           - identifier (count) of the fold
      * @param classesFoldSize - size of the fold
      * @return the tuple consisting of the training and testing data-set.
      */
    def splitStratifiedFold(folds: Seq[Int], classesFoldSize: Map[Label, Int]): Either[ADWError, Stream[(SparseMatrixData, SparseMatrixData)]] = {
      val results = folds.toStream.map {
        fold =>
          // Make sure that the class labels change between folds
          val cvSet: Iterable[CVData] = classesKeys.map(classi => collectTrainAndTest(fold, classi, classesFoldSize, classesSizes, classes))
          val p: CVData = cvSet.foldLeft(CVData()) { (a, e) => a + e }
          // Because we split the data into training and test data sets at the same time, the
          // labels will be ordered in the same way for both. Make sure thus does not happen.
          // In addition to this we concatenate each class one at a time, so the labels will for the
          // generated examples will all be in sequence - if we generates labels '0,1 and 2 all 0 will
          // appear first, 1 next and so on. Make sure thus does not happen
          //val train = MatrixData(p.trainLabel, p.trainData)
          //val test = MatrixData(p.testLabel, p.testData)
          val testIndex = p.testData.indices
          val trainIndex = p.trainData.indices
          val trainData = shuffle(trainIndex, p.trainData)
          val testData = shuffle(testIndex, p.testData)
          val train = new SparseMatrixData(trainData)
          val test = new SparseMatrixData(testData)
          (train, test)
      }
      Right(results)
    }

    /**
      * This is an ugly hack to solve a simple problem. This function is
      * used to split the data of non-classification data (regression).
      * We therefore bypass class counting by setting up a single class
      * that consists of all of the data rows. see [[splitStratifiedFold]].
      *
      * @param folds           - identifier (count) of the fold
      * @param classesFoldSize - size of the fold
      * @return the tuple consisting of the training and testing data-set.
      */
    def splitFold(folds: Seq[Int], classesFoldSize: Map[Label, Int]): Either[ADWError, Stream[(SparseMatrixData, SparseMatrixData)]] = {
      // We assume a single class
      val classesSizes0 : Map[Label, Int]= Map(0.0 -> index.length)
      // All rows belong to that class, just ensure that they are randomized (necessary?)
      val rndFeatures = rnd.shuffle[Row, IndexedSeq](data).toArray
      // This class is in all indexes
      val classes0: Map[Label, Rows] = Map( 0.0 -> rndFeatures)
      val classesKeys0: Iterable[Label] = List(0.0)
      // Now split the single class
      val results = folds.toStream.map {
        fold =>
          // Make sure that the class labels change between folds
          val cvSet: Iterable[CVData] = classesKeys0.map(classi => collectTrainAndTest(fold, classi, classesFoldSize, classesSizes0, classes0))
          val p: CVData = cvSet.foldLeft(CVData()) { (a, e) => a + e }
          // Because we split the data into training and test data sets at the same time, the
          // labels will be ordered in the same way for both. Make sure this does not happen.
          // In addition to this we concatenate each class one at a time, so the labels will for the
          // generated examples will all be in sequence - if we generates labels '0,1 and 2 all 0 will
          // appear first, 1 next and so on. Make sure thus does not happen
          //val train = MatrixData(p.trainLabel, p.trainData)
          //val test = MatrixData(p.testLabel, p.testData)
          val testIndex = p.testData.indices
          val trainIndex = p.trainData.indices
          val trainData = shuffle(trainIndex, p.trainData)
          val testData = shuffle(testIndex, p.testData)
          val train = new SparseMatrixData(trainData)
          val test = new SparseMatrixData(testData)
          (train, test)
      }
      Right(results)
    }

    /**
      * Generates training and test data sets for a one-class problem.
      * The training set has no positive (false) examples. Test data
      * sets have both negative (good signal, class 1) and positive
      * examples (anomalies, ad signal - 0).
      *
      * TODO: remove hack that uses stratified splitting?
      * TODO: allow for one to stipulate the % of class 0 in train set
      * TODO: allow for one to stipulate the % of class 0 in test set (noise)
      *
      * @param folds           - identifier (count) of the fold
      * @param classesFoldSize - size of the fold
      * @return the tuple consisting of the training and testing data-set.
      */
    def splitOneClassFold(folds: Seq[Int], classesFoldSize: Map[Label, Int]):
    Either[ADWError, Stream[(SparseMatrixData, SparseMatrixData)]] = {
      val size = classesKeys.size
      val keys = classesKeys.toList
      if (size != 2)
        Left(ADWError(s"Expected 2 classes but got $size (1)"))
      else if (!( keys.contains(0.0) && keys.contains(1.0)))
        Left(ADWError(s"Expected 2 classes 0 and 1 but got $size (1)"))
      else {
        val results = folds.toStream.map {
          fold =>
            // Make sure that the class labels change between folds
            // Get the folds for the good and bad labels
            val good = 1.0
            val dataG: CVData = collectTrainAndTest(fold, good, classesFoldSize, classesSizes, classes)
            val bad = 0.0
            val dataB = collectTrainAndTest(fold, bad, classesFoldSize, classesSizes, classes)

            // IMPORTANT: training data has only "good" labels
            val trainDataGood = dataG.trainData
            val testDataGoodAndBad = dataG.testData ++ dataB.testData
            val trainIndex = trainDataGood.indices
            val testIndex = testDataGoodAndBad.indices

            // Because we split the data into training and test data sets at the same time, the
            // labels will be ordered in the same way for both. Make sure this does not happen.
            // In addition to this we concatenate each class one at a time, so the labels will for the
            // generated examples will all be in sequence - if we generates labels '0,1 and 2 all 0 will
            // appear first, 1 next and so on. Make sure thus does not happen
            val trainData = shuffle(trainIndex, trainDataGood)
            val testData = shuffle(testIndex, testDataGoodAndBad)
            val train = new SparseMatrixData(trainData)
            val test = new SparseMatrixData(testData)
            (train, test)
        }
        Right(results)
      }
    }

    /**
      * This function first determines what the size of each fold is. If too many folds are
      * requested or too little data is available, folds sizes will be less than 0. In this
      * case we report an error. If not we then iterate through the number of folds. For each
      * fold we split data of each class into a testing and a training set. The stream of
      * data sets can then be used for validation.
      * Splits are stratified.
      *
      * Note: because we process the label ids one by one, the the data records will group the
      * label ids. We must therefore shuffle the data. Note that just shuffling the labels is not
      * enough. The data would still be ordered by labels. We still need to shuffle each class data
      * however to ensure that the testing and training sub-sets are different.
      *
      * @param nrFold - number of folds we want.
      * @return a stream of training and testing data sets.
      */
    override def splitStratifiedFolds(nrFold: Int): Either[ADWError, Stream[(SparseMatrixData, SparseMatrixData)]] = {
      val classesFoldSize: Map[Label, Int] = classesSizes.map(p => (p._1, p._2 / nrFold))
      val samplesLargeEnough = classesFoldSize.forall(_._2 > 0)
      if (!samplesLargeEnough) {
        Left(ADWError("At least one class fold size was 0. Decrease the number of folds or increase the training data-set (1)"))
      } else {
        val folds = 0 until nrFold
        splitStratifiedFold(folds, classesFoldSize)
      }
    } // split folds

    /**
      * This function first determines what the size of each fold is. If too many folds are
      * requested or too little data is available, folds sizes will be less than 0. In this
      * case we report an error. If not we then iterate through the number of folds. We
      * assume a data-set with two classes only:
      *   1 - good signal (negative)
      *   0 - bad signal (positive)
      * The training set contains only class 1 (good). The test data set contains both the
      * good and bad signal (classes 0 and 1 respectively).For each fold we split data of each
      * class into a testing and a training set. The stream of data sets can then be used for
      * validation. Data is shuffled.
      *
      * @param nrFold number of cross validation folds
      * @return lazy stream that will produce the train and test data sets
      */
    override def splitOneClassFolds(nrFold : Int) : Either[ADWError, Stream[(SparseMatrixData, SparseMatrixData)]] = {
      val classesFoldSize: Map[Label, Int] = classesSizes.map(p => (p._1, p._2 / nrFold))
      val samplesLargeEnough = classesFoldSize.forall(_._2 > 0)
      if (!samplesLargeEnough) {
        Left(ADWError("At least one class fold size was 0. Decrease the number of folds or increase the training data-set (1)"))
      } else if (classesSizes.size != 2) {
        Left(ADWError("One-class cross validation assumes data-sest with 2 class (1 - good, 0 - anomalous)"))
      }
      else {
        val folds = 0 until nrFold
        splitOneClassFold(folds, classesFoldSize)
      }
    }

    /**
      * This function first determines what the size of each fold is. If too many folds are
      * requested or too little data is available, folds sizes will be less than 0. In this
      * case we report an error. If not we then iterate through the number of folds. For each
      * fold we split data of each class into a testing and a training set. The stream of
      * data sets can then be used for validation.
      * Splits are *not* stratified.
      *
      * Note: because we process the label ids one by one, the the data records will group the
      * label ids. We must therefore shuffle the data. Note that just shuffling the labels is not
      * enough. The data would still be ordered by labels. We still need to shuffle each class data
      * however to ensure that the testing and training sub-sets are different.
      *
      * @param nrFold - number of folds we want.
      * @return a stream of training and testing data sets.
      */
    override def splitFolds(nrFold: Int): Either[ADWError, Stream[(SparseMatrixData, SparseMatrixData)]] = {
      //val classesFoldSize: Map[Label, Int] = classesSizes.map(p => (p._1, p._2 / nrFold))
      val classesFoldSize: Map[Label, Int] = Map((0, this.index.length / nrFold))
      val samplesLargeEnough = classesFoldSize.forall(_._2 > 0)
      if (!samplesLargeEnough) {
        Left(ADWError("At least one class fold size was 0. Decrease the number of folds or increase the training data-set (5)"))
      } else {
        val folds = 0 until nrFold
        splitFold(folds, classesFoldSize)
      }
    } // split folds

    /**
      * This function also split the at but assumes that only a single fold will be returned.
      * It splits by the indicated percentage but returns the larger set as the first element
      * and the smaller one as the second data set.
      * Assumes splits are stratified.
      *
      * @param percent - percentage of the data to be split by.
      * @return - a single tuple consisting of a training (larger) and testing (smaller) data set
      */
    override def splitStratifiedPercent(percent: Double): Either[ADWError, (SparseMatrixData, SparseMatrixData)] = {
      if (percent <= 0.0 || percent >= 1.0)
        Left(ADWError("Fraction represents a percentage: (0..1)"))
      else {
        // Get the biggest 'fold' - this will be the "training" data set
        val largeSet = Math.max(percent, 1.0 - percent)
        //println(s"largeSet = $largeSet")
        // Set the 'fold' size to the biggest chunk - this way only 2 folds are possible
        val classesFoldSize: Map[Label, Int] = classesSizes.map(p => (p._1, (p._2 * largeSet).toInt))
        val samplesLargeEnough = classesFoldSize.forall(_._2 > 0)
        if (!samplesLargeEnough) {
          Left(ADWError("At least one class fold size was 0. Decrease the number of folds or increase the training data-set (2)"))
        } else {
          // We only one folds (one part is the training and the other the test)
          val folds = List(0)
          val result = splitStratifiedFold(folds, classesFoldSize)
          // Because we return the biggest chunk first, put that in the second place (training)
          result.flatMap { p =>
            val big = p.head._1
            val small = p.head._2
            Right((small, big))
          }
        }
      }
    } // split percentage

    def saveTo(f: File): Unit = {
      f.createIfNotExists(createParents = true)
      data.foreach { case (y, x) =>
        val ll = x.map { case (id, v) => id.toString + ":" + v.toString }.mkString(" ")
        val l = y.toString + " " + ll
        f.appendLine(l)
      }
    }

  }

  /**
    * This class is a data container that can then be used for transformation and evaluation (score).
    * It provides methods to generate a stratified cross-validation training and test data sets. The
    * cross-validation function ensures that the data is properly shuffled to ensure no biasing in
    * learning and evaluating performance.
    *
    * The data is shuffled twice when it is split. This ensures that each call to the splits will
    * generate different folds.
    *
    * NOTE: this is an unoptimized implementation used for sanity checking. Areas in which this can be
    * improved include: avoid concatenating arrays, use lists were possible (more memory by faster
    * concatenation, no indexing required) and pre-allocating the split data arrays (use buffer arrays
    * if possible). We may also consider caching seeing as the splits have always the same sizes.
    *
    * @param rnd - random generator that will be used to shuffling the data
    */
  class DenseMatrixData(private val data: DenseMatrixData#Rows,
                        private val rnd: Random = Random) extends DataIn[Double,Double] {

    type Label = Double
    type Value = Double
    override type Row = Array[Value]
    override type Rows = Array[Row]

    /**
      * Helper class that is used to append two arrays into a single array. It does this for the
      * training set and test set.
      *
      * @param fold      - the number of the (cross validation) fold whose data we are generating
      * @param testData  - test data set with features only
      * @param trainData - train data set with features only
      */
    case class CVData(fold: Int = 0,
                      testData: Rows = Array[Row](),
                      trainData: Rows = Array[Row]()) {
      def +(that: CVData) = CVData(fold, testData ++ that.testData, trainData ++ that.trainData)
    }

    /**
      * This is a simple index used to iterate trough the data.
      */
    private def index: Index = data.indices.toArray // (0 until dependent.length).toArray

    /**
      * This methods does through the data set and groups all of the data features for  each label.
      * We return a map from a label to a set of features. Note that in order to shuffle the data
      * appropriately for the training and testing phases we must ensure that each of the features
      * for all of the classes/labels are shuffled. If this were not done, the training and testing
      * data would always be the same. Shuffling the data on input will not help because the map
      * could then reorder the data for a given key.
      */
    def classes: Map[Label, Rows] = {
      // group by the label
      val groups: Map[Label, Index] = index.groupBy(idx => apply(idx)(0))
      // println(s"index = ${index.mkString(",")}")
      // println(s"groups = ${groups.map( p => p._1.toString + " -> " + p._2.mkString(",") )}")
      val result: Map[Label, Rows] = groups.map {
        p =>
          val label: Label = p._1
          val idxs: Index = p._2
          // map the label data index to the rows themselves
          val features: Rows = idxs.map(data(_))
          val rndFeatures = rnd.shuffle[Row, IndexedSeq](features).toArray
          (label, rndFeatures)
      }
      result
    }

    /**
      * Size of the data set.
      *
      * @return - returns a tuple were the first element are the number of rows and the
      *         second the number of columns (including the label)
      */
    override def size: (Int, Int) = (data.length, data(0).length)

    /**
      * Get the data row label and features at a given index.
      *
      * @param i - index to the data row
      * @return - tuple with the dependent variable and the features
      */
    override def apply(i: Int): Row = data(i)

    /**
      * Applies the function `f` to a column `col` using the fold left
      * function. The function takes in a the index of the row, an
      * accumulator value that is initially se to `zero` and the value
      * of the (row, 'col').
      *
      * @param col - column on which to apply the function
      * @param zero - initial accumulator value
      * @param f - function to apply to the column's row values
      * @tparam T - type of accumulator (allows us to compose functions)
      * @return T that has been accumulated by `f`
      */
    override def apply[T](col: Int, zero: T, f: (Int, T, Value) => T): T = {
      val tmp = index.foldLeft(zero) { (acc, row) =>
        val ele = data(row)(col)
        val nacc: T = f(row, acc, ele)
        nacc
      }
      tmp
    }

    /**
      * Applies the function `f` to a column `col` using the fold left
      * function. The function takes in the value of the (row, 'col')
      * and maps that to another value. Such a mapping can be used
      * for example to scale data per column.
      *
      * NOTE: does in-place changes to data
      *
      * @param col - column on which to apply the function
      * @param f - function to apply to the column's row values
      * @return T that has been accumulated by `f`
      */
    override def apply(col: Int, f: Value => Value): Unit  = {
      index.map { row =>
        val ele = data(row)(col)
        val result = f(ele)
        data(row)(col) = result
        row + 1
      }
    }

    /**
      * Returns a copy of the column `i` data
      *
      * @param i - column of the data set
      * @return a vector of data
      */
    override def getColumn(i: Int): Array[Double] = data.map(p => p(i))

    override def getBooleanColumn(i: Int): Array[Boolean] = data.map(p => if (p(i) > 0) true else false)

    /**
      * Return a subset of the data selected by `func`
      *
      * @param func function used to filter the data
      * @return
      */
    override def select(func: Row => Boolean): DenseMatrixData = {
      val rr = data.filter( func )
      val sparse = new DenseMatrixData(rr,rnd)
      sparse
    }

    /**
      * Return two subsets of the data selected by `func`.
      * Data selected by `func` are placed on the tuple's
      * first element. Data not selected by `func` are placed
      * on the tuple's second element.
      *
      * @param func function used to filter the data
      * @return
      */
    override def split(func: Row => Boolean): (DenseMatrixData, DenseMatrixData) = {
      val (rr1, rr2) = data.partition( func )
      val sparse1 = new DenseMatrixData(rr1,rnd)
      val sparse2 = new DenseMatrixData(rr2,rnd)
      (sparse1, sparse2)
    }

    /**
      * Replace *in place* an element by `substitute` if `sel` os true
      *
      * @param sel predicate that selects elem,ents to be replaced
      * @param substitute replaced the elements (in place)
      */
    override def replace(sel: Row => Boolean, substitute: Row => Row) : Unit = {
      index.foreach( i => if (sel(data(i))) data(i) = substitute(data(i))  )
    }

    /**
      * Returns a copy of data sets as a dense sub-matrix. The order of
      * the `rows` and `col`umns are sets as per the parameters.
      *
      * @param rows - indexes of the rows to select
      * @param cols - indexes of the columns to select
      * @return
      */

    override def getMatrix(rows: Seq[Int], cols: Seq[Int]): Array[Array[Double]] =
      rows.map(x => cols.map(y => data(x)(y)).toArray).toArray

    /**
      * Returns the `rows`
      *
      * @param rows - list iof row  indexes fo the data set
      * @return
      */
    override def getRows(rows: Seq[Int]): Array[Row] = rows.map(data).toArray

    /**
      * The number of unique labels in the data set.
      *
      * @return - number of labels
      */
    override def numLabels[V <: Classification]: Int = classes.size

    /**
      * Get the number of data rows (records) for a given label.
      *
      * @param labelId - label of the data
      * @return - if the label exists return the number of existing records otherwise return 0.
      */
    override def classSize[V <: Classification](labelId: Label): Int = classes.get(labelId) match {
      case Some(d) => d.length
      case None => 0
    }

    /**
      * The class labels that were identified. This a set of unique values.
      */
    def classesKeys: Iterable[Label] = classes.keys
    /**
      * Number of data rows (records) for each of the classes/labels.
      */
    def classesSizes: Map[Label, Int] = classes.map(p => (p._1, p._2.length))
    /**
      * This a map to the indexes of each class. It allows us to iterate through
      */
    def classesIndexes: Map[Label, Index] = classes.map(p => (p._1, p._2.indices.toArray))

    /**
      * Shuffles the dependent and independent data vectors (columns). It uses a
      * common index in order to ensure that the ordering between labels and features
      * is consistent.
      *
      * @param indices - the index for a given label's
      * @return - a tuple with the shuffled dependent and independent variables.
      */
    def shuffle(indices: Seq[Int], rows: Rows): Rows = {
      // note that shuffle expects a TraversableOnce, which an Seq is not. So use toArray
      val shuffled: Index = rnd.shuffle[Int, Seq](indices).toArray
      val shuffledIndep = shuffled.map(rows(_))
      shuffledIndep
    }

    /**
      * This method creates the data sets for the fold number `fold` of a given class (label).
      * It assumes the dara records are splits into a fixed number of folds, each with the same
      * size (fold size). If the class size is not a multiple, then the last fold will hold
      * additional data records. For a given fold we determines the index were this fold starts and
      * ends. We then use these indexes to select the test set. All other folds are then assembled
      * to form the training data set.
      *
      * @param fold            - number of the fold
      * @param classi          - class (or label) that is being processed
      * @param classesFoldSize - number of records (rows or examples) of this label used for the test fold
      * @param classesSizes    - number of all records (rows or examples) of this label
      * @param data            the full data set map (used to obtain the label's data records)
      * @return - return the `CVData` that holds the fold number, training data set and the test set.
      * @see [[DenseMatrixData.CVData]] [[DenseMatrixData.splitStratifiedFolds]]
      */
    def collectTrainAndTest(fold: Int, classi: Label,
                            classesFoldSize: Map[Label, Int],
                            classesSizes: Map[Label, Int],
                            data: Map[Label, Rows]): CVData = {
      val foldSize = classesFoldSize(classi)
      val classSize = classesSizes(classi)
      val start = fold * foldSize
      val end = start + foldSize
      val classData: Array[Array[Double]] = data(classi)
      val test: Rows = classData.slice(start, end)
      //val testLabels = Array.fill[DataIn#L](foldSize)(classi)
      // ?? val testLabels: Dependent[Label] = Array.fill(test.length)(classi)
      // Not all fold sizes may have the same size (use class size for end)
      val train: Rows = classData.slice(0, start) ++ classData.slice(end, classSize)
      //val trainLabels = Array.fill[DataIn#L](foldSize*(classSize -1))(classi)
      // ?? val trainLabels: Dependent[Label] = Array.fill(train.length)(classi)
      // ?? CVData(fold, testLabels, test, trainLabels, train)
      CVData(fold, test, train)
    }

    /**
      * This function iterates through the various folds and splits the data of each `classi`.
      * It also shuffles the data to ensure that no biases are introduced into the data set.
      * Assumes splits are stratified.
      *
      * @param folds           - identifier (count) of the fold
      * @param classesFoldSize - size of the fold
      * @return the tuple consisting of the training and testing data-set.
      */
    def splitStratifiedFold(folds: Seq[Int], classesFoldSize: Map[Label, Int]): Either[ADWError, Stream[(DenseMatrixData, DenseMatrixData)]] = {
      val results = folds.toStream.map {
        fold =>
          // Make sure that the class labels change between folds
          val cvSet: Iterable[CVData] = classesKeys.map(classi => collectTrainAndTest(fold, classi, classesFoldSize, classesSizes, classes))
          val p: CVData = cvSet.foldLeft(CVData()) { (a, e) => a + e }
          // Because we split the data into training and test data sets at the same time, the
          // labels will be ordered in the same way for both. Make sure this does not happen.
          // In addition to this we concatenate each class one at a time, so the labels will for the
          // generated examples will all be in sequence - if we generates labels '0,1 and 2 all 0 will
          // appear first, 1 next and so on. Make sure thus does not happen
          //val train = MatrixData(p.trainLabel, p.trainData)
          //val test = MatrixData(p.testLabel, p.testData)
          val testIndex = p.testData.indices
          val trainIndex = p.trainData.indices
          val trainData = shuffle(trainIndex, p.trainData)
          val testData = shuffle(testIndex, p.testData)
          val train = new DenseMatrixData(trainData)
          val test = new DenseMatrixData(testData)
          (train, test)
      }
      Right(results)
    }

    /**
      * This is an ugly hack to solve a simple problem. This function is
      * used to split the data of non-classification data (regression).
      * We therefore bypass class counting by setting up a single class
      * that consists of all of the data rows. see [[splitStratifiedFold]].
      *
      * @param folds           - identifier (count) of the fold
      * @param classesFoldSize - size of the fold
      * @return the tuple consisting of the training and testing data-set.
      */
    def splitFold(folds: Seq[Int], classesFoldSize: Map[Label, Int]): Either[ADWError, Stream[(DenseMatrixData, DenseMatrixData)]] = {
      // We assume a single class
      val classesSizes0 : Map[Label, Int]= Map(0.0 -> index.length)
      // All rows belong to that class, just ensure that they are randomized (necessary?)
      val rndFeatures = rnd.shuffle[Row, IndexedSeq](data).toArray
      // This class is in all indexes
      val classes0: Map[Label, Rows] = Map( 0.0 -> rndFeatures)
      val classesKeys0: Iterable[Label] = List(0.0)
      // Now split the single class
      val results = folds.toStream.map {
        fold =>
          // Make sure that the class labels change between folds
          val cvSet: Iterable[CVData] = classesKeys0.map(classi => collectTrainAndTest(fold, classi, classesFoldSize, classesSizes0, classes0))
          val p: CVData = cvSet.foldLeft(CVData()) { (a, e) => a + e }
          // Because we split the data into training and test data sets at the same time, the
          // labels will be ordered in the same way for both. Make sure this does not happen.
          // In addition to this we concatenate each class one at a time, so the labels will for the
          // generated examples will all be in sequence - if we generates labels '0,1 and 2 all 0 will
          // appear first, 1 next and so on. Make sure thus does not happen
          //val train = MatrixData(p.trainLabel, p.trainData)
          //val test = MatrixData(p.testLabel, p.testData)
          val testIndex = p.testData.indices
          val trainIndex = p.trainData.indices
          val trainData = shuffle(trainIndex, p.trainData)
          val testData = shuffle(testIndex, p.testData)
          val train = new DenseMatrixData(trainData)
          val test = new DenseMatrixData(testData)
          (train, test)
      }
      Right(results)
    }

    /**
      * Generates training and test data sets for a one-class problem.
      * The training set has no positive (false) examples. Test data
      * sets have both negative (good signal, class 1) and positive
      * examples (anomalies, ad signal - 0).
      *
      * TODO: allow for one to stipulate the % of class 0 in train set
      * TODO: allow for one to stipulate the % of class 0 in test set (noise)
      *
      * @param folds           - identifier (count) of the fold
      * @param classesFoldSize - size of the fold
      * @return the tuple consisting of the training and testing data-set.
      */
    def splitOneClassFold(folds: Seq[Int], classesFoldSize: Map[Label, Int]):
    Either[ADWError, Stream[(DenseMatrixData, DenseMatrixData)]] = {
      val size = classesKeys.size
      val keys = classesKeys.toList
      if (size != 2)
        Left(ADWError(s"Expected 2 classes but got $size (2)"))
      else if (!( keys.contains(0.0) && keys.contains(1.0)))
        Left(ADWError(s"Expected 2 classes 0 and 1 but got $size (2)"))
      else {
        val results = folds.toStream.map {
          fold =>
            // Make sure that the class labels change between folds
            /*
            val cvSet: Iterable[CVData] = classesKeys.map(classi => collectTrainAndTest(fold, classi, classesFoldSize, classesSizes, classes))
            val p: CVData = cvSet.foldLeft(CVData()) { (a, e) => a + e }*/

            // Get the folds for the good and bad labels
            val good = 1.0
            val dataG: CVData = collectTrainAndTest(fold, good, classesFoldSize, classesSizes, classes)
            val bad = 0.0
            val dataB = collectTrainAndTest(fold, bad, classesFoldSize, classesSizes, classes)

            // IMPORTANT: training data has only "good" labels
            val trainDataGood = dataG.trainData
            val testDataGoodAndBad = dataG.testData ++ dataB.testData
            val trainIndex = trainDataGood.indices
            val testIndex = testDataGoodAndBad.indices

            // Because we split the data into training and test data sets at the same time, the
            // labels will be ordered in the same way for both. Make sure this does not happen.
            // In addition to this we concatenate each class one at a time, so the labels will for the
            // generated examples will all be in sequence - if we generates labels '0,1 and 2 all 0 will
            // appear first, 1 next and so on. Make sure thus does not happen
            val trainData = shuffle(trainIndex, trainDataGood)
            val testData = shuffle(testIndex, testDataGoodAndBad)
            val train = new DenseMatrixData(trainData)
            val test = new DenseMatrixData(testData)
            (train, test)
        }
        Right(results)
      }
    }

    /**
      * This function first determines what the size of each fold is. If too many folds are
      * requested or too little data is available, folds sizes will be less than 0. In this
      * case we report an error. If not we then iterate through the number of folds. For each
      * fold we split data of each class into a testing and a training set. The stream of
      * data sets can then be used for validation.
      * Splits are stratified.
      *
      * Note: because we process the label ids one by one, the the data records will group the
      * label ids. We must therefore shuffle the data. Note that just shuffling the labels is not
      * enough. The data would still be ordered by labels. We still need to shuffle each class data
      * however to ensure that the testing and training sub-sets are different.
      *
      * @param nrFold - number of folds we want.
      * @return a stream of training and testing data sets.
      */
    override def splitStratifiedFolds(nrFold: Int): Either[ADWError, Stream[(DenseMatrixData, DenseMatrixData)]] = {
      val classesFoldSize: Map[Label, Int] = classesSizes.map(p => (p._1, p._2 / nrFold))
      val samplesLargeEnough = classesFoldSize.forall(_._2 > 0)
      if (!samplesLargeEnough) {
        Left(ADWError("At least one class fold size was 0. Decrease the number of folds or increase the training data-set (3)"))
      } else {
        val folds = 0 until nrFold
        splitStratifiedFold(folds, classesFoldSize)
      }
    } // split folds

    /**
      * This function first determines what the size of each fold is. If too many folds are
      * requested or too little data is available, folds sizes will be less than 0. In this
      * case we report an error. If not we then iterate through the number of folds. We
      * assume a data-set with two classes only:
      *   1 - good signal (negative)
      *   0 - bad signal (positive)
      * The training set contains only class 1 (good). The test data set contains both the
      * good and bad signal (classes 0 and 1 respectively).For each fold we split data of each
      * class into a testing and a training set. The stream of data sets can then be used for
      * validation. Data is shuffled.
      *
      * @param nrFold number of cross validation folds
      * @return lazy stream that will produce the train and test data sets
      */
    override def splitOneClassFolds(nrFold : Int) : Either[ADWError, Stream[(DenseMatrixData, DenseMatrixData)]] = {
      val classesFoldSize: Map[Label, Int] = classesSizes.map(p => (p._1, p._2 / nrFold))
      val samplesLargeEnough = classesFoldSize.forall(_._2 > 0)
      if (!samplesLargeEnough) {
        Left(ADWError("At least one class fold size was 0. Decrease the number of folds or increase the training data-set (1)"))
      } else if (classesSizes.size != 2) {
        Left(ADWError("One-class cross validation assumes data-sest with 2 class (1 - good, 0 - anomalous)"))
      }
      else {
        val folds = 0 until nrFold
        splitOneClassFold(folds, classesFoldSize)
      }
    }

    /**
      * This function first determines what the size of each fold is. If too many folds are
      * requested or too little data is available, folds sizes will be less than 0. In this
      * case we report an error. If not we then iterate through the number of folds. For each
      * fold we split data of each class into a testing and a training set. The stream of
      * data sets can then be used for validation.
      * Splits are *not* stratified.
      *
      * Note: because we process the label ids one by one, the the data records will group the
      * label ids. We must therefore shuffle the data. Note that just shuffling the labels is not
      * enough. The data would still be ordered by labels. We still need to shuffle each class data
      * however to ensure that the testing and training sub-sets are different.
      *
      * @param nrFold - number of folds we want.
      * @return a stream of training and testing data sets.
      */
    override def splitFolds(nrFold: Int): Either[ADWError, Stream[(DenseMatrixData, DenseMatrixData)]] = {
      //val classesFoldSize: Map[Label, Int] = classesSizes.map(p => (p._1, p._2 / nrFold))
      val classesFoldSize: Map[Label, Int] = Map((0, this.index.length / nrFold))
      val samplesLargeEnough = classesFoldSize.forall(_._2 > 0)
      if (!samplesLargeEnough) {
        Left(ADWError("At least one class fold size was 0. Decrease the number of folds or increase the training data-set (5)"))
      } else {
        val folds = 0 until nrFold
        splitFold(folds, classesFoldSize)
      }
    } // split folds


    /**
      * This function also split the at but assumes that only a single fold will be returned.
      * It splits by the indicated percentage but returns the larger set as the first element
      * and the smaller one as the second data set.
      * Assumes splits are stratified.
      *
      * @param percent - percentage of the data to be split by.
      * @return - a single tuple consisting of a training (larger) and testing (smaller) data set
      */
    override def splitStratifiedPercent(percent: Double): Either[ADWError, (DenseMatrixData, DenseMatrixData)] = {
      if (percent <= 0.0 || percent >= 1.0)
        Left(ADWError("Fraction represents a percentage: (0..1)"))
      else {
        // Get the biggest 'fold' - this will be the "training" data set
        val largeSet = Math.max(percent, 1.0 - percent)
        //println(s"largeSet = $largeSet")
        // Set the 'fold' size to the biggest chunk - this way only 2 folds are possible
        val classesFoldSize: Map[Label, Int] = classesSizes.map(p => (p._1, (p._2 * largeSet).toInt))
        val samplesLargeEnough = classesFoldSize.forall(_._2 > 0)
        if (!samplesLargeEnough) {
          Left(ADWError("At least one class fold size was 0. Decrease the number of folds or increase the training data-set (4)"))
        } else {
          // We only one folds (one part is the training and the other the test)
          val folds = List(0)
          val result = splitStratifiedFold(folds, classesFoldSize)
          // Because we return the biggest chunk first, put that in the second place (training)
          result.flatMap { p =>
            val big = p.head._1
            val small = p.head._2
            Right((small, big))
          }
        }
      }
    } // split percentage

    def saveTo(f: File): Unit = {
      f.createIfNotExists()
      data.foreach { ll: Array[Label] =>
        val l = ll.mkString(",")
        f.appendLine(l)
      }
    }

    override def toString : String = {
      data.foldLeft("") { (acc,ll) =>
        val l = ll.mkString(",")
        acc + l + "\n"
      }
    }
  }


  object MatrixData {

    /**
      * Creates a DenseMatrixData with the type as `T`. This will ensure that we only
      * apply transformations and algorithms (estimators/scorers) that are compatible with
      * a specific algorithm type.
      *
      * @param rnd - random generator that will be used to shuffling the data
      * @return returns a marked MatrixData
      * @see [[pt.inescn.models.Base.AlgorithmType]]
      */
    def dense(data: DenseMatrixData#Rows, rnd: Random = Random): DenseMatrixData =
      new DenseMatrixData(data, rnd)

    /**
      * Creates a SparseMatrixData with the type as `T`. This will ensure that we only
      * apply transformations and algorithms (estimators/scorers) that are compatible with
      * a specific algorithm type.
      *
      * @param rnd - random generator that will be used to shuffling the data
      * @return returns a marked MatrixData
      * @see [[pt.inescn.models.Base.AlgorithmType]]
      */
    def sparse(data: SparseMatrixData#Rows, rnd: Random = Random): SparseMatrixData =
      new SparseMatrixData(data, rnd)
  }

}
