/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.model

import org.scalatest._
import pt.inescn.models.Data.{DenseMatrixData, MatrixData, SparseMatrixData}
import pt.inescn.utils.ADWError

import scala.collection.mutable
import scala.util.Random

/**
  * Contains tests for the SVMlib wrapper.
  * Created by hmf on 26-07-2017.
  *
  * test:compile
  * test:console
  * test:consoleQuick
  * test:run
  * test:runMain
  *
  * sbt test
  * sbt "testOnly pt.inescn.model.DataSpec"
  *
  */
class DataSpec extends FlatSpec with Matchers {

  // TODO: replace tests with these checks
  // TODO: add test to check that union of all CV data-sets reconstruct full data-set. How?
  /**
    * Used to check that two folds do NOT have the common elements.
    *
    * @param a cross validation set generated for [[SparseMatrixData]]
    * @param b cross validation set generated for [[SparseMatrixData]]
    * @return true if they don't share any rows data, true otherwise
    */
  def disjointRows(a: SparseMatrixData,
                   b: SparseMatrixData): Boolean = {

    val aa: Array[(Double, a.SparseFeature)] = a.getRows(0 until a.size._1)
    val bb: Array[(Double, b.SparseFeature)] = b.getRows(0 until b.size._1)

    val setA = aa.map(ee => genericWrapArray(ee._2.toArray.sortBy(_._1))).toSet
    val setB = bb.map(ee => genericWrapArray(ee._2.toArray.sortBy(_._1))).toSet
    setA.intersect(setB).isEmpty
  }

  /**
    * Used to check that two folds do NOT have the common elements.
    *
    * @param a cross validation set generated for [[DenseMatrixData]]
    * @param b cross validation set generated for [[DenseMatrixData]]
    * @return true if they don't share any rows data, true otherwise
    */
  def disjointRows(a: DenseMatrixData,
                   b: DenseMatrixData): Boolean = {

    val aa: Array[Array[a.Value]] = a.getRows(0 until a.size._1)
    val bb: Array[Array[b.Value]] = b.getRows(0 until b.size._1)

    val setA = aa.map(ee => genericWrapArray(ee.drop(0))).toSet
    val setB = bb.map(ee => genericWrapArray(ee.drop(0))).toSet
    setA.intersect(setB).isEmpty
  }

  /**
    * Checks that the distribution of each class is about the same as
    * that of the full data set. Used to check that the stratified
    * cross validation data set generation is correct.
    *
    * Note that the sizes are not exact because the original data set
    * may not be a integer multiple of the number of folds. We
    * therefore check for a range.
    *
    * @param all full data set from which we get the distribution
    * @param a one of the cross validation sets
    * @return true if the distribution is within the tolerance or
    *         false otherwise
    */
  def correctDistribution(all: DenseMatrixData, a: DenseMatrixData): Boolean = {
    val totalSz : Double = all.size._1
    //println(s"totalSz = $totalSz")
    val delta = 1.0 / totalSz
    //println(s"delta = $delta")
    val distribution = all.classesSizes.map( pp => (pp._1, pp._2/totalSz))
    //println(distribution.mkString(","))
    distribution.forall{ case (classe, fraction) =>
      val sz = a.classSize(classe)
      //println(s"sz = $sz")
      val lower = Math.round(fraction * a.size._1).toInt
      val upper = Math.ceil((fraction + delta)* a.size._1).toInt
      val result = (sz >= lower) && (sz <= upper)
      //println(s"lower($lower) <= $sz <= upper($upper) : $result")
      result
    }
  }

  /**
    * Checks that the distribution of each class is about the same as
    * that of the full data set. Used to check that the stratified
    * cross validation data set generation is correct.
    *
    * Note that the sizes are not exact because the original data set
    * may not be a integer multiple of the number of folds. We
    * therefore check for a range.
    *
    * @param all full data set from which we get the distribution
    * @param a one of the cross validation sets
    * @return true if the distribution is within the tolerance or
    *         false otherwise
    */
  def correctDistribution(all: SparseMatrixData, a: SparseMatrixData): Boolean = {
    val totalSz : Double = all.size._1
    //println(s"totalSz = $totalSz")
    val delta = 1.0 / totalSz
    //println(s"delta = $delta")
    val distribution = all.classesSizes.map( pp => (pp._1, pp._2/totalSz))
    //println(distribution.mkString(","))
    distribution.forall{ case (classe, fraction) =>
      val sz = a.classSize(classe)
      //println(s"sz = $sz")
      val lower = Math.round(fraction * a.size._1).toInt
      val upper = Math.ceil((fraction + delta)* a.size._1).toInt
      val result = (sz >= lower) && (sz <= upper)
      //println(s"lower($lower) <= $sz <= upper($upper) : $result")
      result
    }
  }

  "DataMatrix" should "hold and split labeled data (number of folds)" in {

    val labels1 = Array( 1.0,  0.0,  1.0,  2.0,  1.0,  1.0,  2.0,  1.0,  0.0)
    val mat0 : Array[Array[Double]] = Array(
      Array(10.0, 20.0, 30.0, 40.0, 50.0, 60.0, 70.0, 80.0, 90.0),
      Array(11.0, 21.0, 31.0, 41.0, 51.0, 61.0, 71.0, 81.0, 91.0),
      Array( 1.1,  2.1,  3.1,  4.2,  5.1,  6.1,  7.1,  8.1,  9.1)
    )
    val data1 : Array[Array[Double]] = Array(
      labels1,
      mat0(0),
      mat0(1),
      mat0(2)
    ).transpose
    val mat1 = MatrixData.dense(data1, new Random(5587) )
    val sz1 = mat1.size
    sz1._1 shouldBe data1.length       // no. of lines
    sz1._2 shouldBe data1(0).length    // no. columns
    // A row
    val row1 = mat1(0)
    //println((row1._1, row1._2.mkString(",")))
    row1(0) shouldBe data1(0)(0)
    row1(1) shouldBe data1(0)(1)
    row1(2) shouldBe data1(0)(2)
    row1(3) shouldBe data1(0)(3)
    // First column has the labels
    mat1.getColumn(0) should contain theSameElementsInOrderAs labels1
    // Other columns has the features
    val features1 = mat1.getMatrix(1 to 8, 1 to 3)
    features1(0)(0) shouldBe data1(1)(1)
    features1(0)(1) shouldBe data1(1)(2)
    features1(0)(2) shouldBe data1(1)(3)
    features1(7)(0) shouldBe data1(8)(1)
    features1(7)(1) shouldBe data1(8)(2)
    features1(7)(2) shouldBe data1(8)(3)
    mat1.numLabels shouldBe 3
    mat1.classSize(0.0) shouldBe 2
    mat1.classSize(1.0) shouldBe 5
    mat1.classSize(2.0) shouldBe 2
    mat1.getColumn(0) should contain theSameElementsInOrderAs Array(1.0, 0.0, 1.0, 2.0, 1.0, 1.0, 2.0, 1.0, 0.0)

    /* TODO
    // Too may folds / too little data - at least one fold size of a class was 0
    val folds1Tmp: Either[String,
      Stream[(MatrixData[AlgorithmType.Classification], MatrixData[AlgorithmType.Classification])]] = mat1.splitStratifiedFolds(3)
    folds1Tmp.isLeft shouldBe true
*/
    val folds2Tmp: Either[ADWError, Stream[(DenseMatrixData, DenseMatrixData)]] = mat1.splitStratifiedFolds(2)
    folds2Tmp.isRight shouldBe true
    val folds2 = folds2Tmp.right.get
    // 10 samples divided by 2 folds
    folds2.length shouldBe 2
    val fold2_1 = folds2.head
    val fold2_1_train = fold2_1._1
    val fold2_1_test = fold2_1._2
    // Check that the expected sizes are correct. Note that the total number of label tallies with the data set
    // Notice that the class 2 does not split evenly (no modulo division possible), so training gets more data
    fold2_1_train.size shouldBe ((5,4)) // 1 of label 0, 3 of label 1, 1 of label 2
    fold2_1_test.size shouldBe ((4,4)) // 1 of label 0, 2 of label 1, 1 of label 2
    fold2_1_train.numLabels shouldBe 3
    fold2_1_test.numLabels shouldBe 3
    fold2_1_train.classSize(0.0) shouldBe 1
    fold2_1_train.classSize(1.0) shouldBe 3
    fold2_1_train.classSize(2.0) shouldBe 1
    fold2_1_test.classSize(0.0) shouldBe 1
    fold2_1_test.classSize(1.0) shouldBe 2      // data set size not a multiple of fold size
    fold2_1_test.classSize(2.0) shouldBe 1
    fold2_1_test.getColumn(0) should contain theSameElementsInOrderAs Array(2.0, 1.0, 0.0, 1.0)
    fold2_1_train.getColumn(0) should contain theSameElementsInOrderAs Array(1.0, 2.0, 1.0, 0.0, 1.0)
    fold2_1_test.getMatrix(0 until 4, 1 to 3) should contain theSameElementsInOrderAs Array(
      Array(70.0,71.0,7.1),  // label 2
      Array(60.0,61.0,6.1),  // label 1
      Array(90.0,91.0,9.1),  // label 0
      Array(80.0,81.0,8.1))  // label 1
    //val t1 = fold2_1_train.getMatrix(0 until 5, 1 to 3).foldLeft("")((a,p) => a + "\n" + p.mkString(",") + ";")
    //println(t1)
    fold2_1_train.getMatrix(0 until 5, 1 to 3) should contain theSameElementsInOrderAs Array(
      Array(50.0,51.0,5.1),  // label 1
      Array(40.0,41.0,4.2),  // label 2
      Array(30.0,31.0,3.1),  // label 1
      Array(20.0,21.0,2.1),  // label 0
      Array(10.0,11.0,1.1))  // label 1


    // Get the the second fold. Notice that the sizes of the train and tests data sets are the same.
    // However the elements are different and more importantly no data records ever appears in both
    // training and the test data sets.

    val fold2_2 = folds2(1)
    val fold2_2_train = fold2_2._1
    val fold2_2_test = fold2_2._2
    // Check that the expected sizes are correct. Note that the total number of label tallies with the data set
    // Notice that the class 2 does not split evenly (no modulo division possible), so training gets more data
    fold2_2_train.size shouldBe ((5,4)) // 1 of label 0, 3 of label 1, 1 of label 2
    fold2_2_test.size shouldBe ((4,4)) // 1 of label 0, 2 of label 1, 1 of label 2
    fold2_2_train.numLabels shouldBe 3
    fold2_2_test.numLabels shouldBe 3
    fold2_2_train.classSize(0.0) shouldBe 1
    fold2_2_train.classSize(1.0) shouldBe 3
    fold2_2_train.classSize(2.0) shouldBe 1
    fold2_2_test.classSize(0.0) shouldBe 1
    fold2_2_test.classSize(1.0) shouldBe 2      // data set size not a multiple of fold size
    fold2_2_test.classSize(2.0) shouldBe 1
    fold2_2_train.getColumn(0) should contain theSameElementsInOrderAs Array(1.0, 2.0, 1.0, 0.0, 1.0)
    fold2_2_test.getColumn(0) should contain theSameElementsInOrderAs Array(1.0, 0.0, 1.0, 2.0)
    fold2_2_test.getMatrix(0 until 4, 1 to 3) should contain theSameElementsInOrderAs Array(
      Array(50.0,51.0,5.1),  // label 1
      Array(90.0,91.0,9.1),  // label 0
      Array(80.0,81.0,8.1),  // label 1
      Array(40.0,41.0,4.2))  // label 2
    //val t1 = fold2_2_test.getMatrix(0 until 4, 1 to 3).foldLeft("")((a,p) => a + "\n" + p.mkString(",") + ";")
    //println(t1)
    fold2_2_train.getMatrix(0 until 5, 1 to 3) should contain theSameElementsInOrderAs Array(
      Array(60.0,61.0,6.1),  // label 1
      Array(70.0,71.0,7.1),  // label 2
      Array(10.0,11.0,1.1),  // label 1
      Array(20.0,21.0,2.1),  // label 0
      Array(30.0,31.0,3.1))  // label 1

    val folds3Tmp: Either[ADWError, Stream[(DenseMatrixData, DenseMatrixData)]] = mat1.splitStratifiedFolds(2)
    folds3Tmp.isRight shouldBe true
    val folds3 = folds3Tmp.right.get
    val fold3_1 = folds3.head
    val fold3_1_train = fold3_1._1
    val fold3_1_test = fold3_1._2
    // Check that the expected sizes are correct. Note that the total number of label tallies with the data set
    // Notice that the class 2 does not split evenly (no modulo division possible), so training gets more data
    fold3_1_train.size shouldBe ((5,4)) // 1 of label 0, 3 of label 1, 1 of label 2
    fold3_1_test.size shouldBe ((4,4)) // 1 of label 0, 2 of label 1, 1 of label 2
    fold3_1_train.numLabels shouldBe 3
    fold3_1_test.numLabels shouldBe 3
    fold3_1_train.classSize(0.0) shouldBe 1
    fold3_1_train.classSize(1.0) shouldBe 3
    fold3_1_train.classSize(2.0) shouldBe 1
    fold3_1_test.classSize(0.0) shouldBe 1
    fold3_1_test.classSize(1.0) shouldBe 2      // data set size not a multiple of fold size
    fold3_1_test.classSize(2.0) shouldBe 1
    fold3_1_train.getColumn(0) should contain theSameElementsInOrderAs Array(0.0, 1.0, 2.0, 1.0, 1.0)
    fold3_1_test.getColumn(0) should contain theSameElementsInOrderAs Array(1.0, 1.0, 0.0, 2.0)
    fold3_1_test.getMatrix(0 until 4, 1 to 3) should contain theSameElementsInOrderAs Array(
      Array(60.0,61.0,6.1),  // label 1
      Array(10.0,11.0,1.1),  // label 1
      Array(20.0,21.0,2.1),  // label 0
      Array(70.0,71.0,7.1))  // label 2
    //val t1 = fold2_1_test.getFeatures.foldLeft("")((a,p) => a + "\n" + p.mkString(",") + ";")
    //println(t1)
    fold3_1_train.getMatrix(0 until 5, 1 to 3) should contain theSameElementsInOrderAs Array(
      Array(90.0,91.0,9.1),  // label 0
      Array(30.0,31.0,3.1),  // label 1
      Array(40.0,41.0,4.2),  // label 2
      Array(80.0,81.0,8.1),  // label 1
      Array(50.0,51.0,5.1))  // label 1

    // labelling keeps changing
    val fold3_2 = folds3(1)
    val fold3_2_train = fold3_2._1
    val fold3_2_test = fold3_2._2
    fold3_2_train.getColumn(0) should contain theSameElementsInOrderAs Array(1.0, 0.0, 2.0, 1.0, 1.0)
    fold3_2_test.getColumn(0) should contain theSameElementsInOrderAs Array(0.0, 1.0, 2.0, 1.0)

  } // test

  it should "hold and split labeled data (percentage for two folds)" in {

    val labels1 = Array( 1.0,  0.0,  1.0,  2.0,  1.0,  1.0,  2.0,  1.0,  0.0)
    val mat0 : Array[Array[Double]] = Array(
      Array(10.0, 20.0, 30.0, 40.0, 50.0, 60.0, 70.0, 80.0, 90.0),
      Array(11.0, 21.0, 31.0, 41.0, 51.0, 61.0, 71.0, 81.0, 91.0),
      Array( 1.1,  2.1,  3.1,  4.2,  5.1,  6.1,  7.1,  8.1,  9.1)
    )
    val data1 : Array[Array[Double]] = Array(
      labels1,
      mat0(0),
      mat0(1),
      mat0(2)
    ).transpose
    val mat1 = MatrixData.dense(data1, new Random(5587) )
    val sz1 = mat1.size
    sz1._1 shouldBe labels1.length // no. of lines
    sz1._2 shouldBe data1(0).length // no. columns
    val row1 = mat1(0)
    //println((row1._1, row1._2.mkString(",")))
    row1(0) shouldBe data1(0)(0)
    row1(1) shouldBe data1(0)(1)
    row1(2) shouldBe data1(0)(2)
    row1(3) shouldBe data1(0)(3)
    mat1.getColumn(0) should contain theSameElementsInOrderAs labels1
    val features1 = mat1.getMatrix(1 to 8, 1 to 3)
    //println(features1(8).mkString(","))
    features1(0)(0) shouldBe data1(1)(1)
    features1(0)(1) shouldBe data1(1)(2)
    features1(0)(2) shouldBe data1(1)(3)
    features1(7)(0) shouldBe data1(8)(1)
    features1(7)(1) shouldBe data1(8)(2)
    features1(7)(2) shouldBe data1(8)(3)
    mat1.numLabels shouldBe 3
    mat1.classSize(0.0) shouldBe 2
    mat1.classSize(1.0) shouldBe 5
    mat1.classSize(2.0) shouldBe 2
    mat1.getColumn(0) should contain theSameElementsInOrderAs labels1


    val rows1: Array[mat1.Row] = mat1.getRows(List(1,8))
    rows1(0)(0) shouldBe data1(1)(0)
    rows1(0)(1) shouldBe data1(1)(1)
    rows1(0)(2) shouldBe data1(1)(2)
    rows1(0)(3) shouldBe data1(1)(3)
    rows1(1)(1) shouldBe data1(8)(1)
    rows1(1)(2) shouldBe data1(8)(2)
    rows1(1)(3) shouldBe data1(8)(3)

    val folds2Tmp: Either[ADWError, (DenseMatrixData, DenseMatrixData)] = mat1.splitStratifiedPercent(0.7)
    folds2Tmp.isRight shouldBe true
    val folds2 = folds2Tmp.right.get
    val fold2_1_train = folds2._1
    val fold2_1_test = folds2._2
    // Check that the expected sizes are correct. Note that the total number of label tallies with the data set
    // Notice that the class 2 does not split evenly (no modulo division possible), so training gets more data
    fold2_1_train.size shouldBe ((5,4)) // 1 of label 0, 2 of label 1, 1 of label 2
    fold2_1_test.size shouldBe ((4,4)) // 1 of label 0, 3 of label 1, 1 of label 2
    fold2_1_train.numLabels shouldBe 3
    fold2_1_test.numLabels shouldBe 3
    fold2_1_train.classSize(0.0) shouldBe 1
    fold2_1_train.classSize(1.0) shouldBe 3
    fold2_1_train.classSize(2.0) shouldBe 1
    fold2_1_test.classSize(0.0) shouldBe 1
    fold2_1_test.classSize(1.0) shouldBe 2      // data set size not a multiple of fold size
    fold2_1_test.classSize(2.0) shouldBe 1
    fold2_1_test.getColumn(0) should contain theSameElementsInOrderAs Array(1.0, 0.0, 1.0, 2.0)
    fold2_1_train.getColumn(0) should contain theSameElementsInOrderAs Array(2.0, 1.0, 0.0, 1.0, 1.0)
    fold2_1_test.getMatrix(0 until 4, 1 to 3) should contain theSameElementsInOrderAs Array(
      Array(50.0,51.0,5.1),  // label 1
      Array(20.0,21.0,2.1),  // label 0
      Array(30.0,31.0,3.1),  // label 1
      Array(40.0,41.0,4.2))  // label 2
    //val t1 = fold2_1_test.getFeatures.foldLeft("")((a,p) => a + "\n" + p.mkString(",") + ";")
    //println(t1)
    fold2_1_train.getMatrix(0 until 5, 1 to 3) should contain theSameElementsInOrderAs Array(
      Array(70.0,71.0,7.1),  // label 2
      Array(60.0,61.0,6.1),  // label 1
      Array(90.0,91.0,9.1),  // label 0
      Array(80.0,81.0,8.1),  // label 1
      Array(10.0,11.0,1.1))  // label 1
  }

  it should "hold and split sparse binary labeled data (one-class clean)" in {

    val labels1 = Array( 1.0,  0.0,  0.0,  0.0,  1.0,  0.0,  0.0,  1.0,  0.0)
    val mat0 : Array[Array[Double]] = Array(
      Array(10.0, 20.0, 30.0, 40.0, 50.0, 60.0, 70.0, 80.0, 90.0),
      Array(11.0, 21.0, 31.0, 41.0, 51.0, 61.0, 71.0, 81.0, 91.0),
      Array( 1.1,  2.1,  3.1,  4.2,  5.1,  6.1,  7.1,  8.1,  9.1)
    )
    val data1 : Array[Array[Double]] = Array(
      labels1,
      mat0(0),
      mat0(1),
      mat0(2)
    ).transpose
    val mat1 = MatrixData.dense(data1, new Random(5587))
    val sz1 = mat1.size
    sz1._1 shouldBe labels1.length // no. of lines
    sz1._2 shouldBe data1(0).length // no. columns

    val folds2Tmp: Either[ADWError, Stream[(DenseMatrixData, DenseMatrixData)]] = mat1.splitOneClassFolds(2)
    folds2Tmp.isRight shouldBe true
    val folds2 = folds2Tmp.right.get
    // 10 samples divided by 2 folds
    folds2.length shouldBe 2
    val fold2_1 = folds2.head
    val fold2_1_train = fold2_1._1
    val fold2_1_test = fold2_1._2
    // Check that the expected sizes are correct. Note that the total number of label tallies with the data set
    // Notice that the class 2 does not split evenly (no modulo division possible), so training gets more data
    fold2_1_train.size shouldBe ((2,4)) // 2 of label 1 (3 / 2 = 1 but train sets gets last unused index)
    fold2_1_test.size shouldBe ((4,4)) // 1 of label 1 (see above), 3 of label 0 (6 / 2 = 3)
    fold2_1_train.numLabels shouldBe 1
    fold2_1_test.numLabels shouldBe 2

    // Cross-validation training and test sets must not have the same records
    //printRows(fold2_1_train)
    //printRows(fold2_1_test)
    disjointRows(fold2_1_train,fold2_1_train) shouldBe false
    disjointRows(fold2_1_train,fold2_1_test) shouldBe true

    // The test data set must hold the same distribution as the original data set (stratified data sets)
    correctDistribution(mat1, fold2_1_test)
    correctDistribution(mat1, fold2_1_train) shouldBe false
    // But the train test set must only have the "good" signals
    fold2_1_train.classes.size shouldBe 1
    fold2_1_train.classes.contains(0) shouldBe false // no outliers
    fold2_1_train.classes.contains(1) shouldBe true // just good

  }

  "SparseDataMatrix" should "hold and split labeled data (number of folds)" in {

    val data1 = Array(
      (+1.0, Map(1->10.0, 2->11.0, 3->1.1)),
      (+0.0, Map(1->20.0, 2->21.0, 3->2.1)),
      (+1.0, Map(1->30.0, 2->31.0, 3->3.1)),
      (+2.0, Map(1->40.0, 2->41.0, 3->4.2)),
      (+1.0, Map(1->50.0, 2->51.0, 3->5.1)),
      (+1.0, Map(1->60.0, 2->61.0, 3->6.1)),
      (+2.0, Map(1->70.0, 2->71.0, 3->7.1)),
      (+1.0, Map(1->80.0, 2->81.0, 3->8.1)),
      (+0.0, Map(1->90.0, 2->91.0, 3->9.1))
    )
    val mat1 = MatrixData.sparse(data1, new Random(5587))
    val sz1 = mat1.size
    sz1._1 shouldBe data1.length // no. of lines
    sz1._2 shouldBe (data1.head._2.size + 1) // no. columns
    val row1 = mat1(0)
    //println((row1._1, row1._2.mkString(",")))
    row1._1 shouldBe data1.head._1
    row1._2(1) shouldBe data1.head._2(1)
    row1._2(2) shouldBe data1.head._2(2)
    row1._2(3) shouldBe data1.head._2(3)
    mat1.getColumn(0) should contain theSameElementsInOrderAs data1.map(_._1)

    mat1.at(0,0) shouldBe 1.0
    mat1.at(1,0) shouldBe 0.0
    mat1.at(3,0) shouldBe 2.0
    mat1.at(8,0) shouldBe 0.0

    mat1.at(0,1) shouldBe 10.0
    mat1.at(0,2) shouldBe 11.0
    mat1.at(0,3) shouldBe 1.1

    val features1 = mat1.getMatrix(1 to 8, 1 to 3)
    //println(features1(8).mkString(","))
    //val t1 = features1.foldLeft("")((a,p) => a + "\n" + p.mkString(",") + ";")
    //println(t1)
    features1(0)(0) shouldBe data1(1)._2(1)
    features1(0)(1) shouldBe data1(1)._2(2)
    features1(0)(2) shouldBe data1(1)._2(3)
    features1(7)(0) shouldBe data1(8)._2(1)
    features1(7)(1) shouldBe data1(8)._2(2)
    features1(7)(2) shouldBe data1(8)._2(3)
    mat1.numLabels shouldBe 3
    mat1.classSize(0.0) shouldBe 2
    mat1.classSize(1.0) shouldBe 5
    mat1.classSize(2.0) shouldBe 2
    mat1.getColumn(0) should contain theSameElementsInOrderAs Array(1.0, 0.0, 1.0, 2.0, 1.0, 1.0, 2.0, 1.0, 0.0)


    val folds2Tmp: Either[ADWError, Stream[(SparseMatrixData, SparseMatrixData)]] = mat1.splitStratifiedFolds(2)
    folds2Tmp.isRight shouldBe true
    val folds2 = folds2Tmp.right.get
    // 10 samples divided by 2 folds
    folds2.length shouldBe 2
    val fold2_1 = folds2.head
    val fold2_1_train = fold2_1._1
    val fold2_1_test = fold2_1._2
    // Check that the expected sizes are correct. Note that the total number of label tallies with the data set
    // Notice that the class 2 does not split evenly (no modulo division possible), so training gets more data
    fold2_1_train.size shouldBe ((5,4)) // 1 of label 0, 3 of label 1, 1 of label 2
    fold2_1_test.size shouldBe ((4,4)) // 1 of label 0, 2 of label 1, 1 of label 2
    fold2_1_train.numLabels shouldBe 3
    fold2_1_test.numLabels shouldBe 3
    fold2_1_train.classSize(0.0) shouldBe 1
    fold2_1_train.classSize(1.0) shouldBe 3
    fold2_1_train.classSize(2.0) shouldBe 1
    fold2_1_test.classSize(0.0) shouldBe 1
    fold2_1_test.classSize(1.0) shouldBe 2      // data set size not a multiple of fold size
    fold2_1_test.classSize(2.0) shouldBe 1
    fold2_1_test.getColumn(0) should contain theSameElementsInOrderAs Array(2.0, 1.0, 0.0, 1.0)
    fold2_1_train.getColumn(0)should contain theSameElementsInOrderAs Array(1.0, 2.0, 1.0, 0.0, 1.0)
    fold2_1_test.getMatrix(0 until 4, 1 to 3) should contain theSameElementsInOrderAs Array(
      Array(70.0,71.0,7.1),  // label 2
      Array(60.0,61.0,6.1),  // label 1
      Array(90.0,91.0,9.1),  // label 0
      Array(80.0,81.0,8.1))  // label 1
    //val t1 = fold2_1_test.getFeatures.foldLeft("")((a,p) => a + "\n" + p.mkString(",") + ";")
    //println(t1)
    fold2_1_train.getMatrix(0 until 5, 1 to 3) should contain theSameElementsInOrderAs Array(
      Array(50.0,51.0,5.1),  // label 1
      Array(40.0,41.0,4.2),  // label 2
      Array(30.0,31.0,3.1),  // label 1
      Array(20.0,21.0,2.1),  // label 0
      Array(10.0,11.0,1.1))  // label 1

    // Get the the second fold. Notice that the sizes of the train and tests data sets are the same.
    // However the elements are different and more importantly no data records ever appears in both
    // training and the test data sets.

    val fold2_2 = folds2(1)
    val fold2_2_train = fold2_2._1
    val fold2_2_test = fold2_2._2
    // Check that the expected sizes are correct. Note that the total number of label tallies with the data set
    // Notice that the class 2 does not split evenly (no modulo division possible), so training gets more data
    fold2_2_train.size shouldBe ((5,4)) // 1 of label 0, 3 of label 1, 1 of label 2
    fold2_2_test.size shouldBe ((4,4)) // 1 of label 0, 2 of label 1, 1 of label 2
    fold2_2_train.numLabels shouldBe 3
    fold2_2_test.numLabels shouldBe 3
    fold2_2_train.classSize(0.0) shouldBe 1
    fold2_2_train.classSize(1.0) shouldBe 3
    fold2_2_train.classSize(2.0) shouldBe 1
    fold2_2_test.classSize(0.0) shouldBe 1
    fold2_2_test.classSize(1.0) shouldBe 2      // data set size not a multiple of fold size
    fold2_2_test.classSize(2.0) shouldBe 1
    fold2_2_train.getColumn(0) should contain theSameElementsInOrderAs Array(1.0, 2.0, 1.0, 0.0, 1.0)
    fold2_2_test.getColumn(0) should contain theSameElementsInOrderAs Array(1.0, 0.0, 1.0, 2.0)
    fold2_2_test.getMatrix(0 until 4, 1 to 3) should contain theSameElementsInOrderAs Array(
      Array(50.0,51.0,5.1),  // label 1
      Array(90.0,91.0,9.1),  // label 0
      Array(80.0,81.0,8.1),  // label 1
      Array(40.0,41.0,4.2))  // label 2
    //val t1 = fold2_2_test.getMatrix(0 until 5, 1 to 3).foldLeft("")((a,p) => a + "\n" + p.mkString(",") + ";")
    //println(t1)
    fold2_2_train.getMatrix(0 until 5, 1 to 3) should contain theSameElementsInOrderAs Array(
      Array(60.0,61.0,6.1),  // label 1
      Array(70.0,71.0,7.1),  // label 2
      Array(10.0,11.0,1.1),  // label 1
      Array(20.0,21.0,2.1),  // label 0
      Array(30.0,31.0,3.1))  // label 1

    val folds3Tmp: Either[ADWError, Stream[(SparseMatrixData, SparseMatrixData)]] = mat1.splitStratifiedFolds(2)
    folds3Tmp.isRight shouldBe true
    val folds3 = folds3Tmp.right.get
    val fold3_1 = folds3.head
    val fold3_1_train = fold3_1._1
    val fold3_1_test = fold3_1._2
    // Check that the expected sizes are correct. Note that the total number of label tallies with the data set
    // Notice that the class 2 does not split evenly (no modulo division possible), so training gets more data
    fold3_1_train.size shouldBe ((5,4)) // 1 of label 0, 3 of label 1, 1 of label 2
    fold3_1_test.size shouldBe ((4,4)) // 1 of label 0, 2 of label 1, 1 of label 2
    fold3_1_train.numLabels shouldBe 3
    fold3_1_test.numLabels shouldBe 3
    fold3_1_train.classSize(0.0) shouldBe 1
    fold3_1_train.classSize(1.0) shouldBe 3
    fold3_1_train.classSize(2.0) shouldBe 1
    fold3_1_test.classSize(0.0) shouldBe 1
    fold3_1_test.classSize(1.0) shouldBe 2      // data set size not a multiple of fold size
    fold3_1_test.classSize(2.0) shouldBe 1
    fold3_1_train.getColumn(0) should contain theSameElementsInOrderAs Array(0.0, 1.0, 2.0, 1.0, 1.0)
    fold3_1_test.getColumn(0) should contain theSameElementsInOrderAs Array(1.0, 1.0, 0.0, 2.0)
    fold3_1_test.getMatrix(0 until 4, 1 to 3) should contain theSameElementsInOrderAs Array(
      Array(60.0,61.0,6.1),  // label 1
      Array(10.0,11.0,1.1),  // label 1
      Array(20.0,21.0,2.1),  // label 0
      Array(70.0,71.0,7.1))  // label 2
    //val t1 = fold2_1_test.getFeatures.foldLeft("")((a,p) => a + "\n" + p.mkString(",") + ";")
    //println(t1)
    fold3_1_train.getMatrix(0 until 5, 1 to 3) should contain theSameElementsInOrderAs Array(
      Array(90.0,91.0,9.1),  // label 0
      Array(30.0,31.0,3.1),  // label 1
      Array(40.0,41.0,4.2),  // label 2
      Array(80.0,81.0,8.1),  // label 1
      Array(50.0,51.0,5.1))  // label 1

    // labelling keeps changing
    val fold3_2 = folds3(1)
    val fold3_2_train = fold3_2._1
    val fold3_2_test = fold3_2._2
    fold3_2_train.getColumn(0) should contain theSameElementsInOrderAs Array(1.0, 0.0, 2.0, 1.0, 1.0)
    fold3_2_test.getColumn(0) should contain theSameElementsInOrderAs Array(0.0, 1.0, 2.0, 1.0)
  }

  it should "hold and split sparse labeled data (percentage for two folds)" in {

    val data1 = Array(
      (+1.0, Map(1->10.0, 2->11.0, 3->1.1)),
      (+0.0, Map(1->20.0, 2->21.0, 3->2.1)),
      (+1.0, Map(1->30.0, 2->31.0, 3->3.1)),
      (+2.0, Map(1->40.0, 2->41.0, 3->4.2)),
      (+1.0, Map(1->50.0, 2->51.0, 3->5.1)),
      (+1.0, Map(1->60.0, 2->61.0, 3->6.1)),
      (+2.0, Map(1->70.0, 2->71.0, 3->7.1)),
      (+1.0, Map(1->80.0, 2->81.0, 3->8.1)),
      (+0.0, Map(1->90.0, 2->91.0, 3->9.1))
    )
    val mat1 = MatrixData.sparse(data1, new Random(5587))
    val sz1 = mat1.size
    sz1._1 shouldBe data1.length // no. of lines
    sz1._2 shouldBe (data1.head._2.size + 1) // no. columns
    val row1 = mat1(0)
    //println((row1._1, row1._2.mkString(",")))
    row1._1 shouldBe data1.head._1
    row1._2(1) shouldBe data1.head._2(1)
    row1._2(2) shouldBe data1.head._2(2)
    row1._2(3) shouldBe data1.head._2(3)
    mat1.getColumn(0) should contain theSameElementsInOrderAs data1.map(_._1)
    val features1 = mat1.getMatrix(1 to 8, 1 to 3)
    //println(features1(8).mkString(","))
    features1(0)(0) shouldBe data1(1)._2(1)
    features1(0)(1) shouldBe data1(1)._2(2)
    features1(0)(2) shouldBe data1(1)._2(3)
    features1(7)(0) shouldBe data1(8)._2(1)
    features1(7)(1) shouldBe data1(8)._2(2)
    features1(7)(2) shouldBe data1(8)._2(3)
    mat1.numLabels shouldBe 3
    mat1.classSize(0.0) shouldBe 2
    mat1.classSize(1.0) shouldBe 5
    mat1.classSize(2.0) shouldBe 2
    mat1.getColumn(0) should contain theSameElementsInOrderAs Array(1.0, 0.0, 1.0, 2.0, 1.0, 1.0, 2.0, 1.0, 0.0)

    val rows1: Array[mat1.Row] = mat1.getRows(List(1,8))
    rows1(0)._1 shouldBe data1(1)._1
    rows1(0)._2(1) shouldBe data1(1)._2(1)
    rows1(0)._2(2) shouldBe data1(1)._2(2)
    rows1(0)._2(3) shouldBe data1(1)._2(3)
    rows1(1)._2(1) shouldBe data1(8)._2(1)
    rows1(1)._2(2) shouldBe data1(8)._2(2)
    rows1(1)._2(3) shouldBe data1(8)._2(3)

    val folds2Tmp: Either[ADWError, (SparseMatrixData, SparseMatrixData)] = mat1.splitStratifiedPercent(0.7)
    folds2Tmp.isRight shouldBe true
    val folds2 = folds2Tmp.right.get
    val fold2_1_train = folds2._1
    val fold2_1_test = folds2._2
    // Check that the expected sizes are correct. Note that the total number of label tallies with the data set
    // Notice that the class 2 does not split evenly (no modulo division possible), so training gets more data
    fold2_1_train.size shouldBe ((5,4)) // 1 of label 0, 2 of label 1, 1 of label 2
    fold2_1_test.size shouldBe ((4,4)) // 1 of label 0, 3 of label 1, 1 of label 2
    fold2_1_train.numLabels shouldBe 3
    fold2_1_test.numLabels shouldBe 3
    fold2_1_train.classSize(0.0) shouldBe 1
    fold2_1_train.classSize(1.0) shouldBe 3
    fold2_1_train.classSize(2.0) shouldBe 1
    fold2_1_test.classSize(0.0) shouldBe 1
    fold2_1_test.classSize(1.0) shouldBe 2      // data set size not a multiple of fold size
    fold2_1_test.classSize(2.0) shouldBe 1
    fold2_1_test.getColumn(0) should contain theSameElementsInOrderAs Array(1.0, 0.0, 1.0, 2.0)
    fold2_1_train.getColumn(0) should contain theSameElementsInOrderAs Array(2.0, 1.0, 0.0, 1.0, 1.0)
    fold2_1_test.getMatrix(0 until 4, 1 to 3) should contain theSameElementsInOrderAs Array(
      Array(50.0,51.0,5.1),  // label 1
      Array(20.0,21.0,2.1),  // label 0
      Array(30.0,31.0,3.1),  // label 1
      Array(40.0,41.0,4.2))  // label 2
    //val t1 = fold2_1_test.getFeatures.foldLeft("")((a,p) => a + "\n" + p.mkString(",") + ";")
    //println(t1)
    fold2_1_train.getMatrix(0 until 5, 1 to 3) should contain theSameElementsInOrderAs Array(
      Array(70.0,71.0,7.1),  // label 2
      Array(60.0,61.0,6.1),  // label 1
      Array(90.0,91.0,9.1),  // label 0
      Array(80.0,81.0,8.1),  // label 1
      Array(10.0,11.0,1.1))  // label 1
  }

  def printRows(a: SparseMatrixData) : Unit = {
    val t1: Array[(Double, a.SparseFeature)] = a.getRows(0 until a.size._1)
    val p1 = t1.foldLeft("")((a,p) => a + "\n" + p._1 + " => " + p._2.mkString(", ") + ";")
    println(p1)
  }

  it should "hold and split sparse binary labeled data (one-class clean)" in {

    val data1 = Array(
      (+1.0, Map(1 -> 10.0, 2 -> 11.0, 3 -> 1.1)),
      (+0.0, Map(1 -> 20.0, 2 -> 21.0, 3 -> 2.1)),
      (+0.0, Map(1 -> 30.0, 2 -> 31.0, 3 -> 3.1)),
      (+0.0, Map(1 -> 40.0, 2 -> 41.0, 3 -> 4.2)),
      (+1.0, Map(1 -> 50.0, 2 -> 51.0, 3 -> 5.1)),
      (+0.0, Map(1 -> 60.0, 2 -> 61.0, 3 -> 6.1)),
      (+0.0, Map(1 -> 70.0, 2 -> 71.0, 3 -> 7.1)),
      (+1.0, Map(1 -> 80.0, 2 -> 81.0, 3 -> 8.1)),
      (+0.0, Map(1 -> 90.0, 2 -> 91.0, 3 -> 9.1))
    )
    val mat1 = MatrixData.sparse(data1, new Random(5587))
    val sz1 = mat1.size
    sz1._1 shouldBe data1.length // no. of lines
    sz1._2 shouldBe (data1.head._2.size + 1) // no. columns

    val folds2Tmp: Either[ADWError, Stream[(SparseMatrixData, SparseMatrixData)]] = mat1.splitOneClassFolds(2)
    folds2Tmp.isRight shouldBe true
    val folds2 = folds2Tmp.right.get
    // 10 samples divided by 2 folds
    folds2.length shouldBe 2
    val fold2_1 = folds2.head
    val fold2_1_train = fold2_1._1
    val fold2_1_test = fold2_1._2
    // Check that the expected sizes are correct. Note that the total number of label tallies with the data set
    // Notice that the class 2 does not split evenly (no modulo division possible), so training gets more data
    fold2_1_train.size shouldBe ((2,4)) // 2 of label 1 (3 / 2 = 1 but train sets gets last unused index)
    fold2_1_test.size shouldBe ((4,4)) // 1 of label 1 (see above), 3 of label 0 (6 / 2 = 3)
    fold2_1_train.numLabels shouldBe 1
    fold2_1_test.numLabels shouldBe 2

    val fold2_2 = folds2(1)
    //val fold2_2_train = fold2_2._1
    //val fold2_2_test = fold2_2._2
    //printRows(fold2_1_train)
    //printRows(fold2_1_test)
    //printRows(fold2_2_train)
    //printRows(fold2_2_test)

    // Cross-validation training and test sets must not have the same records
    disjointRows(fold2_1_train,fold2_1_train) shouldBe false
    disjointRows(fold2_1_train,fold2_1_test) shouldBe true

    // The test data set must hold the same distribution as the original data set (stratified data sets)
    correctDistribution(mat1, fold2_1_test)
    correctDistribution(mat1, fold2_1_train) shouldBe false
    // But the train test set must only have the "good" signals
    fold2_1_train.classes.size shouldBe 1
    fold2_1_train.classes.contains(0) shouldBe false // no outliers
    fold2_1_train.classes.contains(1) shouldBe true // just good
  }


}
