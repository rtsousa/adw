/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.model


import com.github.cjlin1.SVM
import org.scalatest._
import pt.inescn.etl.Base.ScalerState.{RangeRecorded, Unscaled}
import pt.inescn.etl.LightSVM.{SparseFold, ValidateDepVar}
import pt.inescn.models.Algorithms.TestAlgorithm1
import pt.inescn.models.Algorithms
import pt.inescn.models.Data.SparseMatrixData
import pt.inescn.params.ParamSearch.{ParamSelect, Strategy}
import pt.inescn.utils.{ADWError, Utils}
import java.lang.{System => JSystem}

import pt.inescn.etl.Load.Source
import pt.inescn.etl.Transform.{NOP, Scale, TransformParams}
import pt.inescn.etl.{LightSVM, Transform}
import pt.inescn.eval._
import pt.inescn.models.Base.Prediction
import pt.inescn.samplers.Distribution

/**
  *
  * For examples of use of "inside":
  * @see http://www.scalatest.org/user_guide/other_goodies
  *
  * test:compile
  * test:console
  * test:consoleQuick
  * test:run
  * test:runMain
  *
  * sbt test
  * sbt "testOnly pt.inescn.model.ModelSpec"
  *
  * Created by hmf on 10-08-2017.
  *
  * https://stackoverflow.com/questions/14831246/access-scalatest-test-name-from-inside-test
  * class ModelSpec extends FlatSpec with Matchers with fixture.TestDataFixture {
  */
class ModelSpec extends FlatSpec with Matchers with Inside  {
  val eps = 1e-6

  "Sparse libSVM loader"  should "load the multi-class data implicitly" in {

    import pt.inescn.etl.Load._

    import better.files.Dsl._
    val trainData = cwd / "data/libsvm/svmguide1"
    //val testData = cwd / "data/libsvm/svmguide1.t"
    //val tmp = File(JSystem.getProperty("java.io.tmpdir"))

    val tmp1 = Source.libSVMSparse(ValidateDepVar.oneClass,SparseFold.binaryClassFail) // SparseFold.oneClassTrain)
    val d1: Either[ADWError, SparseMatrixData] = tmp1.map(trainData)
    d1.isRight shouldBe true

    val alg1: TestAlgorithm1 = TestAlgorithm1()
    // https://github.com/atnos-org/eff/issues/72
    // https://github.com/scala/bug/issues/5589
    /*
    val r1 = for {
      di1: SparseMatrixData[OneClass] <- d1
      da1: (SparseMatrixData[OneClass], SparseMatrixData[OneClass]) <- di1.splitStratifiedPercent(0.8)
      mod1 <- alg1.fit(da1._1, null)
      pr1 <- alg1.predict(da1._2, mod1)
    } yield pr1
    r1.isRight shouldBe true
    */
    val r2: Either[ADWError, Prediction[alg1.Label]] = d1.flatMap{
       (d1: SparseMatrixData) =>
         d1.splitStratifiedPercent(0.8).flatMap {
           da1 =>
             alg1.fit(da1._1,("kernel 1", "k1p1")).flatMap {
               mod1 =>
                 alg1.predict(da1._2, mod1)
             } // fit
         } // split
     }
   r2.isRight shouldBe true
  }

  "LibSVM wrapper" should "do SVM classification on LightSVM data files" in {

    // Load SVMLibSpec Astroparticle Physics data into sparse matrix
    // Create set of cross-validation set (examples use a CV-5 test)
    // Initialize SVM with defaults for multi-class classification
    // Use prepareCrossValidation and check that we the same mean
    // Repeat above with testParams

    // NOTE: Scalatest runs the test suites in parallel. This means that
    // the files here could be overridden by other tests nd visa.-versa.
    // Use unique file names to avoid problems.

    import better.files._
    import better.files.Dsl._

    //println(s"this.suiteId = ${this.suiteId}")
    //println(s"this.suiteName = ${this.suiteName}")

    val trainData = cwd / "data/libsvm/svmguide1"
    val testData = cwd / "data/libsvm/svmguide1.t"
    val tmp = File(JSystem.getProperty("java.io.tmpdir"))

    val out = tmp / suiteName
    val dirOut = Utils.temporaryFilename(out)
    val d = File(dirOut)
    d.createDirectories()

    val s0: SVM.Scale[Unscaled] = SVM.Scale(outDir = dirOut)
    val r0: Either[String, SVM.Scale[RangeRecorded]] = s0.xLowerTo(-1).xUpperTo(1).scale(trainData, suiteName)
    r0.isRight shouldBe true
    val scaledTrain = r0.right.get.scaledData(suiteName)
    scaledTrain.name shouldBe ("svmguide1.scaled" + "." + suiteName)
    scaledTrain.exists shouldBe true

    val r1 = r0.flatMap( _.reScale(testData, suiteName) )
    r1.isRight shouldBe true
    val scaledTest = r1.right.get.reScaledData(suiteName)
    scaledTest.name shouldBe ("svmguide1.t.scaled" + "." + suiteName)
    scaledTest.exists shouldBe true

    val svmLoader = Source.libSVMSparse(ValidateDepVar.binaryClassification, SparseFold.binaryClassFail)
    val trainToScaleE: Either[ADWError, SparseMatrixData] = svmLoader.map(trainData)
    trainToScaleE.isRight shouldBe true
    val trainToScale = trainToScaleE.right.get

    // Check if scaling is the same as SVMLib's scaling
    // We scale data
    // default: scaleY = false
    Transform.scale(-1.0, 1.0, trainToScale)

    val (nrows,ncols) = trainToScale.size
    val rows = 0 until nrows
    val scaledRows: Array[(Double, trainToScale.SparseFeature)] = trainToScale.getRows(rows)

    // Get LibSVM's results
    val train = svmLoader.map(scaledTrain)
    train.isRight shouldBe true
    val trainMat: SparseMatrixData = train.right.get
    trainMat.size shouldBe ((3089,5))
    trainMat.numLabels shouldBe 2
    val trainMatData: Array[(Double, trainMat.SparseFeature)] = trainMat.getRows(rows)
    // The same?
    scaledRows should contain theSameElementsInOrderAs trainMatData

    // Note: data has been randomized
    val trainFold = dirOut / ("svmguide1f.scaled" + "." + suiteName)
    trainMat.saveTo(trainFold)

    val test = svmLoader.map(scaledTest)
    // println(s"test = $test")
    test.isRight shouldBe true
    val testMat = test.right.get
    testMat.size shouldBe ((4000,5))
    testMat.numLabels shouldBe 2

    // Note: data has been randomized
    val testFold = dirOut / ("svmguide1f.t.scaled" + "." + suiteName)
    testMat.saveTo(testFold)

    // Test SVM algorithm
    val a1 = Algorithms.MultiClassRadialBasisSVM(dirOut)

    // Generate the full set of all possible parameter combinations
    // Note: if request more samples than are possible, the error will be
    // detected and returned as an empty type. Make sure this does not
    // happen.
    val numSamples = 10
    val ps1 = Strategy.grid(numSamples)(a1.params)
    val pa1 = ps1.toList
    //println(pa1.mkString(";\n"))
    pa1.length shouldBe 121 // we count from the start pint + number of samples

    val expId = 0

    // Use the default values for Radial Basis SVM
    // -s 0 : C-SVC
    // -t 2 : radial basis function
    // -g 1/num_features : radial basis function gamma
    // -c 1 : cost
    val r2 = a1.fit(expId, trainFold, (1.0/trainMat.numLabels, 1.0))
    //println(r2)
    r2.isRight shouldBe true
    //println(r2.right.get)

    val r3: Either[ADWError, File] = r2.flatMap(m => a1.predict(expId, testFold, m))
    r3.isRight shouldBe true
    //println(r3)
    r3.isRight shouldBe true

    val prediction = r3.right.get
    val met1: Either[ADWError, BinaryClassificationMetrics#All] = a1.evaluate(expId, testFold, prediction)
    met1.isRight shouldBe true
    met1.right.get.accuracy.value shouldBe ( 0.96 +- 0.08)   // 0.9615
  }

  it should "do SVM cross-validated classification on LightSVM data files" in {

    // Load SVMLibSpec Astroparticle Physics data into sparse matrix
    // Create set of cross-validation set (examples use a CV-5 test)
    // Initialize SVM with defaults for multi-class classification
    // Use prepareCrossValidation and check that we the same mean
    // Repeat above with testParams

    // NOTE: Scalatest runs the test suites in parallel. This means that
    // the files here could be overridden by other tests and visa-versa.
    // Use unique file names to avoid problems.

    import better.files._
    import better.files.Dsl._

    //println(s"this.suiteId = ${this.suiteId}")
    //println(s"this.suiteName = ${this.suiteName}")

    val trainData = cwd / "data/libsvm/svmguide1"
    //val testData = cwd / "data/libsvm/svmguide1.t"
    val tmp = File(JSystem.getProperty("java.io.tmpdir"))
    val out = tmp / suiteName
    val dirOut = Utils.temporaryFilename(out)
    val d = File(dirOut)
    d.createDirectories()

    // We want to pre-process the data. More specifically we need to
    // scale the dada so that SVM can work correctly
    val scale = Scale(-1.0, 1.0)

    // Generate the 5 fold cross-validation training and test data sets
    val nrFold = 5
    val experimentFolder = dirOut / ("an_experiment" + suiteName)
    //val foldFiles = stratifiedCrossValidationDataFiles(trainData, experimentFolder, nrFold)
    val svmLoader: LightSVM.SVMSource[File, SparseMatrixData] = Source.libSVMSparse(ValidateDepVar.binaryClassification, SparseFold.binaryClassFail)
    val foldFiles = ParamSelect.stratifiedCrossValidationDataFiles(trainData, svmLoader, experimentFolder, scale, nrFold)
    //println(foldFiles)
    foldFiles.isRight shouldBe true
    val files = foldFiles.right.get
    files.length shouldBe 5
    files.forall(p => p._2.exists && p._3.exists) shouldBe true
    val train4 = dirOut / "an_experimentModelSpec" / "train4"
    val test4 = dirOut / "an_experimentModelSpec" / "test4"
    val train3 = dirOut / "an_experimentModelSpec" / "train3"
    val test3 = dirOut / "an_experimentModelSpec" / "test3"
    val train2 = dirOut / "an_experimentModelSpec" / "train2"
    val test2 = dirOut / "an_experimentModelSpec" / "test2"
    val train1 = dirOut / "an_experimentModelSpec" / "train1"
    val test1 = dirOut / "an_experimentModelSpec" / "test1"
    val train0 = dirOut / "an_experimentModelSpec" / "train0"
    val test0 = dirOut / "an_experimentModelSpec" / "test0"
    inside(files) { case  List(
    ( Scale(-1.0,1.0,false,v4), `train4`, `test4`),
    ( Scale(-1.0,1.0,false,v3), `train3`, `test3`),
    ( Scale(-1.0,1.0,false,v2), `train2`, `test2`),
    ( Scale(-1.0,1.0,false,v1), `train1`, `test1`),
    ( Scale(-1.0,1.0,false,v0), `train0`, `test0`)) =>

      v4.length shouldBe 4
      v3.length shouldBe 4
      v2.length shouldBe 4
      v1.length shouldBe 4
      v0.length shouldBe 4
    }

    // Test SVM algorithm
    val a1 = Algorithms.MultiClassRadialBasisSVM(dirOut)

    // Generate the full set of all possible parameter combinations
    // Note: if request more samples than are possible, the error will be
    // detected and returned as an empty type. Make sure this does not
    // happen.
    val numSamples = 10
    val ps1 = Strategy.grid(numSamples)(a1.params)
    val pa1 = ps1.toList
    //println(pa1.mkString(";\n"))
    pa1.length shouldBe 121 // we count from the start pint + number of samples

    val train = dirOut / "an_experimentModelSpec" / "train0"
    val test = dirOut / "an_experimentModelSpec" / "test0"

    // Load and count the number of classes
    val trainTmp = svmLoader.map(trainData)
    trainTmp.isRight shouldBe true
    val trainMat: SparseMatrixData = trainTmp.right.get

    val expId = 0

    // Use the default values for Radial Basis SVM
    // -s 0 : C-SVC
    // -t 2 : radial basis function
    // -g 1/num_features : radial basis function gamma
    // -c 1 : cost
    // Get LibSVM's results
    val params = (1.0/trainMat.numLabels, 1.0)
    val trainEvaluate = ParamSelect.trainAndEval(a1) _
    val result = trainEvaluate(expId, train, test, params)
    result.isRight shouldBe true
    val met1 = result.right.get
    met1._1.accuracy.value shouldBe ( 0.96 +- 0.08)   // 0.9615
  }

  it should "do parameter search on SVM cross-validated classification on LightSVM data files" in {

    // Load SVMLibSpec Astroparticle Physics data into sparse matrix
    // Create set of cross-validation set (examples use a CV-5 test)
    // Initialize SVM with defaults for multi-class classification
    // Use prepareCrossValidation and check that we the same mean
    // Repeat above with testParams

    // NOTE: Scalatest runs the test suites in parallel. This means that
    // the files here could be overridden by other tests and visa-versa.
    // Use unique file names to avoid problems.

    import better.files._
    import better.files.Dsl._
    import pt.inescn.features.Stats.{N, Mean}

    //println(s"this.suiteId = ${this.suiteId}")
    //println(s"this.suiteName = ${this.suiteName}")

    val trainData = cwd / "data/libsvm/svmguide1"
    //val testData = cwd / "data/libsvm/svmguide1.t"
    val tmp = File(JSystem.getProperty("java.io.tmpdir"))
    val out = tmp / suiteName
    val dirOut = Utils.temporaryFilename(out)
    val d = File(dirOut)
    d.createDirectories()

    // We want to pre-process the data. More specifically we need to
    // scale the dada so that SVM can work correctly
    val scale = Scale(-1.0, 1.0)

    // Generate the 5 fold cross-validation training and test data sets
    val nrFold = 5
    val experimentFolder = dirOut / ("an_experiment" + suiteName)
    //val foldFiles = stratifiedCrossValidationDataFiles(trainData, experimentFolder, nrFold)
    val svmLoader: LightSVM.SVMSource[File, SparseMatrixData] = Source.libSVMSparse(ValidateDepVar.binaryClassification, SparseFold.binaryClassFail)
    val foldFiles = ParamSelect.stratifiedCrossValidationDataFiles(trainData, svmLoader, experimentFolder, scale, nrFold)
    //println(foldFiles)
    foldFiles.isRight shouldBe true
    val files = foldFiles.right.get
    files.length shouldBe 5
    files.forall( p => p._2.exists && p._3.exists) shouldBe true
    val train4 = dirOut / "an_experimentModelSpec" / "train4"
    val test4 = dirOut / "an_experimentModelSpec" / "test4"
    val train3 = dirOut / "an_experimentModelSpec" / "train3"
    val test3 = dirOut / "an_experimentModelSpec" / "test3"
    val train2 = dirOut / "an_experimentModelSpec" / "train2"
    val test2 = dirOut / "an_experimentModelSpec" / "test2"
    val train1 = dirOut / "an_experimentModelSpec" / "train1"
    val test1 = dirOut / "an_experimentModelSpec" / "test1"
    val train0 = dirOut / "an_experimentModelSpec" / "train0"
    val test0 = dirOut / "an_experimentModelSpec" / "test0"
    inside(files) { case  List(
    ( Scale(-1.0,1.0,false,v4), `train4`, `test4`),
    ( Scale(-1.0,1.0,false,v3), `train3`, `test3`),
    ( Scale(-1.0,1.0,false,v2), `train2`, `test2`),
    ( Scale(-1.0,1.0,false,v1), `train1`, `test1`),
    ( Scale(-1.0,1.0,false,v0), `train0`, `test0`)) =>

      v4.length shouldBe 4
      v3.length shouldBe 4
      v2.length shouldBe 4
      v1.length shouldBe 4
      v0.length shouldBe 4
    }

    // Test SVM algorithm
    val a1 = Algorithms.MultiClassRadialBasisSVM(dirOut)

    // Generate the full set of all possible parameter combinations
    // Note: if request more samples than are possible, the error will be
    // detected and returned as an empty type. Make sure this does not
    // happen.
    val numSamples = 10
    val ps1 = Strategy.grid(numSamples)(a1.params)
    val pa1 = ps1.toList
    //println(pa1.mkString(";\n"))
    pa1.length shouldBe 121 // we count from the start point + number of samples

    // Lets use the default scheduler: ForkJoin pool
    import monix.execution.Scheduler.Implicits.global
    import scala.concurrent.duration._
    val numThreads = java.lang.Runtime.getRuntime.availableProcessors()
    //println(s"numThreads = $numThreads")
    val timeOut = 2.minutes
    val resultFile = dirOut / ("paramsearch_test" + suiteName + ".txt")

    import pt.inescn.params.ParamSearch.ParamSelect
    import java.io.PrintWriter
    import java.nio.channels.FileLock

    // Create logging file to hold results
    val logger: Either[ADWError, (FileLock, PrintWriter)] = ParamSelect.createResultsFile(resultFile)
    logger.isRight shouldBe true
    val (logFileLoc, logFile) = logger.right.get

    /*
    println(s"pa1.length = ${pa1.length}")
    println(s"files.length = ${files.length}")
    println(s"total = ${pa1.length * files.length}") // 121 * 5 * 605
    */

    // Now launch cross-validation search using the specified algorithm and the parameter samples
    val concurrentSearch =  ParamSelect.searchParams( numThreads, timeOut, logger, a1) _
    val result = concurrentSearch(foldFiles, ps1, Some(3))
    //println(result)
    result.isRight shouldBe true

    // Check that all tasks have executed
    val results = resultFile.lineIterator
      .filter( !_.startsWith(ParamSelect.TotalElapsedTime)) // ignore last line that records total processing time
      .map( ParamSelect.parseResult ) // parse the fields of the result file
      .toList // convert into something that can be sorted
      .sortBy( tr => tr.right.get.id) // sort for checking
    //println(results.mkString(";\n"))
    results.length shouldBe 15 // 5 cross-validation data sets * Some(3) parameters

    // Check that all the results have parsed successfully and hld valid results
    // Note that we converted to list because we cross the iterator several times
    val results1: List[Either[ADWError,
      ParamSelect.SearchResult[
        (File, File, (Double, Double)),
        TransformParams,(BinaryClassificationMetrics#All,
        Algorithms.SVMModel)]]] = ParamSelect.parseSearchResults(resultFile, a1).toList
    //println(results1.mkString(";\n"))
    results1.length shouldBe 15 // 5 cross-validation data sets * Some(3) parameters
    results1.count(_.isLeft) shouldBe 0

    // Lets repeat the step above but now order the experiments so that all
    // of the experiment of a single cross-validation for a given parameters
    // are grouped together. Note that any errors will appear at the start
    // of the result
    val results2 = ParamSelect.sortResultsByParameters(a1)(ParamSelect.parseSearchResults(resultFile, a1))
    //println(results2.mkString(";\n"))
    results2.length shouldBe 15 // 5 cross-validation data sets * Some(3) parameters
    // Lets get the experiment ID and the arguments
    val t2 = results2.map( p => (p.right.get.id,p.right.get.input._3) )
    //println(t2.mkString(";\n"))
    // Lets get the 5 cross-validations grouped together
    val t3 = t2.sliding(5,5).toList
    //println(t3.mkString(","))
    // The experiments are correctly grouped by the parameters
    t3 should contain theSameElementsInOrderAs List(
      List((0,(0.03125,0.03125)), (3,(0.03125,0.03125)), (6,(0.03125,0.03125)), (9,(0.03125,0.03125)), (12,(0.03125,0.03125))),
      List((1,(0.03125,0.125)), (4,(0.03125,0.125)), (7,(0.03125,0.125)), (10,(0.03125,0.125)), (13,(0.03125,0.125))),
      List((2,(0.03125,0.5)), (5,(0.03125,0.5)), (8,(0.03125,0.5)), (11,(0.03125,0.5)), (14,(0.03125,0.5)))
    )

    // Lets repeat the step above but now order the experiments so that all
    // of the experiment of a single cross-validation for a given parameters
    // are grouped together. Note that any errors will appear at the start
    // of the result
    val results3 = ParamSelect.groupResultsByParameters(a1)(ParamSelect.parseSearchResults(resultFile, a1))
    //println(results3.mkString(";\n"))
    results3.size shouldBe 3 // 5 cross-validation data sets * Some(3) parameters
    // Lets get the experiment ID and the arguments per cross-validation data set pair
    val t4 = results3.map{ e  =>
      val (k, v) = e
      k -> v.map{ p =>
        val result = p.right.get
        (result.id,result.input._3)
      }
    }
    //println(t4)

    // Each sub-list has the 5 cross-validations grouped together
    // The experiments are correctly grouped by the parameters
    t4((0.03125,0.5)) should contain theSameElementsAs
      List((2,(0.03125,0.5)), (5,(0.03125,0.5)), (8,(0.03125,0.5)), (11,(0.03125,0.5)), (14,(0.03125,0.5)))
    t4((0.03125,0.03125)) should contain theSameElementsAs
      List((0,(0.03125,0.03125)), (6,(0.03125,0.03125)), (3,(0.03125,0.03125)), (12,(0.03125,0.03125)), (9,(0.03125,0.03125)))
    t4((0.03125,0.125)) should contain theSameElementsAs
      List((4,(0.03125,0.125)), (1,(0.03125,0.125)), (7,(0.03125,0.125)), (10,(0.03125,0.125)), (13,(0.03125,0.125)))

    // Now lest calculate the descriptive statistics for each cross-validation experiment
    // Here we simply convert the lines to columns and calculate the stats
    def statsMetric = ParamSelect.statsBinaryClassMetric _

    def scoreStat = ParamSelect.score(a1)(statsMetric) _

    // So parse the file, group by cross-validation experiment and
    // then calculate the stats for each of those groups
    val results4 = ParamSelect.parseSearchResults(resultFile, a1)
    val mapResults1 = ParamSelect.groupResultsByParameters(a1)(results4)
    val stats1 = ParamSelect.applyResultsByParameters(a1)(scoreStat)(mapResults1)
    //println(stats1.mkString(";\n"))

    // Lets use the raw data tom calculate a stat and check if it is correct
    // First get the result metric
    val t5 = results3.map{ e  =>
      val (k, v) = e
      k -> v.map{ p =>
        val result = p.right.get
        (result.id,result.result._1)
      }
    }
    //println(t5.mkString(";\n"))
    // Grab the first experiment with the following parameters
    val k = (0.03125,0.5)
    //println(t5(k))
    // Now calculate tye mean of the accuracy
    val acc1 = t5(k).map(_._2.accuracy)
    val sum1 = acc1.map(_.value).sum
    val mean1 = sum1 / acc1.length
    // This is what we expect as a result
    val expected = Accuracy(Mean(mean1))
    // println(stats1(k))
    // Lets get the results from the file, again select
    // the cross-validation experiment k and ge the mean
    // of the accuracy
    val tt = stats1(k).accuracy.value.mean
    val got = Accuracy(tt)
    // Now compare both results, they should be about te same
    //println(expected)
    //println(got)
    expected.value.value shouldBe (got.value.value +- eps)
    // Another stat are the number of runs in the cross-validation
    // We should have 5 results, one for each fold
    stats1(k).accuracy.value.n shouldBe N(5.0)

    // Now lets select the best solution using some criterion
    // The return value must be an orderable so that we can order (rank)
    // or select the maximum or minimum score. Here we want the most
    // accurate models that is also the most stable (smallest standard
    // deviation). In this case we should use decreasing order or select
    // the maximum
    def scoreMetric(m: BinaryClassificationMetrics#AllColumnsStats): (Double, Double) = {
      val accuracy = m.accuracy.value
      (accuracy.mean.value, -accuracy.standard_deviation.value)
    }
    // def scoreStats: Seq[BinaryClassificationMetrics#All] => (Double, Double) = statsMetric _ andThen scoreMetric
    def scoreStats = ParamSelect.score(a1)(statsMetric) _ andThen scoreMetric

    val score1 = ParamSelect.applyResultsByParameters(a1)(scoreStats)(mapResults1)
    //println(score1.mkString(";\n"))
    score1.size shouldBe 3

    val ranked1 = score1.toSeq.sortBy( p => (-p._2._1, -p._2._2) )
    //println(ranked1.mkString(";\n"))
    val maxRank0 = ranked1.head
    //println(maxRank0)
    val maxRank1 = ranked1.maxBy(_._2)
    //println(maxRank1)
    maxRank0 shouldBe maxRank1
  }

  // Regression
  // https://bids.github.io/2015-06-04-berkeley/intermediate-python/03-sklearn-abalone.html
  // https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/regression.html

  // Single class
  // https://www.quora.com/Is-there-any-dataset-suitable-to-one-class-classification-problem-with-a-semi-supervised-learning-approach
  // http://homepage.tudelft.nl/n9d04/occ/index.html
}
