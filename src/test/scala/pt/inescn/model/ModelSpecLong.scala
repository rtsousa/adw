/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.model

import java.lang.{System => JSystem}

import better.files.File
import org.scalatest._
import pt.inescn.etl.LightSVM.{SparseFold, ValidateDepVar}
import pt.inescn.etl.Load.Source
import pt.inescn.etl.Transform.{NOP, Scale}
import pt.inescn.etl.{LightSVM, Transform}
import pt.inescn.eval._
import pt.inescn.models.Algorithms
import pt.inescn.models.Data.SparseMatrixData
import pt.inescn.params.ParamSearch.{ParamSelect, Strategy}
import pt.inescn.samplers.Distribution
import pt.inescn.utils.{ADWError, Utils}

/**
  *
  * For examples of use of "inside":
  *
  * @see http://www.scalatest.org/user_guide/other_goodies
  *
  *      test:compile
  *      test:console
  *      test:consoleQuick
  *      test:run
  *      test:runMain
  *
  *      sbt test
  *      sbt "testOnly pt.inescn.model.ModelSpecLong"
  *
  *      https://stackoverflow.com/questions/14831246/access-scalatest-test-name-from-inside-test
  *      class ModelSpec extends FlatSpec with Matchers with fixture.TestDataFixture {
  */
class ModelSpecLong extends FlatSpec with Matchers with Inside {
  val eps = 1e-6
  "LibSVM wrapper" should "do grid parameter search with the same results as the LightSVM guide (1)" in {

    // Load SVMLibSpec Astroparticle Physics data into sparse matrix
    // Create set of cross-validation set (examples use a CV-5 test)
    // Initialize SVM with defaults for multi-class classification
    // Use prepareCrossValidation and check that we the same mean
    // Repeat above with testParams

    // NOTE: Scalatest runs the test suites in parallel. This means that
    // the files here could be overridden by other tests and visa-versa.
    // Use unique file names to avoid problems.

    import better.files._
    import better.files.Dsl._

    //println(s"this.suiteId = ${this.suiteId}")
    //println(s"this.suiteName = ${this.suiteName}")

    // Data set
    val trainData = cwd / "data/libsvm/svmguide1"
    val testData = cwd / "data/libsvm/svmguide1.t"
    val tmp = File(JSystem.getProperty("java.io.tmpdir"))
    // create a temporary directory as a scratchpad
    val out = tmp / suiteName
    val dirOut = Utils.temporaryFilename(out)
    val d = File(dirOut)
    d.createDirectories()

    // We want to pre-process the data. More specifically we need to
    // scale the dada so that SVM can work correctly
    val scale = Scale(-1.0, 1.0)

    // Generate the 5 fold cross-validation training and test data sets
    // Place these in the temporary directory created above
    val nrFold = 5
    val experimentFolder = dirOut / ("an_experiment" + suiteName)
    val svmLoader: LightSVM.SVMSource[File, SparseMatrixData] = Source.libSVMSparse(ValidateDepVar.binaryClassification, SparseFold.binaryClassFail)
    val foldFiles = ParamSelect.stratifiedCrossValidationDataFiles(trainData, svmLoader, experimentFolder, scale, nrFold)
    //println(foldFiles)
    foldFiles.isRight shouldBe true
    val files = foldFiles.right.get
    files.length shouldBe 5
    files.forall(p => p._2.exists && p._3.exists) shouldBe true
    val train4 = dirOut / "an_experimentModelSpecLong" / "train4"
    val test4 = dirOut / "an_experimentModelSpecLong" / "test4"
    val train3 = dirOut / "an_experimentModelSpecLong" / "train3"
    val test3 = dirOut / "an_experimentModelSpecLong" / "test3"
    val train2 = dirOut / "an_experimentModelSpecLong" / "train2"
    val test2 = dirOut / "an_experimentModelSpecLong" / "test2"
    val train1 = dirOut / "an_experimentModelSpecLong" / "train1"
    val test1 = dirOut / "an_experimentModelSpecLong" / "test1"
    val train0 = dirOut / "an_experimentModelSpecLong" / "train0"
    val test0 = dirOut / "an_experimentModelSpecLong" / "test0"
    inside(files) { case List(
    (Scale(-1.0, 1.0, false, v4), `train4`, `test4`),
    (Scale(-1.0, 1.0, false, v3), `train3`, `test3`),
    (Scale(-1.0, 1.0, false, v2), `train2`, `test2`),
    (Scale(-1.0, 1.0, false, v1), `train1`, `test1`),
    (Scale(-1.0, 1.0, false, v0), `train0`, `test0`)) =>

      v4.length shouldBe 4
      v3.length shouldBe 4
      v2.length shouldBe 4
      v1.length shouldBe 4
      v0.length shouldBe 4
    }

    // Test SVM algorithm, instantiate iot first
    val a1 = Algorithms.MultiClassRadialBasisSVM(dirOut)

    // Generate the full set of all possible parameter combinations
    // Note: if request more samples than are possible, the error will be
    // detected and returned as an empty type. Make sure this does not
    // happen.
    val numSamples = 10
    val ps1 = Strategy.grid(numSamples)(a1.params)
    val pa1 = ps1.toList
    //println(pa1.mkString(";\n"))
    pa1.length shouldBe 121 // we count from the start point + number of samples

    // Lets use the default scheduler (ForkJoin pool) to execute the parameter search
    import monix.execution.Scheduler.Implicits.global
    import scala.concurrent.duration._
    val numThreads = java.lang.Runtime.getRuntime.availableProcessors()
    //println(s"numThreads = $numThreads")
    // Wait a maximum of 5 minutes for the results
    val timeOut = 5.minutes
    // And place the results in this file
    val resultFile = dirOut / ("astro_paramsearch_test" + suiteName + ".txt")

    import pt.inescn.params.ParamSearch.ParamSelect
    import java.io.PrintWriter
    import java.nio.channels.FileLock

    // Create logging file to hold results
    val logger: Either[ADWError, (FileLock, PrintWriter)] = ParamSelect.createResultsFile(resultFile)
    logger.isRight shouldBe true
    //val (logFileLoc, logFile) = logger.right.get

    /*
    println(s"pa1.length = ${pa1.length}")
    println(s"files.length = ${files.length}")
    println(s"total = ${pa1.length * files.length}") // 121 * 5 * 605
    */

    // Now launch cross-validation search using the specified algorithm and the parameter samples
    // Note that we do not specify the number of samples to use, so all of the grid will be explore
    // Careful, this may be a very large set
    val concurrentSearch = ParamSelect.searchParams(numThreads, timeOut, logger, a1) _
    val result = concurrentSearch(foldFiles, ps1, None)
    //println(result)
    result.isRight shouldBe true

    // Now lets calculate the descriptive statistics for each cross-validation experiment
    // and use that to score the grid search.
    // Here we simply convert the lines to columns and calculate the stats
    def statsMetric = ParamSelect.statsBinaryClassMetric _

    // Now lets select the best solution using some criterion
    // The return value must be an orderable so that we can order (rank)
    // or select the maximum or minimum score. Here we want the most
    // accurate models that is also the most stable (smallest standard
    // deviation). In this case we should use decreasing order or select
    // the maximum
    def scoreMetric(m: BinaryClassificationMetrics#AllColumnsStats): (Double, Double) = {
      val accuracy = m.accuracy.value
      (accuracy.mean.value, -accuracy.standard_deviation.value)
    }

    // Now lets set this up so that we calculate the stats and then use that
    // to generate the score.
    def scoreStat = ParamSelect.score(a1)(statsMetric) _ andThen scoreMetric

    // All has been set up. So now read the stored data, parse it and
    val results1 = ParamSelect.parseSearchResults(resultFile, a1)
    // score the algorithm's performance (based on accuracy)
    val mapResults1 = ParamSelect.groupResultsByParameters(a1)(results1)
    val score1 = ParamSelect.applyResultsByParameters(a1)(scoreStat)(mapResults1)
    //println(score1.mkString(";\n"))
    // rank those scores by maximum accuracy and minimum standard deviation
    val ranked1 = score1.toSeq.sortBy(p => (-p._2._1, -p._2._2))
    //println(ranked1.take(10).mkString(";\n"))
    // Good solutions have accuracy greater than 96.6% and standard deviation less than 0.01
    val good1 = ranked1.take(10).filter(p => p._2._1 >= 0.966 && Math.abs(p._2._2) <= 0.01)
    good1.size should be > 1
    // And select the best solution
    val maxRank1 = score1.toSeq.maxBy(_._2)
    //println(maxRank1)
    // will of course be good too
    maxRank1._2._1 should be >= 0.966
    maxRank1._2._2 should be <= 0.01

    // Now lets see what the performance is on the test set
    // First get the (best) model to use and the pre-processing that was applied
    val models1 = mapResults1(maxRank1._1)
    //println(models1)
    // Choose anyone of the models generated in the CV experiments using those parameters
    val (preProcess1, model1) = models1.filter(_.isRight).map { r =>
      val res = r.right.get
      val preProcess = res.preProcessing
      val model = res.result._2
      (preProcess, model)
    }.head
    //println(s"preProcess1 = $preProcess1")
    //println(s"model1 = $model1")
    val id = -1
    // Load the test data
    val testLoad = svmLoader.map(testData)
    val scaledTest = dirOut / (testData.path.getFileName + ".scale")
    val accuracy1 = for {
      testMat <- testLoad
      // scale according to training data
      _ = Transform.exec(preProcess1, testMat)
      // Saved scaled data for use with model
      _ = testMat.saveTo(scaledTest)
      // Evaluate model
      prediction <- a1.predict(id, scaledTest, model1)
      metric <- a1.evaluate(id, scaledTest, prediction)
      accuracy = metric.accuracy
    } yield accuracy
    //println(accuracy1)
    accuracy1.isRight shouldBe true
    accuracy1.right.get.value should be >= 0.96 // make it a bit more lenient, tests fail

  }


  it should "do random parameter search with the same results as the LightSVM guide (1)" in {

    // Load SVMLibSpec Astroparticle Physics data into sparse matrix
    // Create set of cross-validation set (examples use a CV-5 test)
    // Initialize SVM with defaults for multi-class classification
    // Use prepareCrossValidation and check that we the same mean
    // Repeat above with testParams

    // NOTE: Scalatest runs the test suites in parallel. This means that
    // the files here could be overridden by other tests and visa-versa.
    // Use unique file names to avoid problems.

    import better.files._
    import better.files.Dsl._

    //println(s"this.suiteId = ${this.suiteId}")
    //println(s"this.suiteName = ${this.suiteName}")

    // Data set
    val trainData = cwd / "data/libsvm/svmguide1"
    val testData = cwd / "data/libsvm/svmguide1.t"
    val tmp = File(JSystem.getProperty("java.io.tmpdir"))
    // create a temporary directory as a scratchpad
    val out = tmp / suiteName
    val dirOut = Utils.temporaryFilename(out)
    val d = File(dirOut)
    d.createDirectories()

    // We want to pre-process the data. More specifically we need to
    // scale the dada so that SVM can work correctly
    val scale = Scale(-1.0, 1.0)

    // Generate the 5 fold cross-validation training and test data sets
    // Place these in the temporary directory created above
    val nrFold = 5
    val experimentFolder = dirOut / ("an_experiment" + suiteName)
    val svmLoader: LightSVM.SVMSource[File, SparseMatrixData] = Source.libSVMSparse(ValidateDepVar.binaryClassification, SparseFold.binaryClassFail)
    val foldFiles = ParamSelect.stratifiedCrossValidationDataFiles(trainData, svmLoader, experimentFolder, scale, nrFold)
    //println(foldFiles)
    foldFiles.isRight shouldBe true
    val files = foldFiles.right.get
    files.length shouldBe 5
    files.forall(p => p._2.exists && p._3.exists) shouldBe true
    val train4 = dirOut / "an_experimentModelSpecLong" / "train4"
    val test4 = dirOut / "an_experimentModelSpecLong" / "test4"
    val train3 = dirOut / "an_experimentModelSpecLong" / "train3"
    val test3 = dirOut / "an_experimentModelSpecLong" / "test3"
    val train2 = dirOut / "an_experimentModelSpecLong" / "train2"
    val test2 = dirOut / "an_experimentModelSpecLong" / "test2"
    val train1 = dirOut / "an_experimentModelSpecLong" / "train1"
    val test1 = dirOut / "an_experimentModelSpecLong" / "test1"
    val train0 = dirOut / "an_experimentModelSpecLong" / "train0"
    val test0 = dirOut / "an_experimentModelSpecLong" / "test0"
    inside(files) { case List(
    (Scale(-1.0, 1.0, false, v4), `train4`, `test4`),
    (Scale(-1.0, 1.0, false, v3), `train3`, `test3`),
    (Scale(-1.0, 1.0, false, v2), `train2`, `test2`),
    (Scale(-1.0, 1.0, false, v1), `train1`, `test1`),
    (Scale(-1.0, 1.0, false, v0), `train0`, `test0`)) =>

      v4.length shouldBe 4
      v3.length shouldBe 4
      v2.length shouldBe 4
      v1.length shouldBe 4
      v0.length shouldBe 4
    }

    // Test SVM algorithm, instantiate iot first
    val a1 = Algorithms.MultiClassRadialBasisSVM(dirOut)

    // Generate the full set of all possible parameter combinations
    // Note: if request more samples than are possible, the error will be
    // detected and returned as an empty type. Make sure this does not
    // happen.
    val numSamples = 10
    val rnd = Distribution.getGenerator(1001)
    val ps1 = Strategy.random(a1.params, rnd)
    val pa1 = ps1.toStream.take(numSamples)
    //println(a1.params)
    //println(pa1.mkString(";\n"))
    pa1.length shouldBe 10 // infinite just take a few

    // Lets use the default scheduler (ForkJoin pool) to execute the parameter search
    import monix.execution.Scheduler.Implicits.global
    import scala.concurrent.duration._
    val numThreads = java.lang.Runtime.getRuntime.availableProcessors()
    //println(s"numThreads = $numThreads")
    // Wait a maximum of 5 minutes for the results
    val timeOut = 5.minutes
    // And place the results in this file
    val resultFile = dirOut / ("astro_paramsearch_test" + suiteName + ".txt")

    import pt.inescn.params.ParamSearch.ParamSelect
    import java.io.PrintWriter
    import java.nio.channels.FileLock

    // Create logging file to hold results
    val logger: Either[ADWError, (FileLock, PrintWriter)] = ParamSelect.createResultsFile(resultFile)
    logger.isRight shouldBe true
    //val (logFileLoc, logFile) = logger.right.get

    // Now launch cross-validation search using the specified algorithm and the parameter samples
    // Note that we MUST specify the number of samples to use because a random sampler
    // has an infinite amount of samples
    val concurrentSearch = ParamSelect.searchParams(numThreads, timeOut, logger, a1) _
    val result = concurrentSearch(foldFiles, ps1, Some(40))
    //println(result)
    result.isRight shouldBe true

    // Now lets calculate the descriptive statistics for each cross-validation experiment
    // and use that to score the grid search.
    // Here we simply convert the lines to columns and calculate the stats
    def statsMetric = ParamSelect.statsBinaryClassMetric _

    // Now lets select the best solution using some criterion
    // The return value must be an orderable so that we can order (rank)
    // or select the maximum or minimum score. Here we want the most
    // accurate models that is also the most stable (smallest standard
    // deviation). In this case we should use decreasing order or select
    // the maximum
    def scoreMetric(m: BinaryClassificationMetrics#AllColumnsStats): (Double, Double) = {
      val accuracy = m.accuracy.value
      (accuracy.mean.value, -accuracy.standard_deviation.value)
    }

    // Now lets set this up so that we calculate the stats and then use that
    // to generate the score.
    def scoreStat = ParamSelect.score(a1)(statsMetric) _ andThen scoreMetric

    // All has been set up. So now read the stored data, parse it and
    val results1 = ParamSelect.parseSearchResults(resultFile, a1)
    // score the algorithm's performance (based on accuracy)
    val mapResults1 = ParamSelect.groupResultsByParameters(a1)(results1)
    //println(mapResults1.mkString(";\n"))
    val score1 = ParamSelect.applyResultsByParameters(a1)(scoreStat)(mapResults1)
    //println(score1.mkString(";\n"))
    // rank those score by maximum accuracy and minimum standard deviation
    val ranked1 = score1.toSeq.sortBy(p => (-p._2._1, -p._2._2))
    //println(ranked1.mkString(";\n"))
    //println(ranked1.take(10).mkString(";\n"))
    // Good solutions have accuracy greater than 96.6% and standard deviation less than 0.01
    val good1 = ranked1.take(10).filter(p => p._2._1 >= 0.966 && Math.abs(p._2._2) <= 0.01)
    good1.size should be >= 1
    // And select the best solution
    val maxRank1 = score1.toSeq.maxBy(_._2)
    //println(maxRank1)
    // will of course be good too
    maxRank1._2._1 should be >= 0.966
    maxRank1._2._2 should be <= 0.01

    // Now lets see what the performance is on the test set
    // First get the (best) model to use and the pre-processing that was applied
    val models1 = mapResults1(maxRank1._1)
    //println(models1)
    // Choose anyone of the models generated in the CV experiments using those parameters
    val (preProcess1, model1) = models1.filter(_.isRight).map { r =>
      val res = r.right.get
      val preProcess = res.preProcessing
      val model = res.result._2
      (preProcess, model)
    }.head
    //println(s"preProcess1 = $preProcess1")
    //println(s"model1 = $model1")
    val id = -1
    // Load the test data
    val testLoad = svmLoader.map(testData)
    val scaledTest = dirOut / (testData.path.getFileName + ".scale")
    val accuracy1 = for {
      testMat <- testLoad
      // scale according to training data
      _ = Transform.exec(preProcess1, testMat)
      // Saved scaled data for use with model
      _ = testMat.saveTo(scaledTest)
      // Evaluate model
      prediction <- a1.predict(id, scaledTest, model1)
      metric <- a1.evaluate(id, scaledTest, prediction)
      accuracy = metric.accuracy
    } yield accuracy
    //println(accuracy1)
    accuracy1.isRight shouldBe true
    accuracy1.right.get.value should be >= 0.96 // make it a bit more lenient, tests fail

  }

  it should "do random parameter search a LightSVM regression example data set (abalone)" in {

    // Load SVMLibSpec Abalone data into sparse matrix
    // Create set of cross-validation set (examples use a CV-5 test)
    // Initialize SVM with defaults for multi-class classification
    // Use prepareCrossValidation and check that we the same mean
    // Repeat above with testParams

    // NOTE: Scalatest runs the test suites in parallel. This means that
    // the files here could be overridden by other tests and visa-versa.
    // Use unique file names to avoid problems.

    import better.files._
    import better.files.Dsl._

    //println(s"this.suiteId = ${this.suiteId}")
    //println(s"this.suiteName = ${this.suiteName}")

    // Data set
    // https://github.com/tensorflow/tensorflow/blob/master/tensorflow/docs_src/extend/estimators.md
    // https://github.com/MichaelSzczepaniak/AbaloneRegression
    // https://github.com/MichaelSzczepaniak/AbaloneRegression/blob/master/AbaloneRegression.pdf
    // Predicting Age of Abalone Using Linear Regression; February 18, 2008
    // http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.135.705&rep=rep1&type=pdf
    // "The root mean squared error for the training set was 2.179 and for the testing set 2.214.
    // Considering that the valid ring values range from 1 to 29, this is a decent error, although
    // not superb."
    val trainData = cwd / "data/libsvm/abalone"
    //val testData = cwd / "data/libsvm/abalone.t"
    val tmp = File(JSystem.getProperty("java.io.tmpdir"))
    // create a temporary directory as a scratchpad
    val out = tmp / suiteName
    val dirOut = Utils.temporaryFilename(out)
    val d = File(dirOut)
    d.createDirectories()

    // We want to pre-process the data. More specifically we need to
    // scale the dada so that SVM can work correctly
    val scale = Scale(-1.0, 1.0)

    // Generate the 5 fold cross-validation training and test data sets
    // Place these in the temporary directory created above
    val nrFold = 5
    val experimentFolder = dirOut / ("an_experiment" + suiteName)
    // This data set has only 28 unique values in the
    //val svmLoader: LightSVM.SVMSource[File, SparseMatrixData] = Source.libSVMSparse(ValidateDepVar.regression, SparseFold.regressionFail)
    val svmLoader: LightSVM.SVMSource[File, SparseMatrixData] = Source.libSVMSparse(ValidateDepVar.regression, SparseFold.neverFail)
    val foldFiles = ParamSelect.crossValidationDataFiles(trainData, svmLoader, experimentFolder, scale, nrFold)
    //println(foldFiles)
    foldFiles.isRight shouldBe true
    val files = foldFiles.right.get
    files.length shouldBe 5
    files.forall(p => p._2.exists && p._3.exists) shouldBe true
    val train4 = dirOut / "an_experimentModelSpecLong" / "train4"
    val test4 = dirOut / "an_experimentModelSpecLong" / "test4"
    val train3 = dirOut / "an_experimentModelSpecLong" / "train3"
    val test3 = dirOut / "an_experimentModelSpecLong" / "test3"
    val train2 = dirOut / "an_experimentModelSpecLong" / "train2"
    val test2 = dirOut / "an_experimentModelSpecLong" / "test2"
    val train1 = dirOut / "an_experimentModelSpecLong" / "train1"
    val test1 = dirOut / "an_experimentModelSpecLong" / "test1"
    val train0 = dirOut / "an_experimentModelSpecLong" / "train0"
    val test0 = dirOut / "an_experimentModelSpecLong" / "test0"
    inside(files) { case List(
    (Scale(-1.0, 1.0, false, v4), `train4`, `test4`),
    (Scale(-1.0, 1.0, false, v3), `train3`, `test3`),
    (Scale(-1.0, 1.0, false, v2), `train2`, `test2`),
    (Scale(-1.0, 1.0, false, v1), `train1`, `test1`),
    (Scale(-1.0, 1.0, false, v0), `train0`, `test0`)) =>

      v4.length shouldBe 8
      v3.length shouldBe 8
      v2.length shouldBe 8
      v1.length shouldBe 8
      v0.length shouldBe 8
    }

    // Test SVM algorithm, instantiate it first (regression)
    val a1 = Algorithms.RegressionRadialBasisSVM(dirOut)

    // Generate the full set of all possible parameter combinations
    // Note: if request more samples than are possible, the error will be
    // detected and returned as an empty type. Make sure this does not
    // happen.
    val numSamples = 10
    //val ps0 = Strategy.grid(numSamples)(a1.params)
    //val l0 = ps0.toList
    //println(l0.mkString(";\n"))
    //println(l0.length)
    val rnd = Distribution.getGenerator(1001)
    val ps1 = Strategy.random(a1.params, rnd)
    val pa1 = ps1.toStream.take(numSamples)
    //println(a1.params)
    //println(pa1.mkString(";\n"))
    pa1.length shouldBe 10 // infinite just take a few

    // Lets use the default scheduler (ForkJoin pool) to execute the parameter search
    import monix.execution.Scheduler.Implicits.global
    import scala.concurrent.duration._
    val numThreads = java.lang.Runtime.getRuntime.availableProcessors()
    //println(s"numThreads = $numThreads")
    // Wait a maximum of 5 minutes for the results
    val timeOut = 6.minutes
    // And place the results in this file
    val resultFile = dirOut / ("astro_paramsearch_test" + suiteName + ".txt")

    import pt.inescn.params.ParamSearch.ParamSelect
    import java.io.PrintWriter
    import java.nio.channels.FileLock

    // Create logging file to hold results
    val logger: Either[ADWError, (FileLock, PrintWriter)] = ParamSelect.createResultsFile(resultFile)
    logger.isRight shouldBe true
    //val (logFileLoc, logFile) = logger.right.get

    // Now launch cross-validation search using the specified algorithm and the parameter samples
    // Note that we MUST specify the number of samples to use because a random sampler
    // has an infinite amount of samples
    val concurrentSearch = ParamSelect.searchParams(numThreads, timeOut, logger, a1) _
    val result = concurrentSearch(foldFiles, ps1, Some(50))
    // println(result)
    result.isRight shouldBe true

    // Now lets calculate the descriptive statistics for each cross-validation experiment
    // and use that to score the grid search.
    // Here we simply convert the lines to columns and calculate the stats
    def statsMetric = ParamSelect.statsRegressionClassMetric _

    // Now lets select the best solution using some criterion
    // The return value must be an orderable so that we can order (rank)
    // or select the maximum or minimum score. Here we want the most
    // accurate models (least mean square error) that is also the most stable
    // (smallest standard deviation). In this case we should use decreasing
    // order or select the maximum
    def scoreMetric(m: RegressionMetrics#AllColumnsStats): (Double, Double) = {
      val error = m.rmse.value
      (error.mean.value, error.standard_deviation.value)
    }

    // Now lets set this up so that we calculate the stats and then use that
    // to generate the score.
    def scoreStat = ParamSelect.score(a1)(statsMetric) _ andThen scoreMetric

    // All has been set up. So now read the stored data, parse it and
    val results1 = ParamSelect.parseSearchResults(resultFile, a1)
    // score the algorithm's performance (based on RMSE)
    val mapResults1 = ParamSelect.groupResultsByParameters(a1)(results1)
    //println(mapResults1.mkString(";\n"))
    val score1 = ParamSelect.applyResultsByParameters(a1)(scoreStat)(mapResults1)
    //println(score1.mkString(";\n"))
    // rank those score by minimum error and minimum standard deviation
    val ranked1 = score1.toSeq.sortBy(p => (p._2._1, p._2._2))
    //println(ranked1.mkString(";\n"))
    // println(ranked1.take(20).mkString(";\n"))
    // Good solutions have accuracy 2.17 (train) and 2.214 for (test). So set it a little higher
    val good1 = ranked1.take(10).filter(p => p._2._1 <= 2.35 && p._2._2 <= 0.2)
    good1.size should be >= 1
    // And select the best solution
    val maxRank1 = score1.toSeq.minBy(_._2)
    //println(maxRank1)
    // will of course be good too
    maxRank1._2._1 should be <= 2.35
    maxRank1._2._2 should be <= 0.2

  }

  def hasClass(loader:LightSVM.SVMSource[File, SparseMatrixData], classes:Double*)(f: File): Boolean = {
    val train = loader.map(f)
    if (train.isLeft)
      false
    else {
      val trainData: SparseMatrixData = train.right.get
      val keys = trainData.classesKeys.toSet
      keys.equals(classes.toSet)
    }
  }


  /*
      Iris data set
      . Number of Instances: 150
      . Associated Tasks: Classification
      Attribute Information:
      1. sepal length in cm
      2. sepal width in cm
      3. petal length in cm
      4. petal width in cm
      5. class:
          -- Iris Setosa (50)       - 1
          -- Iris Versicolour (50)  - 2
          -- Iris Virginica (50)    - 3

     One-class SVM is designed to estimate the support of a distribution
     http://research.microsoft.com/pubs/69731/tr-99-87.pdf
     https://www.quora.com/Does-the-parameter-C-affect-one-class-SVM

     LibSVM example
     LIBSVM for SVDD and finding the smallest sphere containing all data
     https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/#libsvm_for_svdd_and_finding_the_smallest_sphere_containing_all_data

     One class results
     http://homepage.tudelft.nl/n9d04/occ/index.html
     503: Iris virginica
     We have a top score of 99.0 (0.2) for L_1-ball

     In our experiments we can get good results such as:
        ((1.0,(0.0078125,0.03125)),(0.96,-0.0328331263268846));
        ((1.0,(0.0078125,0.0078125)),(0.96,-0.032842833090238395));
        ((1.0,(0.015625,0.015625)),(0.96,-0.032865921200710434));
        ((1.0,(0.015625,0.0625)),(0.96,-0.03299144395369289));
        ((1.0,(0.125,0.25)),(0.96,-0.03926767262493008));

     Note that the parameters used must be less 1.0 otherwise exceptions
     wil occur. Unfortunately the data-set is small and the variabiliy
     significant

     NOTE: for large samples this creates many files. Youcan quickly get
     get out of disk space.

     dd_tools (Matlab)
     https://www.tudelft.nl/ewi/over-de-faculteit/afdelingen/intelligent-systems/pattern-recognition-bioinformatics/pattern-recognition-laboratory/
     . http://prtools.org/disdatasets/index.html
     . http://homepage.tudelft.nl/n9d04/occ/index.html
     . http://homepage.tudelft.nl/n9d04/milweb/index.html
     . http://homepage.tudelft.nl/n9d04/dd_manual.pdf
     . http://prtools.org/
     . https://www.tudelft.nl/ewi/over-de-faculteit/afdelingen/intelligent-systems/pattern-recognition-bioinformatics/pattern-recognition-laboratory/data-and-software/dd-tools/
   */
  it should "do random parameter search for once class (iris, 1 outlier class - Schölkopf)" in {

    // Load SVMLibSpec Abalone data into sparse matrix
    // Create set of cross-validation set (examples use a CV-5 test)
    // Initialize SVM with defaults for multi-class classification
    // Use prepareCrossValidation and check that we the same mean
    // Repeat above with testParams

    // NOTE: Scalatest runs the test suites in parallel. This means that
    // the files here could be overridden by other tests and visa-versa.
    // Use unique file names to avoid problems.

    import better.files.Dsl._
    import better.files._

    //println(s"this.suiteId = ${this.suiteId}")
    //println(s"this.suiteName = ${this.suiteName}")

    // Data set
    // https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/multiclass.html#iris
    // Data already scaled and in libSVM format
    val trainData = cwd / "data/libsvm/iris.scale"
    //val testData = cwd / "data/libsvm/bezdekiris.data"
    val tmp = File(JSystem.getProperty("java.io.tmpdir"))
    // create a temporary directory as a scratchpad
    val out = tmp / suiteName
    val dirOut = Utils.temporaryFilename(out)
    val d = File(dirOut)
    d.createDirectories()

    // we now create split the data into two sets: good and bad
    // The good are the: setosa and veriscolor
    // The bad are the: virginica
    val experimentFolder = dirOut / ("an_experiment" + suiteName)
    val svmLoader: LightSVM.SVMSource[File, SparseMatrixData] = Source.libSVMSparse(ValidateDepVar.multiClassification, SparseFold.multiClassFail)
    val iris = svmLoader.map(trainData)
    //println(iris)
    iris.isRight shouldBe true

    // Lets relabel the data to have only two classes - good and bad
    val irisData = iris.right.get
    // make sure you label to a new value not in the data set so that the next relabelling is not an issue
    irisData.replace(e => (e._1 == 1) || (e._1 == 2), e => (0.0, e._2)) // bad
    irisData.replace(e => e._1 == 3, e => (1.0, e._2)) // good

    // save as data set to be used
    val irisBinary1 = experimentFolder / "irisBinary1"
    //println(irisBinary1)
    irisData.saveTo(irisBinary1)

    // Generate the 5 fold cross-validation training and test data sets
    // Place these in the temporary directory created above
    // Note that because the data has been scaled, we don't do that here
    // What the effect the "bad" are by definition no within the "good" range
    val nrFold = 5
    val foldFiles = ParamSelect.oneClassCrossValidationDataFiles(irisBinary1, svmLoader, experimentFolder, NOP(), nrFold, delete = false)
    //println(foldFiles)
    foldFiles.isRight shouldBe true
    val files = foldFiles.right.get
    files.length shouldBe 5
    files.forall(p => p._2.exists && p._3.exists) shouldBe true
    files should contain theSameElementsInOrderAs List(
      (NOP(),
        dirOut / "an_experimentModelSpecLong" / "train4",
        dirOut / "an_experimentModelSpecLong" / "test4"),
      (NOP(),
        dirOut / "an_experimentModelSpecLong" / "train3",
        dirOut / "an_experimentModelSpecLong" / "test3"),
      (NOP(),
        dirOut / "an_experimentModelSpecLong" / "train2",
        dirOut / "an_experimentModelSpecLong" / "test2"),
      (NOP(),
        dirOut / "an_experimentModelSpecLong" / "train1",
        dirOut / "an_experimentModelSpecLong" / "test1"),
      (NOP(),
        dirOut / "an_experimentModelSpecLong" / "train0",
        dirOut / "an_experimentModelSpecLong" / "test0")
    )
    // make sure we only have good signals (1.0)
    def hasClassGood(f: File) = hasClass(svmLoader, 1.0)(f)
    val r_good = files.map( p => p._2).forall( hasClassGood )
    r_good shouldBe true
    // make sure we have both good (1.0)
    def hasClassBad(f: File) = hasClass(svmLoader, 1.0, 0.0)(f)
    val r_bad = files.map( p => p._3).forall( hasClassBad )
    r_bad shouldBe true

    // Test SVM algorithm, instantiate it first (regression)
    val a1 = Algorithms.OneClassRadialBasisSVM(dirOut)

    // Generate the full set of all possible parameter combinations
    // Note: if request more samples than are possible, the error will be
    // detected and returned as an empty type. Make sure this does not
    // happen.
    val numSamples = 10
    //val ps0 = Strategy.grid(numSamples)(a1.params)
    //val l0 = ps0.toList
    //println(l0.mkString(";\n"))
    //println(l0.length)
    val rnd = Distribution.getGenerator(1001)
    val ps1 = Strategy.random(a1.params, rnd)
    val pa1 = ps1.toStream.take(numSamples)
    //println(a1.params)
    //println(pa1.mkString(";\n"))
    pa1.length shouldBe 10 // infinite just take a few

    // Lets use the default scheduler (ForkJoin pool) to execute the parameter search
    import monix.execution.Scheduler.Implicits.global
    import scala.concurrent.duration._
    val numThreads = java.lang.Runtime.getRuntime.availableProcessors()
    //println(s"numThreads = $numThreads")
    // Wait a maximum of 5 minutes for the results
    val timeOut = 5.minutes
    // And place the results in this file
    val resultFile = dirOut / ("iris_paramsearch_test" + suiteName + ".txt")

    import pt.inescn.params.ParamSearch.ParamSelect
    import java.io.PrintWriter
    import java.nio.channels.FileLock

    // Create logging file to hold results
    val logger: Either[ADWError, (FileLock, PrintWriter)] = ParamSelect.createResultsFile(resultFile)
    logger.isRight shouldBe true
    //val (logFileLoc, logFile) = logger.right.get

    // Now launch cross-validation search using the specified algorithm and the parameter samples
    // Note that we MUST specify the number of samples to use because a random sampler
    // has an infinite amount of samples
    val concurrentSearch =  ParamSelect.searchParams( numThreads, timeOut, logger, a1) _
    val result = concurrentSearch(foldFiles, ps1, Some(5000)) // 50
    // println(result)
    result.isRight shouldBe true

    // Now lets calculate the descriptive statistics for each cross-validation experiment
    // and use that to score the grid search.
    // Here we simply convert the lines to columns and calculate the stats
    def statsMetric = ParamSelect.statsBinaryClassMetric _

    // Now lets select the best solution using some criterion
    // The return value must be an orderable so that we can order (rank)
    // or select the maximum or minimum score. Here we want the most
    // accurate models that is also the most stable (smallest standard
    // deviation). In this case we should use decreasing order or select
    // the maximum
    def scoreMetric(m: BinaryClassificationMetrics#AllColumnsStats): (Double, Double) = {
      val accuracy = m.accuracy.value
      (accuracy.mean.value, -accuracy.standard_deviation.value)
    }
    // Now lets set this up so that we calculate the stats and then use that
    // to generate the score.
    def scoreStat = ParamSelect.score(a1)(statsMetric) _ andThen scoreMetric

    val expected_score = 0.92
    val expected_score_sd = 0.05
    // All has been set up. So now read the stored data, parse it and
    val results1 = ParamSelect.parseSearchResults(resultFile, a1)
    // score the algorithm's performance (based on accuracy)
    val mapResults1 = ParamSelect.groupResultsByParameters(a1)(results1)
    val score1 = ParamSelect.applyResultsByParameters(a1)(scoreStat)(mapResults1)
    //println(score1.mkString(";\n"))
    // rank those scores by maximum accuracy and minimum standard deviation
    val ranked1 = score1.toSeq.sortBy( p => (-p._2._1, -p._2._2) )
    //println(ranked1.take(10).mkString(";\n"))
    // Good solutions have accuracy greater than 96.6% and standard deviation less than 0.01
    val good1 = ranked1.take(10).filter( p => p._2._1 >= expected_score && Math.abs(p._2._2) <= expected_score_sd)
    good1.size should be > 1
    // And select the best solution
    val maxRank1 = score1.toSeq.maxBy(_._2)
    //println(maxRank1)
    // will of course be good too
    maxRank1._2._1 should be >= expected_score
    Math.abs(maxRank1._2._2) should be <= expected_score_sd

    // Now lets see what the performance is on the test set
    // We don't have one, so use train (not realy worth the check)
    // First get the (best) model to use and the pre-processing that was applied
    val models1 = mapResults1(maxRank1._1)
    //println(models1)
    // Choose anyone of the models generated in the CV experiments using those parameters
    val (preProcess1, model1) = models1.filter(_.isRight).map{ r =>
      val res = r.right.get
      val preProcess = res.preProcessing
      val model = res.result._2
      (preProcess, model) }.head
    //println(s"preProcess1 = $preProcess1")
    //println(s"model1 = $model1")
    val id = -1
    // Load the test data
    val testLoad = svmLoader.map(irisBinary1)
    val scaledTest = dirOut /  (irisBinary1.path.getFileName + ".scale")
    val accuracy1 = for {
      testMat <- testLoad
      // scale according to training data
      _ = Transform.exec(preProcess1, testMat)
      // Saved scaled data for use with model
      _ = testMat.saveTo(scaledTest)
      // Evaluate model
      prediction <- a1.predict(id, scaledTest, model1)
      metric <- a1.evaluate(id, scaledTest, prediction)
      accuracy = metric.accuracy
    } yield accuracy
    //println(accuracy1)
    accuracy1.isRight shouldBe true
    accuracy1.right.get.value should be >= expected_score
  }

}
