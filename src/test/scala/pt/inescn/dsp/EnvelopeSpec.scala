/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.dsp

import better.files.File.root
import org.hipparchus.transform.DftNormalization
import org.scalatest._

/**
  * Contains tests for the DSP related functions. Also used to test some basic DSP library routines.
  *
  * Created by hmf on 12-07-2017.
  *
  * test:compile
  * test:console
  * test:consoleQuick
  * test:run
  * test:runMain
  *
  * sbt test
  * sbt "testOnly pt.inescn.dsp.EnvelopeSpec"
  *
  */
class EnvelopeSpec extends FlatSpec with Matchers {

  val eps = 0.3

  /**
    * Some comments on observed results:
    * 1. After filtering all signals will have their amplitudes changed. Not possible to retain amplitude
    * 2. Half wave envelopes are smoother in general
    * 3. The IIR filters (Butterworth) seems to be significantly beter than the FIR (average, max, RMS) filters in
    * the time and frequency domains. The frequency signature shows most visibly this difference.
    * Good result: Remove DC + full rectifier + Low pass MAX + low pass Butterworth filter
    * Best result: Remove DC + full rectifier + envelope follower + low pass Butterworth filter
    * Hilbert transform is disappointingly of bad quality. In addition top this it has no possible parameter tweaking.
    *
    * IMPORTANT: The Butterworth filter may present transient values n the start of thre signal. The last
    * filter test ( Remove DC + full rectifier + envelope follower + low pass Butterworth filter) shows this.
    * The result are negative values in the envelope that should be removed
    */
  "Envelope detectors" should "produce correct output" in {
    import better.files.Dsl._

    val cl = classOf[javax.sound.sampled.AudioSystem].getClassLoader
    val old = Thread.currentThread.getContextClassLoader

    try {
      Thread.currentThread.setContextClassLoader(cl)
      // codec: Uncompressed 16-bit PCM audio
      // channels: Stereo
      // sampling rate: 44100 Hz
      // Bitrate : N/A

      // /home/hmf/git/adw/data/audio
      val audio = cwd / "data/audio"
      val sample5 = audio / "canary" / "Canary_singing_master_selec_01_sub_sel_2.wav"

      val audioData = Audio.open(sample5)
      //Audio.printFileFormat(audioData)

      //audioData.isDefined shouldBe true
      val data = audioData.get._2
      val Some((channels, idx)) = Audio.load(sample5)
      channels.length shouldBe 2

      val leftChannel = channels(0)

      val time = Fourier.sampledTime(data.getFormat.getFrameRate, data.getFrameLength)
      time shouldBe 1.16575

      val s0 = 1.0 to 10.0 by 1.0
      val t0 = Filter.lowPassWindowSum(2, 1)(s0.toArray)
      t0 should contain theSameElementsInOrderAs List(3.0, 5.0, 7.0, 9.0, 11.0, 13.0, 15.0, 17.0, 19.0)

      val noDC = Filter.removeDC( s0.toArray )
      val noDC_mean = Filter.mean(noDC)
      noDC_mean shouldBe 0.0

      val sf1 = Filter.allPass _ andThen Filter.fullWaveRectify
      val s1 = sf1(leftChannel)
      s1.length shouldBe leftChannel.length
      s1.count( _ < 0.0) shouldBe 0

      val sf2 = Filter.allPass _ andThen Filter.halfWaveRectify
      val s2 = sf2(leftChannel)
      s2.length shouldBe leftChannel.length
      s2.count( _ < 0.0) shouldBe 0

      val winSz3 = 150
      val sf3 = Filter.removeDC _ andThen Filter.fullWaveRectify andThen Filter.lowPassWindowSum(winSz3, 1)
      val s3 = sf3(leftChannel)
      s3.length shouldBe leftChannel.length - (winSz3-1)
      s3.count( _ < 0.0) shouldBe 0

      val winSz4 = 150
      val sf4 = Filter.removeDC _ andThen Filter.fullWaveRectify andThen Filter.lowPassBlockFilterMax(winSz4)
      val s4 = sf4(leftChannel)
      s4.length shouldBe leftChannel.length
      s4.count( _ < 0.0) shouldBe 0

      import pt.inescn.eval.RegressionMetrics

      val err3 = RegressionMetrics(leftChannel, s3).mase
      val err4 = RegressionMetrics(leftChannel, s4).mase
      err4.value should be > err3.value // 1001.2752284751399 versus 220.0746320985805 (can confirm visually)

      val sf5 = Filter.removeDC _ andThen Filter.fullWaveRectify andThen Filter.lowPassBlockFilterMax(35) andThen Filter.lowPassWindowSum(100,50)
      val s5 = sf5(leftChannel)
      s5.length shouldBe 1119 // leftChannel.length // 55956 -> 1119 Due to last window filter
      s5.count( _ < 0.0) shouldBe 0

      // Low pass Butterworth
      import uk.me.berndporr.iirj.Butterworth
      val butterworth300 = new Butterworth
      butterworth300.lowPass(4, data.getFormat.getSampleRate, 300)
      def filterButter300(s: Array[Double]) = s.map( butterworth300.filter )

      // Example of an article's filter
      val sf6 = Filter.removeDC _ andThen Filter.fullWaveRectify andThen Filter.lowPassBlockFilterMax(35) andThen filterButter300
      val s6 = sf6(leftChannel)
      s6.length shouldBe leftChannel.length
      s6.count( _ < 0.0) shouldBe 0

      // Envelope follower
      val attackTime = 10.0 / 300.0 // same as Butterworth at 300 Hz
      val envelope = Envelope.Follower(data.getFormat.getSampleRate, attackTime)
      val sf7 = Filter.removeDC _ andThen Filter.fullWaveRectify andThen envelope.filter andThen filterButter300
      val s7 = sf7(leftChannel)
      s7.length shouldBe leftChannel.length
      //s7.count( _ < 0.0) shouldBe 0

      // Hilbert transform

      // Note: If thre signal does not have power of 2 it is padded with 0's for use  with the FFT,
      // but return length is then clipped to the correct time domain length
      val sf8 = Filter.removeDC _ andThen Filter.fullWaveRectify andThen Hilbert.envelope andThen filterButter300
      val s8 = sf8(leftChannel)
      s8.length shouldBe leftChannel.length
      s8.count( _ < 0.0) shouldBe 0

      if (!java.awt.GraphicsEnvironment.isHeadless) {

        def fft(fs: Double, signal_length: Int, sig: Array[Double], pad: Boolean,fftNormalization: DftNormalization) =
          Fourier.FFT(fs, signal_length, sig)

        val file = root/"tmp"/"nofilter.png"
        Fourier.plotFourierTransform(fft, data.getFormat.getFrameRate, 2, leftChannel, "Original Signal",
          fileName = file.toString, fftNormalization = DftNormalization.STANDARD)
        file.exists shouldBe true
        file.delete(true)

        val f1 = root/"tmp"/"filter1.png"
        Fourier.plotFourierTransform(fft, data.getFormat.getFrameRate, 2, s1, "Full wave rectify", fileName = f1.toString)
        f1.exists shouldBe true
        f1.delete(true)

        val f2 = root/"tmp"/"filter2.png"
        Fourier.plotFourierTransform(fft, data.getFormat.getFrameRate, 2, s2, "Half wave rectify", fileName = f2.toString)
        f2.exists shouldBe true
        f2.delete(true)

        val f3 = root/"tmp"/"filter3.png"
        Fourier.plotFourierTransform(fft, data.getFormat.getFrameRate, 2, s3, "Low pass SUM + full wave rectify", fileName = f3.toString)
        f3.exists shouldBe true
        f3.delete(true)

        val f4 = root/"tmp"/"filter4.png"
        Fourier.plotFourierTransform(fft, data.getFormat.getFrameRate, 2, s4, "Low pass MAX + full wave rectify", fileName = f4.toString)
        f4.exists shouldBe true
        f4.delete(true)

        // IMPORTANT: number of samples changes with the window filter
        val f5 = root/"tmp"/"filter5.png"
        Fourier.plotFourierTransform(fft, data.getFormat.getFrameRate, 2, s5, "Low pass MAX + LP Window + full wave rectify", fileName = f5.toString)
        f5.exists shouldBe true
        f5.delete(true)

        val f6 = root/"tmp"/"filter6.png"
        Fourier.plotFourierTransform(fft, data.getFormat.getFrameRate, 2, s6, "MAX Block + full wave rectify + Butterworth@300", fileName = f6.toString)
        f6.exists shouldBe true
        f6.delete(true)

        // IMPORTANT transients in Butterworth cause negative values
        val f7 = root/"tmp"/"filter7.png"
        Fourier.plotFourierTransform(fft, data.getFormat.getFrameRate, 2, s7, "full wave rectify + envelope follower + Butterworth@300", fileName = f7.toString)
        f7.exists shouldBe true
        f7.delete(true)

        val f8 = root/"tmp"/"filter8.png"
        Fourier.plotFourierTransform(fft, data.getFormat.getFrameRate, 2, s8, "full wave rectify + Hilbert envelope + Butterworth@300", fileName = f8.toString)
        f8.exists shouldBe true
        f8.delete(true)
      }

    } finally
      Thread.currentThread.setContextClassLoader(old)

  }


}
