/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.dsp

import better.files.Dsl._
import better.files.File.root
import org.scalatest._

/**
  * Contains tests for the DSP related functions. Also used to test some basic DSP library routines.
  *
  * Created by hmf on 12-07-2017.
  *
  * test:compile
  * test:console
  * test:consoleQuick
  * test:run
  * test:runMain
  *
  * sbt test
  * sbt "testOnly pt.inescn.dsp.HilbertSpec"
  *
  */
class HilbertSpec extends FlatSpec with Matchers {

  val eps = 0.3

  "Hilbert transform" should "produce correct analytical signal" in {

    val cl = classOf[javax.sound.sampled.AudioSystem].getClassLoader
    val old = Thread.currentThread.getContextClassLoader

    try {
      Thread.currentThread.setContextClassLoader(cl)
      // codec: Uncompressed 16-bit PCM audio
      // channels: Stereo
      // sampling rate: 44100 Hz
      // Bitrate : N/A

      // /home/hmf/git/adw/data/audio
      val audio = cwd / "data/audio"
      val sample5 = audio / "canary" / "Canary_singing_master_selec_01_sub_sel_2.wav"

      val audioData = Audio.open(sample5)
      //Audio.printFileFormat(audioData)

      //audioData.isDefined shouldBe true
      val data = audioData.get._2
      val Some((channels, idx)) = Audio.load(sample5)
      channels.length shouldBe 2

      val leftChannel = channels(0)

      val time = Fourier.sampledTime(data.getFormat.getFrameRate, data.getFrameLength)
      time shouldBe 1.16575
      // Hilbert transform

      // Partial Signal
      val len = 5000 // 4096
      // All samples used (careful, very inefficient)
      val ht0 = Hilbert.transformDFT(data.getFormat.getSampleRate, leftChannel.slice(0,len))
      ht0.length shouldBe len
      // No padding (so signal will be truncated to power of 2)
      val ht1 = Hilbert.transformFFT(data.getFormat.getSampleRate, leftChannel.slice(0,len), pad = false)
      ht1.length shouldBe 4096

      // Full signal
      val sampleTime = Fourier.sampledTime( data.getFormat.getFrameRate, leftChannel.length)
      sampleTime shouldBe 1.16575 // seconds
      // Use the default FFT version (with padding so the original signal length is obtained)
      val fht = Hilbert.transform(data.getFormat.getSampleRate, leftChannel )
      fht.length shouldBe leftChannel.length
      if (!java.awt.GraphicsEnvironment.isHeadless) {
        val f0 = root / "tmp" / "hilbert0.png"
        Fourier.plotMagnitudeInstantaneousPhase(data.getFormat.getSampleRate, fht, "Hilbert via FFT (with padding)", fileName = f0.toString)
        f0.exists shouldBe true
        f0.delete(true)
      }

    } finally
      Thread.currentThread.setContextClassLoader(old)
  }

}
