/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.dsp

import java.nio.{ByteBuffer, ByteOrder}
import javax.sound.sampled.AudioInputStream

import better.files.File
import better.files.File.root
import org.hipparchus.transform.DftNormalization
import org.scalatest._

/**
  * Contains tests for the DSP related functions. Also used to test some basic DSP library routines.
  *
  * Created by hmf on 12-07-2017.
  *
  * test:compile
  * test:console
  * test:consoleQuick
  * test:run
  * test:runMain
  *
  * sbt test
  * sbt "testOnly pt.inescn.dsp.AudioSpec"
  *
  */
class AudioSpec extends FlatSpec with Matchers {

  val eps = 0.3

  "Conversions from bytes to Integer" should "produce the correct value (Endian aware) " in  {

    val sysIsBigEndian = ByteOrder.nativeOrder().equals(ByteOrder.BIG_ENDIAN)
    sysIsBigEndian shouldBe false
    ByteOrder.nativeOrder() shouldBe ByteOrder.LITTLE_ENDIAN

    // The system's Endianess is LITTLE
    // The default of ByteBuffer is BIG_ENDIAN, opposite of the system
    // Big Endian - bytes at smallest address are most significant bytes
    // Little Endian - bytes at smallest address are least significant bytes
    val v1 = 16909060
    val it3 = Array[Byte](1, 2, 3, 4)
    val b4 = ByteBuffer.wrap(it3)
    b4.order() shouldBe ByteOrder.BIG_ENDIAN
    val bt6 = b4.getInt
    bt6 shouldBe v1
    b4.order(ByteOrder.LITTLE_ENDIAN)
    b4.rewind()
    val bt7 = b4.getInt
    bt7 should not be v1
    bt7 shouldBe 67305985
    b4.order(ByteOrder.BIG_ENDIAN)
    b4.rewind()
    val bt8 = b4.getInt
    bt8 shouldBe v1

    // Test a short value
    val t0 : Short = 1337

    //Integer.BYTES
    //java.lang.Short.BYTES

    // The default of ByteBuffer is BIG_ENDIAN, opposite of the system
    //val arra1 = ByteBuffer.allocate(4).order(ByteOrder.BIG_ENDIAN).putInt(t0).array()
    val arra1 = ByteBuffer.allocate(2).putShort(t0).array()
    arra1 should contain theSameElementsInOrderAs List(5,57)
    val bt2a = ByteBuffer.wrap(arra1).getShort
    bt2a shouldBe t0
    Audio.getIntBigEndian(arra1) shouldBe t0

    // The default of ByteBuffer is BIG_ENDIAN, opposite of the system
    //val arra1 = ByteBuffer.allocate(4).order(ByteOrder.BIG_ENDIAN).putInt(t0).array()
    val arra2 = ByteBuffer.allocate(4).putInt(t0).array()
    arra2 should contain theSameElementsInOrderAs List(0,0,5,57)
    val bt2b = ByteBuffer.wrap(arra2).getInt
    Audio.toInt(arra2, isBigEndian = true) shouldBe bt2b

    // Big endian format
    val bytearray0 = BigInt(t0).toByteArray
    bytearray0.length shouldBe 2
    bytearray0 should contain theSameElementsInOrderAs List(5,57)
    Audio.getIntBigEndian(bytearray0) shouldBe t0
    Audio.toInt(b0 = bytearray0(0), b1 = bytearray0(1), isBigEndian = true) shouldBe t0
    val bt0 = ByteBuffer.wrap(bytearray0).getShort
    Audio.getIntBigEndian(bytearray0) shouldBe bt0

    val it1 = Array[Byte](0, 0, 5, 57) // Little endian
    val bt1 = ByteBuffer.wrap(it1).getInt
    Audio.getIntBigEndian(bytearray0) shouldBe bt1

    // Test the maximum short value, Big endian format
    val t1 = 32767
    val bytearray1 = BigInt(t1).toByteArray
    bytearray1.length shouldBe 2
    bytearray1 should contain theSameElementsInOrderAs List(127,-1)
    Audio.getIntBigEndian(bytearray1) shouldBe t1
    Audio.toInt(bytearray1(0), bytearray1(1), isBigEndian = true) shouldBe t1
    val it2 = Array[Byte](127,-1)
    val bt3 = ByteBuffer.wrap(it2).getShort
    Audio.getIntBigEndian(bytearray1) shouldBe bt3

    // test little endian
    val vx1 = 1298.toShort
    val arrax1 = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN).putShort(vx1).array()
    arrax1 should contain theSameElementsInOrderAs List(18,5)
    Audio.getIntLittleEndian(arrax1) shouldBe vx1
    Audio.toShort(arrax1,isBigEndian = false) shouldBe vx1
    val vx2 = 4613
    val arrax2 = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(vx2).array()
    arrax2 should contain theSameElementsInOrderAs List(5,18,0,0)
    Audio.toInt(arrax2,isBigEndian = false) shouldBe vx2

    val vx3: Short = -11
    val arrax3 = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN).putShort(vx3).array()
    arrax3 should contain theSameElementsInOrderAs List(-11,-1)
    Audio.getIntLittleEndian(arrax3) shouldBe vx3
    Audio.toShort(arrax3,isBigEndian = false) shouldBe vx3
    val vx4 = -257
    val arrax4 = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(vx4).array()
    arrax4 should contain theSameElementsInOrderAs List(-1, -2, -1, -1)
    Audio.toInt(arrax4,isBigEndian = false) shouldBe vx4

    // Simulate reading PCM data
    val audioBytesX = Array[Byte](1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20)
    // Assume it is uncompressed 16-bit PCM audio
    val d1 = audioBytesX.grouped(2).toArray
    // If it is little endian
    d1.map( _.mkString(",")).mkString(";") shouldBe "1,2;3,4;5,6;7,8;9,10;11,12;13,14;15,16;17,18;19,20"
    val le =  List(513,1027,1541,2055,2569,3083,3597,4111,4625,5139)
    d1.map( p => Audio.toInt(p(0), p(1), isBigEndian = false) ) should contain theSameElementsInOrderAs le
    d1.map( p => Audio.toShort(p, isBigEndian = false) ) should contain theSameElementsInOrderAs le
    d1.map( p => Audio.getIntLittleEndian(p) ) should contain theSameElementsInOrderAs le
    d1.map( p => ByteBuffer.wrap(p).order(ByteOrder.LITTLE_ENDIAN).getShort ) should contain theSameElementsInOrderAs le
    // If it is big endian
    val be = List(258,772,1286,1800,2314,2828,3342,3856,4370,4884)
    d1.map( p => Audio.toInt(p(0), p(1), isBigEndian = true) ) should contain theSameElementsInOrderAs be
    d1.map( p => Audio.toShort(p, isBigEndian = true) ) should contain theSameElementsInOrderAs be
    d1.map( p => Audio.getIntBigEndian(p) ) should contain theSameElementsInOrderAs be
    d1.map( p => ByteBuffer.wrap(p).getShort ) should contain theSameElementsInOrderAs be

  }

  /**
    * NOTE: we have used a specific class loader to circumvent the problem of SBT
    * altering the class loader used. For a standard run we need only fork the process
    * (see the build.sbt file). However, when testing, either forking is not working or
    * the SBT class loader is still use. To solve tis we use the system's class loader.
    *
    * @see https://stackoverflow.com/questions/31727385/sbt-scala-audiosystem
    * @see http://www.scala-sbt.org/0.13/docs/Testing.html
    */
  "Loading audio data" should "produce the read all values correctly " in {
    import better.files._
    import better.files.Dsl._
    import javax.sound.sampled.AudioFileFormat

    val cl = classOf[javax.sound.sampled.AudioSystem].getClassLoader
    val old = Thread.currentThread.getContextClassLoader

    try {
      Thread.currentThread.setContextClassLoader(cl)
      // codec: Uncompressed 16-bit PCM audio
      // channels: Stereo
      // sampling rate: 44100 Hz
      // Bitrate : N/A

      // /home/hmf/git/adw/data/audio
      val audio = cwd / "data/audio"
      val sample1: File = audio / "auphonic-dehum-unprocessed.wav"
      sample1.exists shouldBe true

      val audioData: Option[(AudioFileFormat, AudioInputStream)] = Audio.open(sample1)
      //Audio.printFileFormat(audioData)
      audioData shouldBe 'defined

      val data = audioData.get._2
      data.getFrameLength shouldBe 1075289

      val Some((channels, idx)) = Audio.load(sample1)
      channels.length shouldBe 2
      idx shouldBe data.getFrameLength

      val leftChannel = channels(0)

      val time = Fourier.sampledTime(data.getFormat.getFrameRate, data.getFrameLength)
      time shouldBe 24.38297052154195

      leftChannel(0) shouldBe 0.0
      leftChannel(1) shouldBe 0.0
      leftChannel(10) shouldBe 0.0
      leftChannel(100) shouldBe 0.0
      leftChannel(1000) shouldBe -370.0
      leftChannel(2000) shouldBe 2024.0
      leftChannel(10000) shouldBe -559.0
      leftChannel(20000) shouldBe 854.0

      // Headless environment causes plotting routines to throw exception
      if (!java.awt.GraphicsEnvironment.isHeadless) {
        val file = root/"tmp"/"testaudio1.png"
        def fft(fs: Double, signal_length: Int, sig: Array[Double], pad: Boolean,fftNormalization: DftNormalization) =
          Fourier.FFT(fs, signal_length, sig, pad, fftNormalization)
        Fourier.plotFourierTransform(fft, data.getFormat.getFrameRate, time, leftChannel, "Audio",
          fileName = file.toString, fftNormalization = DftNormalization.STANDARD)
        file.exists shouldBe true
        file.delete(true)
      }

    } finally
      Thread.currentThread.setContextClassLoader(old)
  }


}
