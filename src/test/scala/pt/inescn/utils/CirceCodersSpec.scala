/*******************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  ******************************************************************************/
package pt.inescn.utils

import better.files.File
import io.circe
import io.circe.Decoder.Result
import io.circe.{Decoder, Encoder, Json}
import io.circe.parser.decode
import org.scalatest._


// import scala.collection.JavaConverters._

/**
  *
  *
  * test:compile
  * test:console
  * test:consoleQuick
  * test:run
  * test:runMain
  *
  * sbt test
  * sbt "testOnly pt.inescn.utils.CirceCodersSpec"
  *
  *
  */
class CirceCodersSpec extends FlatSpec with Matchers {

  "Circe example" should "handle Java Instant" in {
    import cats.syntax.either._

    import java.time.Instant

    implicit val encodeInstant: Encoder[Instant] = Encoder.encodeString.contramap[Instant](_.toString)

    implicit val decodeInstant: Decoder[Instant] = Decoder.decodeString.emap { str =>
      Either.catchNonFatal(Instant.parse(str)).leftMap(t => "Instant")
    }

    val i1 = Instant.parse("2007-12-03T10:15:30.00Z")
    val jsoni1: Json = encodeInstant(i1)
    /*println(jsoni1.noSpaces)
    println(jsoni1.name)
    println(jsoni1.toString)*/
    jsoni1.noSpaces shouldBe """"2007-12-03T10:15:30Z""""
    val i2: Either[circe.Error, Instant] = decode(jsoni1.noSpaces)
    i2.isRight shouldBe true
    //println(i2)
    i2.right.get shouldBe i1
  }

  it should "automatically deal with case classes" in {
    import io.circe.generic.auto._
    import io.circe.syntax._

    case class Person(name: String)
    case class Greeting(salutation: String, person: Person, exclamationMarks: Int)

    val case1 = Greeting("Hey", Person("Chris"), 3)
    val jasonCase1 = case1.asJson
    //println(jasonCase1.noSpaces)
    val case2 = jasonCase1.as[Greeting]
    //println(case2)
    case2.isRight shouldBe true
    case2.right.get shouldBe case1
  }

  "File codec" should "encode and decode to/from JSON" in {

    import CirceCoders._

    val f1 = File("/tmp/circe.test")
    val jsonf1: Json = encodeFile(f1)
    val f2: Result[File] = jsonf1.as
    f2.isRight shouldBe true
    f2.right.get shouldBe f1
    //println(jsonf1.noSpaces)
    val f3 = decode[File](jsonf1.noSpaces)
    f3.isRight shouldBe true
    f3.right.get shouldBe f1
  }

  it should "encode and decode via auto-syntax" in {

    import CirceCoders._
    import io.circe.syntax._

    val f1 = File("/tmp/circe.test")
    val jsonf1: Json = f1.asJson
    val f2: Result[File] = jsonf1.as
    f2.isRight shouldBe true
    f2.right.get shouldBe f1
    //println(jsonf1.noSpaces)
    val f3 = decode[File](jsonf1.noSpaces)
    f3.isRight shouldBe true
    f3.right.get shouldBe f1
  }

  "Metric codec" should "encode and decode BinaryClassificationMetrics#All to/from JSON" in {
    import pt.inescn.eval._
    import io.circe.generic.auto._
    import io.circe.syntax._


    val r1 = Recall(1.0)
    val jsonr1 = r1.asJson
    //println(jsonr1.noSpaces)
    jsonr1.noSpaces shouldBe """{"value":1.0}"""
    val r2 = jsonr1.as[Recall[Double]]
    r2.isRight shouldBe true
    r2.right.get shouldBe r1
    val r3 = decode[Recall[Double]](jsonr1.noSpaces)
    r3.isRight shouldBe true
    r3.right.get shouldBe r1

    val f1 = FScore(FBeta(2.0),6.0)
    val jsonf1 = f1.asJson
    //println(jsonf1.noSpaces)
    jsonf1.noSpaces shouldBe """{"beta":{"value":2.0},"value":6.0}"""
    val f2 = jsonf1.as[FScore[Double]]
    f2.isRight shouldBe true
    f2.right.get shouldBe f1
    val f3 = decode[FScore[Double]](jsonf1.noSpaces)
    f3.isRight shouldBe true
    f3.right.get shouldBe f1

    val m1 = BinaryClassifications(
               Recall(1.0), Precision(2.0), Accuracy(3.0), MCC(4.0),
               FScore(FBeta(1.0),5.0), FScore(FBeta(2.0),6.0), FScore(FBeta(0.5),7.0),
               GMeasure(8.0))

    val jsonm1 = m1.asJson
    //println(jsonm1.noSpaces)
    // BinaryClassificationTypes#All
    jsonm1.noSpaces shouldBe """{"recall":{"value":1.0},"precision":{"value":2.0},"accuracy":{"value":3.0},"mcc":{"value":4.0},"fscore_1":{"beta":{"value":1.0},"value":5.0},"fscore_2":{"beta":{"value":2.0},"value":6.0},"fscore_05":{"beta":{"value":0.5},"value":7.0},"gmeasure":{"value":8.0}}"""
    val m2 = decode[BinaryClassificationTypes#All](jsonm1.noSpaces)
    m2.isRight shouldBe true
    m2.right.get shouldBe m1
  }

}
